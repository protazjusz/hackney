package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Button;

/**
 * Fragment responsible for interacting with user in application touchpad module
 * 
 * @author HackneyTeam
 *
 */
public class TouchpadFragment extends Fragment implements OnTouchListener, OnGestureListener{
	
	private Connection connection = RemoteControlActivity.connection;
	private Button left;
	private Button right;
	private ImageButton lock;
	private View tap,scrollUp,scrollDown;
	
	private GestureDetector gestureDetector;
	private boolean locked;
	private boolean touchpadReleased;
	
	SharedPreferences settings;
	public static float sensitivity;
	public double czulosc;

	
	public TouchpadFragment(){
		locked = false;
		gestureDetector = new GestureDetector(this);
		touchpadReleased = true;
		
		czulosc = 0.3;
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		sensitivity= (float)((((settings.getFloat("sensitivity", Options.DEFAULT_SENSITIVITY)/(float)100)-(float)0.5)*(float)2)+(float)1) ;
		if(sensitivity==0)sensitivity=(float) 0.1;
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		return inflater.inflate(R.layout.touchpad, container, false);
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		
		ActivityProperties.get().findViewById(R.id.touchpad_button).setPressed(true);
		tap = ActivityProperties.get().findViewById(R.id.touchpad_tap);
		tap.setOnTouchListener(this);
		tap.setPressed(false);
		
		scrollUp = ActivityProperties.get().findViewById(R.id.touchpadScrollUp);
		scrollUp.setOnTouchListener(this);
		scrollUp.setPressed(false);
		
		scrollDown = ActivityProperties.get().findViewById(R.id.touchpadScrollDown);
		scrollDown.setOnTouchListener(this);
		scrollDown.setPressed(false);
		
		left = (Button) ActivityProperties.get().findViewById(R.id.left_mouse_button);
		right = (Button) ActivityProperties.get().findViewById(R.id.right_mouse_button);
		lock = (ImageButton) ActivityProperties.get().findViewById(R.id.lock_button);
		
		left.setOnTouchListener(this);
		right.setOnTouchListener(this);
		lock.setOnTouchListener(this);
		
	}
	
	/**
	 * Updates layout and makes sure that nothing is pressed
	 */
	@Override
	public void onPause(){
		ActivityProperties.get().findViewById(R.id.touchpad_button).setPressed(false);
		MessageData data;	
		if(left.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
			connection.outStream.write(data);
		}
		if(right.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
			connection.outStream.write(data);
		}
		if(lock.isPressed()){
			lock.setPressed(false);
			locked = false;
		}
		if(scrollUp.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)-36});
			connection.outStream.write(data);
		}
		if(scrollDown.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)-37});
			connection.outStream.write(data);
		}
			
		super.onPause();
	}

	/**
	 * Defines reactions for interacting with interface
	 */
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		
	
		int action= arg1.getAction();	
		

		MessageData data;

		if(action == MotionEvent.ACTION_DOWN || action ==MotionEvent.ACTION_POINTER_DOWN){
			switch(arg0.getId()){
				case R.id.left_mouse_button:
					if(!lock.isPressed()){
					
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)49});
						connection.outStream.write(data);
						
						left.setPressed(true);	
						
					}
					else {
						if(left.isPressed()){
							left.setPressed(false);
						
							lock.setPressed(false);
						
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
							connection.outStream.write(data);
							locked = false;
						}
						else 
							if(right.isPressed()){
							right.setPressed(false);
						
							lock.setPressed(false);
						
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
							connection.outStream.write(data);
							locked = false;
						}
						else{
							left.setPressed(true);
						
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)49});
							connection.outStream.write(data);
						}
					}
					
				return true;
				
				case R.id.right_mouse_button:
					if(!lock.isPressed()){
					
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)51});
						connection.outStream.write(data);
						right.setPressed(true);
					
					}
					else {
						if(left.isPressed()){
							left.setPressed(false);
						
							lock.setPressed(false);
							
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
							connection.outStream.write(data);
							locked = false;
						}
						else 
							if(right.isPressed()){
							right.setPressed(false);
					
							lock.setPressed(false);
					
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
							connection.outStream.write(data);
							locked = false;
						}
						else{
							right.setPressed(true);
						
							data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)51});
							connection.outStream.write(data);
						}
					}
					
					
				return true;
				
				case R.id.lock_button:
					lock.setPressed(true);
				
					if(!locked) locked = true;
					else locked = false;
					
					
				return true;
				
				case R.id.touchpad_tap:
					touchpadReleased = true;
					return gestureDetector.onTouchEvent(arg1);
					
				case R.id.touchpadScrollUp:
					scrollUp.setPressed(true);
					data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)-36});
					connection.outStream.write(data);
				return true;
				case R.id.touchpadScrollDown:
					scrollDown.setPressed(true);
					data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)-37});
					connection.outStream.write(data);

				return true; 
			
			}
			
		}

		else if(action == MotionEvent.ACTION_UP){
			switch(arg0.getId()){
				case R.id.left_mouse_button:

					if(!locked){
						left.setPressed(false);

						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
						connection.outStream.write(data);
					}
					
				return true;
				
				case R.id.right_mouse_button:

					if(!locked){
						right.setPressed(false);
					
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
						connection.outStream.write(data);
					}
					
					
				return true;
				
				case R.id.lock_button:
					if(!locked){
						lock.setPressed(false);
				
					}
					if(left.isPressed()){
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
						connection.outStream.write(data);
						
						locked = false;
						left.setPressed(false);
				
					}
					else if(right.isPressed()){
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
						connection.outStream.write(data);
						
						locked = false;
						right.setPressed(false);
				
					}
					
				return true;
				case R.id.touchpad_tap:
					touchpadReleased = true;
					tap.setPressed(false);
					return gestureDetector.onTouchEvent(arg1);
					
				case R.id.touchpadScrollUp:
						scrollUp.setPressed(false);	
						data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)-36});
						connection.outStream.write(data);
					return true; 
				case R.id.touchpadScrollDown:
					scrollDown.setPressed(false);
					data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)-37});
					connection.outStream.write(data);
				return true;
			}
			return true;
		}
		else if(action == MotionEvent.ACTION_MOVE ){
			switch(arg0.getId()){
			case R.id.touchpad_tap:

				return gestureDetector.onTouchEvent(arg1);
			}

			return false;
		}
		return true;
	}

	/**
	 * Updates layout
	 */
	@Override
	public boolean onDown(MotionEvent e) {
		tap.setPressed(true);
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	/**
	 * Defines reaction for scrolling on touchpad view
	 */
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		
		if(!touchpadReleased){
			tap.setPressed(true);
			MessageData data;
			data= new MessageData(new byte[]{ MessageData.MOUSE_SCREEN_MOVE , 0 , (byte)(sensitivity*(-distanceX)), (byte)(sensitivity*(-distanceY))});
			connection.outStream.write(data);
		}

		touchpadReleased=false;
		return true;
	}

	/**
	 * Updates layout
	 */
	@Override
	public void onShowPress(MotionEvent e) {
		tap.setPressed(true);
	}

	/**
	 * Defines reaction on single tap up
	 */
	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		MessageData data;
		tap.setPressed(true);
		data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)49});
		connection.outStream.write(data);
		data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
		connection.outStream.write(data);
		
		if(locked){
		if(left.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
			connection.outStream.write(data);
			
			locked = false;
			left.setPressed(false);
		}
		else if(right.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
			connection.outStream.write(data);
			
			locked = false;
			right.setPressed(false);
		}
		lock.setPressed(false);
		}
		tap.setPressed(false);
		
		return true;
	}
	
}
