package pl.umk.mat.hackney.kopyto.ui;

import java.util.ArrayList;
import java.util.List;

import pl.umk.mat.hackney.kopyto.R;
import pl.umk.mat.hackney.networking.BluetoothHandler;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.bluetooth.BluetoothDevice;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;


/**
 * Class representing list of application shortcuts
 * @author HackneyTeam
 *
 */
public class DeviceList {
	
	private ListView deviceListView;
	
	// interfejs do implementacji reakcji na zdarzenie wybrania el z listy
	public interface DeviceListListener {
		public void onDeviceChoosen(BluetoothDevice device);
	}

	
	private DeviceListListener deviceListListener;
	private List<BluetoothDevice> deviceList;
	private ArrayAdapter<String> adapter;
	
	/**
	 * creates new list
	 * @param listListener
	 */
	public DeviceList(DeviceListListener listListener){
		this.deviceListListener = listListener;
		deviceList = new ArrayList<BluetoothDevice>();
		deviceListView= (ListView)ActivityProperties.get().findViewById(R.id.deviceList);
				
		adapter = new ArrayAdapter<String>(ActivityProperties.get(),R.layout.list_item, R.id.textItem);
		deviceListView.setAdapter(adapter);
		
		deviceListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				deviceListListener.onDeviceChoosen(deviceList.get(position));
			}
		});
		
		
	}
	
	/**
	 * Adds new device to list
	 * @param device
	 */
	public void add(BluetoothDevice device){
			deviceList.add(device);
			adapter.add(device.getName());
	}
	
	/**
	 * Check if list include given device
	 * @param device
	 * @return
	 */
	public boolean include(BluetoothDevice device){	
			if (deviceList.indexOf(device)<0)return false;
			return true;
	}

	
	//wyczyszczenie listy i wyszukiwanie na nowo
	/**
	 * Clears and refreshes the list
	 * @param btHandler
	 */
	public void refresh(BluetoothHandler btHandler){
		deviceList.clear();
		adapter.clear();
		btHandler.cancelDiscovery();
		btHandler.discoverDevices();
	}
}
