package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.kopyto.RemoteControlActivity.MessagesForShortcutsListener;
import pl.umk.mat.hackney.kopyto.ui.ApplicationShortcutsList;
import pl.umk.mat.hackney.kopyto.ui.ApplicationShortcutsList.ApplicationShortcutsListListener;
import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import pl.umk.mat.hackney.utility.StringRebuilder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Fragment responsible for displaying choosen application shortcuts and reacting on choosing it
 * @author HackneyTeam
 *
 */
public class ShortcutsApplicationFragment extends Fragment implements OnClickListener, ApplicationShortcutsListListener, MessagesForShortcutsListener {

	private Connection connection = RemoteControlActivity.connection;
	private ApplicationShortcutsList applicationShortcutsList;
	private TextView currentApp;
	private StringRebuilder stringRebuilder;
	private String application;
	
	
	/**
	 * 
	 * @param app Application which shortcuts will be displayed
	 */
	public ShortcutsApplicationFragment(String app){
		this.application = app;	
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		stringRebuilder = new StringRebuilder();
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		RemoteControlActivity.messagesForShortcutsListener = this;
		return inflater.inflate(R.layout.app_shortcuts, container, false);
  }
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
	
		ActivityProperties.get().findViewById(R.id.shortcuts_button).setPressed(true);
		currentApp = (TextView) ActivityProperties.get().findViewById(R.id.currentApp);
		currentApp.setText(application);
		
		Button backButton =(Button) ActivityProperties.get().findViewById(R.id.backToApplicationListButton);
        backButton.setOnClickListener(this);
		
		applicationShortcutsList = new ApplicationShortcutsList(this);
		applicationShortcutsList.add(this.getString(R.string.launchApplication));
		
		MessageData data = new MessageData(new byte[]{ MessageData.HOTKEY_APPHOTKEY_REQUEST , 0 , (byte)1});
		connection.outStream.write(data);
		
	}
	
	/**
	 * Updates activity layout
	 */
	@Override
	public void onPause(){
		super.onPause();
		ActivityProperties.get().findViewById(R.id.shortcuts_button).setPressed(false);
	}

	/**
	 * Defines reaction for shortcut choosen
	 */
	@Override
	public void onShortcutChoosen(String application) {
		MessageData data = new MessageData(new byte[]{ MessageData.HOTKEY_USED , 0 , (byte)applicationShortcutsList.indexOf(application)});
		connection.outStream.write(data);
		
	}

	/**
	 * Defines on button click reaction
	 */
	@Override
	public void onClick(View arg0) {
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();		
		ShortcutsFragment sf = new ShortcutsFragment();
		fragmentTransaction.replace(R.id.content, sf);
		fragmentTransaction.commit();
		
		
	}

	
	@Override
	public void onApplicationReceived(MessageData application) {
		// never happens	
	}

	/**
	 * Defines reaction for incoming message from server
	 */
	@Override
	public void onShortcutReceived(MessageData shortcut) {
		applicationShortcutsList.add(stringRebuilder.fromBytes(shortcut.getPayload()).trim());
		
	}
}
