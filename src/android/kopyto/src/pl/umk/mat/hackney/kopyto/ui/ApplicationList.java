package pl.umk.mat.hackney.kopyto.ui;

import java.util.ArrayList;
import java.util.List;


import pl.umk.mat.hackney.kopyto.R;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.View;


/**
 * Class representing list of applications
 * @author HackneyTeam
 *
 */
public class ApplicationList {
	
	private ListView applicationListView;

	private List<String> applicationList;
	private ArrayAdapter<String> applicationAdapter;
	
	public interface ApplicationListListener {
		public void onApplicationChoosen(String application);
		
	}
	private ApplicationListListener applicationListListener;
	
	/**
	 * creates new list
	 * @param listListener
	 */
	public ApplicationList(ApplicationList.ApplicationListListener listListener){
		this.applicationListListener = listListener;
		applicationList = new ArrayList<String>();
		applicationListView= (ListView)ActivityProperties.get().findViewById(R.id.applicationListView);

		applicationAdapter = new ArrayAdapter<String>(ActivityProperties.get(),R.layout.list_item, R.id.textItem);
		
		applicationListView.setAdapter(applicationAdapter);
		
		applicationListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				applicationListListener.onApplicationChoosen(applicationList.get(position));
			}
		});
		
		
	}
	
	//dodanie aplikacji
	/**
	 * Adds new application to list
	 * @param application
	 */
	public void add(String application){
			applicationList.add(application);
			applicationAdapter.add(application);
	}
	
	/**
	 * Check if list include given application 
	 * @param application 
	 * @return 
	 */
	public boolean include(String application){	
			if (applicationList.indexOf(application)<0)return false;
			return true;
	}
	
	/** 
	 *  Returns index in the list of given application
	 * @param application
	 * @return
	 */
	public int indexOf(String application){
		
		if(applicationList.indexOf(application)<0) return -1;
		else return applicationList.indexOf(application);	
	}
	
	/**
	 * clears the list
	 */
	public void clear(){
		applicationList.clear();
		applicationAdapter.clear();
	}
}
