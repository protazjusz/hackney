package pl.umk.mat.hackney.kopyto;

import java.util.Locale;

import pl.umk.mat.hackney.networking.BluetoothHandler;
import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Activity responsible for enabling bluetooth on user agreement
 * 
 * @author HackneyTeam
 *
 */
public class EnableBluetoothActivity extends FragmentActivity implements OnClickListener {
	
	public static BluetoothHandler btHandler; 
	private Button yesButton;
	private Button noButton;
	private String next;
	private ProgressDialog dialog;
	private AlertDialog alertDialog;
	
	
	/**
	 * Initialize required objects
	 */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		
        setContentView(R.layout.enable_bluetooth);

        btHandler = new BluetoothHandler();
        yesButton = (Button) findViewById(R.id.yesEnableBluetoothButton);
        noButton = (Button) findViewById(R.id.noEnableBluetoothButton);
        
        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);
        dialog = new ProgressDialog(this);
        Bundle bundle = getIntent().getExtras();

		next = bundle.getString("nextActivity");          
    }
    
    /**
     *  Hide dialog on activity end
     */
    @Override
    public void onDestroy(){
    	super.onDestroy(); 
		if(dialog.isShowing()) dialog.dismiss();
    }

    /**
	 * Define on buttons click reaction
	 */
	@Override
	public void onClick(View arg0) {
		class EnableBluetoothTask extends AsyncTask<Void, Void, Boolean> {

			@Override
			protected Boolean doInBackground(Void... arg0) {
				return btHandler.enableBluetooth();
			}
			@Override
			protected void onPostExecute(Boolean result){
				if((boolean)result) launchDevicesListActivity();
				else showError();
			}
			
		}	
		
		switch(arg0.getId()){
			
			case R.id.noEnableBluetoothButton:
				this.onBackPressed();
			break;
			
			case R.id.yesEnableBluetoothButton:

				dialog = ProgressDialog.show(this, "", this.getString(R.string.enablingBluetoothDialog), true);

				EnableBluetoothTask ebt= new EnableBluetoothTask();
				ebt.execute();
			break;
		}

	}

	/**
	 * Show bluetooth adapter error
	 */
	public void showError() {
		if(dialog.isShowing())dialog.dismiss();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(this.getString(R.string.bluetoothInitializationError))
    	       .setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					ActivityProperties.get().finish();
				}
			});
   
    	alertDialog = builder.create();
    	alertDialog.show(); 
		
	}

	/**
	 * Launch proper activity
	 */
	public void launchDevicesListActivity() {
		ActivityManager activityManager = new ActivityManager(this);
		
		if (next.compareTo("search")==0){
			activityManager.openActivityNoHistory(SearchActivity.class);
		
			finish();
		}
		else if(next.compareTo("bonded")==0){
			activityManager.openActivityNoHistory(BondedDevicesActivity.class);
		}
		finish();
	}

}
