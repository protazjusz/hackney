package pl.umk.mat.hackney.kopyto;

import java.io.IOException;
import java.util.Locale;

import pl.umk.mat.hackney.networking.BluetoothHandler;
import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import pl.umk.mat.hackney.kopyto.ui.DeviceList;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


/**
 * Class responsible for discovering nearby devices and connecting to it on user choice
 * @author HackneyTeam
 *
 */
public class SearchActivity extends Activity implements DeviceList.DeviceListListener, OnClickListener {

	public static BluetoothDevice deviceToPair;	
	private BluetoothHandler btHandler = EnableBluetoothActivity.btHandler;
	private DeviceList deviceList;
	private BroadcastReceiver receiver;
	private BluetoothDevice choosen;	
	private ProgressDialog progressDialog;
	private AlertDialog alertDialog;
	
	
	/**
	 * Initializes required objects
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());      

        setContentView(R.layout.search);
        
        deviceList = new DeviceList(this);
  
        progressDialog = new ProgressDialog(this);
              
    
        receiver = new BroadcastReceiver() {
        	@Override
			public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
            
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
        
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if(device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.COMPUTER && !deviceList.include(device))deviceList.add(device);
                }
        	}
        };
        
        
        if(btHandler==null){
        	btHandler = new BluetoothHandler();
        }
        
        Button refreshButton =(Button) findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(this);
        

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);    
        
        btHandler.discoverDevices();     
    }
    
    /**
     * Cancels discovering devices
     */
    @Override
    public void onDestroy(){
    	super.onDestroy();
	
    	unregisterReceiver(receiver);
    	if (btHandler != null)btHandler.cancelDiscovery();
    }
    
    /**
     * Updates progress dialog
     * @param integer
     */
    private void updateProgress(Integer integer) {
		if(progressDialog.isShowing())progressDialog.setProgress(integer);
	}
    
    /**
     * Shows connecting dialog
     */
    void dialogShow() {
		progressDialog.setMessage(ActivityProperties.get().getString(R.string.connectingDialog));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();
    }

    /**
     * Hides connecting dialog
     */
    void dialogHide() {
    	if(progressDialog.isShowing())progressDialog.dismiss();
    }
    
    /**
     * Shows connection error
     */
    void showConnectionError(){
    
    	if(progressDialog.isShowing())progressDialog.dismiss();
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(ActivityProperties.get().getString(R.string.connectionErrorDialog))
    	       .setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
   
    	alertDialog = builder.create();
    	alertDialog.show(); 	
    }
    
    /**
     * Connects to choosen device
     */
	@Override
	public void onDeviceChoosen(BluetoothDevice device) {

		choosen = device;

		class ConnectionTask extends AsyncTask<Void, Integer, Boolean>{
			public ConnectionTask(){


			}

			@Override
			protected void onPreExecute(){
				dialogShow();

			}

			@Override
			protected Boolean doInBackground(Void... a) {

				btHandler.cancelDiscovery();
				
				try {
					btHandler.initializeConnection(choosen);
				} catch (IOException e1) {
					return false;
				}
				
				this.publishProgress(10);
				try {
					btHandler.connect(choosen);
				} catch (IOException e) {
					return false;
				}

				this.publishProgress(40);
				
				RemoteControlActivity.connection = new Connection(btHandler.getSocket());
				
				this.publishProgress(50);
				
				try {
					RemoteControlActivity.connection.createOutStream();
					this.publishProgress(70);
					RemoteControlActivity.connection.createInStream();
				} catch (IOException e) {
					return false;
				}
				
				this.publishProgress(80);
				if(!RemoteControlActivity.connection.tryHandShake()) return false;
			

				this.publishProgress(90);
				if(RemoteControlActivity.connection.isConnected()){
					Intent intent = new Intent(ActivityProperties.get(),RemoteControlActivity.class);
					startActivity(intent);
				}
				
				else{
					return false;
				}
				return true;
			}
			@Override
			protected void onPostExecute(Boolean result){
				if(result){
					dialogHide();
				}
				else{
					dialogHide();
					showConnectionError();
				}
			}

			@Override
			protected void onProgressUpdate(Integer... val){
				updateProgress(val[0]);
			}
		}


		if(choosen.getBondState()==BluetoothDevice.BOND_BONDED){
			new ConnectionTask().execute();
		}
		else{
			
			deviceToPair=choosen;
			
			ActivityManager activityMgr = new ActivityManager(this);
			activityMgr.openActivity(DevicePairingActivity.class);
			this.finish();


		}
	}

	/**
	 * Refreshes device list on refresh button click
	 */
	@Override
	public void onClick(View arg0) {
		deviceList.refresh(btHandler);
	} 
    
    
}