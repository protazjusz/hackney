================================================================================================
							*Glazed And Lightweight Open Protocol*
							timestamp opisu protoko�u: 08.04, 15:10
											wersja 0.3
================================================================================================
	Podstawow� jednostk� komunikacji w Projekcie stanowi komunikat okre�lony za pomoc� 
	klasy MessageData. Dane s� postaci tablicy bajt�w:
	
									[0][1] | (0)(1){2} ... {30}
	
	Ka�dy komunikat sk�ada si� z dwubajtowago nag��wka (header, elementy oznaczone "[]") oraz 
	przynajmniej dwubajtowago bloku danych (payload, elementy podane jako "()"). 
	Bajt 0 nag��wka okre�la typ komunikatu, bajt 1 opisuje identyfikator sesji, w przypadku 
	ju� nawi�zanego po��czenia lub identyfikator urz�dzenia. D�ugo�� nag��wka jest sta�a. 
	Bajty 2-30 (powy�ej przedstawione jako "{}") bloku danych oznaczone s� jako opcjonalne i 
	przeznaczone do u�ytku jedynie przez komunikaty typu STRING_EXCHANGE. 
================================================================================================
	Definicja komunikat�w, ze wzgl�du na typ (zerowy bajt nag��wka). Przez oznaczenie "SK" 
	opisujemy komunikaty przeznaczone do wysy�ania przez serwer, "KS" - przez klienta, 
	brak jednego z powy�szych oznacze� - typ dost�pny w obustronnej wymianie informacji. 
	
	Komunikaty og�lne:
	
		1) CONNECTION_HELLO = 1
			Opis: Komunikat wykorzystywany do nawi�zywania po��czenia.
			Pierwszy bajt nag��wka: identyfikator urz�dzenia, w przypadku komunikatu wysy�anego 
				przez klienta (zg�oszenie si� urz�dzenia danego typu) lub identyfikator sesji, 
				b�d�cy odpowiedzi� zwrotn� od serwera na wspomniane zg�oszenie klienta.
			Predefiniowane identyfikatory urz�dzenia: 	
				J2ME: 		1,
				Android:	2.
			Dane: ignorowane.
			
		2) CONNECTION_GOODBYE = 2
			Opis: Zako�czenie po��czenia.
			Pierwszy bajt nag��wka: identyfikator sesji.
			Dane: ignorowane.
			
	Komunikaty modu�u obs�ugi myszy:
			
		3) MOUSE_NUMPAD_START_MOVE = 10 (KS)
			Opis: Wci�ni�cie klawisza lub rozpocz�cie ruchu myszk� za pomoc� klawiatury lub 
				joysticka. Akcja wykonywana do otrzymania komunikatu typu MOUSE_NUMPAD_STOP.
			Pierwszy bajt nag��wka: jak wy�ej. Podobnie, dla kolejnych typ�w. Z tego powodu, 
				opis tego pola b�dzie pomijany w dalszej cz�ci tego dokumentu.
			Dane: 	(0) - kod klawisza.
			
		4) MOUSE_NUMPAD_STOP = 11 (KS)
			Opis: Zatrzymanie akcji powi�zanej z komunikatem MOUSE_NUMPAD_START_MOVE.
			Dane: 	(0) - kod klawisza.
			
		5) MOUSE_SCREEN_PRESSED = 12 (KS)
			Opis: Dotkni�cie ekranu, wykonywane przed rozpocz�ciem w�a�ciwego ruchu myszk�.
			Dane: 	(0) - 1 
						lub
				  	(0) - odci�ta wektora kierunku, w kt�rym wykonano ruch.
					(1) - rz�dna wektora kierunku, w kt�rym wykonano ruch.
						zale�nie od implementacji.
			
		6) MOUSE_SCREEN_MOVE = 13 (KS)
			Opis: Ruch myszk�, za pomoc� ekranu dotykowego.
			Dane:  	(0) - odci�ta wektora kierunku, w kt�rym wykonano ruch.
					(1) - rz�dna wektora kierunku, w kt�rym wykonano ruch.
					
		7) MOUSE_SCREEN_RELEASED = 14 (KS)
			Opis: Puszczenie ekranu.
			Dane: 	(0) - 1
						lub
				  	(0) - odci�ta wektora kierunku, w kt�rym wykonano ruch.
					(1) - rz�dna wektora kierunku, w kt�rym wykonano ruch.
						zale�nie od implementacji.
					
	Komunikaty modu�u obs�ugi klawiatury:
			
		8) KEYBOARD_PRESSED = 20 (KS)
			Opis: Wci�ni�cie klawisza klawiaturowego.
			Dane:	(0) - kod ASCII klawisza (w przypadku liter - wielkiej litery).
					(1) - modyfikatory.
						Poprzez modyfikator rozumiany jest klawisz specjalny, zmieniaj�cy 
						u�ywany do zmiany funkcji klawiszy standardowych.
						Przewidziane modyfikatory, wraz z warto�ciami:
							SHIFT:		2^0,
							CTRL:		2^1,
							ALT:		2^2,
							ALTGR:		2^3 (zale�nie od implementacji),
							SUPER/META/WIN : 2^4 (zale�nie od implementacji).
						Kombinacja modyfikator�w mo�liwa jest do uzyskania poprzez sum� ich 
						warto�ci - przyk�adowo SHIFT i ALT: 2^0 + 2^2 = 5.
						Nie przewiduje si� u�ycia wi�cej ni� 3 modyfikator�w jednocze�nie.
						
	Komunikaty modu�u obs�ugi skr�t�w klawiaturowych i zarz�dzania aplikacjami:
						
		9) HOTKEY_APPLIST_REQUEST = 30 (KS)
			Opis: Zg�oszenie w celu otrzymania listy aplikacji, dla kt�rych zdefiniowano 
				skr�ty klawiaturowe po stronie serwera.
			Dane:	(0) - 1.
		
		10) HOTKEY_APPLIST_COUNT = 31 (SK)
			Opis: Informacja zwrotna wysy�ana do klienta, zawieraj�ca liczb� element�w 
				wchodz�cych w sk�ad listy aplikacji.
			Dane: 	(0) - liczba element�w listy aplikacji.
		
		11) HOTKEY_APP_CHOSED = 32 (KS)
			Opis: Wyb�r aplikacji spo�r�d listy nades�anej przez serwer.
			Dane:	(0) - numer elementu z listy aplikacji.
			
		12) HOTKEY_APPHOTKEY_REQUEST = 33 (KS) 
			Opis: Zg�oszenie w celu otrzymania listy skr�t�w, zdefiniowanych dla wybranego 
				wcze�niej programu.
			Dane:	(0) - 1.		
			
		13) HOTKEY_APPHOTKEY_COUNT = 34 (SK) 
			Opis: Informacja zwrotna wysy�ana do klienta, zawieraj�ca liczb� element�w z
				listy skr�t�w.
			Dane: 	(0) - liczba element�w listy skr�t�w.			
			
		14) HOTKEY_USED = 35 (KS)
			Opis: Kod wykorzystywanego skr�tu.
			Dane:	(0) - numer elementu z listy skr�t�w.
			
		15) STRING_EXCHANGE = 39 (SK) 
			Opis: Typ komunikatu u�ywany do przesy�ania opis�w aplikacji i skr�t�w 
				klawiszowych. Wspomagany jest przez komunikaty HOTKEY_APPLIST_COUNT oraz 
				HOTKEY_APPHOTKEY_COUNT. Jako jedyny przewiduje wykorzystanie poszerzonego 
				bloku danych, o maksymalnej d�ugo�ci 30 bajt�w.
			Dane:	(0) ... {30} - ci�g znak�w zakodowany w standardzie UTF-8 (15 znak�w,
					ka�dy kodowany przez 2 bajty).
					
		16) GAMEPAD_ACCEL_MOVE = 40 (KS)
			Opis: Rozpocz�cie akcji/ruchu wywo�anego przez zmian� orientacji urz�dzenia 
				wzgl�dem jego pocz�tkowych ustawie�. Pomiar dokonywany za pomoc� akcelerometru.
			Dane:	(0) - kod odpowiadaj�cy zmianie po�o�enia wzgl�dem pocz�tkowych ustawie�:
						2 - wychylenie urz�dzenia w lewo, 
						4 - wychylenie urz�dzenia w prawo, 
						8 - wychylenie urz�dzenia w lewo wzgl�dem jego wysoko�ci,
						16 - wychylenie urz�dzenia w prawo wzgl�dem jego wysoko�ci, 
						32 - wychylenie urz�dzenia w prz�d,
						64 - wychylenie urz�dzenia w ty�.
					(1) - warto�� z zakresu 0..4, identyfikuj�ca gracza (wed�ug komunikatu typu
						GAMEPAD_PLAYER_SET).
			
		17) GAMEPAD_ACCEL_STOP = 41 (KS)
			Opis: Zako�czenie akcji/ruchu zapocz�tkowanego przez komunikat typu 
				GAMEPAD_ACCEL_MOVE
			Dane:	(0) - kod zmiany po�o�enia, dla kt�rego wykonywana akcja zostanie przerwana.	
					Warto�ci identyczne, jak dla komunikatu GAMEPAD_ACCEL_MOVE.
					(1) - warto�� z zakresu 0..4, identyfikuj�ca gracza (wed�ug komunikatu typu
						GAMEPAD_PLAYER_SET).
					
		18) GAMEPAD_KEY_PRESSED = 42 (KS)
			Opis: Typ komunikatu odpowiadaj�cy wci�ni�ciu specjalnego klawisza funkcyjnego. 
				Klawisze te wsp�pracuj� razem z akcelerometrem, realizuj�c razem funkcjonalno�� 
				kontrolera typu gamepad.
			Dane:	(0) - kod odpowiadaj�cy specjalnemu klawiszowi funkcyjnemu:
						2 - klawisz A,
						4 - klawisz B,
						8 - klawisz C,
						16 - klawisz D,
						32 - klawisz E,
						64 - klawisz F.
					(1) - warto�� z zakresu 0..4, identyfikuj�ca gracza (wed�ug komunikatu typu
						GAMEPAD_PLAYER_SET).
						
		19) GAMEPAD_KEY_RELEASED = 43 (KS)
			Opis: Typ komunikatu odpowiadaj�cy puszczeniu specjalnego klawisza funkcyjnego. 
			Dane:	Jak w przypadku komunikatu SENSOR_KEY_SET.	
			
		20) GAMEPAD_PLAYER_SET = 44
			Opis: Komunikat zawieraj�cy informacj�, kt�ry zestaw klawiszy zosta� przydzielony 
				graczowi lub zwolnienie konfiguracji przez klienta.
			Dane:	(0) - warto�� z zakresu 0..4 identyfikuj�ca poprzedni zestaw klawiszy po stronie klienta.
					(1) - warto�� z zakresu 0..4 identyfikuj�ca nowy zestaw klawiszy po stronie klienta.
					
			
================================================================================================
