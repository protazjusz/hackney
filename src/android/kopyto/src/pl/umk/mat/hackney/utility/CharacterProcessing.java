package pl.umk.mat.hackney.utility;

public class CharacterProcessing {
	
	public CharacterProcessing() {
		
	}
	
	public byte[] getBytesFromChar(char keyChar)
	{ // ifsort powah
		byte[] data = {(byte)0, (byte)0};
		
		if (keyChar == '\n'){
			data[0] = (byte)'\n';
		}
		else if (keyChar == '\b'){
			data[0] = (byte)'\b';
		}
		else if (Character.isWhitespace(keyChar)){
			data[0] = (byte)32;
		}
		else if (Character.isLowerCase(keyChar)) {
			switch (keyChar) {
				case 'ą': data[0] = (byte)'A'; data[1] = (byte)8; break;
				case 'ć': data[0] = (byte)'C'; data[1] = (byte)8; break;
				case 'ę': data[0] = (byte)'E'; data[1] = (byte)8; break;
				case 'ł': data[0] = (byte)'L'; data[1] = (byte)8; break;
				case 'ń': data[0] = (byte)'N'; data[1] = (byte)8; break;
				case 'ó': data[0] = (byte)'O'; data[1] = (byte)8; break;
				case 'ś': data[0] = (byte)'S'; data[1] = (byte)8; break;
				case 'ź': data[0] = (byte)'X'; data[1] = (byte)8; break;
				case 'ż': data[0] = (byte)'Z'; data[1] = (byte)8; break; 
				default: int workVal = (int)keyChar; data[0] = (byte) (workVal - 32); break;
			}
		}
		else if (Character.isUpperCase(keyChar)) {
			switch (keyChar) {
				case 'Ą': data[0] = (byte)'A'; data[1] = (byte)9; break;
				case 'Ć': data[0] = (byte)'C'; data[1] = (byte)9; break;
				case 'Ę': data[0] = (byte)'E'; data[1] = (byte)9; break;
				case 'Ł': data[0] = (byte)'L'; data[1] = (byte)9; break;
				case 'Ń': data[0] = (byte)'N'; data[1] = (byte)9; break;
				case 'Ó': data[0] = (byte)'O'; data[1] = (byte)9; break;
				case 'Ś': data[0] = (byte)'S'; data[1] = (byte)9; break;
				case 'Ź': data[0] = (byte)'X'; data[1] = (byte)9; break;
				case 'Ż': data[0] = (byte)'Z'; data[1] = (byte)9; break;
				default: data[0] = (byte)keyChar; data[1] = (byte)1; break;
			}
		}
		else 
			switch (keyChar) {
			case '!': data[0] = (byte)'1'; data[1] = (byte)1; break;
			case '"': data[0] = -(byte)'\''; data[1] = (byte)1; break;
			case '#': data[0] = (byte)'3'; data[1] = (byte)1; break;
			case '$': data[0] = (byte)'4'; data[1] = (byte)1; break;
			case '%': data[0] = (byte)'5'; data[1] = (byte)1; break;
			case '&': data[0] = (byte)'7'; data[1] = (byte)1;break;
			case '\'': data[0] = -(byte)'\''; break;
			case '(': data[0] = (byte)'9'; data[1] = (byte)1; break;
			case ')': data[0] = (byte)'0'; data[1] = (byte)1; break;
			case '*': data[0] = (byte)'8'; data[1] = (byte)1; break;
			case '+': data[0] = (byte)'='; data[1] = (byte)1;break;
			case ',': data[0] = (byte)','; break;
			case '-': data[0] = (byte)'-'; break;
			case '.': data[0] = (byte)'.'; break;
			case '/': data[0] = (byte)'/'; break;
			case ':': data[0] = (byte)';'; data[1] = (byte)1; break;
			case ';': data[0] = (byte)';'; break;
			case '<': data[0] = (byte)','; data[1] = (byte)1; break;
			case '=': data[0] = (byte)'='; break;
			case '>': data[0] = (byte)'.'; data[1] = (byte)1; break;
			case '?': data[0] = (byte)'/'; data[1] = (byte)1; break; 
			case '@': data[0] = (byte)'2'; data[1] = (byte)1; break; 
			case '[': data[0] = (byte)'['; break;
			case '\\': data[0] = (byte)'\\'; break; 
			case ']': data[0] = (byte)']'; break;
			case '^': data[0] = (byte)'6'; data[1] = (byte)1; break;
			case '_': data[0] = (byte)'-'; data[1] = (byte)1; break;
			case '`': data[0] = (byte)'`'; break;
			case '{' : data[0] = (byte)'['; data[1] = (byte)1; break; 
			case '|': data[0] = (byte)'\\'; data[1] = (byte)1; break;
			case '}': data[0] = (byte)']'; data[1] = (byte)1; break;
			case '~': data[0] = (byte)'`'; data[1] = (byte)1; break;
			case '€': data[0] = (byte)'u'; data[1] = (byte)8; break;
			default: data[0] = (byte)keyChar; break;
			}
		return data;
	}
}
