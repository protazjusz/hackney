package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import pl.umk.mat.hackney.utility.StringRebuilder;
import pl.umk.mat.hackney.kopyto.RemoteControlActivity.MessagesForShortcutsListener;
import pl.umk.mat.hackney.kopyto.ui.ApplicationList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment responsible for interacting with user in application shortcuts module
 * 
 * @author HackneyTeam
 *
 */
public class ShortcutsFragment extends Fragment implements ApplicationList.ApplicationListListener, MessagesForShortcutsListener {
	
	private Connection connection = RemoteControlActivity.connection;
	private ApplicationList applicationList;
	private StringRebuilder stringRebuilder;

	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		stringRebuilder = new StringRebuilder();
		
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {	
		RemoteControlActivity.messagesForShortcutsListener = this;
		return inflater.inflate(R.layout.shortcuts, container, false);
  }
	

	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().findViewById(R.id.shortcuts_button).setPressed(true);
		MessageData data;
		applicationList = new ApplicationList(this) ;    
		
		data= new MessageData(new byte[]{ MessageData.HOTKEY_APPLIST_REQUEST , 0 , (byte)1});
		connection.outStream.write(data);	
		
	}
	
	/**
	 * Updates activity layout
	 */
	@Override
	public void onPause(){
		super.onPause();
		ActivityProperties.get().findViewById(R.id.shortcuts_button).setPressed(false);
	}
	
	
	/**
	 * Defines reaction for application choosen
	 */
	@Override
	public void onApplicationChoosen(String application) {

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();		
		ShortcutsApplicationFragment saf = new ShortcutsApplicationFragment(application);
		fragmentTransaction.replace(R.id.content, saf);
		fragmentTransaction.commit();
		
		MessageData data = new MessageData(new byte[]{ MessageData.HOTKEY_APP_CHOSED , 0 , (byte)applicationList.indexOf(application)});
		connection.outStream.write(data);
	}

	/**
	 * Defines reaction for incoming message from server
	 */
	@Override
	public void onApplicationReceived(MessageData application) {
		String tmp = stringRebuilder.fromBytes(application.getPayload()).trim();
		if(!applicationList.include(tmp))applicationList.add(tmp);
		
	}

	@Override
	public void onShortcutReceived(MessageData application) {
		// never should happen
	}
}
