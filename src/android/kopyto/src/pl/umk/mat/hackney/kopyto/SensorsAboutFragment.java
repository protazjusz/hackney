package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Fragment responsible for interacting with user in application sensors about tab
 * 
 * @author HackneyTeam
 *
 */
public class SensorsAboutFragment extends Fragment implements OnClickListener {

	Button settings;
	Button disconnect;
	Button playerProfile;
	TextView info;
	

	private boolean change;
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.about_sensors, container, false);
		
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(true);
		
		settings=(Button) ActivityProperties.get().findViewById(R.id.settingsButton);
		disconnect=(Button) ActivityProperties.get().findViewById(R.id.disconnectButton);
		playerProfile=(Button) ActivityProperties.get().findViewById(R.id.profileButton);
		info = (TextView) ActivityProperties.get().findViewById(R.id.fragmentInfoText);
		playerProfile.setOnClickListener(this);
		settings.setOnClickListener(this);
		disconnect.setOnClickListener(this);
		change = true;
		
		info.setText(R.string.sensorsInfo);
	}
	
	/**
	 * Updates layout
	 */
	@Override 
	public void onDestroyView(){
		super.onDestroyView();
		if(change)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(false);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}
	
	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View arg0) {
		FragmentManager fragmentManager=getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		switch(arg0.getId()){
			case R.id.profileButton:
				change = false;
				SensorsProfileOptionsFragment spof= new SensorsProfileOptionsFragment();
				fragmentTransaction.replace(R.id.content, spof);
				fragmentTransaction.commitAllowingStateLoss();
				
				break;
			case R.id.settingsButton:
				change = false;
				SensorsOptionsFragment sof= new SensorsOptionsFragment();
				fragmentTransaction.replace(R.id.content, sof);
				
				fragmentTransaction.commitAllowingStateLoss();
				
				break;
			case R.id.disconnectButton:
				change = true;
				RemoteControlActivity.showDisconnectDialog();
				break;
		}
	}
	

}
