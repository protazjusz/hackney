package pl.umk.mat.hackney.kopyto;

/**
 * 
 * Class for constants 
 * @author HackneyTeam
 *
 */
public class Options {
	public static final String HACKNEY_CONF_FILE = "HackneyConfiguration";
	public static final float DEFAULT_SENSITIVITY = 50;
	public static final float DEFAULT_AXIS_SENSITIVITY=3;
}
