package pl.umk.mat.hackney.kopyto;

import java.io.IOException;


import pl.umk.mat.hackney.networking.BluetoothHandler;
import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


/**
 * Activity responsible for pairing and connecting devices
 * 
 * @author HackneyTeam
 *
 */
public class DevicePairingActivity extends Activity implements OnClickListener{

	private BluetoothDevice device = SearchActivity.deviceToPair;
	private BluetoothHandler btHandler = EnableBluetoothActivity.btHandler;
	
	private Button pairButton;
	private Button backButton; 
	
	private ProgressDialog progressDialog;
	private AlertDialog alertDialog;
	
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		ActivityProperties.set(this);
		setContentView(R.layout.device_pairing);
		pairButton = (Button) this.findViewById(R.id.pairButton);
		backButton = (Button) this.findViewById(R.id.backButton);
		pairButton.setOnClickListener(this);
		backButton.setOnClickListener(this);
		
		progressDialog = new ProgressDialog(this);	
	}

	/**
	 * Sets progress value on dialog
	 * @param integer progress
	 */
	private void updateProgress(Integer integer) {
		if(progressDialog.isShowing())progressDialog.setProgress(integer);
	}

	/**
	 * Shows connection progress dialog 
	 */
	void dialogShow() {
		progressDialog.setMessage(ActivityProperties.get().getString(R.string.connectingDialog));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	/**
	 * Hides connection progress dialog 
	 */
	void dialogHide() {
		if(progressDialog.isShowing())progressDialog.dismiss();
	}
	
	/**
	 * Shows connection error
	 */
	void showConnectionError(int stringId){

		if(progressDialog.isShowing())progressDialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(ActivityProperties.get().getString(stringId))
		.setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		alertDialog = builder.create();
		alertDialog.show(); 	
	}

	/**
	 * Task responsible for pairing and connecting to device in background and publishing progress in foreground
	 * 
	 * @author HackneyTeam
	 *
	 */
	class ConnectionTask extends AsyncTask<Void, Integer, Integer>{

		public ConnectionTask(){


		}

		@Override
		protected void onPreExecute(){
			dialogShow();

		}

		@Override
		protected Integer doInBackground(Void... a) {

			if(btHandler==null){EnableBluetoothActivity.btHandler = new BluetoothHandler();
			btHandler=EnableBluetoothActivity.btHandler;
			}
			
			btHandler.cancelDiscovery();

			this.publishProgress(10);
			try{
				btHandler.initializeConnection(device);
			} catch (IOException e) {
				return -1;
			}

			this.publishProgress(20);
			try {
				btHandler.connect(device);
			} catch (IOException e) {
				return -2;
			}

			this.publishProgress(40);
			
			RemoteControlActivity.connection = new Connection(btHandler.getSocket());
			
			this.publishProgress(50);
			try {
				RemoteControlActivity.connection.createOutStream();
				this.publishProgress(70);
				RemoteControlActivity.connection.createInStream();
			} catch (IOException e) {
				return -3;
			}
			this.publishProgress(80);
			if(!RemoteControlActivity.connection.tryHandShake()) return -5;
		

			this.publishProgress(90);
			if(RemoteControlActivity.connection.isConnected()){
				Intent intent = new Intent(ActivityProperties.get(),RemoteControlActivity.class);
				startActivity(intent);
			}
			else{
				return -4;
			}



			return 1;
		}
		@Override
		protected void onPostExecute(Integer result){
			if(result== -1){
				showConnectionError(R.string.pairingFailed);
			
			}
			else if(result == -2){
				showConnectionError(R.string.pairingFailed);
		
			}
			else if(result == -3){
				showConnectionError(R.string.pairingFailed);
			
			}
			else if(result == -4){
				showConnectionError(R.string.pairingFailed);
	
			}
			else if(result == -5){
				showConnectionError(R.string.pairingFailed);
			}
			else{
				dialogHide();
				end();
			}
		}
	
		@Override
		protected void onProgressUpdate(Integer... val){
			updateProgress(val[0]);
		}

		
	}
	
	
	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View arg0) {
		
		switch(arg0.getId()){
			case R.id.pairButton:
			
				new ConnectionTask().execute();
	
			break;
			
			case R.id.backButton:
				
				ActivityManager activityMgr = new ActivityManager(this);
				activityMgr.openActivityNoHistory(SearchActivity.class);
				this.finish();
				
			break;
		
		}
		
	}
	
	/**
	 * Ends activity
	 */
	private void end(){
		this.finish();
	}
}
