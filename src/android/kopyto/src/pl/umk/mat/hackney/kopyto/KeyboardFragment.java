package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import pl.umk.mat.hackney.utility.KeyboardController;
import pl.umk.mat.hackney.utility.CharacterProcessing;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Fragment responsible for interacting with user in application keyboard module
 * 
 * @author HackneyTeam
 *
 */
public class KeyboardFragment extends Fragment {
	private Button btnKeyboardSw;
	private KeyboardController keyboardCtrl;
	private TextView viewKeyCode;
	private EditText editText1;
	private MessageData md;
	private InputMethodManager imm;
	private View view;
	private CharacterProcessing charProcessing;
	private int lastEditableLength = 0;
	private int lastStart = 0;
	private int lastBefore = 0;
	private int lastCount = 0;

	
	private Connection connection = RemoteControlActivity.connection;
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.keyboard, container, false);

		return view;
	}

	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().findViewById(R.id.keyboard_button).setPressed(true);
		btnKeyboardSw = (Button)getActivity().findViewById(R.id.btnKeyboardSw); 
		viewKeyCode = (TextView)getActivity().findViewById(R.id.keyboardTextView);
		charProcessing = new CharacterProcessing();
		editText1 = (EditText)getActivity().findViewById(R.id.editText1);	
		editText1.setText("",TextView.BufferType.EDITABLE);
		editText1.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI|EditorInfo.IME_MASK_ACTION); 
		
		editText1.requestFocus();

		keyboardCtrl = new KeyboardController(this.getActivity());
		

		if (keyboardCtrl.getKeyboardType() == 1) btnKeyboardSw.setVisibility(View.GONE);
		
		imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


		btnKeyboardSw.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		
				keyboardCtrl.toggleSoftKeyboard();
				editText1.requestFocus();
			}
		});
		
		editText1.setOnKeyListener(new OnKeyListener() { 
	
			@Override 
		    public boolean onKey(View v, int keyCode, KeyEvent event) 
		    {
		    	if (event.getAction() == KeyEvent.ACTION_DOWN)
		    	{
		    		if (keyCode == KeyEvent.KEYCODE_ENTER)
		    		{
		    			viewKeyCode.setText(" ");
		    			byte bajty[]= charProcessing.getBytesFromChar('\n');
		    			md = new MessageData(MessageData.KEYBOARD_PRESSED, bajty);
		    			connection.outStream.write(md.toBytes());	
		    		}
		    		else if (keyCode == KeyEvent.KEYCODE_DEL)
		    		{
		    			viewKeyCode.setText(" ");
		    			byte bajty[]= charProcessing.getBytesFromChar('\b');
		    			md = new MessageData(MessageData.KEYBOARD_PRESSED, bajty);
		    			connection.outStream.write(md.toBytes());
		    		}	
		    		else if (keyCode == KeyEvent.KEYCODE_BACK)
		    			RemoteControlActivity.showDisconnectDialog();
		    	}
	    	
		        return true;
		    }
		});

		
		editText1.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void afterTextChanged(Editable s) {
				String printResult = "";
				char workChar = ' ';
				try {
					
					if (lastEditableLength == s.length())
					{ 
						lastStart = lastStart - lastBefore; 
						byte bajty[]=charProcessing.getBytesFromChar('\b');
						md = new MessageData(MessageData.KEYBOARD_PRESSED, bajty);
						connection.outStream.write(md.toBytes());						
					}

					for (int ile=0; ile < lastCount; ile++) 
					{
						
						try {	
							workChar = s.charAt(lastStart+ile);
							printResult+=workChar;
							byte bajty[]=charProcessing.getBytesFromChar(workChar);
							md = new MessageData(MessageData.KEYBOARD_PRESSED, bajty);
							connection.outStream.write(md.toBytes());
							Thread.sleep(50);
						} 
						catch (Exception e)
						{
				
						}
					}
					
					viewKeyCode.setText(printResult);
				
					if (editText1.length() > 240) 
						editText1.setText("", TextView.BufferType.EDITABLE);
					editText1.requestFocus();				
					editText1.getText().toString();
				} 
				catch (Exception e)
				{
			
				}

				lastEditableLength = s.length();
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			
				lastBefore = before;
				lastCount = count;
				lastStart = start+before; 
			}
		});
	}

	/**
	 * Updates layout
	 */
	@Override
	public void onPause(){
		imm.hideSoftInputFromWindow(editText1.getWindowToken(), 0);
		ActivityProperties.get().findViewById(R.id.keyboard_button).setPressed(false);
		super.onPause();
	}
	
}
