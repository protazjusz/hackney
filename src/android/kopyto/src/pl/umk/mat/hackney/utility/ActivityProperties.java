package pl.umk.mat.hackney.utility;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


//S�u�y do dzielenia informacji o activity mi�dzy klasy poprzez metody statyczne
public class ActivityProperties {
	
	public static Activity activity;
	
	public static void set(Activity target){
		ActivityProperties.activity = target;
		
	}
	
	public static Activity get(){
		return activity;
	}
	
	
	//wype�nienie grupy widoku root, layoutem o identyfikatorze id
	public static View inflate(int id, ViewGroup root){
		
		LayoutInflater layoutInflater = 
			(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	
		return layoutInflater.inflate(id, root);

	}
	
}
