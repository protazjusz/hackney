package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Fragment responsible for interacting with user in application touchpad about tab
 * 
 * @author HackneyTeam
 *
 */
public class TouchpadAboutFragment extends Fragment implements OnClickListener {

	Button saveButton;
	Button defaultsButton;
	Button backButton;
	Button settings;
	Button disconnect;
	TextView info;
	
	

	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.about_fragment, container, false);
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		ActivityProperties.get().findViewById(R.id.touchpad_button).setSelected(true);
		
		settings=(Button) ActivityProperties.get().findViewById(R.id.settingsButton);
		disconnect=(Button) ActivityProperties.get().findViewById(R.id.disconnectButton);
		info = (TextView) ActivityProperties.get().findViewById(R.id.fragmentInfoText);
		settings.setOnClickListener(this);
		disconnect.setOnClickListener(this);
		
		info.setText(R.string.touchpadInfo);
	}
	
	/**
	 * Updates activity layout
	 */
	@Override
	public void onDestroyView(){
		super.onDestroyView();
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
		ActivityProperties.get().findViewById(R.id.touchpad_button).setSelected(false);
	}

	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View arg0) {
		FragmentManager fragmentManager=getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		switch(arg0.getId()){
			case R.id.settingsButton:
				TouchpadOptionsFragment tof= new TouchpadOptionsFragment();
				fragmentTransaction.replace(R.id.content, tof);
				fragmentTransaction.commitAllowingStateLoss();
			break;
			
			case R.id.disconnectButton:
				RemoteControlActivity.showDisconnectDialog();
			break;
		}
	}
}
