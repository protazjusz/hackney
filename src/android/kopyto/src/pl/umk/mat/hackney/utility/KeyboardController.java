package pl.umk.mat.hackney.utility;

import android.content.Context;
import android.content.res.Configuration;
import android.view.inputmethod.InputMethodManager;

public class KeyboardController {

	private Context context;

	public KeyboardController(Context appContext) {
		this.context = appContext;
	}

	// pokaz/ukryj klawiature ekranowa
	public void toggleSoftKeyboard() {
		InputMethodManager inputMgr = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMgr.toggleSoftInput(0, 0);
	}

	// sprawdz typ klawiatury (0 - soft, 1 - hard, 2 - 12klawiszowa)
	public int getKeyboardType() {
		Configuration config = context.getResources().getConfiguration();

		if (config.keyboard == Configuration.KEYBOARD_NOKEYS)
			return 0;
		else if (config.keyboard == Configuration.KEYBOARD_QWERTY)
			return 1;
		else if (config.keyboard == Configuration.KEYBOARD_12KEY)
			return 2;
		return -1;
	}
	
	//public static byte keyToBigASCII(byte keyValue)
	//{
	//	int workVal = keyValue;
	//	if (workVal >= 97 && workVal <= 122)
	//		workVal = workVal - 32;
	//	return (byte)workVal;
	//}
	

}
