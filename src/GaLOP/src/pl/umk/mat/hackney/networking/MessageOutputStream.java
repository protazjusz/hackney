package pl.umk.mat.hackney.networking;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Implements output stream for the use with MessageData messages.
 * @author HackneyTeam
 */
public class MessageOutputStream 
{
	private DataOutputStream output;
	
	/**
	 * Creates new stream for writing.
	 * @param stream Existing DataOutputStream object
	 */
	public MessageOutputStream(DataOutputStream stream)
	{
		output = stream;
	}
	
	/**
	 * Creates new stream for writing.
	 * @param stream Existing OutputStream object
	 */
	public MessageOutputStream(OutputStream stream)
	{
		output = new DataOutputStream(stream);
	}
	
	/**
	 * Returns a data output stream from this stream.
	 * @return data input stream
	 */
	public DataOutputStream getDataOutputStream()
	{
		return output;
	}
	
	/**
	 * Writes data from bytes array to the output stream.
	 * @param bytes
	 */
	public void write(byte[] bytes)
	{
		try 
		{
			output.write(bytes);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Writes data from MessageData object to the output stream.
	 * @param msgData message to be written to this stream
	 */
	public void write(MessageData msgData)
	{
		byte[] data = msgData.toBytes();
		write(data);
	}
	
	/**
	 * Flushes this output stream and forces any buffered output bytes to be written out.
	 */
	public void flush()
	{
		try 
		{
			output.flush();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Closes this stream.
	 */
	public void close()
	{
		try 
		{
			output.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
