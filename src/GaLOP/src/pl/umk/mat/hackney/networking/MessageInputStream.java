package pl.umk.mat.hackney.networking;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
/**
 * Implements input stream for the use with MessageData messages.
 * @author HackneyTeam
 */
public class MessageInputStream 
{
	private DataInputStream input;
	
	/**
	 * Creates new stream for reading.
	 * @param stream Existing DataInputStream object
	 */
	public MessageInputStream(DataInputStream stream)
	{
		input = stream;
	}
	
	/**
	 * Creates new stream for reading.
	 * @param stream Existing InputStream object
	 */
	public MessageInputStream(InputStream stream)
	{
		input = new DataInputStream(stream);
	}
	
	/**
	 * Returns a data input stream from this stream.
	 * @return data input stream
	 */
	public DataInputStream getDataInputStream()
	{
		return input;
	}
	
	/**
	 * Reads some number of bytes from the contained input stream and stores them into the bytes array.
	 * @param bytes Read data 
	 * @return Count of read data
	 */
	public int read(byte[] bytes)
	{
		int number = -1;
		try 
		{
			number = input.read(bytes);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();			
		}
		return number;
	}

	/**
	 * Returns the number of bytes that can be read (or skipped over) from this input stream without blocking by the next caller of a method for this input stream.
	 * @return The number of bytes that can be read
	 * @throws IOException
	 */
	public int available() throws IOException
	{
		return input.available();
	}
	
	/**
	 * Reads data from the input stream and returns them as new MessageData object of type other than STRING_EXCHANGE.
	 * @return message with data read from the input stream
	 */
	public MessageData readMessageData()
	{
		byte[] bytes = new byte[4];
		read(bytes);
		MessageData msgData = new MessageData(bytes);
		return msgData;
	}
	
	/**
	 * Reads data from the input stream and returns them as new MessageData object of type STRING_EXCHANGE.
	 * @return message with data read from the input stream
	 */	
	public MessageData readStringMessageData()
	{
		byte[] bytes = new byte[32]; 
		read(bytes);
		MessageData msgData = new MessageData(bytes);
		return msgData;
	}
	
	/**
	 * Reads data from the input stream and returns them as new MessageData object (supports any type of message).
	 * @return message with data read from the input stream
	 */		
	public MessageData readAnyMessageData()
	{
		byte[] bytes = new byte[2];
		byte[]bytes2;
		byte[]bytes3;

		read(bytes);
		
		if(bytes[0]!= MessageData.STRING_EXCHANGE) 
		{
			bytes2 = new byte[2];
			bytes3= new byte[4];
			read(bytes2);
			for(int i=0;i<2;i++)
			{
				bytes3[i]=bytes[i];
			}
			for(int i=2;i<4;i++)
			{
				bytes3[i]=bytes2[i-2];
			}
		}
		else {
			bytes2 = new byte[30];
			bytes3 = new byte[32];
			read(bytes2);
			for(int i=0;i<2;i++) {
				bytes3[i]=bytes[i];
			}
			for(int i=2;i<32;i++) {
				bytes3[i]=bytes2[i-2];
			}
		}
		MessageData md = new MessageData();
		md.fromBytes(bytes3);
		return md;
	}
	
	/**
	 * Closes this stream.
	 */
	public void close()
	{
		try 
		{
			input.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
