package pl.umk.mat.hackney.graphics;

import java.io.IOException;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.networking.Client;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.Colors;
import pl.umk.mat.hackney.utility.Dictionary;


/**
 * 
 * Tasks off this class are to handle keys that are pressed, prepare them to communication, 
 * and illustrate it on the screen. It serves as a mouse module.
 * 
 * @author HackneyTeam
 *
 */
public  class MouseCanvas extends javax.microedition.lcdui.Canvas 
{
	private int x,y;
	private boolean on[];
	private int z;
	private Image background;
	private int backgroundColor;
	private int textColor;
	private int selectedTextColor;
	
	public Client cli;	


	
	public MouseCanvas()
	{
		cli=null;
		backgroundColor=Colors.black;
		textColor=Colors.white;
		selectedTextColor=Colors.red;
		on=new boolean[9];
	
		for(int i=0;i<9;i++) on[i]=false;
		try 
		{
			background=Image.createImage("/images/background.png");
		} 
		catch (IOException e)
		{

		}
		
		repaint();
	}
	
	/**
	 * 
	 * Illustrates on the screen, the pressed and sent keys and modifications .
	 * 
	 */
	protected void paint(Graphics graph)
	{
		graph.setColor(backgroundColor);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		if(background!=null)
			graph.drawImage(background, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);
		
		int dx=graph.getClipWidth()/(3+1);
		int dy=graph.getClipHeight()/(3+1);
		int x=dx/2;
		int y=dy/2;
		int dx1=dx/20;
		int dy1=dy/20;
		int dlu=dx-2*dx1;
		int wys=dy-2*dy1;
		int i,j;
		
		String napisy[]={Dictionary.eng("LPM"),"2",Dictionary.eng("PPM")," "," "," ","7"," ","9"};
		for(int k=0;k<9;k++)
		{
			j=k/3;
			i=k%3;
			 graph.setColor(Colors.black);
			graph.fillRect(x+dx*i+dx1 ,y+dy*j+dy1, dlu,wys);
			if(on[k]==false) graph.setColor(textColor);
				else graph.setColor(selectedTextColor); 
			graph.drawRect(x+dx*i+dx1 ,y+dy*j+dy1, dlu,wys);
			graph.drawString(napisy[k],x+dx*i+dx1+dlu/2-graph.getFont().stringWidth(napisy[k])/2 ,y+dy*j+dy1+wys/2-graph.getFont().getHeight()/2, 0);
		}
		
		/* VERTICAL ARROWS */
		int X[]={3,5};
		for(int ii=0;ii<X.length;ii++)
		{
			if(on[X[ii]]==false) graph.setColor(textColor);
				else graph.setColor(selectedTextColor);
			j=X[ii]/3;
			i=X[ii]%3;
			dx1=dx/5;
			dlu=dx-2*dx1;
		
			int sx1=x+dx*i+dx1;
			int sx2=x+dx*i+dx1+dlu;
			int sy=y+dy*j+dy1+wys/2;
			
			graph.fillRect(sx1, sy-1, dlu, 3);
			if(ii==0) graph.fillTriangle(sx1-3,sy,sx1+dx1,sy+wys/5,sx1+dx1,sy-wys/5);
			else      graph.fillTriangle(sx2+3,sy,sx2-dx1,sy+wys/5,sx2-dx1,sy-wys/5);
		}
		
		/* HORIZONTAL ARROWS */
		int X2[]={4,7};
		for(int ii=0;ii<X2.length;ii++)
		{
			if(on[X2[ii]]==false) graph.setColor(textColor);
				else graph.setColor(selectedTextColor);
		
			j=X2[ii]/3;
			i=X2[ii]%3;
			dy1=dy/5;
			wys=dy-2*dy1;
			dx1=dx/5;
			dlu=dx-2*dx1;

			int sx=x+dx*i+dx1+dlu/2;
			int sy1=y+dy*j+dy1;
			int sy2=y+dy*j+dy1+wys;
			
			graph.fillRect(sx-1, sy1, 3, wys);
			if(ii==0) graph.fillTriangle(sx,sy1-3,sx-dlu/3,sy1+dy1,sx+dlu/3,sy1+dy1);
			else      graph.fillTriangle(sx,sy2+3,sx-dlu/3,sy2-dy1,sx+dlu/3,sy2-dy1);
		}
		
	}

	/**
	 * 
	 * Method called when a key is pressed, it sends that key.
	 * 
	 */
	protected void keyPressed(int keyCode)
	{
		int k=keyCode-'0';
		if(cli!=null)
		{
			if(k>=0 && k<=9) on[k-1]=true;
			repaint();
		
			byte S[]={(byte) keyCode};
			
			MessageData pakiet= new MessageData(MessageData.MOUSE_NUMPAD_START_MOVE,S);
			send(pakiet);
		}	
	}
	
	/**
	 * 
	 * Method called when the button is released.
	 * 
	 */
	protected void keyReleased(int keyCode)
	{
		if(cli!=null)
		{ 
			int k=keyCode-'0';
			if(k>=0 && k<=9) on[k-1]=false;
			repaint();
			byte S[]={(byte) keyCode};
			MessageData pakiet= new MessageData(MessageData.MOUSE_NUMPAD_STOP,S);
			send(pakiet);
		}
	
	}
	
	/*Method for touch screens*/
	/**
	 * 
	 * Method called when the screen is touched, informs the server.
	 * 
	 */
	protected void pointerPressed(int a,int b)
	{
		if(cli!=null)
		{
			x=a;
			y=b;
			byte S[]={0,0};
			MessageData pakiet= new MessageData(MessageData.MOUSE_SCREEN_PRESSED,S);
			send(pakiet);
		}	
	}
	
	/*Method for touch screens*/
	/**
	 * 
	 * Method called when the pointer is moved on the screen. counts the direction and sends it to the server
	 * 
	 */
	protected void pointerDragged(int a,int b)
	{
		if(cli!=null)
		{
			x=a;
			y=b;
			byte S[]={(byte)(a-x),(byte)(b-y)};
			MessageData pakiet= new MessageData(MessageData.MOUSE_SCREEN_MOVE,S);
			send(pakiet);
		}	
	}
	
	/*Meotda dla telefonu dotykowego*/
	/**
	 * 
	 * Method called when the screen is released, informs the server.
	 * 
	 */
	protected void pointerReleased(int a,int b)
	{
		if(cli!=null)
		{
			byte S[]={0,0};
			MessageData pakiet= new MessageData(MessageData.MOUSE_SCREEN_RELEASED,S);
			send(pakiet);
		}	
		else
		{
			
		}
	}
	
	/**
	 * 
	 * Wraps the keys or other info prepares for communication and sends to the server.
	 *  
	 * @param msg message to be sent.
	 * 
	 */
	private  void send(MessageData msg)
	{
		try
		{	z=msg.getPayload()[0];
			cli.send(msg);
		}
		catch (IOException e)
		{
		}
	}
	
	public void stop()
	{
		if(cli!=null)
		{ 
			int k=z-'0';
			if(k>=0 && k<=9) on[k-1]=false;
			repaint();
			byte S[]={(byte)z};
			MessageData pakiet= new MessageData(MessageData.MOUSE_NUMPAD_STOP,S);
			send(pakiet);
		}
	}
}
