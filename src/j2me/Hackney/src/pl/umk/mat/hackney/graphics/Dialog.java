package pl.umk.mat.hackney.graphics;

import java.io.IOException;
import java.util.Vector;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.utility.Colors;
import pl.umk.mat.hackney.utility.Dictionary;

/**
 *
 * Purpose of this class is to draw Dialogs or Messages on the screen. The answers to the Dialog question should be added 
 * and implemented in another class. It simply draws the text. It can provide interface to scroll the text if it is too large 
 * to be printed on one screen.
 * 
 * @author HackneyTeam
 *
 */
public class Dialog extends Canvas {
	
	public final static int YES_COMMAND=1;
	public final static int NO_COMMAND=2;
	public final static int BACK_COMMAND=4;
	
	public Command yesCommand,noCommand,backCommand;
	
	
	private String info[];
	private boolean moveable;
	
	private int sy;
	
	private boolean inLimitD=true;
	private boolean inLimitU=true;
	
	private Image background;
	private int backgroundColor;
	private int textColor;
	
	/**
	 * 
	 * Constructor for Dialogs with large texts for which scrolling is needed
	 *  
	 * @param s	Text which be shown. Every next String from this array will start a next line.
	 * @param b	specifies if the text will be movable or not.
	 * @param command specifies which commands will be used in this Dialog (use constants)
	 * 
	 */
	public Dialog(String s[],boolean b,int command)
	{
		info=s;
		moveable=b;
		sy=0;
		
		backgroundColor=Colors.black;
		textColor=Colors.white;
		try
		{
			background=Image.createImage("/images/background.png");
		} 
		catch (IOException e)
		{
		}
		
		yesCommand= new Command(Dictionary.eng("Tak"),Command.OK,1);
		noCommand= new Command(Dictionary.eng("Nie"),Command.STOP,1);
		backCommand= new Command(Dictionary.eng("Powrót"),Command.BACK,1);
		if((command &(YES_COMMAND))==YES_COMMAND ) addCommand(yesCommand);
		
		if((command &(NO_COMMAND))==NO_COMMAND ) addCommand(noCommand);
		
		if((command &(BACK_COMMAND))==BACK_COMMAND ) addCommand(backCommand);
	}
	
	/**
	 * Constructor for Dialogs with short texts for which scrolling is not needed.
	 *  
	 * @param s	Text which be shown.
	 * @param command specifies which commands will be used in this Dialog (use constants)
	 * 
	 */
	public Dialog(String s,int  command)
	{
		
		sy=0;
		backgroundColor=Colors.black;
		textColor=Colors.white;
		info=new String[1];
		info[0]=s;
		moveable=false;
		try
		{
			background=Image.createImage("/images/background.png");
		}
		catch (IOException e)
		{
		}
		yesCommand= new Command(Dictionary.eng("Tak"),Command.OK,1);
		noCommand= new Command(Dictionary.eng("Nie"),Command.STOP,1);
		backCommand= new Command(Dictionary.eng("Powrót"),Command.BACK,1);
		if((command &(YES_COMMAND))==YES_COMMAND ) addCommand(yesCommand);
		
		if((command &(NO_COMMAND))==NO_COMMAND ) addCommand(noCommand);
		
		if((command &(BACK_COMMAND))==BACK_COMMAND ) addCommand(backCommand);
		
	}
	
	/**
	 * 
	 * Splits the String into an array using space as a separator.
	 *  
	 * @param x String which need to be split.
	 * 
	 * @return Array of single words
	 *  
	 */
	private String[] split(String x)
	{
		Vector v=new Vector();
		int b=0;
		int e=x.indexOf(' ',b)+1;

		while(b<e)
		{
			v.addElement(x.substring(b, e));
			b=e;
			e=x.indexOf(' ',b)+1;
		}
		x.indexOf(' ',b);
		
		String res[]=new String[v.size()];
		v.copyInto(res);
		
		return  res;
	}
	
	/**
	 * 
	 * Prepares objects which will be written on the screen
	 *  
	 */
	protected void paint(Graphics graph)
	{
		graph.setColor(backgroundColor);
		graph.fillRect(0, 0, graph.getClipWidth(), graph.getClipHeight());
		
		if(background!=null)
		graph.drawImage(background, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);

		graph.setColor(textColor);
		
		int dy=graph.getFont().getHeight()+4;
		int y=sy;
		int z=this.getHeight()/dy; 
		
		if(y>=0) inLimitU=false;
		else inLimitU=true;
		
		for(int j=0;j<info.length;j++)
		{
			int x=10;
			y++;
			String f[]=split(info[j]+" ");
			for(int i=0;i<f.length;i++)
			{
				int dx=graph.getFont().stringWidth(f[i]);
				if(x+dx>=graph.getClipWidth()) { y++ ; x=10;}
				if(y*dy>0)graph.drawString(f[i], x, y*dy, 0);
				if(x+dx<graph.getClipWidth()) x+=dx;	
			}
		}
		if(y<z) inLimitD=false;
		else	inLimitD=true;	
	}
	
	/**
	 * 
	 *	Method called when a button is pressed. It moves the text on the screen 
	 * 
	 */
	protected void keyPressed(int keyCode)
	{
		if( moveable && inLimitD && (keyCode==-2 || keyCode=='s'))
		{
			sy--;
		}
		if( moveable  && inLimitU && (keyCode==-1 ||  keyCode=='w'))
		{
			sy++;
		}
		repaint();	
	}
	
	/**
	 * 
	 *	Method called when a button is held. It moves the text on the screen   
	 * 
	 */
	protected void keyRepeated(int keyCode)
	{
		if( moveable && inLimitD && (keyCode==-2 || keyCode=='s'))
		{
			sy--;
		}
		if( moveable  && inLimitU && (keyCode==-1 || keyCode=='w'))
		{
			sy++;
		}
		repaint();		
	}
}
