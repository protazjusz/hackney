package pl.umk.mat.hackney.utility;

/**
 * 
 * Exception thrown in case of timeout.
 * 
 * @author HackneyTeam
 *
 */

public class TimeoutException extends Exception
{
	
}
