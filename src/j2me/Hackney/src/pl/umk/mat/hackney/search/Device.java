package pl.umk.mat.hackney.search;


/**
 * 
 * This class represents a device, it contains address and a name of a device. 
 * 
 * @author HackneyTeam
 *
 */
public class Device
{
	
	private String name;
	private String address;
	
	
	public Device(String name, String address)
	{	
		this.name = name;
		this.address = address;
	}

	/**
	 * 
	 * 
	 * @return the name
	 * 
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * 
	 * @return the address
	 * 
	 */
	public String getAddress()
	{
		return address;
	}
	
	

	
	

}
