package pl.umk.mat.hackney.utility;

import java.util.Hashtable;

/**
 * 
 * Class contains descriptions used by the application, their translations and errors.
 * 
 * @author HackneyTeam
 *
 */
public class Dictionary 
{
	/* tablice */
	public static  Hashtable english;
	public static  Hashtable bledy;
	
	/* kody błędów */
	public static final int CONNECTION_LOST=0;
	public static final int TIMEOUT=1;
	public static final int NO_PONIES=2;
	public static final int NO_DEVICES=3;
	public static final int NOTHING=4;
	public static final int FULL = 5;
	public static final int CONNECTION_REFUSED=6;
	
	
	/* Języki */
	public static final int POLISH=0;
	public static final int ENGLISH=1;


	public static int lang;	//identyfikator języka
	
	/* Informacje o aplikacji */
	public static String[] ABOUT_PL={"Copyright 2011-2012, Zespół Hackney ",
"",
"Hackney to projekt powstały na Wydziale Matematyki i Informatyki Uniwersytetu Mikołaja Kopernika w Toruniu, w ramach przedmiotu Programowanie Zespołowe. Pozwala on na zdalne sterowanie komputerem  za pośrednictwem telefonu komórkowego i technologii Bluetooth.",
"",
"Projekt udostępniony jest na licencji BSD.",
"",

"Logo WMiI UMK jest własnością Wydziału i użyte zostało za zgodą Dziekana.",
"",

"Pozostałe grafiki, jeśli nie określono inaczej, są dostępne na licencji Creative Commons BY-NC-SA 3.0"};

	
	public static String[] ABOUT_ENG={"Copyright 2011-2012, Hackney Team  ",
"",
"Hackney is a project developed at the Faculty of Mathematics and Computer Science, Nicolaus Copernicus University in Torun, on Team Programming course. It allows you to remotely control your computer via mobile phone over Bluetooth.",
"",
"The project is released under the BSD License.",
"",

"Logo is the property of Faculty of Mathematics and Computer Science NCU.",
"",

"Other graphics, unless otherwise noted, are licensed under a Creative Commons BY-NC-SA 3.0"};

	static
	{
		lang=0;
		english=new Hashtable();
		english.put("Wybierz",new String("Select"));
		english.put("O aplikacji",new String("About"));
		english.put("Powrót",new String("Back"));
		english.put("Język",new String("Language"));
		english.put("wyczyść dane",new String("Clear Data"));
		english.put("Wybierz urzadzenie",new String("Choose Device"));
		english.put("Wyszukaj urzadzenie",new String("Find Device"));
		english.put("Opcje",new String("Options"));
		english.put("Wyjście",new String("Exit"));
		english.put("Menu główne",new String("Main menu"));
		english.put("Usunąć dane urządzeń ?",new String("Clear devices data ?"));
		english.put("Tak",new String("Yes"));
		english.put("Nie",new String("No"));
		english.put("Uwaga!",new String("Warning!"));
		english.put("Nie ma czego usuwać!",new String("Nothing to delete"));
		english.put("Brak kucyków",new String("No Ponnies found"));
		english.put("Wybierz kucyka",new String("Choose a ponny"));
		english.put("Zerwano połączenie z serwerem",new String("The connection to a serwer was lost"));
		english.put("Dodać do znanych urządzeń",new String("Add to known devices"));
		english.put("Brak urządzeń",new String("No devices found"));
		english.put("Wybierz urządzenie",new String("Choose a device"));
		english.put("Specjalne",new String("Special"));
		english.put("Mysz",new String("Mouse"));
		english.put("Klawiatura",new String("Keyboard"));
		english.put("Skróty",new String("Shortcuts"));
		english.put("LPM",new String("LMB"));
		english.put("PPM",new String("RMB"));
		english.put("Urządzenia",new String("Devices"));
		english.put("Wyszukaj",new String("Find"));
		english.put("Uruchom aplikację",new String("Start application"));
		
		bledy =new Hashtable();
		bledy.put(new Integer(10*CONNECTION_LOST+POLISH),"Zerwano połączenie z serwerem");
		bledy.put(new Integer(10*CONNECTION_LOST+ENGLISH),"The connection to a serwer was lost");
	
		bledy.put(new Integer(10*TIMEOUT+POLISH),"Przekroczono czas oczekiwania");
		bledy.put(new Integer(10*TIMEOUT+ENGLISH),"Timeout");
	
		bledy.put(new Integer(10*NO_PONIES+POLISH),"Brak kucyków");
		bledy.put(new Integer(10*NO_PONIES+ENGLISH),"No Ponnies found");
		
		bledy.put(new Integer(10*NO_DEVICES+POLISH),"Brak urządzeń");
		bledy.put(new Integer(10*NO_DEVICES+ENGLISH),"No devices found");
		
		bledy.put(new Integer(10*NOTHING+POLISH),"Nie ma czego usuwać!");
		bledy.put(new Integer(10*NOTHING+ENGLISH),"Nothing to delete");
		
		bledy.put(new Integer(10*FULL+POLISH),"Nie można dodać urządzenia, Baza jest pełna!");
		bledy.put(new Integer(10*FULL+ENGLISH),"Device cannot be added, Database is full");
		
		bledy.put(new Integer(10*CONNECTION_REFUSED+POLISH),"Połaczenie odrzucone");
		bledy.put(new Integer(10*CONNECTION_REFUSED+ENGLISH),"Connection Refused");
	}

	/**
	 * 
	 * Gives the description using the current language
	 * 
	 * @param s Description which is being searched.
	 * 
	 * @return the proper  description in current language
	 * 
	 */
	public static String eng(String s)
	{
		if(lang==0) return s;
		return (String) english.get(s);
	}

	/**
	 * 
	 * Gives the description for specified error code.
	 *  
	 * @param x error code
	 *  
	 * @return the proper error description in curent language
	 *  
	 */
	public static String blad(int x)
	{
		return (String) bledy.get(new Integer(10*x+lang));
	}
	
	public static String[] about()
	{
		
		if(lang==0) return ABOUT_PL;
		return ABOUT_ENG;
		
	}
}
