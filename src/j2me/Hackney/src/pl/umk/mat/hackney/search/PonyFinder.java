package pl.umk.mat.hackney.search;

import java.io.IOException;
import java.util.TimerTask;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;


import pl.umk.mat.hackney.menu.DeviceMenu;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.RemoteDeviceCopy;


/**
 * 
 * The task of the class is to search devices and servers on those devices.
 * 
 * @author HackneyTeam
 * 
 */
public class PonyFinder  extends  TimerTask implements DiscoveryListener 
{
	private DiscoveryAgent agent;

	private Vector devices;
	private Vector ponies;
	
	private int currentDevice;
	
	private String devicesAdresses[];
	private String devicesNames[];
	
	private DeviceMenu menu;
	
	private int phase;
	
	public static final int FIND_DEVICES=0;
	public static final int FIND_PONIES=1;
	
	public static final UUID HACKNEY_UUID= new UUID(MessageData.UUID_JAVA , false);
	
	public PonyFinder(DiscoveryAgent da)
	{
		devices=new Vector();
		ponies=new Vector();
		agent=da;
		phase=FIND_DEVICES;
		
	}
	
	/**
	 * 
	 * It sets the device menu which will be noticed when the search is over.
	 * 
	 * @param m device menu
	 * 
	 */
	public void setMenu(DeviceMenu m)
	{
		menu=m;
	}
	
	/**
	 * 
	 * Called when a device is found
	 * 
	 */
	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1)
	{
		if(arg1.getMajorDeviceClass()==0x100)	devices.addElement(arg0);
	}
	
	/**
	 * 
	 * Called when the search for devices is over.
	 * 
	 */
	public void inquiryCompleted(int arg0)
	{
		phase=FIND_PONIES;
		setDevices();
		menu.chooseFromNew();
	}
	
	/**
	 * 
	 * Sets the device on which the server will be Sought
	 * 
	 */
	public void setDevices()
	{
		devicesNames=new String[devices.size()];
		devicesAdresses=new String[devices.size()];
		for(int i=0;i<devices.size();i++)
		{
			RemoteDevice temp=(RemoteDevice)devices.elementAt(i);
			try
			{
				devicesAdresses[i]=temp.getBluetoothAddress();
				devicesNames[i]=temp.getFriendlyName(true);	
			}
			catch (IOException e)
			{
				devicesNames[i]=temp.getBluetoothAddress();
			}
		}
	}
	
	/**
	 * 
	 * Sets the device on which the server will be Sought
	 * 
	 * @param known earlier found devices 
	 * 
	 */
	public void setDevices(Device known[])
	{
		devicesAdresses=new String[known.length];
		devicesNames=new String[known.length];
		for(int i=0;i<known.length;i++)
		{
			devicesAdresses[i]=known[i].getAddress();
			devicesNames[i]=known[i].getName();	
		}
		
	}

	/**
	 * 
	 * Called when the search for services is over.
	 * 
	 */
	public void serviceSearchCompleted(int arg0, int arg1)
	{
		phase=FIND_DEVICES;
		menu.choosingPony();
	}
	
	/**
	 * 
	 * Called when a server  is found .
	 * 
	 */
	public void servicesDiscovered(int arg0, ServiceRecord[] arg1)
	{
		for(int i=0;i<arg1.length;i++)
		{
			String url=arg1[i].getConnectionURL(0,false);
			ponies.addElement(url);	
		}
	}
	
	/**
	 * 
	 *  Gives an array with devices names
	 *   
	 * 	@return  devices names
	 *  
	 */
	public String[] getDevicesNames()
	{
		return devicesNames;
	}

	
	/**
	 * 
	 * gives servers addresses
	 * 
	 * @return servers addresses
	 * 
	 */
	public String[] getPonies()
	{
		String stado[]= new String[ponies.size()];
		ponies.copyInto(stado);
		return stado;
	}

	public void run()
	{
		if(phase==FIND_DEVICES)
		{
			devices.removeAllElements();
			try
			{
				agent.startInquiry(DiscoveryAgent.GIAC, this);
			} 
			catch (BluetoothStateException e)
			{
				e.printStackTrace();
			}
		}
		if(phase==FIND_PONIES)
		{
			ponies.removeAllElements();
			int  q[]={0x0100, 0x0005};
			UUID p[]={HACKNEY_UUID};			
			try
			{
				RemoteDevice temp= new RemoteDeviceCopy(devicesAdresses[currentDevice]); 
				agent.searchServices(q, p, temp, this);
			}
			catch (BluetoothStateException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Sets the current device, the current device is that where the servers are being sought
	 * 
	 * @param i device which is going to be marked as current
	 * 
	 */
	public void setCurrentDevice(int i)
	{
		currentDevice=i;
		phase=PonyFinder.FIND_PONIES;
	}

	/**
	 * 
	 * Defines searching phase
	 *   
	 * @param p phase
	 *  
	 */
	public void setPhase(int p)
	{
		phase = p;
	}
	
	
	public Device getCurrent()
	{
		return new Device(devicesNames[currentDevice],devicesAdresses[currentDevice]);
		
	}
}
