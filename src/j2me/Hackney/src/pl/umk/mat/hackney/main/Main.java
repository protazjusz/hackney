package pl.umk.mat.hackney.main;

import java.util.Timer;
import java.util.TimerTask;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreNotFoundException;

import pl.umk.mat.hackney.graphics.Dialog;
import pl.umk.mat.hackney.graphics.ListCanvas;
import pl.umk.mat.hackney.graphics.Picture;
import pl.umk.mat.hackney.menu.DeviceMenu;
import pl.umk.mat.hackney.search.DevicesDatabase;
import pl.umk.mat.hackney.utility.Dictionary;


/**
 * 
 * Applications main class  MIDlet it manages the whole application 
 * It creates the main menu and implements some of its options.
 * Creates secondary menus and handles them.
 * Furthermore it creates objects used later by the application such as the display and the Bluetooth objects.
 *  
 * @author HackneyTeam
 *
 */

public class Main extends MIDlet implements CommandListener
{
	private Command cancelC;
	
	private Display display;
	private LocalDevice localDevice;
	private DiscoveryAgent discoveryAgent;
	
	private ListCanvas 
		mainMenu,							// Główne menu
		options,							// Lista menu opcji
		languages;							// Lista języków
	
	private DeviceMenu menu;
	
	private final static int DEVICE=0;
	private final static int OPTIONS=1;
	private final static int ABOUT=2;
	private final static int EXIT=3;

	private final static int CLEAR_DATA=0;
	private final static int LANGUAGES=1;
	
	private Dialog clearingDevices, aboutDialog;

	/**
	 * 
	 *Constructor, creates the main menu and it elements.
	 *
	 */
	public Main()
	{
		display=Display.getDisplay(this);
		update();
		updateOptions();
	}
	
	protected void startApp() throws MIDletStateChangeException
	{
		try
		{			
			localDevice=LocalDevice.getLocalDevice();
			localDevice.setDiscoverable(DiscoveryAgent.GIAC);
			discoveryAgent=localDevice.getDiscoveryAgent();	
		} 
		catch (BluetoothStateException e)
		{
			e.printStackTrace();
		}
		
		display.setCurrent(new Picture("/images/splash.png"));
		Timer t=new Timer();
		t.schedule(
				new TimerTask()
				{
					public void run()
					{
						display.setCurrent(mainMenu);
					}
				},3000 );
	}
	
	/**
	 * 
	 * 	Implements methods for commands, which appear in the main and secondary menus.
	 * 
	 */
	public  void commandAction(Command c, Displayable d)
	{
		if(c==mainMenu.backCommand)
		{
			try
			{
				destroyApp(false);
				notifyDestroyed();
			}
			catch (MIDletStateChangeException e)
			{
				e.printStackTrace();
			}
		}
		
		if(c==mainMenu.selectCommand)
		{
			switch(mainMenu.getSelectedIndex())
			{
				case DEVICE:
				{
					menu=new DeviceMenu(discoveryAgent,display,mainMenu);
					menu.choose();
					break;
				}
			
				case OPTIONS:
				{
					updateOptions();
					display.setCurrent(options);
					break;
				}
				
				case ABOUT:
				{
					display.setCurrent(aboutDialog);
					break;
				}
					
				case EXIT:
				{
					try
					{
						destroyApp(false);
						notifyDestroyed();
					} 
					catch (MIDletStateChangeException e)
					{
						e.printStackTrace();
					}
					break;
				}
			}
		}
		
		if(c==options.selectCommand)
		{
			int q=options.getSelectedIndex();
			switch(q)
			{
				case CLEAR_DATA:
				{
					display.setCurrent(clearingDevices);
					break;	
				}
			
				case LANGUAGES:
				{
					display.setCurrent(languages);
					break;
				}	
			}
		}
		
		if(c==clearingDevices.yesCommand)
		{
			{
				try
				{
					DevicesDatabase db=new DevicesDatabase();
					if(db.getSize()!=0)
					{
						db.remove();
						display.setCurrent(options);
					}
					
					else
					{
						Alert a =new Alert(Dictionary.eng("Uwaga!"), Dictionary.blad(Dictionary.NOTHING), null, AlertType.WARNING);
						a.setTimeout(Alert.FOREVER);
						display.setCurrent(a, options);
					}
					
				}
				catch (RecordStoreNotFoundException e)
				{
					e.printStackTrace();
				}
				catch (RecordStoreException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		if( c==languages.backCommand || c==clearingDevices.noCommand )
		{
			display.setCurrent(options);
		}

		if(c==options.backCommand  ||  c==aboutDialog.backCommand)
		{
			display.setCurrent(mainMenu);
		}	

		if(c==languages.selectCommand)
		{
			int q=languages.getSelectedIndex();
			Dictionary.lang=q;
			update();
			updateOptions();
			display.setCurrent(languages);
		}
		
		if(c.equals(cancelC))
		{
			discoveryAgent.cancelInquiry(menu.finder);
		}
	}

	protected void pauseApp()
	{
	
	}
	
	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException
	{
		
	}
	
	/**
	 * 
	 *  Updates the main Menu and its elements
	 *   
	 */
	private void update()
	{
		aboutDialog=new Dialog(Dictionary.about(),true,Dialog.BACK_COMMAND);
		aboutDialog.setCommandListener(this);
		
		clearingDevices=new Dialog(Dictionary.eng("Usunąć dane urządzeń ?"),Dialog.YES_COMMAND|Dialog.NO_COMMAND);
		clearingDevices.setCommandListener(this);
		
		cancelC=new Command(Dictionary.eng("Powrót"),Command.BACK,1);
		
		int j=0;
		try
		{
			j=mainMenu.getSelectedIndex();
		}
		catch(NullPointerException e)
		{
			j=0;
		}
		
		String opcjeMenu[]=
		{
			Dictionary.eng("Urządzenia"),
			Dictionary.eng("Opcje"),
			Dictionary.eng("O aplikacji"),
			Dictionary.eng("Wyjście")
		};
		String commands[]={Dictionary.eng("Wybierz"),Dictionary.eng("Wyjście")};
		mainMenu=new ListCanvas("Hackney",opcjeMenu,commands);
		mainMenu.setCommandListener(this);
		mainMenu.setSelectedIndex(j);	
	}
	
	/**
	 * 
	 * Updates the secondary menus (Options menu, Language menu), and their elements.
	 *  
	 */
	private void updateOptions()
	{
		int i=0;
		int j=0;
		
		String listaOpcji[]={Dictionary.eng("wyczyść dane"),Dictionary.eng("Język")};
		String listaJezykow[]={"Polski","English"};
		
		try
		{
			 i=languages.getSelectedIndex();
		}
		catch(NullPointerException e)
		{
			i=0;
		}
		
		try
		{
			 j=options.getSelectedIndex();
		}
		catch(NullPointerException e)
		{
			j=0;
		}

		options= new ListCanvas(Dictionary.eng("Opcje"),listaOpcji,null);
	    options.setCommandListener(this);
	    options.setSelectedIndex(j);

		languages= new ListCanvas(Dictionary.eng("Język"),listaJezykow,null);
	    languages.setCommandListener(this);
	    languages.setSelectedIndex(i);
	  }
}