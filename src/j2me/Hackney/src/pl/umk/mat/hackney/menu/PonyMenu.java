package pl.umk.mat.hackney.menu;

import java.io.IOException;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.rms.RecordStoreFullException;

import pl.umk.mat.hackney.graphics.CanvasManager;
import pl.umk.mat.hackney.graphics.Dialog;

import pl.umk.mat.hackney.networking.Client;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.search.Device;
import pl.umk.mat.hackney.search.DevicesDatabase;
import pl.umk.mat.hackney.utility.Dictionary;
import pl.umk.mat.hackney.utility.StringRebuilder;
import pl.umk.mat.hackney.utility.TimeoutException;

/**
 * 
 * Task of this class is to interface to choose a server that will be used for communication
 * 
 * @author HackneyTeam
 *
 */
public class PonyMenu implements CommandListener
{
	private DeviceMenu deviceMenu;
	
	private String ponies[];
	private Display display;
	
	private CanvasManager cManager;
	
	private DevicesDatabase database;
	private Device device;
	private Dialog addingDeviceDialog;

	/**
	 * 
	 * @param disp display used to show graphical contents  
	 * @param mu devices menu
	 * @param serversList servers addresses
	 * 
	 */
	public PonyMenu( Display disp, DeviceMenu mu,String serversList[],Device current)
	{
		display=disp;
		ponies=serversList;
		deviceMenu=mu;
		database=new DevicesDatabase();
		device=current;
		addingDeviceDialog=new Dialog(Dictionary.eng("Dodać do znanych urządzeń")+'?',Dialog.YES_COMMAND|Dialog.NO_COMMAND);

		addingDeviceDialog.setCommandListener(this);
	}
	
	/**
	 * 
	 * Gets list of found servers and chooses the 'oldest', or an information that none were found
	 * 
	 */
	public void choosingPonny()
	{
		if(ponies.length==0)
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.NO_PONIES),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a,deviceMenu.menu);
		}
		else
		{
			connect();
		}
	}
	
	/**
	 * 
	 * Method gets the oldest server address and attempts to make a connection
	 *  
	 */
	public void connect()
	{
		Client client=null;
		String url;
		int idKucyka=ponies.length-1;
		display.vibrate(1000);
		
		cManager=new CanvasManager(display);
		cManager.setWyjscie(deviceMenu.menu);
		
		
		url=ponies[idKucyka];
		
		try
		{
			client=new Client(url);
		} 
		catch (IOException e3)
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_LOST),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a, deviceMenu.menu);
		}
		
		try
		{	
			client.handshake();
		}		
		catch (IOException e1)
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_LOST),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			client.close();
			display.setCurrent(a, deviceMenu.menu);
		}
		
		
		catch (TimeoutException e)
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.TIMEOUT),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			client.close();
			display.setCurrent(a, deviceMenu.menu);
		}
		
		if (client.wasHandshaken())
		{
			StringRebuilder sr=new StringRebuilder();
			cManager.setClient(client);	
			
			byte msg[]=new byte [2];
			msg[0]=1;
			
			MessageData pakiet =new MessageData(MessageData.HOTKEY_APPLIST_REQUEST,msg);
			try
			{
				client.send(pakiet);
			}
			catch (IOException e2)
			{
				Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_LOST),null,AlertType.WARNING);
				a.setTimeout(Alert.FOREVER);
				a.setType(AlertType.WARNING);
				display.setCurrent(a, deviceMenu.menu);
			}
			
			msg=new byte [32];
			try {
				client.receive(msg);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			pakiet.fromBytes(msg);
			
			byte size=pakiet.getPayload()[0];
			String aplikacje[]=new String[size];
			
			for(int i=0;i<size;i++)
			{
				msg=new byte [32];  
				try {
					client.receive(msg);
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				pakiet =new MessageData(MessageData.STRING_EXCHANGE);
				pakiet.fromBytes(msg);
				
				String s=sr.fromBytes(pakiet.getPayload());
				aplikacje[i]=s.trim();
			}
			
			cManager.setSkroty(aplikacje);
			Thread t=new Thread(cManager);
			t.start();
			
			if(!database.isKnown(device.getAddress()))
			{
			
				display.setCurrent(addingDeviceDialog);
			}
			else
			{
				display.setCurrent(cManager.mouse);	
			}
		
		}
		else
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_REFUSED),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			client.close();
			display.setCurrent(a, deviceMenu.menu);
		}
	}
	
	/**
	 * 
	 * Method implements behaviors for appearing Commands
	 *  
	 */
	public void commandAction(Command c, Displayable arg1)
	{
		
		
		if(c==addingDeviceDialog.yesCommand)
		{
			try
			{
				database.addDevice(device.getName(), device.getAddress());
			}
			catch (RecordStoreFullException e)
			{
				Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.FULL),null,AlertType.WARNING);
				a.setTimeout(Alert.FOREVER);
				a.setType(AlertType.WARNING);
				display.setCurrent(a,cManager.mouse);
			}
			display.setCurrent(cManager.mouse);
		}
		if(c==addingDeviceDialog.noCommand)
		{
			display.setCurrent(cManager.mouse);
		}
	
	}
	

}
