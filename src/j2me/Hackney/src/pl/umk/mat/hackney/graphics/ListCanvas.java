package pl.umk.mat.hackney.graphics;


import java.io.IOException;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.utility.Colors;
import pl.umk.mat.hackney.utility.Dictionary;


/**
 * 
 * Creates an list which can be navigated with keys.
 * 
 * @author HackneyTeam
 *
 */
public  class ListCanvas extends javax.microedition.lcdui.Canvas 
{
	private String names[];
	private String title;
	
	private int size=0;
	private int current=0;
	private int first=0;
	private int max=9;
	
	private int backgroundColor;
	private int textColor;
	private int selectedTextColor;
	private int selectedOptionColor;
	private int optionColor;
	private Image backgroundimg;
	
	public Command selectCommand,backCommand;
	
	/**
	 * 
	 * 
	 * Prepare texts for the List
	 *  
	 * @param title Title of the list 
	 * @param options positions on the list
	 * @param commandNames names for selectCommand and backCommand respectively in an String Array. if null defaults will be used. 
	 * 
	 */
	public ListCanvas(String title,String options[],String commandNames[])
	{
		backgroundColor=Colors.black;
		textColor=Colors.white;
		selectedTextColor=Colors.white;
		optionColor=Colors.white;
		selectedOptionColor=Colors.red;
		
		names=options;
		this.title=title;
		size=options.length;
		
		if (commandNames==null || commandNames.length<2)
		{
			selectCommand=new Command(Dictionary.eng("Wybierz"),Command.OK,1);
			backCommand=new Command(Dictionary.eng("Powrót"),Command.BACK,1);
		}
		else
		{
			selectCommand=new Command(commandNames[0],Command.OK,1);
			backCommand=new Command(commandNames[1],Command.BACK,1);
		}
		
		addCommand(selectCommand);
		addCommand(backCommand);
		
		try
		{
			backgroundimg=Image.createImage("/images/background.png");
		} 
		catch (IOException e)
		{

		}
		
		repaint();	
	}
	
	/**
	 * 
	 * Draws the list on the screen
	 * 
	 */
	protected void paint(Graphics graph)
	{
		int height=graph.getFont().getHeight()+20;
		int x=graph.getClipWidth()/20;	
		int width=graph.getClipWidth()-2*x;
	
		max=graph.getClipHeight()/(height);
		max-=1;
		
		graph.setColor(backgroundColor);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		if(backgroundimg!=null)
			graph.drawImage(backgroundimg, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);

		double di=(max-((max>size)?size:max))/2;
		if (max>size && size%2==1) di+=0.5;
		di+=1;
		
		graph.setColor(optionColor);
		graph.drawString(title, graph.getClipWidth()/2-graph.getFont().stringWidth(title)/2, 4, 0);
		
		for(int i=0;i<((max<size)?max:size);i++)
		{
			if(i==current-first) graph.setColor(selectedOptionColor);
				else graph.setColor(optionColor);
			graph.drawRoundRect(x, (int)((di+i)*height+4), width,height-18,width/8,(height-2)/5);
			
			if(i==current-first) graph.setColor(selectedTextColor); 
				else graph.setColor(textColor);
			graph.drawString(names[(i+first)%size], graph.getClipWidth()/2-graph.getFont().stringWidth(names[(i+first)%size])/2, (int)((di+i)*height+4), 0);
		}
	}
	
	/**
	 * 
	 *	Method called when a button is pressed, it permits to use keys to navigate thru the list. 
	 * 
	 */
	protected void keyPressed(int keyCode)
	{
		if(keyCode==-1 || keyCode=='w')
		{
			current=(current+size-1)%size;
			if(current>=max) first=(first+size-1)%size; else first=0;
		}
		if(keyCode==-2 || keyCode=='s')
		{
			current=(current+1)%size;
			if(current>=max) first=(first+1)%size; else first=0;
		}
	
		repaint();
	}
	
	/**
	 * 
	 *	Method called when a button is held, it permits to use keys to navigate thru the list. 
	 * 
	 */
	protected void keyRepeated(int keyCode)
	{
		if(keyCode==-1 || keyCode=='w')
		{
			current=(current+size-1)%size;
			if(current>=max) first=(first+size-1)%size; else first=0;		
		}
		if(keyCode==-2 || keyCode=='s')
		{
			current=(current+1)%size;
			if(current>=max) first=(first+1)%size; else first=0;
		}
		
		repaint();	
	}
	
	/**
	 * 
	 * It returns the current index , which is selected on the list.
	 * 
	 * @return index on the list
	 *  
	 */
	public int  getSelectedIndex()
	{
		return current;
	}
	
	/**
	 * 
	 * It sets the current index on the list.
	 * 
	 * @param x index on the list.
	 * 
	 */
	public void  setSelectedIndex(int x)
	{
		current=x;
	}
	
	/**
	 * Method gives the information about how many positions on the list are they. 
	 * 
	 * @return	number of positions on the list.
	 * 
	 */
	public int size()
	{
		return names.length;
	}
	
	/**
	 * 
	 * Method gives the label of the specified position on ther list
	 * 
	 * @param x index on the list
	 * 
	 * @return	label bounded with that index
	 *  
	 */
	public String getString(int x)
	{
		return names[x];
	}
	
}
