package pl.umk.mat.hackney.graphics;

import java.io.IOException;
import java.util.Vector;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

import pl.umk.mat.hackney.networking.Client;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.Dictionary;
import pl.umk.mat.hackney.utility.StringRebuilder;


/**
 * 
 * The task of this class is to create and manage canvas that are sending data to the server.
 * It creates simple canvas such as MouseCanvas and KeyboardCanvas.
 * It also creates a menu from which the canvas can be chosen. It provides an interface for adding new PresetCanvas to this menu.
 * It also implements methods for Commands in all canvas managed by it.   
 *
 * @author HackneyTeam
 */
public class CanvasManager  implements CommandListener , Runnable 
{
	private final String primeListEntries[]={Dictionary.eng("Mysz"),Dictionary.eng("Klawiatura"),Dictionary.eng("Skróty"),Dictionary.eng("Wyjście")};
	
	private Display display;
	
	private Vector canvas,canvasNames;
	
	public MouseCanvas mouse;
	public KeyboardCanvas keyboard;
	
	private Command 
		mouseC, 			// szybkie przejście na Canvas myszy
		keyboardC,			// Szybkie przejscie na Canvas klawiatury
		menuC;				// Przejscie do menu Canv
		
	
	private ListCanvas 
		canvasMenu,						// Menu canv
		shortcutMenu,					// Menu skrótów
	 	mainMenu;						// Menu główne	
	
	private int knownSchortcuts[];
	private Client cli;
	private StringRebuilder sr;
	
	/**
	 * 
	 * Creates MouseCanvas, KeyboardCanvas and Commands used in those
	 * 
	 * @param d display on which canvas will be drawn 
	 
	 */
	public CanvasManager(Display d) 
	{
		display = d;
		sr=new StringRebuilder();
		canvas=new Vector();
		canvasNames=new Vector();
		
		menuC=new Command("Menu",Command.BACK,1);
		keyboardC=new Command(Dictionary.eng("Klawiatura"),Command.BACK,1);
		mouseC=new Command(Dictionary.eng("Mysz"),Command.BACK,1);
		
		
		for(int i=0;i<primeListEntries.length;i++)
			canvasNames.addElement(primeListEntries[i]);
		updateCanvas();
		
		/* KLAWIATURA*/
		keyboard = new KeyboardCanvas();
		addCanva(keyboard);
		keyboard.addCommand(mouseC);
		
		/* MYSZ */
		mouse = new MouseCanvas();
		addCanva(mouse);
		mouse.addCommand(keyboardC);
	}

	/**
	 * 
	 * Implements behaviors for Command buttons, that appear on screen.
	 *  
	 */
	public void commandAction(Command c, Displayable arg1)
	{
		if(c.equals(keyboardC))
		{
			mouse.stop();
			keyboard.repaint();
			display.setCurrent(keyboard);
			resetKlawa();	
		}
		
		if(c.equals(mouseC))
		{	
			keyboard.changeState(false);
			display.setCurrent(mouse);
		}
		
		if(c==canvasMenu.backCommand)
		{	
			wyjscie();
		}
		
		if(c.equals(menuC) || c==shortcutMenu.backCommand)
		{
			if(arg1.equals(mouse)) mouse.stop();
			keyboard.changeState(false);
			display.setCurrent(canvasMenu);
		}
		
		if(c==canvasMenu.selectCommand)
		{
			int q=canvasMenu.getSelectedIndex();
			/*
			 ***znaczenie poszczegolnych wartosci***
			 * 
			 * 0 - Canva myszy
			 * 1 - Canva Klawiatury
			 * 2 - Menu skrótów
			 * pomiędzy - Canvy skrótów 
			 * lista.size-1 {ostatnia pozycja} - Wyjscie do głównego menu
			 */
			
			if(q==0)
			{
				display.setCurrent(mouse);
			}
			
			if(q==1)
			{
				keyboard.repaint();
				resetKlawa();
				display.setCurrent(keyboard);
			}
		
			if(q==2)
			{
				if(shortcutMenu.size()!=0)
				{
					display.setCurrent(shortcutMenu);
				}
				else
				{
					Alert a=new Alert(Dictionary.eng("Uwaga!"),"brak skrotow",null,AlertType.WARNING);
					a.setTimeout(Alert.FOREVER);
					a.setType(AlertType.WARNING);
					display.setCurrent(a,canvasMenu);
				}
			}
		
			if(q<canvasMenu.size()-1 && q>2)
			{
				PresetCanvas current=(PresetCanvas)canvas.elementAt(q-3); 
				byte choose[]={current.getNumber(),(byte)0};
				MessageData pakiet =new MessageData(MessageData.HOTKEY_APP_CHOSED,choose);
				
				try 
				{
					cli.send(pakiet);
					display.setCurrent(current);	
				}
				catch (IOException e)
				{		
				}				
			}
			
			if(q==canvasMenu.size()-1)
			{
				wyjscie();
			}
		}
		
		if(c==shortcutMenu.selectCommand)
		{
			int x=shortcutMenu.getSelectedIndex();
			byte choose[]={(byte)x,(byte)0};
			MessageData pakiet =new MessageData(MessageData.HOTKEY_APP_CHOSED,choose);
			
			try
			{
				cli.send(pakiet);
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			
			if(knownSchortcuts[x]==-1)
			{
				byte S[]={(byte)x,(byte)0};
				pakiet =new MessageData(MessageData.HOTKEY_APPHOTKEY_REQUEST,S);
				
				try
				{
					cli.send(pakiet);
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}		
			}
			else
			{
				display.setCurrent((Canvas)canvas.elementAt(knownSchortcuts[x]));	
			}	
		}
	}
	
	/**
	 * 
	 * Sets a link to the main Menu, making it possible to get back.
	 * 
	 * @param m The list that is the main Menu.
	 * 
	 */
	public void setWyjscie(ListCanvas  m)
	{
		mainMenu=m;
	}
	
	/**
	 * 
	 * Creates the shortcut menu from a String array.
	 *   
	 * @param skroty An array that contains shortcut descriptions.
	 * 
	 */
	public void setSkroty(String[] skroty)
	{
		String commands[]={Dictionary.eng("Wybierz"),"Menu"};
		shortcutMenu=new ListCanvas(Dictionary.eng("Wybierz")+':',skroty,commands);
		shortcutMenu.setCommandListener(this);
		knownSchortcuts=new int[skroty.length];
		for(int i=0;i<skroty.length;i++) knownSchortcuts[i]=-1;
	}
	
	/**
	 * 
	 * Method sets the client which is used by canvas to communicate which the server.
	 *  
	 * @param c the client
	 *
	 */
	public void setClient(Client c)
	{
		cli=c;
		mouse.cli=cli;
		keyboard.cli=cli;	
	}
	
	/**
	 * 
	 *  Updates the Canvas Menu.
	 *  
	 */
	public void updateCanvas()
	{
		String commands[]={Dictionary.eng("Wybierz"),Dictionary.eng("Wyjście")};
		String canvasNamesList[] =new String[canvasNames.size()];
		
		canvasNames.copyInto(canvasNamesList);
		canvasMenu=new ListCanvas(Dictionary.eng("Wybierz")+':',canvasNamesList,commands);
		canvasMenu.setCommandListener(this);
	}
	
	/**
	 *
	 * Method adds new Canvas to the manager, so it can manage its Commands and adds an Exit to the canvas
	 *
	 * @param c Canvas which will be added
	 *
	 */
	private void addCanva(Canvas c)
	{
		c.setCommandListener(this);
		c.addCommand(menuC);
	}
	
	/**
	 * 
	 * Triggers the exit to the main menu.
	 * 
	 */
	private void wyjscie()
	{
		try
		{
			cli.send(new MessageData(MessageData.CONNECTION_GOODBYE));
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		cli.close();
		display.setCurrent(mainMenu);
	}
	
	/**
	 * 
	 * Starts a new Keyboard thread.
	 * 
	 */
	private void resetKlawa()
	{
		Thread t;
		keyboard.changeState(true);
		t=new Thread(keyboard);
		t.start();
		keyboard.repaint();
	}

	/**
	 * Waits for new shortcut descriptions or GOODBYE message
	 */
	public void run()
	{
		byte b[]=new byte[32];
		
		try
		{
			while(cli.receive(b)!=-1)
			{
				MessageData msg=new MessageData(b);
				
				switch(msg.getMessageType())
				{
					case MessageData.HOTKEY_APPHOTKEY_COUNT:
					{
						byte size=msg.getPayload()[0];
						byte msg2[] = null;
						int x=shortcutMenu.getSelectedIndex();
						String s[]=new String[size];
						PresetCanvas result;
					
					
						for(int i=0;i<size;i++)
						{
							msg2 =new byte[32];
							try
							{
								cli.receive(msg2);
							}
							catch (IOException e)
							{		
								e.printStackTrace();
							} 
							
							MessageData pak=new MessageData(MessageData.STRING_EXCHANGE);
							pak.fromBytes(msg2);
							String ss=sr.fromBytes(pak.getPayload());
							s[i]=ss.trim();
							
						}	
						
						result = new PresetCanvas(shortcutMenu.getString(x),s,(byte)x);
						result.c=cli;
						display.setCurrent(result);
					
						addCanva(result);
						canvas.addElement(result);				
						canvasNames.insertElementAt(shortcutMenu.getString(x),canvasNames.size()-1);
						updateCanvas();
					
						knownSchortcuts[x]=canvas.size()-1;		
						break;
					}	
				
					case MessageData.CONNECTION_GOODBYE: 
					{
						cli.close();
						Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_LOST),null,AlertType.WARNING);
						a.setTimeout(Alert.FOREVER);
						a.setType(AlertType.WARNING);
						display.setCurrent(a, mainMenu);
					}			
				}
			}
		}
		catch (IOException e)
		{
			cli.close();
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.CONNECTION_LOST),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a, mainMenu);
		}
	}
	
}