package pl.umk.mat.hackney.graphics;

import java.io.IOException;

import java.io.InputStream;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;

import pl.umk.mat.hackney.utility.Colors;

/**
 * 
 * This class responsibility is to show an animated image used for a transition or a normal static image
 * if it is impossible.
 *  
 * @author HackneyTeam
 * 
 */

public class Transition extends Canvas
{
	private Player player;
	private VideoControl vidc;
	private Image img;
	
	/**
	 * 
	 * Constructor, creates an image which is going to be drawn.
	 *  
	 * @param url images address
	 * 
	 */
	public Transition(String url)
	{
		InputStream ins = getClass().getResourceAsStream(url);
		try
		{
			img=Image.createImage("/images/hackney0.png");	
		} 
		catch (IOException e)
		{
		}
		
        try
        {
        	player = Manager.createPlayer(ins, "image/gif");
			player.start();
	        if ((vidc = (VideoControl) player.getControl("VideoControl")) != null)
	        	vidc.initDisplayMode(VideoControl.USE_DIRECT_VIDEO, this);
	        vidc.setDisplaySize(this.getWidth(),this.getHeight());
	        vidc.setDisplayLocation(0, 0);
	        vidc.setVisible(true);	
		}
		catch (IOException e1)
		{
		} 
        catch (MediaException e1)
		{	
		}
        
        repaint();
	}
	
	/**
	 * 
	 * Draws an backup  image in case if the animated one does not work.
	 * 
	 */
	protected void paint(Graphics graph)
	{
		graph.setColor(Colors.black);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		if((vidc==null || player==null) && img!=null )
			graph.drawImage(img, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);
	}		
}
