package pl.umk.mat.hackney.utility;

import javax.bluetooth.RemoteDevice;

/**
 * 
 * This class is used to create an RemoteDevice using its address
 *  
 * @author HackneyTeam
 *
 */
public class RemoteDeviceCopy extends RemoteDevice
{
	/**
	 * 
	 * Constructor ,uses the superclass constructor to create an RemoteDevice from an address.
	 *  
	 * @param arg0 address of the remote device.
	 * 
	 */
	public RemoteDeviceCopy(String arg0)
	{
		super(arg0);
	}
}
