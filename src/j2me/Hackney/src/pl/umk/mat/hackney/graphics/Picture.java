package pl.umk.mat.hackney.graphics;

import java.io.IOException;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.utility.Colors;


/**
 * 
 * Task of this canvas is to prepare and show an Image.
 * 
 * @author HackneyTeam
 *
 */
public class Picture extends Canvas 
{
	private Image img = null;
	
	/**
	 * 
	 * Constructor, prepares an image , which will be drawn on the screen.
	 * 
	 * @param url address of the image
	 * 
	 */
	public Picture(String url)
	{
		try
        {
        	img=Image.createImage(url);    	
        }
        catch (IOException e1)
        {
		}
	}
	
	/**
	 * 
	 * Draws the centered Image.
	 * 
	 */
	protected void paint(Graphics graph)
	{
		graph.setColor(Colors.black);
		graph.fillRect(0, 0, graph.getClipWidth(), graph.getClipHeight());
		if(img!=null)
			graph.drawImage(img, graph.getClipWidth()/2,graph.getClipHeight()/2, Graphics.HCENTER| Graphics.VCENTER);
	}
}
