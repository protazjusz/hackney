package pl.umk.mat.hackney.utility;

/**
 * 
 * The purpose of this class is to make an way to represent specific characters so they will be understood by
 * both side of the comunication. 
 * 
 * @author HackneyTeam
 *
 */
public class StringRebuilder
{
	
	/**
	 * 
	 * Decompose a String to a array of bytes
	 * 
	 * @param s the String
	 * 
	 * @return bytes Array which will contain the codes of Strings characters
	 * 
	 */
	public byte [] toBytes(String s)
	{
		byte res[]=new byte[s.length()];
		
		for(int i=0;i<s.length();i++)
		{
			char c=s.charAt(i);
			int ic=(int)c;
			res[i]=0;
			
			if(c=='Ą') res[i]=(byte)-128;
			if(c=='Ć') res[i]=(byte)-127;
			if(c=='Ę') res[i]=(byte)-126;
			if(c=='Ł') res[i]=(byte)-125;
			if(c=='Ń') res[i]=(byte)-124;
			if(c=='Ó') res[i]=(byte)-123;
			if(c=='Ś') res[i]=(byte)-122;
			if(c=='Ź') res[i]=(byte)-121;
			if(c=='Ż') res[i]=(byte)-120;
			if(c=='ą') res[i]=(byte)-119;
			if(c=='ć') res[i]=(byte)-118;
			if(c=='ę') res[i]=(byte)-117;
			if(c=='ł') res[i]=(byte)-116;
			if(c=='ń') res[i]=(byte)-115;
			if(c=='ó') res[i]=(byte)-114;
			if(c=='ś') res[i]=(byte)-113;
			if(c=='ź') res[i]=(byte)-112;
			if(c=='ż') res[i]=(byte)-111;
			if(res[i]==0)
				if(ic>=0 && ic<=127) res[i]=(byte)ic;
				else res[i]=(byte)(' ');					
		}
		
		return res;	
	}
	
	/**
	 * 
	 * Creates String form bytes array.
	 *  
	 * @param b bytes Array
	 * 
	 * @return String composed from bytes array
	 * 
	 */
	public String  fromBytes(byte b[])
	{
		String res="";

		for(int i=0;i<b.length;i++)
		{
			if(b[i]==-128) res+='Ą';
			if(b[i]==-127) res+='Ć';
			if(b[i]==-126) res+='Ę';
			if(b[i]==-125) res+='Ł';
			if(b[i]==-124) res+='Ń';
			if(b[i]==-123) res+='Ó';
			if(b[i]==-122) res+='Ś';
			if(b[i]==-121) res+='Ź';
			if(b[i]==-120) res+='Ż';
			if(b[i]==-119) res+='ą';
			if(b[i]==-118) res+='ć';
			if(b[i]==-117) res+='ę';
			if(b[i]==-116) res+='ł';
			if(b[i]==-115) res+='ń';
			if(b[i]==-114) res+='ó';
			if(b[i]==-113) res+='ś';
			if(b[i]==-112) res+='ź';
			if(b[i]==-111) res+='ż';
			if(b[i]>=0 && b[i]<=127) res+=(char)b[i];
		}
		return res;	
	}
}
