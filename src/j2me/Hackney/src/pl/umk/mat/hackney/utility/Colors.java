package pl.umk.mat.hackney.utility;

/**
 * 
 * Contains definitions of basic colors.
 * 
 * @author HackneyTeam
 *
 */
public class Colors
{	
	public static final int white=0xFFFFFF;
	public static final int red=0xFF0000;
	public static final int black=0x000000;
	public static final int green=0x00FF00F;
	public static final int blue=0x0000FF;
}
