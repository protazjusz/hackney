package pl.umk.mat.hackney.graphics;

import java.io.IOException;
import java.util.Date;

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.networking.Client;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.Colors;


/**
 * 
 * Tasks off this class are to handle keys that are pressed, prepare them to communication, 
 * and illustrate it on the screen. It serves as a keyboard module.
 *  
 * @author HackneyTeam
 *
 */
public  class KeyboardCanvas extends javax.microedition.lcdui.Canvas implements Runnable
{
	public Client cli;
	private int code,offs;
	private long time;
	private boolean on,pushed,held;
	private Integer turn;
	private byte modificators;
	private Image splash;
	private int  signs[][]={
			{' ','0','=','[',']'},{'.',',','-',';','\'','/','1'},{'A','B','C','2'},
			{'D','E','F','3'},{'G','H','I','4'},
			{'J','K','L','5'},{'M','N','O','6'},{'P','Q','R','S','7'},
			{'T','U','V','8'},{'W','X','Y','Z','9'},{'\n'}
			
	}; 
	private int  signsBig[][]={
			{' ',')','+','{','}'},{'>','<','_',':','"','?','!'},{'A','B','C','@'},
			{'D','E','F','#'},{'G','H','I','$'},
			{'J','K','L','%'},{'M','N','O','^'},{'P','Q','R','S','&'},
			{'T','U','V','*'},{'W','X','Y','Z','('},{'\n'}
	};
	
	/* przechowuje informacje o stanie przytrzymania
	 * 0 - guzik nie jest przytrzymany 
	 * 1 - guizik jest przytrzymany temporialnie, czyli po wysłaniu przestanie być wciśnięty
	 * 2 - guzik jest przytrzymany permanętnie, czyli aż do wyłaczenia
	 */
	private int onC[]=new int[12];
	private int modC[]={2,0,3,5,7};
	
	private Image letters[][];
	private Image lettersBig[][];
    
	
	/**
	 * 
	 * Constructor, prepares letter images. 
	 * 
	 */
	public KeyboardCanvas()
	{
		try
		{
			splash=Image.createImage("/images/background.png");
		}
		catch (IOException e2)
		{
		}
		
		cli=null;
		on=pushed=held=false;
		turn=new Integer(0);		
		code=offs=0;			
		letters=new Image[signs.length][];
		lettersBig=new Image[signsBig.length][];
		
		for(int i=0;i<letters.length;i++)
		{
			letters[i]=new Image[signs[i].length];
			lettersBig[i]=new Image[signsBig[i].length];
			
			for(int j=0;j<letters[i].length;j++)
			{
				try
				{
					letters[i][j]=Image.createImage("/letters/"+(int)Character.toLowerCase((char) signs[i][j])+".jpeg");
				} 
				catch (IOException e)
				{
				}
				try
				{
					lettersBig[i][j]=Image.createImage("/letters/"+signsBig[i][j]+".jpeg");
				} 
				catch (IOException e)
				{
				}		
			}
		}

	}
	
	/**
	 * 
	 * Illustrates on the screen, the pressed and sent keys and modifications .
	 * 
	 */
	protected void paint(Graphics graph)
	{
		int tlo=0x000000;
		graph.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM));
		graph.setColor(tlo);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		graph.setColor(0, 0, 0);
		if(splash!=null)
			graph.drawImage(splash, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);
	
	

		int dx=graph.getClipWidth()/(3+1);
		int dy=graph.getClipHeight()/(4+1);
		int x=dx/2;
		int y=7*dy/8;
		int dx1=dx/20;
		int dy1=dy/20;
		int dlu=dx-2*dx1;
		int wys=dy-2*dy1;
		int i,j;
		
		/* NAPISY */
		String napisy[]={"CTRL","2","SHIFT","ALT","5","ALT_G","7","super","9"," "," "," "};
		for(int k=0;k<12;k++)
		{
			j=k/3;
			i=k%3;
			graph.setColor(Colors.black);
			graph.fillRect(x+dx*i+dx1 ,y+dy*j+dy1, dlu,wys);
			
			if(onC[k]==0) graph.setColor(Colors.white);
				else graph.setColor(Colors.red);
			if(onC[k]==2)  graph.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_UNDERLINED, Font.SIZE_MEDIUM));
			else graph.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM));
		
			graph.drawRect(x+dx*i+dx1 ,y+dy*j+dy1, dlu,wys);
			graph.drawString(napisy[k],x+dx*i+dx1+dlu/2-graph.getFont().stringWidth(napisy[k])/2 ,y+dy*j+dy1+wys/2-graph.getFont().getHeight()/2, 0);
		}
		
		/* SPACJA */		
		if(onC[10]==0) graph.setColor(Colors.white);
			else graph.setColor(Colors.red);
		
		dx1=dx/5;
		dlu=dx-2*dx1;
		j=10/3;
		i=10%3;
		
		int sx1=x+dx*i+dx1;
		int sx2=x+dx*i+dlu+dx1;
		int sy1=y+dy*j+4*dlu/5-1;
		int sy2=y+dy*j+dlu-1;
		
		graph.fillRect(sx1, sy2, dlu+3, 3);
		graph.fillRect(sx1, sy1, 3,sy2-sy1);	
		graph.fillRect(sx2, sy1, 3, sy2-sy1);
		
		/* BACKSPACE */
		if(onC[9]==0) graph.setColor(Colors.white);
			else graph.setColor(Colors.red);
		j=9/3;
		i=9%3;
		dx1=dx/5;
		dlu=dx-2*dx1;sx1=x+dx*i+dx1;
		sx2=x+dx*i+dx1+dlu;
		sy1=y+dy*j+dy1+wys/2;
		
		graph.fillRect(sx1, sy1-1, dlu, 3);
		graph.fillTriangle(sx1-3,sy1,sx1+dx1,sy1+wys/5,sx1+dx1,sy1-wys/5);
	
		/* ENTER */
		if(onC[11]==0) graph.setColor(Colors.white);
			else graph.setColor(Colors.red);
		j=11/3;
		i=11%3;
		dx1=dx/5;
		dlu=dx-2*dx1;
		sx1=x+dx*i+dx1;
		sx2=x+dx*i+dx1+dlu;
		sy1=y+dy*j+dy1+wys/2+wys/5;

		graph.fillRect(sx1, sy1-1, dlu, 3);
		graph.fillTriangle(sx1-3,sy1,sx1+dx1,sy1+wys/5,sx1+dx1,sy1-wys/5);
		graph.fillRect(sx2-3, sy1-wys/3, 3, wys/3);
		
		
		int stY=10;
		
		/* LITERY */
		if(code!='*' && code!=0)	
		{
			int guzior=code-'0';
			graph.setColor(Colors.white);
			int size=signs[guzior].length;
			Image img[] = new Image[5];
			
			for( i=-2;i<3;i++)
			{
				int cc=(offs+i+size)%size;
				if(modificators%2==0)img[i+2]=letters[guzior][cc];
					else img[i+2]=lettersBig[guzior][cc];
			}
			
			int xxx=32;
			
			if(img[0]!=null) img[0].getWidth();
			int xx=graph.getClipWidth()/2-xxx/2;
			
			for( i=-2;i<3;i++)
				if(img[i+2]!=null ) graph.drawImage( img[i+2], xx+i*(xxx+10), stY, 0);
				else 
				{
					int cc=(offs+i+size)%size;
					graph.setColor(Colors.white);
					graph.fillRect(xx+i*(xxx+10),stY, 32, 32);
					graph.setColor(Colors.black);
					if(modificators%2==0) graph.drawString(Character.toLowerCase((char)signs[guzior][cc])+"", xx+i*(xxx+10)+16-graph.getFont().stringWidth((char)signs[guzior][cc]+"")/2, 10+graph.getFont().getHeight()/2, 0);
					else graph.drawString((char)signsBig[guzior][cc]+"", xx+i*(xxx+10)+16-graph.getFont().stringWidth((char)signsBig[guzior][cc]+"")/2, 10+graph.getFont().getHeight()/2, 0);
					
				}
			graph.setColor(Colors.red);
			graph.drawRect(xx,stY, 32, 32);		
			graph.setColor(Colors.black);
		}
	}

	/**
	 * 
	 * Method called when a key is pressed, it sends or change the current key.
	 * 
	 */
	protected void keyPressed(int keyCode)
	{
		synchronized(turn)
		{
			/*czas*/
			Date zegar=new Date();
			long h=zegar.getTime();
			
			/*enter*/
			if(keyCode=='#') keyCode='9'+1;
			
			/*kursor*/
			if(keyCode>=-4 && keyCode <=-1)
			{
				byte arrows[]={38,40,37,-128};
				byte key=arrows[Math.abs(keyCode)-1];
				send(key);
			}
			else 
			{
				/*backspace*/
				if(keyCode==-8 || keyCode=='*')
				{
					send(8);
					wyzeruj();
					time=h;	
				}
				else
				{
					/*numpad*/
					if((keyCode>='0' && keyCode<='9'+1) || keyCode=='*') 
					{
						if(pushed==false)
						{
							zegar=new Date();
							h=zegar.getTime();
							time=h;
							pushed=true;
							code=keyCode;
							offs=0;
						}
						else
						{
							if(keyCode==code)
							{
								int guzior=code-'0';
								int size=signs[guzior].length;
								offs=(offs+1)%size;
								h=zegar.getTime();
								time=h;
							}
							else
							{
								int guzior=code-'0';
								code=signs[guzior][offs];
								send(code);
								code=keyCode;
								offs=0;
								zegar=new Date();
								h=zegar.getTime();
								time=h;
							}
						}
					}
					else
					/*QWERTY*/
					send(Character.toUpperCase((char)keyCode));
				}
			}
			repaint();
		}
	}
	
	/**
	 * 
	 * Method called when the button is released.
	 * 
	 */
	protected void keyReleased(int keyCode)
	{
		held=false;	
	}
	
	/**
	 * 
	 * Method called when a button is held, significant for modifications.
	 * 
	 */
	protected void keyRepeated(int keyCode)
	{
		Date zegar=new Date();
		long h=zegar.getTime();
	
		synchronized(turn)
		{	
			if(!held)
			{	
				held=true;
				for(int i=0;i<modC.length;i++) 
					if(keyCode==('1'+modC[i]))
					{
						if(onC[modC[i]]!=1) modificators^=(1<<i); 
						onC[modC[i]]=(onC[modC[i]]+1)%3;
					}
				repaint();
				time=h;
				wyzeruj();
			}
		}
	}

	/**
	 * 
	 * Wraps the key, prepares it to communication, and changes the modifications accordingly.
	 * 
	 */
	public void   send(int k)
	{
		if(cli!=null)
		{
			if(k==39) k=-39;
			if(k==-128) k=39;
			
			byte S[]={(byte) k,modificators};
			MessageData pakiet= new MessageData(MessageData.KEYBOARD_PRESSED,S);
			try
			{
				cli.send(pakiet);
			}
			catch (IOException e)
			{
			
				e.printStackTrace();
			}
			modificators=0;
			for(int i=0;i<modC.length;i++) 
				if(onC[modC[i]]==2) modificators^=(1<<i); 
					else onC[modC[i]]=0;
		}	
	}
	
	/**
	 * 
	 * When they is a key ready to send, and the specified time pasts it send that key.
	 *  
	 */
	public void run()
	{
		while(on)
		{
			{
				synchronized(turn)
				{
					Date zegar=new Date();
					long h=zegar.getTime();
					
					if(  (!held && pushed== true && (h-time)>1000)  )
					{
						int guzior=code-'0';
						code=signs[guzior][offs];
						send(code);
						wyzeruj();
						time=h;
						repaint();
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * Resets variables bounded with holding a key.
	 *  
	 */
	private void wyzeruj()
	{
		pushed=false;
		code=offs=0;
	}
	
	/** 
	 * 
	 * Changes the state of the keyboard Thread;
	 * 
	 * @param b state to be set.
	 * 
	 */
	public void changeState(boolean b)
	{
		on=b;
	}

}
