package pl.umk.mat.hackney.networking;

import java.io.IOException;

//import java.util.Date;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

import pl.umk.mat.hackney.utility.TimeoutException;


/**
 * 
 * Class provides the interface for communication with the server.
 * Creates a connection and opens streams used to send and receive data packed using GALOP .
 *  
 * @author HackneyTeam
 *
 */
public class Client 
{
	private byte sessionID=0;
	
	private StreamConnection connection;
	private MessageInputStream in;
	private MessageOutputStream out;

	/**
	 * 
	 * Opens the connection and opens streams for comunnication
	 * 
	 * @param s servers address
	 * 
	 * @throws IOException 
	 * 
	 */
	public Client(String s) throws IOException
	{
		connection = (StreamConnection) Connector.open(s);
		out = new MessageOutputStream(connection.openDataOutputStream());
		in = new MessageInputStream(connection.openDataInputStream());
	}
	
	/**
	 * 
	 * Sends a greeting message to the server. If the servers response is positive an id session will be granted.   
	 *
	 * @throws IOException
	 * 
	 * @throws TimeoutException
	 * 
	 */
	public void handshake() throws IOException, TimeoutException 
	{
		byte pl[]=new byte[2];
		pl[1]=pl[0]=0;
		
		MessageData pakiet =new MessageData(MessageData.CONNECTION_HELLO,MessageData.DEVICE_J2ME,pl);
		send(pakiet);
		
		pakiet =new MessageData((byte) 0,pl);
		byte msg[]=new byte[4];
		receive(msg);
		pakiet.fromBytes(msg);
		
		if(pakiet.getMessageType()==MessageData.CONNECTION_HELLO)
		{
			sessionID=pakiet.getSessionID();
		}
	}
	
	/**
	 * 
	 * Sends data to the server.
	 *  
	 * @param pakiet data
	 * 
	 * @throws IOException
	 *  
	 */
	public void send(MessageData pakiet) throws IOException
	{
		if(pakiet.getMessageType()!=MessageData.CONNECTION_HELLO || pakiet.getMessageType()!=MessageData.CONNECTION_GOODBYE  ) pakiet.setSessionID(sessionID);
		out.write(pakiet);
		out.flush();
	}
	
	/**
	 * 
	 * Receives data from server.
	 *  
	 * @param r	array for the response
	 *  
	 * @return size of the given message
	 *  
	 * @throws IOException
	 * 
	 */
	public int  receive(byte r[]) throws IOException
	{
	/*	Date zegar=new Date();
		long h=zegar.getTime();
		long z=0;
	
		if(in.available()!=-1)
		{
			while(z<10000 && in.available()==0)
			{
				zegar=new Date();
				z=zegar.getTime()-h;
			}
			if(z>=10000)
			{
				throw new TimeoutException();
			}
		}
		*/
		return in.read(r);
	}
	
	/**
	 * 
	 * Closes the streams and the connection to the server.
	 *
	 */
	public void close()
	{
		try
		{
			in.close();
			out.close();
			connection.close();
			
		}
		catch (IOException e)
		{
		}
	}
	
	/**
	 * 
	 * Answers the question whether the handshake was positive.
	 * 
	 * @return handshake status
	 * 
	 */
	public boolean wasHandshaken()
	{
		return this.sessionID!=0;
	}
}
	