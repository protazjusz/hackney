package pl.umk.mat.hackney.search;

import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;
import javax.microedition.rms.RecordStoreNotOpenException;

/**
 * 
 * Role of this class is to manage a RecordStore.
 * It provides an interface to add new entries , delete them, checking if an Device is known
 * and listing known Devices.
 *  
 * @author HackneyTeam
 *
 */
public class DevicesDatabase
{
	private RecordStore database;
	private Device knownDevices[];
	private int size;
	
	/**
	 * 
	 * Constructor, updates the database.
	 * 
	 */
	public DevicesDatabase()
	{
		update();
	}
	
	/**
	 * 
	 * Gives size of knownDevice list. 
	 * 
	 * @return number of known Devices.
	 * 
	 */
	public  int getSize()
	{
		return size;
	}
	
	/**
	 * 
	 * Remove all known Devices from database.
	 * 
	 * @throws RecordStoreNotFoundException
	 * @throws RecordStoreException
	 * 
	 */
	public void remove() throws RecordStoreNotFoundException, RecordStoreException
	{
		RecordStore.deleteRecordStore("hackney");
	}
	
	/**
	 * 
	 * Finds out if the specified device is known, that means is it in the database
	 * 
	 * @param s address of the device to be checked
	 * 
	 * @return device status
	 * 
	 */
	public boolean isKnown(String s)
	{
	
		
		boolean wynik=false;
		for(int i=0;i<knownDevices.length;i++) if(knownDevices[i].getAddress().equals(s)) wynik=true;
		return wynik;
	}
	
	/**
	 * 
	 * Adds specified  device to database.
	 * 
	 * @param name	  name of the Device to be added
	 * @param address address of the Device to be added 
	 * 
	 * @throws RecordStoreFullException
	 * 
	 */
	public void addDevice(String name,String address) throws RecordStoreFullException
	{
		
		try
		{
			database=RecordStore.openRecordStore("hackney",true);
			
			byte sB[]=name.getBytes();
			byte sA[]=address.getBytes();
			database.addRecord(sA, 0,sA.length );
			database.addRecord(sB, 0, sB.length);
			database.closeRecordStore();
		}
		catch (RecordStoreNotFoundException e)
		{
		}
		catch (RecordStoreException e)
		{
			e.printStackTrace();
		}
		
		update();
	}
	
	/**
	 * 
	 * updates the database
	 * 
	 */
	private void update()
	{
		try
		{
			database=RecordStore.openRecordStore("hackney",true);
			size=database.getNumRecords()/2;
			knownDevices=new Device[size] ;
			
			if(database.getNumRecords()%2!=0) 
			{
				database.closeRecordStore();
				RecordStore.deleteRecordStore("hackney");
			}
			else
			{
				for(int i=1;i<=2*size;i+=2)	
				{
					byte o[]=new byte[database.getRecordSize(i)];
					byte o2[]=new byte[database.getRecordSize(i+1)];
					
					database.getRecord(i,o,0);
					database.getRecord(i+1,o2,0);
					
					knownDevices[i/2]=new Device(new String (o2),new String (o) );
				}	
				database.closeRecordStore();
			}
		}
		catch (RecordStoreNotOpenException e)
		{
		}
		catch (RecordStoreFullException e)
		{
		}
		catch (RecordStoreException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Lists known devices names.
	 * 
	 * @return array of known Devices names
	 */
	public Device[] getDevices()
	{
		return knownDevices;
	}
	
	/**
	 * 
	 * Lists known devices.
	 * 
	 * @return array of known Devices
	 */
	public String[] getNames()
	{
		String res[]=new String[size];
		for(int i=0;i<size;i++)res[i]=knownDevices[i].getName();
		return res;
	}
	
}
