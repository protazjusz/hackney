package pl.umk.mat.hackney.menu;

import javax.bluetooth.DiscoveryAgent;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;


import pl.umk.mat.hackney.graphics.ListCanvas;
import pl.umk.mat.hackney.graphics.Transition;
import pl.umk.mat.hackney.search.DevicesDatabase;
import pl.umk.mat.hackney.search.PonyFinder;
import pl.umk.mat.hackney.utility.Dictionary;


/**
 * 
 * Task of this class is to implements an option from the main menu. The device menu option.
 * It provides an interface for selecting and searching for a device. 
 * 
 * @author HackneyTeam
 *
 */
public class DeviceMenu implements CommandListener
{
	public final static int KNOWN_DEVICES=0;
	public final static int NEW_DEVICES=1;

	private ListCanvas 
		devicesList;		// Menu urządzeń 
	
	ListCanvas menu;					// Menu główne
	
	private int device;
	private int type;
	
	public PonyFinder finder;
	private DiscoveryAgent discoveryAgent;
	private Display display;
	
			
	/**
	 * 
	 * @param agent discoveryAgent used for searching for devices 
	 * @param disp display used to show graphical contents
	 * @param mMenu link to main Menu
	 */
	public DeviceMenu(DiscoveryAgent agent, Display disp, ListCanvas mMenu)
	{
		menu=mMenu;
		display=disp;
		discoveryAgent=agent;
		finder=new PonyFinder(discoveryAgent);
	}
	
	/**
	 * 
	 * When called gives the control to NewPonnyFinder which searches for nearby devices.  	 
	 * 
	 */
	public void search()
	{
		
		finder.setMenu(this);
		Thread t= new Thread (finder);
		t.start();
	}
	
	/**
	 * 
	 * Gets list of known devices from database and creates a
	 * list  so one of them could be chosen, and an option to find new devices.
	 * 
	 */
	public void choose()
	{
		DevicesDatabase db=new DevicesDatabase();
		String temp[]=db.getNames();
		int deviceCount;
		
		finder.setMenu(this);
		finder.setDevices(db.getDevices());
		
		String urzadzenia[]=null;
		if(temp==null || temp.length==0) deviceCount=0;
			else deviceCount=temp.length;
		
		urzadzenia=new String[deviceCount+1];
		if(temp!=null && temp.length!=0) System.arraycopy(temp, 0, urzadzenia, 0, temp.length);
		
		urzadzenia[urzadzenia.length-1]=Dictionary.eng("Wyszukaj");
		devicesList=new ListCanvas(Dictionary.eng("Wybierz urządzenie")+':',urzadzenia,null);
		devicesList.setCommandListener(this);
	
		display.setCurrent(devicesList);
		type=DeviceMenu.KNOWN_DEVICES;
	}
	
	/**
	 * 
	 * Implements methods for appearing Commands
	 * 
	 */
	public  void commandAction(Command c, Displayable d)
	{
		if(c==devicesList.selectCommand)
		{
			Transition 	transition=new Transition("/images/hackney2.gif");
			device=devicesList.getSelectedIndex();
			
			if (type==0 && device==devicesList.size()-1)
			{
				display.setCurrent(transition);
				search();
			}
			else 
			{
				finder.setCurrentDevice(device);
				Thread t=new Thread(finder);
				t.start();	
				display.setCurrent(transition);
			}
		}
		
		if(c==devicesList.backCommand)
		{
			display.setCurrent(menu);
		}
	}
	
	/**
	 * 
	 * Method creates list of found devices so one could be chosen or an information that no devices were found.
	 * 
	 */
	public void chooseFromNew()
	{
		display.vibrate(1000);
		String urzadzenia[]=finder.getDevicesNames();
		
		if((urzadzenia==null || urzadzenia.length==0) )
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.NO_DEVICES),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a, menu);
		}
		else
		{
			devicesList=new ListCanvas(Dictionary.eng("Wybierz urządzenie")+':',urzadzenia,null);
			devicesList.setCommandListener(this);
			display.setCurrent(devicesList);
		}		
		type=DeviceMenu.NEW_DEVICES;
	}
	
	/**
	 * 
	 * Exits to Main menu.
	 * 
	 */
	public void returnToMainMenu()
	{
		display.setCurrent(menu);
	}
	
	/**
	 * 
	 * Gives control to the PonnyMenu
	 * 
	 */
	public void choosingPony()
	{
		PonyMenu ponnyMenu=new PonyMenu(display,this,finder.getPonies(),finder.getCurrent());
		ponnyMenu.choosingPonny();
	}
	
	/**
	 * 
	 * Exits to Device Menu.
	 * 
	 */
	public void returnToMenuSelection()
	{
		display.setCurrent(devicesList);
	}
	
	
	
}
