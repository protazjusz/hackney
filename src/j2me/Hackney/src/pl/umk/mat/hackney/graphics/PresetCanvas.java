
package pl.umk.mat.hackney.graphics;

import java.io.IOException;

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import pl.umk.mat.hackney.networking.Client;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.Colors;
import pl.umk.mat.hackney.utility.Dictionary;


/**
 * 
 * Tasks off this class are to handle keys that are pressed and  prepare them to communication
 * It also shows on the screen descriptions for the specific shortcuts and provides an interface to scroll the screen 
 * if it is to small to fit all.
 * It serves as a generic shortcut module.
 * 
 * @author HackneyTeam
 *
 */
public  class PresetCanvas extends javax.microedition.lcdui.Canvas 
{
	private String title;
	private String names[];
	private Image background;
	
	private int size=0;
	private byte number;

	private int backgroundColor;
	private int textColor;
	
	public Client c;
	
	private int sy;

	/**
	 * 
	 * Prepare texts for the shortcuts 
	 *  
	 * @param t Title of the preset 
	 * @param s descriptions of specific shortcuts
	 * @param kod The identification number for this Canvas
	 * 
	 */
	public PresetCanvas(String t,String s[],byte kod)
	{
		sy=0;
		title=t;
		c=null;
		number=kod;
		backgroundColor=Colors.black;
		textColor=Colors.white;
		names=new String[s.length+1];
		names[0]=Dictionary.eng("Uruchom aplikację");
		System.arraycopy(s,0, names, 1, s.length);
		size=names.length;
		
		try
		{
			 background = Image.createImage("/images/background.png");
		}
		catch (IOException e2)
		{
		}
		
		repaint();	
	}
	
	/**
	 * 
	 * Prints the shortcut descriptions on the screen.
	 *  
	 */
	protected void paint(Graphics graph)
	{
		int wysokoscWiersza=graph.getFont().getHeight()+0;
		int x=8;
		
		
		graph.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM));
		graph.setColor(backgroundColor);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		if(background!=null)
			graph.drawImage(background, graph.getClipWidth()/2, graph.getClipHeight()/2, Graphics.VCENTER | Graphics.HCENTER);
		
		
		graph.setColor(textColor);
		
		graph.drawString(title, graph.getClipWidth()/2-graph.getFont().stringWidth(title)/2, 4, 0);
		for(int i=0;i<size-sy;i++)
		{
			graph.drawString((sy+i)+": "+names[sy+i], x, (i+2)*wysokoscWiersza, 0);
		}
	}
	
	/**
	 * 
	 * Method called when a key is pressed, it sends that key, or scrolls the screen if the direction key  is used.
	 * 
	 */
	protected void keyPressed(int keyCode)
	{
		if(c!=null)
		{
			if( (keyCode==-1 || keyCode=='w') && sy>0)
			{
				sy--;
			}
			if( (keyCode==-2 || keyCode=='s') && sy<names.length-1)
			{
				sy++;
			}
			repaint();	
			
			if(keyCode-'0'>=0 && keyCode-'0'<=9 )
			{
				
				byte S[]={(byte) (keyCode-'0'),0};
				MessageData pakiet= new MessageData(MessageData.HOTKEY_USED,S);
				
				try
				{
					c.send(pakiet);
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}		
	}
	
	/**
	 * 
	 * Method called when a key is held, it sends that key, or scrolls the screen if the direction key  is used.
	 * 
	 */
	protected void keyRepeated(int keyCode)
	{
		if( (keyCode==-1 || keyCode=='w') && sy>0)
		{
			sy--;
		}
		if( (keyCode==-2 || keyCode=='s') && sy<names.length-1)
		{
			sy++;
		}
		repaint();	
	}
	
	/**
	 * 
	 * Method is used to get the identification number of this PresetCanvas.
	 * 
	 * @return canvas id
	 * 
	 */
	public byte  getNumber()
	{
		return number;
	}
}
