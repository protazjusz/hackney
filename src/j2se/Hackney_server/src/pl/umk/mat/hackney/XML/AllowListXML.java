package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Connection.ClientInfo;
import pl.umk.mat.hackney.Connection.ClientInfoList;


/**
 * Reads and writes allow list entries from XML files
 * @author HackneyTeam
 *
 */
public class AllowListXML extends XMLConfiguration {

	
	/**
	 * Constructor
	 */
	public AllowListXML() {
		this.fileName="allowlist.xml";
		fullPath = this.path + "config/" + this.fileName /*"allowlist.xml"*/;
		file = new File(fullPath);
	}
	
	/**
	 * Constructor
	 * @param def file name to save xml
	 */
	public AllowListXML(String def) {
		this.fileName=def;
		fullPath = /*this.path*/"defaults/" + this.fileName /*"allowlist.xml"*/;
		file = new File(fullPath);
	}
	
	/**
	 * Reads allowed list form XML file
	 * @return list of allowed devices
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public ClientInfoList getAllowedList() throws SAXException, IOException, ParserConfigurationException {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(this.file);	// Load XML file
		
		ClientInfoList ccInfoList = new ClientInfoList(); 
		Element root = doc.getDocumentElement();	//Get root element
		
		NodeList nodes = root.getElementsByTagName("item");  //Read list of items from XML
		
		
		for(int i=0;i<nodes.getLength();i++) {	//Iterate by all items
			Node node = nodes.item(i);
			
			ClientInfo ccInfo = new ClientInfo();
			Element el = (Element)node;
			Element nameElement = (Element)el.getElementsByTagName("name").item(0);
			Element idElement = (Element)el.getElementsByTagName("id").item(0);
			
			ccInfo.setName(nameElement.getTextContent());
			ccInfo.setMac(idElement.getTextContent());
			
			ccInfoList.add(ccInfo);
		}

		return ccInfoList;
	}
	
	/**
	 * Saves allowed list items to XML file
	 * @param allowList list of allowed devices
	 * @throws ParserConfigurationException
	 * @throws FileNotFoundException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public void saveAllowList(ClientInfoList allowList) throws ParserConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.newDocument();
		
		Element rootElement = doc.createElement("main");
		Element itemElement = null;
		
		for (int j=0;j<allowList.size();j++) {	//Iterate by shortcuts of application
			ClientInfo ccInfo = allowList.item(j);
			itemElement = doc.createElement("item");
			
			Element nameEl = doc.createElement("name");
			nameEl.appendChild(doc.createTextNode(ccInfo.getName()));
			Element idEl = doc.createElement("id");
			idEl.appendChild(doc.createTextNode(ccInfo.getMac()));
			
			
			itemElement.appendChild(nameEl);
			itemElement.appendChild(idEl);
			
			rootElement.appendChild(itemElement);
		}
		
		doc.appendChild(rootElement);
		saveFile(doc);
	}
	
	
	
}
