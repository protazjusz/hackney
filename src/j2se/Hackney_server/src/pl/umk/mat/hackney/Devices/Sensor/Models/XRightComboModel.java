
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors X Axis right jComboBox model class
 * @author HackneyTeam
 *
 */
public class XRightComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8654240088715480489L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public XRightComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setxRight(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getxRight());
	}
}
