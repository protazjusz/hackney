package pl.umk.mat.hackney.Utility;

/**
 * Contains translation version for every element of GUI
 * @author HackneyTeam
 *
 */
public class Language {
	
	/**
	 * @return  the mainTab
	 */
	public String getMainTab() {
		return mainTab;
	}
	/**
	 * @param mainTab  the mainTab to set
	 */
	public void setMainTab(String mainTab) {
		this.mainTab = mainTab;
	}
	/**
	 * @return  the shortcutsTab
	 */
	public String getShortcutsTab() {
		return shortcutsTab;
	}
	/**
	 * @param shortcutsTab  the shortcutsTab to set
	 */
	public void setShortcutsTab(String shortcutsTab) {
		this.shortcutsTab = shortcutsTab;
	}
	/**
	 * @return  the connectionsTab
	 */
	public String getConnectionsTab() {
		return connectionsTab;
	}
	/**
	 * @param connectionsTab  the connectionsTab to set
	 */
	public void setConnectionsTab(String connectionsTab) {
		this.connectionsTab = connectionsTab;
	}
	/**
	 * @return  the sensorsTab
	 */
	public String getSensorsTab() {
		return sensorsTab;
	}
	/**
	 * @param sensorsTab  the sensorsTab to set
	 */
	public void setSensorsTab(String sensorsTab) {
		this.sensorsTab = sensorsTab;
	}
	/**
	 * @return  the allowedTab
	 */
	public String getAllowedTab() {
		return allowedTab;
	}
	/**
	 * @param allowedTab  the allowedTab to set
	 */
	public void setAllowedTab(String allowedTab) {
		this.allowedTab = allowedTab;
	}
	/**
	 * @return  the onlyAllowed
	 */
	public String getOnlyAllowed() {
		return onlyAllowed;
	}
	/**
	 * @param onlyAllowed  the onlyAllowed to set
	 */
	public void setOnlyAllowed(String onlyAllowed) {
		this.onlyAllowed = onlyAllowed;
	}
	/**
	 * @return  the columnModif1
	 */
	public String getColumnModif1() {
		return columnModif1;
	}
	/**
	 * @param columnModif1  the columnModif1 to set
	 */
	public void setColumnModif1(String columnModif1) {
		this.columnModif1 = columnModif1;
	}
	/**
	 * @return  the columnModif2
	 */
	public String getColumnModif2() {
		return columnModif2;
	}
	/**
	 * @param columnModif2  the columnModif2 to set
	 */
	public void setColumnModif2(String columnModif2) {
		this.columnModif2 = columnModif2;
	}
	/**
	 * @return  the columnModif3
	 */
	public String getColumnModif3() {
		return columnModif3;
	}
	/**
	 * @param columnModif3  the columnModif3 to set
	 */
	public void setColumnModif3(String columnModif3) {
		this.columnModif3 = columnModif3;
	}
	/**
	 * @return  the columnKey
	 */
	public String getColumnKey() {
		return columnKey;
	}
	/**
	 * @param columnKey  the columnKey to set
	 */
	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}
	/**
	 * @return  the columnDescription
	 */
	public String getColumnDescription() {
		return columnDescription;
	}
	/**
	 * @param columnDescription  the columnDescription to set
	 */
	public void setColumnDescription(String columnDescription) {
		this.columnDescription = columnDescription;
	}
	/**
	 * @return  the columnRemove
	 */
	public String getColumnRemove() {
		return columnRemove;
	}
	/**
	 * @param columnRemove  the columnRemove to set
	 */
	public void setColumnRemove(String columnRemove) {
		this.columnRemove = columnRemove;
	}
	/**
	 * @return  the nameColumn
	 */
	public String getNameColumn() {
		return nameColumn;
	}
	/**
	 * @param nameColumn  the nameColumn to set
	 */
	public void setNameColumn(String nameColumn) {
		this.nameColumn = nameColumn;
	}
	/**
	 * @return  the player
	 */
	public String getPlayer() {
		return player;
	}
	/**
	 * @param player  the player to set
	 */
	public void setPlayer(String player) {
		this.player = player;
	}
	/**
	 * @return  the player1
	 */
	public String getPlayer1() {
		return player1;
	}
	/**
	 * @param player1  the player1 to set
	 */
	public void setPlayer1(String player1) {
		this.player1 = player1;
	}
	/**
	 * @return  the player2
	 */
	public String getPlayer2() {
		return player2;
	}
	/**
	 * @param player2  the player2 to set
	 */
	public void setPlayer2(String player2) {
		this.player2 = player2;
	}
	/**
	 * @return  the player3
	 */
	public String getPlayer3() {
		return player3;
	}
	/**
	 * @param player3  the player3 to set
	 */
	public void setPlayer3(String player3) {
		this.player3 = player3;
	}
	/**
	 * @return  the player4
	 */
	public String getPlayer4() {
		return player4;
	}
	/**
	 * @param player4  the player4 to set
	 */
	public void setPlayer4(String player4) {
		this.player4 = player4;
	}
	/**
	 * @return  the left
	 */
	public String getLeft() {
		return left;
	}
	/**
	 * @param left  the left to set
	 */
	public void setLeft(String left) {
		this.left = left;
	}
	/**
	 * @return  the right
	 */
	public String getRight() {
		return right;
	}
	/**
	 * @param right  the right to set
	 */
	public void setRight(String right) {
		this.right = right;
	}
	/**
	 * @return  the forward
	 */
	public String getForward() {
		return forward;
	}
	/**
	 * @param forward  the forward to set
	 */
	public void setForward(String forward) {
		this.forward = forward;
	}
	/**
	 * @return  the back
	 */
	public String getBack() {
		return back;
	}
	/**
	 * @param back  the back to set
	 */
	public void setBack(String back) {
		this.back = back;
	}
	/**
	 * @return  the add
	 */
	public String getAdd() {
		return add;
	}
	/**
	 * @param add  the add to set
	 */
	public void setAdd(String add) {
		this.add = add;
	}
	/**
	 * @return  the remove
	 */
	public String getRemove() {
		return remove;
	}
	/**
	 * @param remove  the remove to set
	 */
	public void setRemove(String remove) {
		this.remove = remove;
	}
	/**
	 * @return  the saveAll
	 */
	public String getSaveAll() {
		return saveAll;
	}
	/**
	 * @param saveAll  the saveAll to set
	 */
	public void setSaveAll(String saveAll) {
		this.saveAll = saveAll;
	}
	/**
	 * @return  the save
	 */
	public String getSave() {
		return save;
	}
	/**
	 * @param save  the save to set
	 */
	public void setSave(String save) {
		this.save = save;
	}
	/**
	 * @return  the exit
	 */
	public String getExit() {
		return exit;
	}
	/**
	 * @param exit  the exit to set
	 */
	public void setExit(String exit) {
		this.exit = exit;
	}
	/**
	 * @return  the file
	 */
	public String getFile() {
		return file;
	}
	/**
	 * @param file  the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}
	/**
	 * @return  the sensorsSettings
	 */
	public String getSensorsSettings() {
		return sensorsSettings;
	}
	/**
	 * @param sensorsSettings  the sensorsSettings to set
	 */
	public void setSensorsSettings(String sensorsSettings) {
		this.sensorsSettings = sensorsSettings;
	}
	/**
	 * @return  the shortcutsSettings
	 */
	public String getShortcutsSettings() {
		return shortcutsSettings;
	}
	/**
	 * @param shortcutsSettings  the shortcutsSettings to set
	 */
	public void setShortcutsSettings(String shortcutsSettings) {
		this.shortcutsSettings = shortcutsSettings;
	}
	/**
	 * @return  the devicesAllowList
	 */
	public String getDevicesAllowList() {
		return devicesAllowList;
	}
	/**
	 * @param devicesAllowList  the devicesAllowList to set
	 */
	public void setDevicesAllowList(String devicesAllowList) {
		this.devicesAllowList = devicesAllowList;
	}
	/**
	 * @return  the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language  the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return  the yes
	 */
	public String getYes() {
		return yes;
	}
	/**
	 * @param yes  the yes to set
	 */
	public void setYes(String yes) {
		this.yes = yes;
	}
	/**
	 * @return  the no
	 */
	public String getNo() {
		return no;
	}
	/**
	 * @param no  the no to set
	 */
	public void setNo(String no) {
		this.no = no;
	}
	/**
	 * @return  the reallyRemove
	 */
	public String getReallyRemove() {
		return reallyRemove;
	}
	/**
	 * @param reallyRemove  the reallyRemove to set
	 */
	public void setReallyRemove(String reallyRemove) {
		this.reallyRemove = reallyRemove;
	}
	/**
	 * @return  the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question  the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return  the newApplication
	 */
	public String getNewApplication() {
		return newApplication;
	}
	/**
	 * @param newApplication  the newApplication to set
	 */
	public void setNewApplication(String newApplication) {
		this.newApplication = newApplication;
	}
	/**
	 * @return  the giveApplicationName
	 */
	public String getGiveApplicationName() {
		return giveApplicationName;
	}
	/**
	 * @param giveApplicationName  the giveApplicationName to set
	 */
	public void setGiveApplicationName(String giveApplicationName) {
		this.giveApplicationName = giveApplicationName;
	}
	/**
	 * @return  the keys
	 */
	public String getKeys() {
		return keys;
	}
	/**
	 * @param keys  the keys to set
	 */
	public void setKeys(String keys) {
		this.keys = keys;
	}
	/**
	 * @return  the options
	 */
	public String getOptions() {
		return options;
	}
	/**
	 * @param options  the options to set
	 */
	public void setOptions(String options) {
		this.options = options;
	}
	/**
	 * @return  the clearChanges
	 */
	public String getClearChanges() {
		return clearChanges;
	}
	/**
	 * @param clearChanges  the clearChanges to set
	 */
	public void setClearChanges(String clearChanges) {
		this.clearChanges = clearChanges;
	}
	/**
	 * @return  the infoAbout
	 */
	public String getInfoAbout() {
		return infoAbout;
	}
	/**
	 * @param infoAbout  the infoAbout to set
	 */
	public void setInfoAbout(String infoAbout) {
		this.infoAbout = infoAbout;
	}
	/**
	 * @return  the restoreDefaults
	 */
	public String getRestoreDefaults() {
		return restoreDefaults;
	}
	/**
	 * @param restoreDefaults  the restoreDefaults to set
	 */
	public void setRestoreDefaults(String restoreDefaults) {
		this.restoreDefaults = restoreDefaults;
	}
	/**
	 * @return  the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path  the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return  the clearChangesQuestion
	 */
	public String getClearChangesQuestion() {
		return clearChangesQuestion;
	}
	/**
	 * @param clearChangesQuestion  the clearChangesQuestion to set
	 */
	public void setClearChangesQuestion(String clearChangesQuestion) {
		this.clearChangesQuestion = clearChangesQuestion;
	}
	/**
	 * @return  the defaultSettingsQuestion
	 */
	public String getDefaultSettingsQuestion() {
		return defaultSettingsQuestion;
	}
	/**
	 * @param defaultSettingsQuestion  the defaultSettingsQuestion to set
	 */
	public void setDefaultSettingsQuestion(String defaultSettingsQuestion) {
		this.defaultSettingsQuestion = defaultSettingsQuestion;
	}
	/**
	 * @return  the restartApplicationRequest
	 */
	public String getRestartApplicationRequest() {
		return restartApplicationRequest;
	}
	/**
	 * @param restartApplicationRequest  the restartApplicationRequest to set
	 */
	public void setRestartApplicationRequest(String restartApplicationRequest) {
		this.restartApplicationRequest = restartApplicationRequest;
	}
	/**
	 * @return  the information
	 */
	public String getInformation() {
		return information;
	}
	/**
	 * @param information  the information to set
	 */
	public void setInformation(String information) {
		this.information = information;
	}
	/**
	 * @return  the changeLanguage
	 */
	public String getChangeLanguage() {
		return changeLanguage;
	}
	/**
	 * @param changeLanguage  the changeLanguage to set
	 */
	public void setChangeLanguage(String changeLanguage) {
		this.changeLanguage = changeLanguage;
	}
	/**
	 * @return  the addToWhiteList
	 */
	public String getAddToWhiteList() {
		return addToWhiteList;
	}
	/**
	 * @param addToWhiteList  the addToWhiteList to set
	 */
	public void setAddToWhiteList(String addToWhiteList) {
		this.addToWhiteList = addToWhiteList;
	}
	/**
	 * @return  the button
	 */
	public String getButton() {
		return button;
	}
	/**
	 * @param button  the button to set
	 */
	public void setButton(String button) {
		this.button = button;
	}
	/**
	 * @return the saved
	 */
	public String getSaved() {
		return saved;
	}
	/**
	 * @param saved the saved to set
	 */
	public void setSaved(String saved) {
		this.saved = saved;
	}
	/**
	 * @return the addedToAllowList
	 */
	public String getAddedToAllowList() {
		return addedToAllowList;
	}
	/**
	 * @param addedToAllowList the addedToAllowList to set
	 */
	public void setAddedToAllowList(String addedToAllowList) {
		this.addedToAllowList = addedToAllowList;
	}
	private String mainTab = "Main";
	private String shortcutsTab = "Shortcuts";
	private String connectionsTab = "Connections";
	private String sensorsTab = "Sensors";
	private String allowedTab = "Allowed" ;	
	private String onlyAllowed = "Only for devices on allowed list";
	private String columnModif1 = "Modifier";
	private String columnModif2 = "Modifier";
	private String columnModif3 = "Modifier";
	private String columnKey = "Key";
	private String columnDescription = "Description";
	private String columnRemove = "Remove";
	private String nameColumn = "Name";
	private String player = "Player";
	private String player1 = "Player 1";
	private String player2 = "Player 2";
	private String player3 = "Player 3";
	private String player4 = "Player 4";
	private String left = "Left";
	private String right = "Right";
	private String forward = "Forward";
	private String back = "Back";
	private String add = "Add";
	private String remove = "Remove";
	private String saveAll = "Save All";
	private String save = "Save";
	private String exit = "Exit";
	private String file = "File";
	private String sensorsSettings = "Sensor settings";
	private String shortcutsSettings = "Shortcut settings";
	private String devicesAllowList = "Devices allow list";
	private String language = "Language";
	private String yes = "yes";
	private String no = "no";
	private String reallyRemove = "Do you really want to remove?";
	private String question = "Question";
	private String newApplication = "New application";
	private String giveApplicationName = "Insert application name:";
	private String keys = "Keys";
	private String options = "Options";
	private String clearChanges = "Clear all unsaved changes";
	private String infoAbout = "About program";
	private String restoreDefaults = "Restore defaults settings";
	private String path = "Path";
	private String clearChangesQuestion = "All unsaved settings will be cleared!\nDo You really want it?";
	private String defaultSettingsQuestion = "Defaults settings will be restored! \nDo You really want it?";
	private String restartApplicationRequest = "You have to restart application to active settings. " ;
	private String information = "Information";
	private String changeLanguage = "Change language";
	private String addToWhiteList = "To allowed";
	private String button = "Button";
	private String saved = "Saved!";
	private String addedToAllowList = "Added to allow list!";

}
