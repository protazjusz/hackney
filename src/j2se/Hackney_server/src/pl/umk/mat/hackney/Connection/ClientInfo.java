package pl.umk.mat.hackney.Connection;

/**
 * Contains informations about client device
 * @author HackneyTeam
 *
 */
public class ClientInfo {

	/**
	 * Constructor
	 */
	public ClientInfo() {
	}
	
	
	/**
	 * Constructor
	 * @param name	device friendly name
	 * @param mac	device mac address
	 */
	public ClientInfo(String name, String mac) {
		this.name = name;
		this.mac = mac;
	}

	
	/**
	 * Returns friendly name of device
	 * @return  the friendly name
	 */
	public String getName() {
		return name;
	}


	/**
	 * Sets friendly name
	 * @param name  friendly name
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * Returns MAC address of device
	 * @return   MAC address
	 */
	public String getMac() {
		return mac;
	}


	/**
	 * Sets MAC address of device
	 * @param MAC  address
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	
	private String name;
	private String mac;
}
