
package pl.umk.mat.hackney.Models;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;


/**
 * Choosen application comboBox model
 * @author HackneyTeam
 *
 */
public class ChooseAppComboBoxModel extends AbstractListModel implements ComboBoxModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2081308982949293128L;
	
	/**
	 * Constructor
	 * @param list applications list
	 */
	public ChooseAppComboBoxModel(ApplicationsList list){
		appList = list;
		selection = appList.getCurrentApplication().getName();
		current = appList.getCurrent();
	}
	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize() {
		return appList.getSize();
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	public Object getElementAt(int index) {
		return appList.item(index).getName();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.ComboBoxModel#setSelectedItem(java.lang.Object)
	 */
	public void setSelectedItem(Object anItem) {
		selection = (String)anItem;
		
	}
	/*
	 * (non-Javadoc)
	 * @see javax.swing.ComboBoxModel#getSelectedItem()
	 */
	public Object getSelectedItem() {
		return selection;
	}
	
	/**
	 * Sets current application
	 * @param  curr
	 */
	public void setCurrent(int curr){
		current = curr;
		appList.setCurrent(current);
	}
	
	/**
	 * @param appList  the appList to set
	 */
	public void setAppList(ApplicationsList appList) {
		this.appList = appList;
	}

	private ApplicationsList appList;
	private String selection;
	private int current;
}
