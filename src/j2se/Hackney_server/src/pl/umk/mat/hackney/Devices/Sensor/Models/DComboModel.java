package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button D jComboBox model class
 * @author HackneyTeam
 *
 */
public class DComboModel extends MainSensorComboBoxModel implements ComboBoxModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3097121845099714197L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public DComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setD(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getD());
	}
}
