package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * Parent class for all which use reading/writing to XML
 * @author HackneyTeam
 *
 */
public class XMLConfiguration {

	/**
	 * Save XML structure to file
	 * @param doc xml document structure
	 * @throws TransformerFactoryConfigurationError
	 * @throws FileNotFoundException
	 * @throws TransformerException
	 */
	protected void saveFile(Document doc) throws TransformerFactoryConfigurationError, FileNotFoundException, TransformerException {		
		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(this.file)));
	}

	protected String fullPath;
	protected String path = "";
	protected String fileName;
	protected File file;
	

}