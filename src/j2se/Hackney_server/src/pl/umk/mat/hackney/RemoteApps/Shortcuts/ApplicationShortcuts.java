package pl.umk.mat.hackney.RemoteApps.Shortcuts;

import java.util.ArrayList;

/**
 * Class represents one application with all defined shortcuts
 * @author HackneyTeam
 */
public class ApplicationShortcuts {

	/**
	 *  Constructor
	 */
	public ApplicationShortcuts() {
		this.list = new ArrayList<Shortcut>();
	}
	
	/**
	 * Constructor
	 * @param name Application name
	 * @param id Application id
	 */
	public ApplicationShortcuts(String name, int id) {
		this.name = name;
		this.id = id;
		this.list = new ArrayList<Shortcut>();
	}
	/**
	 * ApplicationShortcuts class constructor
	 * @param name	Application	name
	 * @param id	Application id
	 * @param list	ArrayList containing shortcuts
	 */
	public ApplicationShortcuts(String name, int id, ArrayList<Shortcut> list) {
		this.name = name;
		this.id = id;
		this.list = list;
	}
	/**
	 * @return  the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name  the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return  the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path  the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return  the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id  the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the list
	 */
	public ArrayList<Shortcut> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<Shortcut> list) {
		this.list = list;
	}
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public Shortcut item(int index) {
		return this.list.get(index);
	}
	
	/**
	 * Add shortcut to ArrayList
	 * @param sc
	 */
	public void addShortcut(Shortcut sc){
		this.list.add(sc);
	}
	
	/**
	 * Return elements of shortcuts
	 * @return amount of items
	 */
	public int getSize() {
		return list.size();
	}
	
	/**
	 * Remove shortcut
	 * @param index number of shortcut
	 */
	public void remove(int index){
		
		this.list.remove(index);
	}
	
	/**
	 * Returns list of shortcuts descriptions
	 * @return ArrayList with descriptions of shortcuts
	 */
	public ArrayList<String> getDescriptionList() {
		ArrayList<String> desc = new ArrayList<String>();
		
		for(Shortcut sc : this.list) {
			desc.add(sc.getDescription());
		}
		
		return desc;
	}
	
	private String name;
	private String path;
	private int id;

	private ArrayList<Shortcut> list;
}
