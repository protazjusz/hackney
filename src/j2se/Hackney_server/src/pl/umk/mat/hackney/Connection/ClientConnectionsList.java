package pl.umk.mat.hackney.Connection;

import java.util.ArrayList;

/**
 * Contains list with clients connections
 * @author HackneyTeam
 *
 */
public class ClientConnectionsList {

	/**
	 * Constructor
	 */
	public ClientConnectionsList(){
		this.ccList = new ArrayList<ClientConnection>();
	}
	
	
	/**
	 * Add client connection
	 * @param cc client connection
	 */
	public void add(ClientConnection cc){
		ccList.add(cc);
	}
	
	/**
	 * Remove client connection from list
	 * @param index number of connection
	 */
	public void remove(int index) {
		ccList.remove(index);
	}
		
	/**
	 * Remove connection specified by MAC address
	 * @param mac device MAC address
	 */
	public void remove(String mac){

		for(int i=ccList.size()-1;i>=0;i--){
			if(ccList.get(i).getInfo().getMac().equals(mac)==true)
			ccList.remove(i);
		}
	}
	
	/**
	 * Finish connection specified by MAC address
	 * @param mac device MAC address
	 */
	public void finish(String mac){
	//	System.out.println(ccList.size());
		for(int i=0;i<ccList.size();i++){
			if(ccList.get(i).getInfo().getMac().equals((String)mac))
				ccList.get(i).getHandler().finish();
		}
		
	}
	/**
	 * Finish all connections
	 */
	public void finishAll(){
		
		for(int i=ccList.size()-1;i>=0;i--){
			ccList.get(i).getHandler().finish();
	
			
		}
	}
	/**
	 * Returns number of connections
	 * @return number of connections
	 */
	public int size(){
		return ccList.size();
	}
	
	/**
	 * Returns item
	 * @param index number
	 * @return list item
	 */
	public ClientConnection item(int index){
		return ccList.get(index);
	}
	
	
	private ArrayList<ClientConnection> ccList;

}
