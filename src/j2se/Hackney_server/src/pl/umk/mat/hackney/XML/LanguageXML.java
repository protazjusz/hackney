package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Utility.Language;


/**
 * Reads translation from XML file
 * @author HackneyTeam
 *
 */
public class LanguageXML {

	/**
	 * Constructor
	 * @param name file name
	 */
	public LanguageXML(String name){
		fileName = name + ".xml";
		fullPath = this.path + this.fileName;
		
		file = new File(fullPath);
		
	}
	
	/**
	 * Reads language from XML file
	 * @return Language object
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
public Language getLanguage() throws SAXException, IOException, ParserConfigurationException {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(file);	// Load XML file
		
		Element root = doc.getDocumentElement();	//Get root element
		
		Language language = new Language();
		
		Element mainTabElement = (Element)root.getElementsByTagName("mainTab").item(0);
		Element shortcutsTabElement = (Element)root.getElementsByTagName("shortcutsTab").item(0);
		Element connectionsTabElement = (Element)root.getElementsByTagName("connectionsTab").item(0);
		Element sensorsTabElement = (Element)root.getElementsByTagName("sensorsTab").item(0);
		Element allowedTabElement = (Element)root.getElementsByTagName("allowedTab").item(0);
		Element onlyAllowedElement = (Element)root.getElementsByTagName("onlyAllowed").item(0);
		
		Element columnModif1Element = (Element)root.getElementsByTagName("columnModif1").item(0);
		Element columnModif2Element = (Element)root.getElementsByTagName("columnModif2").item(0);
		Element columnModif3Element = (Element)root.getElementsByTagName("columnModif3").item(0);
		Element columnKeyElement = (Element)root.getElementsByTagName("columnKey").item(0);
		Element columnDescriptionElement = (Element)root.getElementsByTagName("columnDescription").item(0);
		Element columnRemoveElement = (Element)root.getElementsByTagName("columnRemove").item(0);
		Element nameColumnElement = (Element)root.getElementsByTagName("nameColumn").item(0);

		
		Element playerElement = (Element)root.getElementsByTagName("player").item(0);
		Element player1Element = (Element)root.getElementsByTagName("player1").item(0);
		Element player2Element = (Element)root.getElementsByTagName("player2").item(0);
		Element player3Element = (Element)root.getElementsByTagName("player3").item(0);
		Element player4Element = (Element)root.getElementsByTagName("player4").item(0);
		
		Element leftElement = (Element)root.getElementsByTagName("left").item(0);
		Element rightElement = (Element)root.getElementsByTagName("right").item(0);
		Element forwardElement = (Element)root.getElementsByTagName("forward").item(0);
		Element backElement = (Element)root.getElementsByTagName("back").item(0);
		Element addElement = (Element)root.getElementsByTagName("add").item(0);
		Element removeElement = (Element)root.getElementsByTagName("remove").item(0);
		Element saveAllElement = (Element)root.getElementsByTagName("saveAll").item(0);
		
		Element saveElement = (Element)root.getElementsByTagName("save").item(0);
		Element exitElement = (Element)root.getElementsByTagName("exit").item(0);
		Element fileElement = (Element)root.getElementsByTagName("file").item(0);
		Element sensorSettingsElement = (Element)root.getElementsByTagName("sensorSettings").item(0);
		Element shortcutsSettingsElement = (Element)root.getElementsByTagName("shortcutsSettings").item(0);
		Element devicesAllowListElement = (Element)root.getElementsByTagName("devicesAllowList").item(0);
		Element languageElement = (Element)root.getElementsByTagName("language").item(0);
		Element yesElement = (Element)root.getElementsByTagName("yes").item(0);
		Element noElement = (Element)root.getElementsByTagName("no").item(0);
		Element reallyRemoveElement = (Element)root.getElementsByTagName("reallyRemove").item(0);
		Element questionElement = (Element)root.getElementsByTagName("question").item(0);
		Element newApplicationElement = (Element)root.getElementsByTagName("newApplication").item(0);
		Element giveApplicationNameElement = (Element)root.getElementsByTagName("giveApplicationName").item(0);
		Element keysElement = (Element)root.getElementsByTagName("keys").item(0);
		Element optionsElement = (Element)root.getElementsByTagName("options").item(0);
		Element clearChangesElement = (Element)root.getElementsByTagName("clearChanges").item(0); 
		Element infoAboutElement = (Element)root.getElementsByTagName("infoAbout").item(0); 
		Element restoreDefaultsElement = (Element)root.getElementsByTagName("restoreDefaults").item(0); 
		Element pathElement = (Element)root.getElementsByTagName("path").item(0);
		Element defaultSettingsQuestionElement = (Element)root.getElementsByTagName("defaultSettingsQuestion").item(0);
		Element clearChangesQuestionElement = (Element)root.getElementsByTagName("clearChangesQuestion").item(0);
		Element restartAppRequestElement = (Element)root.getElementsByTagName("restartAppRequest").item(0);
		Element changeLanguageElement = (Element)root.getElementsByTagName("changeLanguage").item(0);
		Element informationElement = (Element)root.getElementsByTagName("information").item(0);
		Element addToWhitelistElement = (Element)root.getElementsByTagName("addToWhiteList").item(0);
		Element buttonElement = (Element)root.getElementsByTagName("button").item(0);
		Element savedElement = (Element)root.getElementsByTagName("saved").item(0);
		Element addedToAllowElement = (Element) root.getElementsByTagName("addedToAllowList").item(0);
		
		language.setMainTab(mainTabElement.getTextContent());
		language.setShortcutsTab(shortcutsTabElement.getTextContent());
		language.setConnectionsTab(connectionsTabElement.getTextContent());
		language.setSensorsTab(sensorsTabElement.getTextContent());
		language.setAllowedTab(allowedTabElement.getTextContent());
		language.setOnlyAllowed(onlyAllowedElement.getTextContent());
		

		language.setColumnModif1(columnModif1Element.getTextContent());
		
		language.setColumnModif2(columnModif2Element.getTextContent());
		
		language.setColumnModif3(columnModif3Element.getTextContent());
		
		
		language.setColumnKey(columnKeyElement.getTextContent());
		language.setColumnDescription(columnDescriptionElement.getTextContent());
		language.setColumnRemove(columnRemoveElement.getTextContent());
		language.setNameColumn(nameColumnElement.getTextContent());
		
		language.setPlayer(playerElement.getTextContent());
		language.setPlayer1(player1Element.getTextContent());
		language.setPlayer2(player2Element.getTextContent());
		language.setPlayer3(player3Element.getTextContent());
		language.setPlayer4(player4Element.getTextContent());
		
		language.setLeft(leftElement.getTextContent());
		language.setRight(rightElement.getTextContent());
		language.setForward(forwardElement.getTextContent());
		language.setBack(backElement.getTextContent());
		
		language.setAdd(addElement.getTextContent());
		language.setRemove(removeElement.getTextContent());
		
		language.setSaveAll(saveAllElement.getTextContent());
		language.setSave((saveElement).getTextContent());
		language.setExit(exitElement.getTextContent());
		language.setFile(fileElement.getTextContent());
		language.setSensorsSettings(sensorSettingsElement.getTextContent());
		language.setShortcutsSettings(shortcutsSettingsElement.getTextContent());
		language.setDevicesAllowList(devicesAllowListElement.getTextContent());
		language.setLanguage(languageElement.getTextContent());
		language.setYes(yesElement.getTextContent());
		language.setNo(noElement.getTextContent());
		language.setReallyRemove(reallyRemoveElement.getTextContent());
		language.setQuestion(questionElement.getTextContent());
		language.setNewApplication(newApplicationElement.getTextContent());
		language.setGiveApplicationName(giveApplicationNameElement.getTextContent());
		language.setKeys(keysElement.getTextContent());
		language.setOptions(optionsElement.getTextContent());
		language.setClearChanges(clearChangesElement.getTextContent());
		language.setInfoAbout(infoAboutElement.getTextContent());
		language.setRestoreDefaults(restoreDefaultsElement.getTextContent());
		language.setPath(pathElement.getTextContent());
		language.setClearChangesQuestion(clearChangesQuestionElement.getTextContent());
		language.setDefaultSettingsQuestion(defaultSettingsQuestionElement.getTextContent());
		language.setRestartApplicationRequest(restartAppRequestElement.getTextContent());
		language.setInformation(informationElement.getTextContent());
		language.setChangeLanguage(changeLanguageElement.getTextContent());
		language.setAddToWhiteList(addToWhitelistElement.getTextContent());
		language.setButton(buttonElement.getTextContent());
		language.setSaved(savedElement.getTextContent());
		language.setAddedToAllowList(addedToAllowElement.getTextContent());

		return language;
	}



	private String fullPath;
	private String path = "languages/";
	private String fileName;
	private File file;
	
}
