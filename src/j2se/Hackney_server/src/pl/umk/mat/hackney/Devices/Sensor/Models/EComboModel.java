
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button E jComboBox model class
 * @author HackneyTeam
 *
 */
public class EComboModel extends MainSensorComboBoxModel implements ComboBoxModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6915188330669517824L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public EComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setE(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getE());
	}
}
