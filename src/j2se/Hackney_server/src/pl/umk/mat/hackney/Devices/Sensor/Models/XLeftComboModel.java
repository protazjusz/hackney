/**
 * 
 */
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors X Axis left jComboBox model class
 * @author HackneyTeam
 *
 */
public class XLeftComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	private static final long serialVersionUID = 4124969797722784096L;

	/**
	 * @param sensor
	 */
	public XLeftComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setxLeft(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getxLeft());
	}
}
