/**
 * 
 */
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors Y Axis left jComboBox model class
 * @author HackneyTeam
 *
 */
public class YLeftComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8877190924833240553L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public YLeftComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setyLeft(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getyLeft());
	}
}

