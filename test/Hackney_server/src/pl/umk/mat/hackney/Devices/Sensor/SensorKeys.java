package pl.umk.mat.hackney.Devices.Sensor;

import java.awt.event.KeyEvent;

/**
 * Represents one kit of defined keys
 * @author HackneyTeam
 *
 */
public class SensorKeys {


	/**
	 * @return  the yLeft
	 */
	public int getyLeft() {
		return yLeft;
	}
	/**
	 * @param yLeft  the yLeft to set
	 */
	public void setyLeft(int yLeft) {
		this.yLeft = yLeft;
	}
	/**
	 * @return  the yRight
	 */
	public int getyRight() {
		return yRight;
	}
	/**
	 * @param yRight  the yRight to set
	 */
	public void setyRight(int yRight) {
		this.yRight = yRight;
	}
	/**
	 * @return  the zUp
	 */
	public int getzUp() {
		return zUp;
	}
	/**
	 * @param zUp  the zUp to set
	 */
	public void setzUp(int zUp) {
		this.zUp = zUp;
	}
	/**
	 * @return  the zDown
	 */
	public int getzDown() {
		return zDown;
	}
	/**
	 * @param zDown  the zDown to set
	 */
	public void setzDown(int zDown) {
		this.zDown = zDown;
	}
	/**
	 * @return  the xLeft
	 */
	public int getxLeft() {
		return xLeft;
	}
	/**
	 * @param xLeft  the xLeft to set
	 */
	public void setxLeft(int xLeft) {
		this.xLeft = xLeft;
	}
	/**
	 * @return  the xRight
	 */
	public int getxRight() {
		return xRight;
	}
	/**
	 * @param xRight  the xRight to set
	 */
	public void setxRight(int xRight) {
		this.xRight = xRight;
	}
	/**
	 * @return  the a
	 */
	public int getA() {
		return a;
	}
	/**
	 * @param a  the a to set
	 */
	public void setA(int a) {
		this.a = a;
	}
	/**
	 * @return  the b
	 */
	public int getB() {
		return b;
	}
	/**
	 * @param b  the b to set
	 */
	public void setB(int b) {
		this.b = b;
	}
	/**
	 * @return  the c
	 */
	public int getC() {
		return c;
	}
	/**
	 * @param c  the c to set
	 */
	public void setC(int c) {
		this.c = c;
	}
	/**
	 * @return  the d
	 */
	public int getD() {
		return d;
	}
	/**
	 * @param d  the d to set
	 */
	public void setD(int d) {
		this.d = d;
	}
	/**
	 * @return  the e
	 */
	public int getE() {
		return e;
	}
	/**
	 * @param e  the e to set
	 */
	public void setE(int e) {
		this.e = e;
	}
	/**
	 * @return  the f
	 */
	public int getF() {
		return f;
	}
	/**
	 * @param f  the f to set
	 */
	public void setF(int f) {
		this.f = f;
	}
	private int yLeft = KeyEvent.VK_UNDEFINED;
	private int yRight = KeyEvent.VK_UNDEFINED;
	private int zUp = KeyEvent.VK_UNDEFINED;
	private int zDown = KeyEvent.VK_UNDEFINED;
	private int xLeft = KeyEvent.VK_UNDEFINED;
	private int xRight = KeyEvent.VK_UNDEFINED;
	private int a=KeyEvent.VK_UNDEFINED;
	private int b=KeyEvent.VK_UNDEFINED;
	private int c=KeyEvent.VK_UNDEFINED;
	private int d=KeyEvent.VK_UNDEFINED;
	private int e=KeyEvent.VK_UNDEFINED;
	private int f=KeyEvent.VK_UNDEFINED;
	
}
