package pl.umk.mat.hackney.Devices;
import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.Vector;

/**
 * Class to operate Mouse
 * @author HackneyTeam
 *
 */
public class Mouse extends Thread
{
	
	Robot ster;

	int akcja;
	byte dane[]={0,0,0};
	byte payload[];
	boolean cl;
	private byte type;
	Integer kolej;
	private Vector x;
	private Vector y;
	
	
	public static final byte PRESS = 0;
	public static final byte RELEASE = 1;
	public static final byte TOUCH = 3;
	
	
	public Mouse()
	{
		kolej=new Integer(0);
		x=new Vector();
		y=new Vector();
		akcja=0;

		
		try
		{
			ster=new Robot();
		} 
		catch (AWTException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int kod2action(int kod)
	{
		if(kod==-1 || kod==53) return 1;//up
		if(kod==-2 || kod==56) return 2; //down
		if(kod==-4 || kod==54) return 4;//left
		if(kod==-3 || kod==52) return 8;//right
		if(kod==-5 || kod==49) return 16;//LPM
		if( kod==51) return 32;//PPM	
		if(kod==-36) return 64;//wheel up
		if(kod==-37) return 128;//wheel down
		return 0;
	}
	
	public int kod2direction(int kod)
	{
		if(kod==-1 || kod==53) return -1;//up
		if(kod==-2 || kod==56) return 1; //down
		if(kod==-4 || kod==54) return 1;//left
		if(kod==-3 || kod==52) return -1;//right
		return 0;
	}
	
	@Override
	public void run()
	{
		while(akcja!=-10)
		{
			
				int dx;
				int dy;
				
				synchronized(kolej)
				{
					if(x.size()!=0)dx= kod2direction((Integer)x.lastElement());
						else dx=0;
					
					if(y.size()!=0) dy= kod2direction((Integer)y.lastElement());
						else dy=0;
				}
				ster.mouseMove(MouseInfo.getPointerInfo().getLocation().x+dx,MouseInfo.getPointerInfo().getLocation().y+dy);
				if(this.type==0)
				{
								
				if(akcja==64 ) 
				{
					try {
							Mouse.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ster.mouseWheel(-1);	
				}
				if(akcja==128 )  
				{
					try {
						Mouse.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ster.mouseWheel(1);	
				}
			}

			try
			{
				Mouse.sleep(5);
			} 
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void releaseAll() {
		akcja=-10;	
	}
	
	@SuppressWarnings("unchecked")
	public void setAkcja(byte type, byte o[])
	{
		
		this.type = type;
		
		byte kod=o[0];
		
		if(type==0)
		{
			synchronized(kolej)
			{
				if(kod==-1 || kod==53 || kod==-2 || kod==56) this.y.add(new Integer(kod));
				if(kod==-4 || kod==54 || kod==-3 || kod==52) this.x.add(new Integer(kod));
			}
			
			if(kod==-5 || kod==49) 	ster.mousePress(InputEvent.BUTTON1_MASK);
			if(kod==51) 			ster.mousePress(InputEvent.BUTTON3_MASK);		 			
		}

		if(type==1)
		{
			synchronized(kolej)
			{
				if(kod==-1 || kod==53 || kod==-2 || kod==56)
				{
					int z=-1;
					for(int i=0;i<y.size();i++)
						if(((Integer)y.get(i)).byteValue()==kod) z=i; 
					if(z!=-1)this.y.remove(z);
					
				}
				
				if(kod==-4 || kod==54 || kod==-3 || kod==52)
				{
					int z=-1;
					for(int i=0;i<x.size();i++)
						if(((Integer)x.get(i)).byteValue()==kod) z=i; 
					if(z!=-1)this.x.remove(z);
					
				}
			}
			
			if(kod==-5 || kod==49) 	ster.mouseRelease(InputEvent.BUTTON1_MASK);
			if(kod==51) 	ster.mouseRelease(InputEvent.BUTTON3_MASK);
		
		}
		

		if(type==0) akcja=kod2action(o[0]);
		payload=o;
		if(type==3){
			ster.mouseMove(MouseInfo.getPointerInfo().getLocation().x+payload[0],MouseInfo.getPointerInfo().getLocation().y+payload[1]);
		} else
		if(type==1){
			ster.mouseMove(MouseInfo.getPointerInfo().getLocation().x,MouseInfo.getPointerInfo().getLocation().y);
		}
	}
}