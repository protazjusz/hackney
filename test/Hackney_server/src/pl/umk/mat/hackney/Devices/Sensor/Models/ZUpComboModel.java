/**
 * 
 */
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors Z Axis up jComboBox model class
 * @author HackneyTeam
 *
 */
public class ZUpComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4585008871641288811L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public ZUpComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setzUp(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getzUp());
	}
}
