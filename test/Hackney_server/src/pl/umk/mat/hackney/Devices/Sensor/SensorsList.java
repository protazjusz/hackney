package pl.umk.mat.hackney.Devices.Sensor;

import java.util.ArrayList;

/**
 * Represents list of Sensor objects
 * @author HackneyTeam
 *
 */
public class SensorsList {

	/**
	 * Constructor
	 */
	public SensorsList() {
		this.sensors = new ArrayList<Sensor>();
	}

	/**
	 * Add item to list
	 * @param s Sensor object
	 */
	public void add(Sensor s){
		sensors.add(s);
	}
	
	/**
	 * Removes item from list
	 * @param item
	 */
	public void remove(int item){
		sensors.remove(item);
	}
	
	/**
	 * Gets an item from list
	 * @param index item number
	 * @return Sensor object from list
	 */
	public Sensor getItem(int index){
		
		return sensors.get(index);
	}
	
	/**
	 * Returns number of elements which contains list
	 * @return number of elements
	 */
	public int size() {
		return sensors.size();
	}
	
	private ArrayList<Sensor> sensors;
}
