package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors Y Axis right jComboBox model class
 * @author HackneyTeam
 *
 */
public class YRightComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6523733726797384123L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public YRightComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setyRight(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getyRight());
	}
}
