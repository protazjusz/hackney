package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button C jComboBox model class
 * @author HackneyTeam
 *
 */
public class CComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6461492404630954482L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public CComboModel(SensorsList sensor) {
		super(sensor);
	}


	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setC(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getC());
	}
}