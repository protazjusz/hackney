/**
 * 
 */
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.AbstractListModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ShortcutKeys;


/**
 * Parent class for Sensors JComboBoxes models
 * @author HackneyTeam
 *
 */
public class MainSensorComboBoxModel extends AbstractListModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8391051412457852074L;
	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public MainSensorComboBoxModel(SensorsList sensor) {
		this.sensorList = sensor;
		this.sk = new ShortcutKeys();
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize() {
		// TODO Auto-generated method stub
		return ShortcutKeys.all_items.length;
	}

	public Object getElementAt(int index) {
		// TODO Auto-generated method stub
		return ShortcutKeys.all_items[index];
	}
	
	/**
	 * Set current
	 * @param playerNo  the playerNo to set
	 * @uml.property  name="playerNo"
	 */
	public void setPlayerNo(int playerNo) {
		this.playerNo = playerNo;
	}

	/**
	 * @return  the sensorList
	 * @uml.property  name="sensorList"
	 */
	public SensorsList getSensorList() {
		return sensorList;
	}

	/**
	 * @param sensorList  the sensorList to set
	 * @uml.property  name="sensorList"
	 */
	public void setSensorList(SensorsList sensorList) {
		this.sensorList = sensorList;
	}

	/**
	 * @uml.property  name="current"
	 */
	protected int current;
	/**
	 * @uml.property  name="playerNo"
	 */
	protected int playerNo=1;
	/**
	 * @uml.property  name="sensorList"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	protected SensorsList sensorList;
	/**
	 * @uml.property  name="sk"
	 * @uml.associationEnd  multiplicity="(1 1)"
	 */
	protected ShortcutKeys sk;

}
