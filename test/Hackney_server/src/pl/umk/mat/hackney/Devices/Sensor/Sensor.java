package pl.umk.mat.hackney.Devices.Sensor;

import java.awt.AWTException;
import java.awt.Robot;


/**
 * Class to operate on defined SensorKeys 
 * @author HackneyTeam
 *
 */
public class Sensor {
	
	public static final int Y_LEFT =2 ;
	public static final int Y_RIGHT =4 ;
	public static final int X_LEFT =8 ;
	public static final int X_RIGHT =16 ;
	public static final int Z_UP =32 ;
	public static final int Z_DOWN =64;
	
	public static final int A =2 ;
	public static final int B =4 ;
	public static final int C =8 ;
	public static final int D =16 ;
	public static final int E =32 ;
	public static final int F =64;
	
	
	
	/**
	 * Constructor
	 */
	public Sensor(){
		initRobot();
		
		sKeys = new SensorKeys();
	}
	
	/**
	 * Constructor
	 * @param keys SensorKeys object
	 */
	public Sensor(SensorKeys keys){
		initRobot();
		
		sKeys = keys;
	}

	public void startYLeft() {
		rob.keyPress(sKeys.getyLeft());
	}
	
	public void startYRight() {
		rob.keyPress(sKeys.getyRight());
	}
	
	public void startZUp() {	
		rob.keyPress(sKeys.getzUp());
	}
	public void startZDown() {

		rob.keyPress(sKeys.getzDown());
	}
	
	public void startXLeft(){
		rob.keyPress(sKeys.getxLeft());
	}
	
	public void startXRight(){
		rob.keyPress(sKeys.getxRight());
	}
	
	public void startA(){
		rob.keyPress(sKeys.getA());
	}
	public void startB(){
		rob.keyPress(sKeys.getB());
	}
	public void startC(){
		rob.keyPress(sKeys.getC());
	}
	public void startD(){
		rob.keyPress(sKeys.getD());
	}
	public void startE(){
		rob.keyPress(sKeys.getE());
	}
	public void startF(){
		rob.keyPress(sKeys.getF());
	}
	
	
	public void stopYLeft() {
		rob.keyRelease(sKeys.getyLeft());
	}
	
	public void stopYRight() {	
		rob.keyRelease(sKeys.getyRight());
	}
	
	public void stopZUp() {
		rob.keyRelease(sKeys.getzUp());
	}
	public void stopZDown() {
		rob.keyRelease(sKeys.getzDown());
	}
	public void stopXLeft(){
		rob.keyRelease(sKeys.getxLeft());
	}
	public void stopXRight(){
		rob.keyRelease(sKeys.getxRight());
	}
	public void stopA(){
		rob.keyRelease(sKeys.getA());
	}
	public void stopB(){
		rob.keyRelease(sKeys.getB());
	}
	public void stopC(){
		rob.keyRelease(sKeys.getC());
	}
	public void stopD(){
		rob.keyRelease(sKeys.getD());
	}
	public void stopE(){
		rob.keyRelease(sKeys.getE());
	}
	public void stopF(){
		rob.keyRelease(sKeys.getF());
	}
	
	
	/**
	 * @return  the sKeys
	 */
	public SensorKeys getsKeys() {
		return sKeys;
	}

	/**
	 * @param sKeys  the sKeys to set
	 */
	public void setsKeys(SensorKeys sKeys) {
		this.sKeys = sKeys;
	}

	public void releaseAll(){
		
		try{
			stopXLeft();
			stopXRight();
			stopYLeft();
			stopYRight();
			stopZUp();
			stopZDown();
			
			
			stopA();
			stopB();
			stopC();
			stopD();
			stopE();
			stopF();
		}
		catch(Exception e){}
	}
	
	private void initRobot() {
		try {
			rob = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private Robot rob;
	private SensorKeys sKeys;
}
