
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button A jComboBox model class
 * @author HackneyTeam
 *
 */
public class AComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2451092591170045155L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public AComboModel(SensorsList sensor) {
		super(sensor);
	}
	
	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setA(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getA());
	}
}
