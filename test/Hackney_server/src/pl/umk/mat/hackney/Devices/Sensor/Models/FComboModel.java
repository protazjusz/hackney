
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button F jComboBox model class
 * @author HackneyTeam
 *
 */
public class FComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6104811944280915049L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public FComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setF(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getF());
	}
}
