/**
 * 
 */
package pl.umk.mat.hackney.Devices.Sensor.Models;

import javax.swing.ComboBoxModel;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Sensors button B jComboBox model class
 * @author HackneyTeam
 *
 */
public class BComboModel extends MainSensorComboBoxModel implements ComboBoxModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2196031972083230867L;

	/**
	 * Constructor
	 * @param sensor list contains defined sensor kits
	 */
	public BComboModel(SensorsList sensor) {
		super(sensor);
	}

	public void setSelectedItem(Object anItem) {
		sensorList.getItem(playerNo).getsKeys().setB(sk.getKeyCode((String)anItem));
	}

	public Object getSelectedItem() {
		return (String)sk.getKey(sensorList.getItem(playerNo).getsKeys().getB());
	}
}