package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Utility.MainConf;


/**
 * Reads and writes MainConf object from XML file
 * @author HackneyTeam
 *
 */
public class MainConfXML extends XMLConfiguration {

	/**
	 * Constructor
	 */
	public MainConfXML() {
		this.fileName = "mainconf.xml";
		fullPath = this.path + "config/"+ this.fileName;
		file = new File(fullPath);
		
	}
	
	/**
	 * Constructor
	 * @param def file name
	 */
	public MainConfXML(String def) {
		this.fileName = def;
		fullPath = "defaults/" + this.fileName;
		file = new File(fullPath);
		
	}
	
	/**
	 * Read MainConf object content from xml file
	 * @return MainConf object
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public MainConf getConf() throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		MainConf conf = new MainConf();
		Document doc = builder.parse(this.file);	// Load XML file
		
		Element root = doc.getDocumentElement();	//Get root element

		Element nameElement = (Element)root.getElementsByTagName("language").item(0);
		Element allowStateElement = (Element)root.getElementsByTagName("allowState").item(0);
			
		if(allowStateElement.getTextContent().equals("true")==true)
			conf.setAllowedStatus(true);
		else
			if(allowStateElement.getTextContent().equals("false")==true)
				conf.setAllowedStatus(false);
		
		conf.setDefaultLanguage(nameElement.getTextContent());
		return conf;
		
	}
	
	/**
	 * Save MainConf object content to xml file
	 * @param conf MainConf object
	 * @throws ParserConfigurationException
	 * @throws FileNotFoundException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public void saveConf(MainConf conf) throws ParserConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.newDocument();
		
		Element rootElement = doc.createElement("main");
		
		Element allowStateEl = doc.createElement("allowState");
		if(conf.isAllowedStatus() == true)
			allowStateEl.appendChild(doc.createTextNode("true"));
		else
			allowStateEl.appendChild(doc.createTextNode("false"));
		
		Element languageEl = doc.createElement("language");
		languageEl.appendChild(doc.createTextNode(conf.getDefaultLanguage()));
		
		
		rootElement.appendChild(languageEl);
		rootElement.appendChild(allowStateEl);
		
		doc.appendChild(rootElement);
		saveFile(doc);
	}
}
