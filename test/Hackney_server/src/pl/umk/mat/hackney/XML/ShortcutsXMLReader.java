
package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationShortcuts;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.Shortcut;


/**
 * Reads from xml files defined shortcuts
 * @author HackneyTeam
 *
 */
public class ShortcutsXMLReader extends XMLConfiguration {
	
	/**
	 * Constructor
	 */
	public ShortcutsXMLReader() {
		this.fileName = "conf.xml";
		fullPath = this.path + "config/" + this.fileName;
		file = new File(fullPath);
	}
	
	/**
	 * Constructor
	 * @param def file name
	 */
	public ShortcutsXMLReader(String def) {
		this.fileName = def;
		fullPath = "defaults/" + this.fileName;
		file = new File(fullPath);
	}

	/**
	 * Read applications shortcuts configuration from XML file
	 * @return read AppliactionList from file
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public ApplicationsList getApplicationList() throws ParserConfigurationException, SAXException, IOException {
						
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(file);	// Load XML file
		ApplicationsList appList = new ApplicationsList();
		Element root = doc.getDocumentElement();	//Get root element
		
		NodeList nodes = root.getElementsByTagName("app");  //Read list of apps from XML
		
		for(int i=0;i<nodes.getLength();i++) {	//Iterate by all apps
			Node node = nodes.item(i);
			
			Element el = (Element)node;
			
			ApplicationShortcuts as = getApplicationsShortcuts(el, i);
			appList.addApplication(as);
		}
		
		return appList;
	}
	
	/**
	 * Save applications shortcuts to xml file
	 * @param apps ApplicationList with defined shortcuts
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 * @throws TransformerFactoryConfigurationError 
	 * @throws FileNotFoundException 
	 */
	public void saveApplicationsList(ApplicationsList apps) throws ParserConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.newDocument();
		
		Element rootElement = doc.createElement("main");

		//deleteFile();
		Element appElement = null;
		for(int i=0;i<apps.getSize();i++) {	//Iterate by applications
			ApplicationShortcuts appShort = apps.item(i);
			appElement = doc.createElement("app");
			appElement.setAttribute("name", appShort.getName()); //Add name of application as a attribute
			appElement.setAttribute("path", appShort.getPath()); //Add name of application as a attribute
			Element scElement = null;
		
			for (int j=0;j<appShort.getSize();j++) {	//Iterate by shortcuts of application
				Shortcut sc = appShort.item(j);
				scElement = doc.createElement("shortcut");
				
				Element desc = doc.createElement("desc");
				desc.appendChild(doc.createTextNode(sc.getDescription()));
				Element m1 = doc.createElement("m1");
				m1.appendChild(doc.createTextNode(String.valueOf(sc.getModifier1())));
				Element m2 = doc.createElement("m2");
				m2.appendChild(doc.createTextNode(String.valueOf(sc.getModifier2())));
				Element m3 = doc.createElement("m3");
				m3.appendChild(doc.createTextNode(String.valueOf(sc.getModifier3())));
				Element key = doc.createElement("key");
				key.appendChild(doc.createTextNode(String.valueOf(sc.getKey())));
				
				scElement.appendChild(desc);
				scElement.appendChild(m1);
				scElement.appendChild(m2);
				scElement.appendChild(m3);
				scElement.appendChild(key);
				
				appElement.appendChild(scElement);
			}
			rootElement.appendChild(appElement);
			
		}

		doc.appendChild(rootElement);
		saveFile(doc);
	}

	private ApplicationShortcuts getApplicationsShortcuts(Element el, int id){
		ApplicationShortcuts app = new ApplicationShortcuts();
		String name = el.getAttribute("name");
		String path = el.getAttribute("path");
		app.setPath(path);
		app.setName(name);
		app.setId(id);
		
		NodeList shortcutsNodes = el.getElementsByTagName("shortcut");
				
		for(int i=0;i<shortcutsNodes.getLength();i++){
			Node shortcutNode = shortcutsNodes.item(i);
			Element shortcutElement = (Element) shortcutNode;
			
			Shortcut sc = getShortcut(shortcutElement);
			app.addShortcut(sc);
		
		}
		
		return app;
		
	}
	
	private Shortcut getShortcut(Element el){
		
		Element descElement = (Element)el.getElementsByTagName("desc").item(0);
		Element m1Element = (Element)el.getElementsByTagName("m1").item(0);
		Element m2Element = (Element)el.getElementsByTagName("m2").item(0);
		Element m3Element = (Element)el.getElementsByTagName("m3").item(0);
		Element keyElement = (Element)el.getElementsByTagName("key").item(0);
		
		String desc = descElement.getTextContent();
		int m1 = Integer.parseInt(m1Element.getTextContent());
		int m2 = Integer.parseInt(m2Element.getTextContent());
		int m3 = Integer.parseInt(m3Element.getTextContent());
		int key = Integer.parseInt(keyElement.getTextContent());
		
		Shortcut shortcut = new Shortcut(m1,m2,m3,key,desc);
		return shortcut;
	}
	
}
