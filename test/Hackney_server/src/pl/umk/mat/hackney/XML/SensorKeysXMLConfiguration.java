package pl.umk.mat.hackney.XML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Devices.Sensor.Sensor;
import pl.umk.mat.hackney.Devices.Sensor.SensorKeys;
import pl.umk.mat.hackney.Devices.Sensor.SensorsList;


/**
 * Reads from xml files defined sensors configuration
 * @author HackneyTeam
 *
 */
public class SensorKeysXMLConfiguration extends XMLConfiguration {

	/**
	 * Constructor
	 */
	public SensorKeysXMLConfiguration() {
		this.fileName = "sensor.xml";
		fullPath = this.path+ "config/" + this.fileName;
		file = new File(fullPath);
	}
	
	/**
	 * Constructor
	 * @param def file name
	 */
	public SensorKeysXMLConfiguration(String def) {
		this.fileName = def;
		fullPath = "defaults/" + this.fileName;
		file = new File(fullPath);
	}
	
	
	
	/**
	 * Reads sensors defined keys from XML file
	 * @return list with sensor keys definition
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public SensorsList getSensorsList() throws SAXException, IOException, ParserConfigurationException {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(file);	// Load XML file
		SensorsList sensorList = new SensorsList();
		sensorList.add(new Sensor());
		Element root = doc.getDocumentElement();	//Get root element
		
		NodeList nodes = root.getElementsByTagName("sensor");  //Read list of apps from XML
		
		for(int i=0;i<nodes.getLength();i++) {	//Iterate by all apps
			Node node = nodes.item(i);
			
			SensorKeys sensorKeys = new SensorKeys();
			Element el = (Element)node;
			Element aElement = (Element)el.getElementsByTagName("a").item(0);
			Element bElement = (Element)el.getElementsByTagName("b").item(0);
			Element cElement = (Element)el.getElementsByTagName("c").item(0);
			Element dElement = (Element)el.getElementsByTagName("d").item(0);
			Element eElement = (Element)el.getElementsByTagName("e").item(0);
			Element fElement = (Element)el.getElementsByTagName("f").item(0);
			
			Element xlElement = (Element)el.getElementsByTagName("xleft").item(0);
			Element xrElement = (Element)el.getElementsByTagName("xright").item(0);
			Element ylElement = (Element)el.getElementsByTagName("yleft").item(0);
			Element yrElement = (Element)el.getElementsByTagName("yright").item(0);
			Element zuElement = (Element)el.getElementsByTagName("zup").item(0);
			Element zdElement = (Element)el.getElementsByTagName("zdown").item(0);
			
			
			sensorKeys.setA(Integer.parseInt(aElement.getTextContent()));
			sensorKeys.setB(Integer.parseInt(bElement.getTextContent()));
			sensorKeys.setC(Integer.parseInt(cElement.getTextContent()));
			sensorKeys.setD(Integer.parseInt(dElement.getTextContent()));
			sensorKeys.setE(Integer.parseInt(eElement.getTextContent()));
			sensorKeys.setF(Integer.parseInt(fElement.getTextContent()));
			
			sensorKeys.setxLeft(Integer.parseInt(xlElement.getTextContent()));
			sensorKeys.setxRight(Integer.parseInt(xrElement.getTextContent()));
			sensorKeys.setyLeft(Integer.parseInt(ylElement.getTextContent()));
			sensorKeys.setyRight(Integer.parseInt(yrElement.getTextContent()));
			sensorKeys.setzUp(Integer.parseInt(zuElement.getTextContent()));
			sensorKeys.setzDown(Integer.parseInt(zdElement.getTextContent()));
			
			
			sensorList.add(new Sensor(sensorKeys));
		}

		
		return sensorList;
	}
	
	/**
	 * Save sensors defined keys to XML file
	 * @param sensors list with sensor keys definition
	 * @throws ParserConfigurationException
	 * @throws FileNotFoundException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public void saveSensorsList(SensorsList sensors) throws ParserConfigurationException, FileNotFoundException, TransformerFactoryConfigurationError, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		
		Element rootElement = doc.createElement("main");
		Element scElement = null;
		
		for (int j=1;j<sensors.size();j++) {	//Iterate by shortcuts of application
			Sensor sensor = sensors.getItem(j);
			scElement = doc.createElement("sensor");
			
			Element aEl = doc.createElement("a");
			aEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getA())));
			Element bEl = doc.createElement("b");
			bEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getB())));
			Element cEl = doc.createElement("c");
			cEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getC())));
			Element dEl = doc.createElement("d");
			dEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getD())));
			Element eEl = doc.createElement("e");
			eEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getE())));
			Element fEl = doc.createElement("f");
			fEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getF())));
			
			Element xlEl = doc.createElement("xleft");
			xlEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getxLeft())));
			Element xrEl = doc.createElement("xright");
			xrEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getxRight())));
			Element ylEl = doc.createElement("yleft");
			ylEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getyLeft())));
			Element yrEl = doc.createElement("yright");
			yrEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getyRight())));
			Element zuEl = doc.createElement("zup");
			zuEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getzUp())));
			Element zdEl = doc.createElement("zdown");
			zdEl.appendChild(doc.createTextNode(String.valueOf(sensor.getsKeys().getzDown())));
			
			scElement.appendChild(aEl);
			scElement.appendChild(bEl);
			scElement.appendChild(cEl);
			scElement.appendChild(dEl);
			scElement.appendChild(eEl);
			scElement.appendChild(fEl);
			
			scElement.appendChild(xlEl);
			scElement.appendChild(xrEl);
			scElement.appendChild(ylEl);
			scElement.appendChild(yrEl);
			scElement.appendChild(zuEl);
			scElement.appendChild(zdEl);
			rootElement.appendChild(scElement);
		}
		doc.appendChild(rootElement);
		saveFile(doc);
	}

}
