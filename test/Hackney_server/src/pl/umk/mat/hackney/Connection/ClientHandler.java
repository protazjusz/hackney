package pl.umk.mat.hackney.Connection;

import java.awt.event.KeyEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.io.StreamConnection;
import javax.swing.JTable;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Devices.Mouse;
import pl.umk.mat.hackney.Devices.Sensor.Sensor;
import pl.umk.mat.hackney.Devices.Sensor.SensorsList;
import pl.umk.mat.hackney.Dialogs.MessageDialogInfo;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationShortcuts;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.Shortcut;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ShortcutKeys;
import pl.umk.mat.hackney.Utility.AvailabilityKit;
import pl.umk.mat.hackney.Utility.Informations;
import pl.umk.mat.hackney.Utility.Log;
import pl.umk.mat.hackney.Utility.StringRebuilder;
import pl.umk.mat.hackney.XML.ShortcutsXMLReader;

/**
 * Serve connections with clients according to specification: 
 * https://datatracker.ietf.org/doc/draft-ruminski-homenet-galop-proto
 * @author HackneyTeam
 *
 */

public class ClientHandler implements Runnable
{

	private StreamConnection polaczenie;
	private DataInputStream in;
	private DataOutputStream out;
	protected ApplicationsList apps = null;
	protected Mouse mysz;
	protected MessageDialogInfo mdi =null;
	protected MessageInputStream is = null;
	protected MessageOutputStream os = null;
	protected int numberOfApps=0;
	protected Sensor sensor;
	protected boolean finished=false;
	protected SensorsList sensors;
	protected AvailabilityKit skroty;
	protected byte sessionId;
	protected ClientInfo info;
	protected Boolean allowToConnect;
	protected ClientConnectionsList cConnections;
	protected JTable connectionsTable;
	protected StringRebuilder sr;
	protected Log log;
	protected Timer timer;
	protected Boolean[] setTaken;
	protected ShortcutsXMLReader ScXML = null;

	/**
	 * 
	 * @param connection connection object
	 * @param z
	 * @param s session id
	 * @param apps shortcuts application list
	 * @param sensors defined sensor keys kits
	 * @param cConnections connections list
	 * @param info device information
	 * @param allowToConnect allow list check device
	 * @param table connections table
	 */
	public ClientHandler()
	{
		
	}
	public ClientHandler(Object sc, AvailabilityKit z,byte s, SensorsList sensors,ClientConnectionsList cConnections, ClientInfo info, Boolean allowToConnect, JTable table,Boolean[] setTaken)
	{
		StreamConnection connection = (StreamConnection) sc; 
		mdi = new MessageDialogInfo();
		polaczenie= connection;
		this.allowToConnect = allowToConnect;
		sessionId=s;
		this.info = info;
		this.cConnections = cConnections; 
		this.connectionsTable = table;
		sr = new StringRebuilder();
		log = new Log();
		timer = new Timer();
		this.setTaken = setTaken;
		this.sensors = sensors;
		//this.apps = apps;
		reloadApps();
		
		skroty=z;
		z.set(sessionId);
		
		
		try {
			in=polaczenie.openDataInputStream();
			out= polaczenie.openDataOutputStream();
			this.is = new MessageInputStream(in);
			this.os = new MessageOutputStream(out);
			
			sensor = new Sensor();
			mysz=new Mouse();
			mysz.start();
		
		}
		catch (IOException e) {
			mdi.bluetoothConnectionError();
		}

		
	}


	
	@SuppressWarnings("finally")
	public void run()
	{
		byte dane[];
		dane=new byte[32];
		byte message_type;
		MessageData toSend;		
	
		try
		{
			log.newConnection(info);
			while(!finished && is.read(dane) != -1 ) {
				
				MessageData md = new MessageData(dane);
				message_type = md.getMessageType();
				
				if(allowToConnect == false){
					sendGoodbye(is, os);
					break;
				}
					
				
				if(apps.getSize()!=0)
					this.numberOfApps = apps.getSize();
				
			//	System.out.println("Otrzymano komunikat: "+message_type+" FN: "+info.getName()+" MAC: "+info.getMac());
				log.receiveCommunicate(message_type,info);
				switch(message_type) {
					case MessageData.CONNECTION_HELLO: {
						timer.cancel();
						toSend = new MessageData();
						toSend.setMessageType(MessageData.CONNECTION_HELLO);
						toSend.setSessionID(sessionId); 
						Informations.ID_NO++;
						os.write(toSend);
						log.sendCommunicate(MessageData.CONNECTION_HELLO, info);
						if (md.getSessionID() == MessageData.DEVICE_ANDROID) {
							gPlayerID = (byte)(sessionId %4 +1);
							if (setTaken[gPlayerID] == true)
								gPlayerID = 0;
							toSend = new MessageData();		
							toSend.setMessageType(MessageData.GAMEPAD_PLAYER_SET);
							toSend.setSessionID(sessionId);
							toSend.setPayload(new byte[]{0, gPlayerID});
							os.write(toSend);
							log.sendCommunicate(MessageData.GAMEPAD_PLAYER_SET, info);
						}

						
						break;
					}
					case MessageData.CONNECTION_GOODBYE: {
						timer.cancel();
						if(this.gPlayerID > 0)
							setTaken[gPlayerID] = false;
						finished = true;
						sendGoodbye(is, os); 
						break;
					}
					
					//KEYBOARD
					case MessageData.KEYBOARD_PRESSED: {
						timer.cancel();
						byte[] payload = md.getPayload();
						int key = (int)payload[0];
						if(key==-39)
							key=KeyEvent.VK_QUOTE;
						int modifiers = (int)payload[1];
						Shortcut sc = new Shortcut(modifiers, key);
						if((Informations.getSystem()==Informations.WINDOWS) && 
								(sc.getModifier1() == KeyEvent.VK_ALT_GRAPH || 
								 sc.getModifier2() == KeyEvent.VK_ALT_GRAPH ||
								 sc.getModifier3() == KeyEvent.VK_ALT_GRAPH))
						{
							if(sc.getModifier1() != KeyEvent.VK_SHIFT && sc.getModifier2() != KeyEvent.VK_SHIFT && sc.getModifier3() != KeyEvent.VK_SHIFT) {
								sc.setModifier1(KeyEvent.VK_CONTROL);
								sc.setModifier2(KeyEvent.VK_ALT);
								sc.setModifier3(KeyEvent.VK_UNDEFINED);
							}else {
								if(sc.getModifier1() == KeyEvent.VK_SHIFT  ){
									sc.setModifier2(KeyEvent.VK_CONTROL);
									sc.setModifier3(KeyEvent.VK_ALT);	
								} else
								if(sc.getModifier2() == KeyEvent.VK_SHIFT){
									sc.setModifier1(KeyEvent.VK_CONTROL);
									sc.setModifier3(KeyEvent.VK_ALT);	
								} else
								if(sc.getModifier3() == KeyEvent.VK_SHIFT){
									sc.setModifier1(KeyEvent.VK_CONTROL);
									sc.setModifier2(KeyEvent.VK_ALT);	
								}
							}
						}

						ShortcutKeys.generateShortcutEvent(sc);
					//	log.sendCommunicate(MessageData.KEYBOARD_PRESSED, info);
						
						break;
					}
					//SENSOR
					case MessageData.GAMEPAD_ACCEL_MOVE: {
						timer = new Timer();
						timer.schedule(new SensorsTask() , delay,period);
						byte[] payload = md.getPayload();
						int val = (int)payload[0];
						int playerNo = (int)payload[1];
						switch(val){
							case Sensor.Y_LEFT: {
								sensors.getItem(playerNo).startYLeft();
								break;
							}
							case Sensor.Y_RIGHT: {
								sensors.getItem(playerNo).startYRight();
								break;
							}
							case Sensor.Z_UP: {
								sensors.getItem(playerNo).startZUp();
								break;
							}
							case Sensor.Z_DOWN: {
								sensors.getItem(playerNo).startZDown();
								break;
							}
							case Sensor.X_LEFT: {
								sensors.getItem(playerNo).startXLeft();
								break;
							}
							case Sensor.X_RIGHT: {
								sensors.getItem(playerNo).startXRight();
								break;
							}
						
						}
						
						break;
					}
					
					case MessageData.GAMEPAD_ACCEL_STOP: {
						timer.cancel();
						byte[] payload = md.getPayload();
						int val = (int)payload[0];
						int playerNo = (int)payload[1];
						switch(val){
							case Sensor.Y_LEFT: {
								sensors.getItem(playerNo).stopYLeft();
								break;
							}
							case Sensor.Y_RIGHT: {
								sensors.getItem(playerNo).stopYRight();
								break;
							}
							case Sensor.Z_UP: {
								sensors.getItem(playerNo).stopZUp();
								break;
							}
							case Sensor.Z_DOWN: {
								sensors.getItem(playerNo).stopZDown();
								break;
							}	
							case Sensor.X_LEFT: {
								sensors.getItem(playerNo).stopXLeft();
								break;
							}
							case Sensor.X_RIGHT: {
								sensors.getItem(playerNo).stopXRight();
								break;
							}
						}
						break;
					}
					
					
					case MessageData.GAMEPAD_KEY_PRESSED: {
						timer = new Timer();
						timer.schedule(new SensorsTask(), delay,period);
						byte[] payload = md.getPayload();
						int val = (int)payload[0];
						int playerNo = (int)payload[1];
						switch(val){
							case Sensor.A: {
								sensors.getItem(playerNo).startA();
								break;
							}
							case Sensor.B: {
								sensors.getItem(playerNo).startB();
								break;
							}
							case Sensor.C: {
								sensors.getItem(playerNo).startC();
								break;
							}
							case Sensor.D: {
								sensors.getItem(playerNo).startD();
								break;
							}
							case Sensor.E: {
								sensors.getItem(playerNo).startE();
								break;
							}
							case Sensor.F: {
								sensors.getItem(playerNo).startF();
								break;
							}
						
						}
						
						break;
					}
					
					case MessageData.GAMEPAD_KEY_RELEASED: {
						timer.cancel();
						byte[] payload = md.getPayload();
						int val = (int)payload[0];
						int playerNo = (int)payload[1];
						switch(val){
							case Sensor.A: {
								sensors.getItem(playerNo).stopA();
								break;
							}
							case Sensor.B: {
								sensors.getItem(playerNo).stopB();
								break;
							}
							case Sensor.C: {
								sensors.getItem(playerNo).stopC();
								break;
							}
							case Sensor.D: {
								sensors.getItem(playerNo).stopD();
								break;
							}
							case Sensor.E: {
								sensors.getItem(playerNo).stopE();
								break;
							}
							case Sensor.F: {
								sensors.getItem(playerNo).stopF();
								break;
							}
						
						}
						
						break;
					}
					
					case MessageData.GAMEPAD_PLAYER_SET:
					{
						timer.cancel();
						
						byte oldSet = md.getPayload()[0];

						
						if (oldSet > 0)
						{	
							skroty.unSet(oldSet);
							synchronized(this){
								setTaken[oldSet] = false;
							}
						}
						byte newSet = md.getPayload()[1];

						if (newSet > 0)
						{
							if (setTaken[newSet] == false)
							{
								skroty.set(newSet);
								synchronized(this){
									setTaken[newSet] = true;
								}
							}
							else 
							{
								newSet = 0;
								skroty.set(newSet);
								synchronized(this){
									setTaken[newSet] = true;
								}
							}
						}

					/*	for(byte i=0;i<=4;i++)
						{
							System.out.println(setTaken[i]);
						}*/
						setTaken[0] = false;
						
						MessageData pakiet2 =new MessageData(MessageData.GAMEPAD_PLAYER_SET,new byte[]{oldSet,newSet});
						os.write(pakiet2);
						log.sendCommunicate(MessageData.GAMEPAD_PLAYER_SET, info);
					
						break;
					}
					
					//MOUSE
					case MessageData.MOUSE_NUMPAD_START_MOVE: {
						timer = new Timer();
						timer.schedule(new MouseTask(), delay,period);
						
						mysz.setAkcja(Mouse.PRESS,md.getPayload());
						break;
					}
					case MessageData.MOUSE_NUMPAD_STOP: {
						timer.cancel();
						mysz.setAkcja(Mouse.RELEASE,md.getPayload());
						break;
					}
					case MessageData.MOUSE_SCREEN_PRESSED: {
						timer.cancel();
						//mysz.setAkcja(Mycha.RELEASE,md.getPayload());
						break;
					}
					case MessageData.MOUSE_SCREEN_MOVE: {
						timer = new Timer();
						timer.schedule(new MouseTask(), delay,period);
						mysz.setAkcja(Mouse.TOUCH,md.getPayload());
						break;
					}
					case MessageData.MOUSE_SCREEN_RELEASED: {
						mysz.setAkcja(Mouse.RELEASE,md.getPayload());
						break;
					}
					

					//Applications hotkeys
					case MessageData.HOTKEY_APPLIST_REQUEST: {
						timer.cancel();
						reloadApps();
						toSend = new MessageData();
						toSend.setMessageType(MessageData.HOTKEY_APPLIST_COUNT);
						byte[] tab = new byte[1];
						tab[0] = (byte)this.numberOfApps;
						toSend.setPayload(tab);
						os.write(toSend);
						log.sendCommunicate(MessageData.HOTKEY_APPLIST_COUNT, info);
						
						for(int i=0;i<this.numberOfApps;i++) {	//sending all Application names
							String nameApp = apps.item(i).getName();
							toSend = new MessageData();
							toSend.setMessageType(MessageData.STRING_EXCHANGE);
							//byte[] appBytes = nameApp.getBytes("UTF-8");
							byte[] appBytes = sr.toBytes(nameApp);
							toSend.setPayload(MessageData.STRING_EXCHANGE,appBytes);
							
							
							os.write(toSend);
							log.sendCommunicate(MessageData.STRING_EXCHANGE, info);
							os.flush();
						}
						
						break;
					}
					case MessageData.HOTKEY_APP_CHOSED: {
						timer.cancel();
						byte[] payload = md.getPayload();
						int numToChoose = (int)payload[0];
						apps.setCurrent(numToChoose);
						break;
					}
					case MessageData.HOTKEY_APPHOTKEY_REQUEST: {
						timer.cancel();
						ApplicationShortcuts currentApp=null;
						try{
							currentApp = apps.getCurrentApplication();
							
							toSend = new MessageData();
							toSend.setMessageType(MessageData.HOTKEY_APPHOTKEY_COUNT);
							byte[] tab = new byte[2];
							tab[0] = (byte)currentApp.getSize();
							toSend.setPayload(tab);
							os.write(toSend);
							log.sendCommunicate(MessageData.HOTKEY_APPHOTKEY_COUNT, info);
							
							for(int i=0;i<currentApp.getSize();i++) { //sending all descriptions of shortcuts
								String shortcutDesc = currentApp.item(i).getDescription();
								toSend = new MessageData();
								toSend.setMessageType(MessageData.STRING_EXCHANGE);
								//byte[] appBytes = shortcutDesc.getBytes("UTF-8");
								byte[] appBytes = sr.toBytes(shortcutDesc);
								toSend.setPayload(appBytes);
								os.write(toSend);
								log.sendCommunicate(MessageData.STRING_EXCHANGE, info);
							}
						}
						catch(IndexOutOfBoundsException e) {
							;
						}
						finally {
						
							break;
						}
					}
					
					case MessageData.HOTKEY_USED: {
						timer.cancel();
						byte[] payload = md.getPayload();
						int numToChoose = (int)payload[0];
						if(numToChoose == 0){
							try{
								Runtime.getRuntime().exec(apps.getCurrentApplication().getPath());
							} catch(Exception ex){
							}
							
						}
						else{
							try{
								Shortcut sc = apps.getCurrentApplication().item(numToChoose-1);
								ShortcutKeys.generateShortcutEvent(sc);
							}
							catch(IndexOutOfBoundsException e){
								;
							}
							finally{
							
								break;
							}
						}
					}	
					
				//	default: break;
				}	//end switch
				
				if(this.finished==true)
					break;
			} //end of while
		
		}//end try
		catch (Exception e1)
		{
			// TODO Auto-generated catch block

		} 
		
		
	}


	/**
	 * @param finished  the finished to set
	 */
	public void finish() {
		sendGoodbye(is, os);
		this.finished = true;
	}

	private void sendGoodbye(MessageInputStream is,
			MessageOutputStream os) {
		
		mysz.releaseAll();
		sensor.releaseAll();
		
		is.close();
		cConnections.remove(info.getMac());
		this.connectionsTable.updateUI();
		MessageData toSend;
		toSend = new MessageData();
		toSend.setMessageType(MessageData.CONNECTION_GOODBYE);
	
		try{
		
			os.write(toSend);
		}
		catch (Exception e){
		}
		os.close();
		
		try {
			in.close();
			polaczenie.close();
		}
		catch (IOException e) {
			mdi.bluetoothConnectionClosingWarning();
		}
		log.endConnection(info);
		
	}
	private void reloadApps() {
		ScXML = new ShortcutsXMLReader();
		try {
			this.apps = ScXML.getApplicationList();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		} catch (SAXException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
	}
	
	protected class MouseTask extends TimerTask {
        public void run() {
        	mysz.releaseAll();
        	mysz=new Mouse();
			mysz.start();
			this.cancel();

        }
    }
	
	protected class SensorsTask extends TimerTask {
        public void run() {
        	sensor.releaseAll();
        	this.cancel();
        }
    }
	protected byte gPlayerID = -1;
	
	protected int delay = 16000;
	protected int period = 5000;
}