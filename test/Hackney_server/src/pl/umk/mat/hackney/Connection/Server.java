package pl.umk.mat.hackney.Connection;


import java.io.IOException;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
//import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.swing.JTable;

import pl.umk.mat.hackney.Devices.Sensor.SensorsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;
import pl.umk.mat.hackney.Utility.AvailabilityKit;
import pl.umk.mat.hackney.Utility.Log;

/**
 * Class is responsible for listening for connections. Makes decision to accept or refuse connection.
 * @author HackneyTeam
 *
 */
public class Server implements Runnable
{
	private static final UUID HACKNEY_UUID = new UUID(MessageData.UUID_JAVA/*"0000112400001000800000805f9b34fb"*/, false);
	private static final String url = "btspp://localhost:" + HACKNEY_UUID.toString() +";name=KopytkoService";
	private byte nextSession;
	private AvailabilityKit skroty;
	//private ApplicationsList apps;
	private SensorsList sensors;
	private ClientConnectionsList ccList;
	private JTable table;
	private boolean finished = false;
	private boolean checkAllowList=true;
	private ClientInfoList allowList;
	private StreamConnection connection;
	private boolean allowToConnect = true;
	private boolean onListening = true;
	private StreamConnectionNotifier socket;
	private Log log;
	private Boolean[] setTaken;
	
	

	/**
	 * 
	 * @param dev
	 * @param apps shortcuts application list
	 * @param sensors defined sensor keys kits
	 * @param ccList connections
	 * @param table connections table
	 * @param allowList allowed applications
	 * @return 
	 */
	public  Server(LocalDevice dev, ApplicationsList apps, SensorsList sensors, ClientConnectionsList ccList, JTable table,ClientInfoList allowList/*, Boolean onListening*/){
		nextSession=40;
	//	this.onListening = onListening;
		//this.apps = apps;
		this.sensors = sensors;
		this.ccList = ccList;
		this.table = table;
		this.allowList = allowList;
		skroty=new AvailabilityKit();	
		log = new Log();
		this.setTaken = new Boolean[5];
	}
	
	public void run()
	{
		setTaken[0]=false;
		setTaken[1]=false;
		setTaken[2]=false;
		
		setTaken[3]=false;
		setTaken[4]=false;
		log.startServer(HACKNEY_UUID.toString());
		
		try
		{
			this.socket = (StreamConnectionNotifier) Connector.open(url);
			
			//System.out.println(sr.getConnectionURL(0, false));
			boolean nasluchuje=true; 
			
			while(!finished && nasluchuje)
			{
		//		System.out.println(url);
				this.connection = socket.acceptAndOpen();
							
				RemoteDevice rd =RemoteDevice.getRemoteDevice(connection);
				
				if(this.onListening && ((checkAllowList && allowList.containMAC(rd.getBluetoothAddress())) || !checkAllowList))
					this.allowToConnect = true;
				else
					this.allowToConnect = false;
				ClientInfo info = new ClientInfo(rd.getFriendlyName(true), rd.getBluetoothAddress());
				ClientConnection cCon = new ClientConnection( info);
				cCon.setHandler(new ClientHandler(connection,skroty,(byte) (nextSession++), sensors,ccList,info,allowToConnect, table, setTaken));
				cCon.createThread();
				cCon.getThread().start();
				ccList.add(cCon);
				table.updateUI();
				
				if(finished==true){
					
					break;
				}
			}	//end while
		} 
		catch (IOException e)
		{
		//	e.printStackTrace();
		}		
	}

	/**
	 * Sets that have to be checked if is on allowed list 
	 * @param checkAllowList the checkAllowList to set
	 */
	public void setCheckAllowList(Boolean checkAllowList) {
		this.checkAllowList = checkAllowList;
	}
	
	/**
	 * @param onListening the onListening to set
	 */
	public void setOnListening(Boolean onListening) {

		this.onListening = onListening;
	//	System.out.println("Stan: " + this.onListening);
	}


	/**
	 * Set listening finish
	 * 
	 */
	public void finish(){
		try {
			//this.polaczenie.close();
			this.socket.close();

			//System.out.println("KONIEC gniazada");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		}
		this.finished = true;
		log.stopServer(HACKNEY_UUID.toString());
	}
	
}