/**
 * 
 */
package pl.umk.mat.hackney.Connection;

import java.util.ArrayList;

/**
 * Contains list of informations about client devices
 * @author HackneyTeam
 *
 */
public class ClientInfoList {

	
	/**
	 * Constructor
	 */
	public ClientInfoList() {
		ccInfo = new ArrayList<ClientInfo>();
	}

	/**
	 * Constructor
	 * @param ccInfo 
	 */
	public ClientInfoList(ArrayList<ClientInfo> ccInfo) {
		this.ccInfo = ccInfo;
	}
	
	/**
	 * Return element from list
	 * @param index element number
	 * @return object
	 */
	public ClientInfo item(int index) {
		return this.ccInfo.get(index);
	}
	
	/**
	 * Returns number of elements
	 * @return number of elements
	 */
	public int size(){
		return this.ccInfo.size();
	}
	
	/**
	 * Adds to list
	 * @param cci element to add
	 */
	public void add(ClientInfo cci){
		this.ccInfo.add(cci);
	}
	
	/**
	 * Removes one or more items form list
	 * @param tab array with indexes to remove from list
	 */
	public void remove(int[] tab){
		for(int i=tab.length-1;i>=0;i--){
			ccInfo.remove(tab[i]);
		}
	}

	/**
	 * Check if device is on list
	 * @param mac Device MAC address
	 * @return true if device is on list otherwise is returned false
	 */
	public boolean containMAC(String mac){
		for(int i=0;i<ccInfo.size();i++){
			if(ccInfo.get(i).getMac().equals(mac)==true)
				return true;
		}
		return false;
		
	}
	
	private ArrayList<ClientInfo> ccInfo;
}
