package pl.umk.mat.hackney.Connection;

/**
 * Contains data about one client connection
 * @author HackneyTeam
 *
 */
public class ClientConnection {

	/**
	 * Constructor
	 * @param info
	 * @param handler
	 */
	public ClientConnection(ClientInfo info) {
		this.info = info;
	}

	/**
	 * @return  the info
	 */
	public ClientInfo getInfo() {
		return info;
	}

	/**
	 * @return  the handler
	 */
	public ClientHandler getHandler() {
		return handler;
	}

	
	/**
	 * @param handler  the handler to set
	 */
	public void setHandler(ClientHandler handler) {
		this.handler = handler;
		
	}

	/**
	 * Returns reference to connection thread
	 * @return  the thread
	 */
	public Thread getThread() {
		return thread;
	}

		
	/**
	 * Create new thread to serve client
	 */
	public void createThread(){
		thread = new Thread(handler);
	}
	
	public void createWifiThread(){
		thread = new Thread(wifihandler);
	}
	

	private ClientInfo info;
	private ClientHandler handler;
	private ClientWifiHandler wifihandler;
	private Thread	thread;
}
