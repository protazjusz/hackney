
package pl.umk.mat.hackney.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import pl.umk.mat.hackney.Connection.ClientInfo;
import pl.umk.mat.hackney.Connection.MessageData;

/**
 * @author HackneyTeam
 *
 */
public class Log {
	
	public void writeEntry(String s)  {
			
		System.out.println(getEntryDateTime()+ " ==>  "+s);
			
	}
	public void newConnection(ClientInfo cci){
		writeEntry("Connected new device\t" + cci.getName()+" "+cci.getMac());
	}
	
	public void endConnection(ClientInfo cci){
		writeEntry("End connection with\t" + cci.getName()+" "+cci.getMac());
	}
	
	public void receiveCommunicate(byte commType, ClientInfo from){
		writeEntry("Recv\t" + getCommunicateText(commType) 
				+ "\tFROM\t" + from.getName()+" "+from.getMac() );
	}
	
	public void sendCommunicate(byte commType, ClientInfo from){
		writeEntry("Send\t" + getCommunicateText(commType) 
				+ "\tTO\t" + from.getName()+" "+from.getMac() );
	}
	
	public void startServer(String url){
		writeEntry("Server started\t"+ url);
	}
	
	public void stopServer(String url){
		writeEntry("Server stopped\t"+ url);
	}
	
	private String getEntryDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
	}
	
	
	private String getCommunicateText(byte communicate){
		switch (communicate){
			case MessageData.CONNECTION_HELLO: {
				return "HELLO";
			}
			case MessageData.CONNECTION_GOODBYE: {
				return "GOODBYE";
			}
			
			case MessageData.MOUSE_NUMPAD_START_MOVE:{
				return "MOUSE NUMPAD START MOVE";
			}
			case MessageData.MOUSE_NUMPAD_STOP:{
				return "MOUSE NUMPAD STOP";
			}
			
			case MessageData.MOUSE_SCREEN_PRESSED:{
				return "MOUSE SCREEN PRESSED";
			}
			
			case MessageData.MOUSE_SCREEN_MOVE:{
				return "MOUSE SCREEN MOVE";
			}
			
			case MessageData.MOUSE_SCREEN_RELEASED:{
				return "MOUSE SCREEN RELEASED";
			}
			
			case MessageData.KEYBOARD_PRESSED:{
				return "KEYBOARD PRESSED";
			}
			
			case MessageData.HOTKEY_APP_CHOSED:{
				return "HOTKEY APP CHOSED";
			}
			
			case MessageData.HOTKEY_APPHOTKEY_COUNT:{
				return "HOTKEY APPHOTKEY COUNT";
			}
			
			case MessageData.STRING_EXCHANGE:{
				return "STRING EXCHANGE";
			}
			
			case MessageData.HOTKEY_APPHOTKEY_REQUEST:{
				return "HOTKEY APPHOTKEY REQUEST";
			}
			
			case MessageData.HOTKEY_APPLIST_COUNT:{
				return "HOTKEY APPLIST COUNT";
			}
			case MessageData.HOTKEY_APPLIST_REQUEST:{
				return "HOTKEY APPLIST REQUEST";
			}
			
			case MessageData.HOTKEY_USED:{
				return "HOTKEY USED";
			}
			
			case MessageData.GAMEPAD_ACCEL_MOVE:{
				return "GAMEPAD ACCEL MOVE";
			}
			
			case MessageData.GAMEPAD_ACCEL_STOP:{
				return "GAMEPAD ACCEL STOP";
			}
			
			case MessageData.GAMEPAD_KEY_PRESSED:{
				return "GAMEPAD KEY PRESSED";
			}
			
			case MessageData.GAMEPAD_KEY_RELEASED:{
				return "GAMEPAD KEY RELEASED";
			}
			
			case MessageData.GAMEPAD_PLAYER_SET:{
				return "GAMEPAD PLAYER SET";
			}
			
			default: {
				return "";

			}
		}

	}
}