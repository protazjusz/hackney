package pl.umk.mat.hackney.Utility;

/**
 * Contains configuration of state application before turn off
 * @author HackneyTeam
 *
 */
public class MainConf {
	
	/**
	 * @return  the defaultLanguage
	 */
	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	/**
	 * @param defaultLanguage  the defaultLanguage to set
	 */
	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	/**
	 * @return  the allowedStatus
	 */
	public boolean isAllowedStatus() {
		return allowedStatus;
	}

	/**
	 * @param allowedStatus  the allowedStatus to set
	 */
	public void setAllowedStatus(boolean allowedStatus) {
		this.allowedStatus = allowedStatus;
	}

	private String defaultLanguage;
	private boolean allowedStatus;
}
