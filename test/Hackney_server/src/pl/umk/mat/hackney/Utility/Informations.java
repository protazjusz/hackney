package pl.umk.mat.hackney.Utility;

import java.io.IOException;

/**
 * 
 * @author HackneyTeam
 *
 */
public class Informations {

	/**
	 * Check local machine operating system 
	 * @return operating system id
	 */
	public static int getSystem(){
		
		if(System.getProperty("os.name").contains("inux")==true)
			return Informations.LINUX;
		else
		if(System.getProperty("os.name").contains("indows")==true)
			return Informations.WINDOWS;
		else
		if(System.getProperty("os.name").contains("ac")==true)
			return Informations.MAC;  
		else
			return Informations.OTHER;
	}
	
	/**
	 * Open url in default browser
	 * @param url url adress to open
	 */
	public static void openSite(String url){
		if(Informations.getSystem() == Informations.WINDOWS){
			try {
				Runtime.getRuntime().exec("cmd.exe /c start " + url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			try {
				Runtime.getRuntime().exec("xdg-open "+url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static byte ID_NO = 13;

	public static final int LINUX = 0;
	public static final int WINDOWS = 1;
	public static final int MAC = 2;
	public static final int OTHER = 3;
}
