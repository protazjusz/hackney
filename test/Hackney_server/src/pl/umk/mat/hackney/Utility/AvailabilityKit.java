package pl.umk.mat.hackney.Utility;

/**
 * 
 * @author HackneyTeam
 *
 */
public class AvailabilityKit  {

	private byte[] gracze;
	private int max;
	
	public AvailabilityKit(){
		max=5;
		gracze=new byte[max];
	}
	
	public boolean isUsed(int x){
		if(gracze[x]!=0) return true; 
				return false;
	}
	
	public byte set(byte y){
		int i=0;
		while(i<max && isUsed(i)) i++;
		if(i==max) return -1;
		gracze[i]=y;
		return y;
	}
	
	public void unSet(int x){
		try {
		gracze[x]=0;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			
		}
	}
	
	
	public byte get(byte x){
		return gracze[x];
	}

}