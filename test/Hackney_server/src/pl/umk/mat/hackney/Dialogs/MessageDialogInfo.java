package pl.umk.mat.hackney.Dialogs;

import java.awt.Frame;

import javax.swing.JOptionPane;

/**
 * Class which contains methods to show message dialogs windows with informations about
 * errors and warnings.
 * @author HackneyTeam
 */
public class MessageDialogInfo {

	/**
	 * Constructor
	 */
	public MessageDialogInfo(){
		frame = new Frame();
	}
	//errors
	
	public void shortcutsFileLoadError(){
		error("Error","Plik konfiguracyjny nie może zostać wczytany!");
	}
	
	public void shortcutsFileParseError(){
		error("Error","Błąd w parsowaniu pliku ze skrótami klawiszowymi!");
	}
	
	public void bluetoothDeviceInitializeError(){
		error("Error","Błąd przy inicjalizacji urządzenia bluetooth!");
	}
	
	public void bluetoothConnectionError() {
		error("Error","Nastąpił błąd połączenia!");
	}
	
	public void applicationChoosedError() {
		error("Error","Błędny wybór aplikacji");
	}
	
	
	//warnings 
	public void bluetoothConnectionClosingWarning() {
		warning("Warning","Występuje problem z zamknięciem połączenia!");
	}
	
	/*
	 * Template to 
	 */
	private void error(String title, String content) {
		JOptionPane.showMessageDialog(frame, content, title, JOptionPane.ERROR_MESSAGE);	
	}
	
	private void warning(String title, String content) {
		JOptionPane.showMessageDialog(frame, content, title, JOptionPane.WARNING_MESSAGE);	
	}
	
	/**
	 * @uml.property  name="frame"
	 */
	private Frame frame;
}
