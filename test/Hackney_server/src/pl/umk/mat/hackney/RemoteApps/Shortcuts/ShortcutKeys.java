
package pl.umk.mat.hackney.RemoteApps.Shortcuts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

/**
 * Class is responsible for operate on key codes and executing shortcuts
 * @author HackneyTeam
 */
public class ShortcutKeys {

	/**
	 * Execute shortcut
	 * @param sc shortcut
	 */
	public static void generateShortcutEvent(Shortcut sc){
		
		Robot rob = null;
		try {
			rob = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			if(sc.getModifier1()!=KeyEvent.VK_UNDEFINED)
				rob.keyPress(sc.getModifier1());
			if(sc.getModifier2()!=KeyEvent.VK_UNDEFINED)
				rob.keyPress(sc.getModifier2());
			if(sc.getModifier3()!=KeyEvent.VK_UNDEFINED)
				rob.keyPress(sc.getModifier3());
			
			
			rob.keyPress(sc.getKey());
			rob.keyRelease(sc.getKey());
			
			if(sc.getModifier3()!=KeyEvent.VK_UNDEFINED)
				rob.keyRelease(sc.getModifier3());
			if(sc.getModifier2()!=KeyEvent.VK_UNDEFINED)
				rob.keyRelease(sc.getModifier2());
			if(sc.getModifier1()!=KeyEvent.VK_UNDEFINED)
				rob.keyRelease(sc.getModifier1());
			}
		catch(IllegalArgumentException ex){
			
		}
	}
	
	/**
	 * Turn key string name into key code
	 * @param key key name
	 * @return key code
	 */
	public int getKeyCode(String key) {
		//Start modifiers
		if (key.equals(""))
			return KeyEvent.VK_UNDEFINED;
		else
		if (key.equals("SHIFT"))
			return KeyEvent.VK_SHIFT;
		else
			if (key.equals("ALT"))
				return KeyEvent.VK_ALT;
		else
			if (key.equals("ALT GRAPH"))
				return KeyEvent.VK_ALT_GRAPH;
		else
			if (key.equals("CONTROL"))
				return KeyEvent.VK_CONTROL;
		else
			if (key.equals("ENTER"))
				return KeyEvent.VK_ENTER;
		else
			if (key.equals("SUPER"))
				return KeyEvent.VK_WINDOWS;
		//End modifiers
		else
			if (key.equals("DELETE"))
				return KeyEvent.VK_DELETE;
		else
			if (key.equals("PG UP"))
				return KeyEvent.VK_PAGE_UP;
		else
			if (key.equals("PG DOWN"))
				return KeyEvent.VK_PAGE_DOWN;
		else
			if (key.equals("HOME"))
				return KeyEvent.VK_HOME;
		else
			if (key.equals("END"))
				return KeyEvent.VK_END;
		else
			if (key.equals("TAB"))
				return KeyEvent.VK_TAB;
		else
			if (key.equals("ESCAPE"))
				return KeyEvent.VK_ESCAPE;
		
		else
			if (key.equals("SPACE"))
				return KeyEvent.VK_SPACE;
		else
			if (key.equals(","))
				return KeyEvent.VK_COMMA;
		else
			if (key.equals("-"))
				return KeyEvent.VK_MINUS;
		else
			if (key.equals("+"))
				return KeyEvent.VK_ADD;
		
		//Arrows
		else
			if(key.equals("UP"))
				return KeyEvent.VK_UP;
		else
			if(key.equals("DOWN"))
				return KeyEvent.VK_DOWN;
		else
			if(key.equals("LEFT"))
				return KeyEvent.VK_LEFT;
		else
			if(key.equals("RIGHT"))
				return KeyEvent.VK_RIGHT;
		
		//Start alpha
		else
			if (key.equals("A"))
				return KeyEvent.VK_A;
		else
			if (key.equals("B"))
				return KeyEvent.VK_B;
		else
			if (key.equals("C"))
				return KeyEvent.VK_C;
		else
			if (key.equals("D"))
				return KeyEvent.VK_D;
		else
			if (key.equals("E"))
				return KeyEvent.VK_E;
		else
			if (key.equals("F"))
				return KeyEvent.VK_F;
		else
			if (key.equals("G"))
				return KeyEvent.VK_G;
		else
			if (key.equals("H"))
				return KeyEvent.VK_H;
		else
			if (key.equals("I"))
				return KeyEvent.VK_I;
		else
			if (key.equals("J"))
				return KeyEvent.VK_J;
		else
			if (key.equals("K"))
				return KeyEvent.VK_K;
		else
			if (key.equals("L"))
				return KeyEvent.VK_L;
		else
			if (key.equals("M"))
				return KeyEvent.VK_M;
		else
			if (key.equals("N"))
				return KeyEvent.VK_N;
		else
			if (key.equals("O"))
				return KeyEvent.VK_O;
		else
			if (key.equals("P"))
				return KeyEvent.VK_P;
		else
			if (key.equals("Q"))
				return KeyEvent.VK_Q;
		else
			if (key.equals("R"))
				return KeyEvent.VK_R;
		else
			if (key.equals("S"))
				return KeyEvent.VK_S;
		else
			if (key.equals("T"))
				return KeyEvent.VK_T;
		else
			if (key.equals("U"))
				return KeyEvent.VK_U;
		else
			if (key.equals("V"))
				return KeyEvent.VK_V;
		else
			if (key.equals("W"))
				return KeyEvent.VK_W;
		else
			if (key.equals("X"))
				return KeyEvent.VK_X;
		else
			if (key.equals("Y"))
				return KeyEvent.VK_Y;
		else
			if (key.equals("Z"))
				return KeyEvent.VK_Z;
		//End alpha
		//Start numerical
		else
			if (key.equals("0"))
				return KeyEvent.VK_0;
		else
			if (key.equals("1"))
				return KeyEvent.VK_1;
		else
			if (key.equals("2"))
				return KeyEvent.VK_2;
		else
			if (key.equals("3"))
				return KeyEvent.VK_3;
		else
			if (key.equals("4"))
				return KeyEvent.VK_4;
		else
			if (key.equals("5"))
				return KeyEvent.VK_5;
		else
			if (key.equals("6"))
				return KeyEvent.VK_6;
		else
			if (key.equals("7"))
				return KeyEvent.VK_7;
		else
			if (key.equals("8"))
				return KeyEvent.VK_8;
		else
			if (key.equals("9"))
				return KeyEvent.VK_9;
	//End numerical
		else
			if (key.equals("F1"))
				return KeyEvent.VK_F1;
		else
			if (key.equals("F2"))
				return KeyEvent.VK_F2;
		else
			if (key.equals("F3"))
				return KeyEvent.VK_F3;
		else
			if (key.equals("F4"))
				return KeyEvent.VK_F4;
		else
			if (key.equals("F5"))
				return KeyEvent.VK_F5;
		else
			if (key.equals("F6"))
				return KeyEvent.VK_F6;
		else
			if (key.equals("F7"))
				return KeyEvent.VK_F7;
		else
			if (key.equals("F8"))
				return KeyEvent.VK_F8;
		else
			if (key.equals("F9"))
				return KeyEvent.VK_F9;
		else
			if (key.equals("F10"))
				return KeyEvent.VK_F10;
		else
			if (key.equals("F11"))
				return KeyEvent.VK_F11;
		else
			if (key.equals("F12"))
				return KeyEvent.VK_F12;
		else
			if (key.equals("F13"))
				return KeyEvent.VK_F13;
		else
			if (key.equals("F14"))
				return KeyEvent.VK_F14;
		else
			if (key.equals("F15"))
				return KeyEvent.VK_F15;
		
		else
		return KeyEvent.VK_UNDEFINED;
	}
	
	/**
	 * Turn key code into string name
	 * @param keyCode keyboard key code
	 * @return key name string
	 */
	public String getKey(int keyCode) {
		
		if(keyCode==KeyEvent.VK_SHIFT)
			return "SHIFT";
		if(keyCode==KeyEvent.VK_ALT)
			return "ALT";
		if(keyCode==KeyEvent.VK_ALT_GRAPH)
			return "ALT GRAPH";
		if(keyCode==KeyEvent.VK_WINDOWS)
			return "SUPER";
		if(keyCode==KeyEvent.VK_CONTROL)
			return "CONTROL";
		if(keyCode==KeyEvent.VK_ENTER)
			return "ENTER";
		
		if(keyCode == KeyEvent.VK_DELETE)
			return "DELETE";
		if(keyCode == KeyEvent.VK_PAGE_UP)
			return "PG UP";
		if(keyCode == KeyEvent.VK_PAGE_DOWN)
			return "PG DOWN";
		if(keyCode == KeyEvent.VK_END)
			return "END";
		if(keyCode == KeyEvent.VK_HOME)
			return "HOME";
		if(keyCode == KeyEvent.VK_TAB)
			return "TAB";
		if(keyCode == KeyEvent.VK_ESCAPE)
			return "ESCAPE";
		
		if(keyCode==KeyEvent.VK_SPACE)
			return "SPACE";
		if(keyCode==KeyEvent.VK_COMMA)
			return ",";
		if(keyCode==KeyEvent.VK_ADD)
			return "+";
		if(keyCode==KeyEvent.VK_MINUS)
			return "-";
		
		//ARROWS
		if(keyCode==KeyEvent.VK_UP)
			return "UP";
		if(keyCode==KeyEvent.VK_LEFT)
			return "LEFT";
		if(keyCode==KeyEvent.VK_RIGHT)
			return "RIGHT";
		if(keyCode==KeyEvent.VK_DOWN)
			return "DOWN";
		
		//LETTERS
		if(keyCode==KeyEvent.VK_A)
			return "A";
		if(keyCode==KeyEvent.VK_B)
			return "B";
		if(keyCode==KeyEvent.VK_C)
			return "C";
		if(keyCode==KeyEvent.VK_D)
			return "D";
		if(keyCode==KeyEvent.VK_E)
			return "E";
		if(keyCode==KeyEvent.VK_E)
			return "E";
		if(keyCode==KeyEvent.VK_F)
			return "F";
		if(keyCode==KeyEvent.VK_G)
			return "G";
		if(keyCode==KeyEvent.VK_H)
			return "H";
		if(keyCode==KeyEvent.VK_I)
			return "I";
		if(keyCode==KeyEvent.VK_J)
			return "J";
		if(keyCode==KeyEvent.VK_K)
			return "K";
		if(keyCode==KeyEvent.VK_L)
			return "L";
		if(keyCode==KeyEvent.VK_M)
			return "M";
		if(keyCode==KeyEvent.VK_N)
			return "N";
		if(keyCode==KeyEvent.VK_O)
			return "O";
		if(keyCode==KeyEvent.VK_P)
			return "P";
		if(keyCode==KeyEvent.VK_Q)
			return "Q";
		if(keyCode==KeyEvent.VK_R)
			return "R";
		if(keyCode==KeyEvent.VK_S)
			return "S";
		if(keyCode==KeyEvent.VK_T)
			return "T";
		if(keyCode==KeyEvent.VK_U)
			return "U";
		if(keyCode==KeyEvent.VK_V)
			return "V";
		if(keyCode==KeyEvent.VK_W)
			return "W";
		if(keyCode==KeyEvent.VK_X)
			return "X";
		if(keyCode==KeyEvent.VK_Y)
			return "Y";
		if(keyCode==KeyEvent.VK_Z)
			return "Z";
		
		//DIGITS
		if(keyCode==KeyEvent.VK_0)
			return "0";
		if(keyCode==KeyEvent.VK_1)
			return "1";
		if(keyCode==KeyEvent.VK_2)
			return "2";
		if(keyCode==KeyEvent.VK_3)
			return "3";
		if(keyCode==KeyEvent.VK_4)
			return "4";
		if(keyCode==KeyEvent.VK_5)
			return "5";
		if(keyCode==KeyEvent.VK_6)
			return "6";
		if(keyCode==KeyEvent.VK_7)
			return "7";
		if(keyCode==KeyEvent.VK_8)
			return "8";
		if(keyCode==KeyEvent.VK_9)
			return "9";
		
		//F
		if(keyCode==KeyEvent.VK_F1)
			return "F1";
		if(keyCode==KeyEvent.VK_F2)
			return "F2";
		if(keyCode==KeyEvent.VK_F3)
			return "F3";
		if(keyCode==KeyEvent.VK_F4)
			return "F4";
		if(keyCode==KeyEvent.VK_F5)
			return "F5";
		if(keyCode==KeyEvent.VK_F6)
			return "F6";
		if(keyCode==KeyEvent.VK_F7)
			return "F7";
		if(keyCode==KeyEvent.VK_F8)
			return "F8";
		if(keyCode==KeyEvent.VK_F9)
			return "F9";
		if(keyCode==KeyEvent.VK_F10)
			return "F10";
		if(keyCode==KeyEvent.VK_F11)
			return "F11";
		if(keyCode==KeyEvent.VK_F12)
			return "F12";
		if(keyCode==KeyEvent.VK_F13)
			return "F13";
		if(keyCode==KeyEvent.VK_F14)
			return "F14";
		if(keyCode==KeyEvent.VK_F15)
			return "F15";
		
			
		return "";
	}
	
	
	public static final String modifiers_items[] = {"","CONTROL","SHIFT","ALT","ALT GRAPH","SUPER"};
	public static final String keys_items[] = {"","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R",
			"S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0","+","-",",",
			"F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","UP","DOWN","LEFT","RIGHT","SPACE",
			"PG UP","PG DOWN","HOME", "END","DELETE", "TAB","ESCAPE" };
	

	public static final String all_items[] = {"","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R",
		"S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0","+","-",",",
		"F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15",
		"UP","DOWN","LEFT","RIGHT","CONTROL","SHIFT","ALT","ALT GRAPH","SUPER", "SPACE",
		"PG UP","PG DOWN","HOME", "END","DELETE", "TAB","ESCAPE"};

}
