package pl.umk.mat.hackney.RemoteApps.Shortcuts;

import java.util.ArrayList;

/**
 * Represents list of applications
 * @author HackneyTeam
 *
 */
public class ApplicationsList {
	
	/**
	 * Constructor
	 */
	public ApplicationsList() {
		this.apps = new ArrayList<ApplicationShortcuts>();
	}
	
	/**
	 * Constructor
	 * @param apps ArrayList with ApplicationShortcuts
	 */
	public ApplicationsList(ArrayList<ApplicationShortcuts> apps) {
		this.apps = apps;
	}
	
	/**
	 * Return number of current application
	 * @return  the current
	 */
	public int getCurrent() {
		return current;
	}
	/**
	 * Set Application as current
	 * @param current  the current to set
	 */
	public void setCurrent(int current) {
		this.current = current;
	}

	/**
	 * Adding application to list
	 * @param as ApplicationShortcuts object
	 */
	public void addApplication(ApplicationShortcuts as) {
		this.apps.add(as);
		
	}
	
	/**
	 * Method returns active application 
	 * @return ApplicationShortcuts object
	 */
	public ApplicationShortcuts getCurrentApplication() throws IndexOutOfBoundsException {
		return this.apps.get(this.current);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getSize() {
		return apps.size();
	}

	/**
	 * Return ApplicationsShortcuts object. Return first object with the same name of application as an argument.
	 * If the application not found method return null
	 * @param name application name
	 * @return	reference to application if found on list otherwise null
	 */
	public ApplicationShortcuts getApplicationByName(String name) {
		for(int i=0;i<this.apps.size();i++) {
			if(name.equals(this.apps.get(i).getName()) == true) {
				return this.apps.get(i);
			}	
		}
		return null;
	}
	
	/**
	 * Remove item from list
	 * @param index number of item
	 */
	public void removeApp(int index){
		apps.remove(index);
		current=0;
	}
	
	/**
	 * Remove application set as current
	 */
	public void removeCurrentApp(){
		apps.remove(current);
		current=0;
	}
	
	/**
	 * Returns applications names
	 * @return array with applications names
	 */
	public String[] getApplicationsNames(){
		
		new ArrayList<String>();
		
		String[] namesArray = new String[this.apps.size()];
		for(int i=0;i<this.apps.size();i++) {
			namesArray[i] = this.apps.get(i).getName();
		}
		return namesArray;
	}
	
	/**
	 * Returns ApplicationShortcuts element
	 * @param index number of element
	 * @return ApplicationShortcuts element
	 */
	public ApplicationShortcuts item(int index) throws IndexOutOfBoundsException {
		return apps.get(index);
	}
	

	public static final int MAX_APPS = 9;
	
	private int current=0;
	private ArrayList<ApplicationShortcuts> apps;
}
