package pl.umk.mat.hackney.RemoteApps.Shortcuts;

import java.awt.event.KeyEvent;

/**
 * Simple class represents shortcut
 * @author HackneyTeam
 *
 */
public class Shortcut {

	/**
	 * Constructor
	 * @param modifier1 modifier1 key code
	 * @param modifier2 modifier2 key code
	 * @param modifier3 modifier3 key code
	 * @param key key code
	 */
	public Shortcut(int modifier1, int modifier2, int modifier3, int key) {
		super();
		this.modifier1 = modifier1;
		this.modifier2 = modifier2;
		this.modifier3 = modifier3;
		this.key = key;
	}
	/**
	 * Constructor
	 * @param modifier1 modifier1 key code
	 * @param modifier2 modifier2 key code
	 * @param modifier3 modifier3 key code
	 * @param key key code
	 * @param description shortcut description 
	 */
	public Shortcut(int modifier1, int modifier2, int modifier3, int key,
			String description) {
		super();
		this.modifier1 = modifier1;
		this.modifier2 = modifier2;
		this.modifier3 = modifier3;
		this.key = key;
		this.description = description;
	}
	
	/**
	 * 
	 * @param modifiers modifiers coded on one byte
	 * @param key key code
	 */
	public Shortcut(int modifiers, int key){

		StringBuffer binaryArray = new StringBuffer("00000000");
		
		for(int i=0;i<=7;i++){
			if(modifiers==0) break;
			if(modifiers%2 == 1)
			{
				binaryArray.setCharAt(i, '1');
			}
			modifiers/=2;
			//System.out.println(binaryArray);
		}
		
		this.key = key;

				
		if(binaryArray.charAt(0) == '1')
			this.modifier1 = KeyEvent.VK_SHIFT;
		else
			this.modifier1 = KeyEvent.VK_UNDEFINED;

		if(binaryArray.charAt(1) == '1')
			this.modifier2 = KeyEvent.VK_CONTROL;
		else
			this.modifier2 = KeyEvent.VK_UNDEFINED;

		if(binaryArray.charAt(2) == '1')
			this.modifier3 = KeyEvent.VK_ALT;
		else
			this.modifier3 = KeyEvent.VK_UNDEFINED;
	
		if(binaryArray.charAt(3) == '1'){
			if(this.modifier3 == KeyEvent.VK_UNDEFINED) 
				this.modifier3 = KeyEvent.VK_ALT_GRAPH;
			else {
				if(this.modifier2 == KeyEvent.VK_UNDEFINED) {
					this.modifier2 = KeyEvent.VK_ALT_GRAPH;
					
				}
				else {
					if(this.modifier1 == KeyEvent.VK_UNDEFINED) 
						this.modifier1 = KeyEvent.VK_ALT_GRAPH;			
				}
			}
		}
		if(binaryArray.charAt(4) == '1') {
			if(this.modifier3 == KeyEvent.VK_UNDEFINED) 
				this.modifier3 = KeyEvent.VK_META;
			else {
				if(this.modifier2 == KeyEvent.VK_UNDEFINED) 
					this.modifier2 = KeyEvent.VK_META;
				else {
					if(this.modifier1 == KeyEvent.VK_UNDEFINED) 
						this.modifier1 = KeyEvent.VK_META;			
				}
			}
		}
	}

	
	/**
	 * Returns modifier1 value
	 * @return  modifier
	 */
	public int getModifier1() {
		return modifier1;
	}
	/**
	 * Sets modifier1 value
	 * @param modifier1  modifier code
	 */
	public void setModifier1(int modifier1) {
		this.modifier1 = modifier1;
	}
	/**
	 * Returns modifier2 value
	 * @return  modifier
	 */
	public int getModifier2() {
		return modifier2;
	}
	/**
	 * Sets modifier2 value
	 * @param modifier2  modifier code
	 */
	public void setModifier2(int modifier2) {
		this.modifier2 = modifier2;
	}
	/**
	 * Returns modifier3 value
	 * @return  modifier
	 */
	public int getModifier3() {
		return modifier3;
	}
	/**
	 * Sets modifier3 value
	 * @param modifier3  modifier code
	 */
	public void setModifier3(int modifier3) {
		this.modifier3 = modifier3;
	}
	
	/**
	 * Returns key code
	 * @param key  key code
	 */
	public int getKey() {
		return key;
	}
	
	/**
	 * Sets key code
	 * @return  key code
	 */
	public void setKey(int key) {
		this.key = key;
	}
	/**
	 * Returns shortcut description
	 * @return  description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Returns shortcut description
	 * @param description  description string
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	private int modifier1;
	private int modifier2;
	private int modifier3;
	private int key;
	private String description; 
}
