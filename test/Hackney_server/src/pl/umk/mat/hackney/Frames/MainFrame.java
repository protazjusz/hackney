package pl.umk.mat.hackney.Frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.LocalDevice;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.xml.sax.SAXException;

import pl.umk.mat.hackney.Connection.ClientInfoList;
import pl.umk.mat.hackney.Connection.ClientConnectionsList;
import pl.umk.mat.hackney.Connection.Server;
import pl.umk.mat.hackney.Connection.ServerWifi;
import pl.umk.mat.hackney.Devices.Sensor.SensorsList;
import pl.umk.mat.hackney.Devices.Sensor.Models.AComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.BComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.CComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.DComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.EComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.FComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.XLeftComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.XRightComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.YLeftComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.YRightComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.ZDownComboModel;
import pl.umk.mat.hackney.Devices.Sensor.Models.ZUpComboModel;
import pl.umk.mat.hackney.Dialogs.MessageDialogInfo;
import pl.umk.mat.hackney.Models.AllowListTableModel;
import pl.umk.mat.hackney.Models.ChooseAppComboBoxModel;
import pl.umk.mat.hackney.Models.ConnectionsTableModel;
import pl.umk.mat.hackney.Models.ShortcutsTableModel;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationShortcuts;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.Shortcut;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ShortcutKeys;
import pl.umk.mat.hackney.Utility.Language;
import pl.umk.mat.hackney.Utility.MainConf;
import pl.umk.mat.hackney.XML.AllowListXML;
import pl.umk.mat.hackney.XML.LanguageXML;
import pl.umk.mat.hackney.XML.MainConfXML;
import pl.umk.mat.hackney.XML.SensorKeysXMLConfiguration;
import pl.umk.mat.hackney.XML.ShortcutsXMLReader;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Creates main frame of application
 * @author HackneyTeam
 *
 */
public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8208560249376935902L;
	private Boolean removeTable[];
	private ShortcutsXMLReader xml;
	private SensorKeysXMLConfiguration sensorXML;
	private AllowListXML allowListXML;
	private MainConfXML mainConfXML;
	private ApplicationsList appList;
	private JTable table;
	private ShortcutsTableModel model;
	private ChooseAppComboBoxModel appModel;
	private JPanel panel_4;
	private JButton addApplicationButton;
	private JButton removeApplicationButton;
	private JButton shortcutAddButton;
	private JScrollPane scrollPane_1;
	private Component horizontalStrut;
	private Component horizontalGlue;
	private JButton btnNewButton_2;
	private JPanel panel_7;
	private JPanel panel_7a;
	private JPanel panel_7b;
	private JPanel panel_7c;
	private JToggleButton tglbtnNewToggleButton;
	private JToggleButton tglbtnWifiToggleButton;
	private Thread serverThread;
	private JPanel sensorKeysTab;
	private JPanel playerKeysSelectPanel;
	private JPanel playerSelectPanel;
	private JRadioButton playerOneRadioButton;
	private JComboBox zUpComboBox;
	private JComboBox fComboBox;
	private JComboBox dComboBox;
	private JComboBox bComboBox;
	private JComboBox eComboBox;
	private JComboBox cComboBox;
	private JComboBox aComboBox;
	private JComboBox zDownComboBox;
	private JComboBox yRightComboBox;
	private JComboBox xRightComboBox;
	private JComboBox yLeftComboBox;
	private JComboBox xLeftComboBox;
	private JRadioButton playerTwoRadioButton;
	private JRadioButton playerThreeRadioButton;
	private JRadioButton playerFourRadioButton;
	private JButton addToAllowListButton;
	private JTable connectionsTable;
	private JScrollPane connectionsScrollPane;
	private JPanel allowListTab;
	private JScrollPane allowListScrollPane;
	private JTable allowListTable;
	private JButton allowItemRemoveButton;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private JPanel appsShortcutsPanel;
	private JPanel connectionsTab;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	private JTextField pathTextField;
	private JPanel chooseAppPanel;
	private JPanel appsShortcutsTopPanel;
	private JComboBox modifiersCombo;
	private JComboBox lettersCombo;
	private JComboBox chooseAppCombo;
	private SensorsList sensorsList;
	private ClientConnectionsList ccList;
	private ConnectionsTableModel connTableModel;
	private ClientInfoList ccInfoList;
	private AllowListTableModel allowListTableModel;
	private AComboModel aComboModel;
	private BComboModel bComboModel;
	private CComboModel cComboModel;
	private DComboModel dComboModel;
	private EComboModel eComboModel;
	private FComboModel fComboModel;
	private XLeftComboModel xLeftComboModel;
	private XRightComboModel xRightComboModel;
	private YLeftComboModel yLeftComboModel;
	private YRightComboModel yRightComboModel;
	private ZUpComboModel zUpComboModel;
	private ZDownComboModel zDownComboModel;
	private Server server;
	
	private JCheckBox chckbxNewCheckBox;
	private MainConf mainConf;
	private LanguageXML languageXML;
	private Language language;
	private AboutFrame aboutFrame;
	private Boolean onListening=Boolean.FALSE;
	private ServerWifi serverwifi;
	private Thread wifiServerThread;
	private JLabel lblStartstop2;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hackney");
					MainFrame frame = new MainFrame();
					//frame.set
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		
		removeTable = new Boolean[ApplicationsList.MAX_APPS];
		clearRemoveTable();
		ccList = new ClientConnectionsList();
		ccInfoList = new ClientInfoList();
		
		mainConfXML = new MainConfXML();
		
		loadMainConf(mainConfXML);
		

		languageXML = new LanguageXML(mainConf.getDefaultLanguage());
		try {
			language = languageXML.getLanguage();
		} catch (SAXException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		} catch (ParserConfigurationException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		}
		allowListXML= new AllowListXML();
		
		loadAllowList(allowListXML);
		
		xml = new ShortcutsXMLReader();
		loadAppList(xml);
		
		sensorXML = new SensorKeysXMLConfiguration();
		loadSensorsList(sensorXML);
		
		this.allowListTableModel = new AllowListTableModel(ccInfoList,language);
		
		
		initGUI();
	}

	private void loadMainConf(MainConfXML mainConfXML) {
		try {
			mainConf = mainConfXML.getConf();
		} catch (SAXException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		} catch (ParserConfigurationException e2) {
			// TODO Auto-generated catch block
	//		e2.printStackTrace();
		}
	}

	private void loadAllowList(AllowListXML allowListXML) {
		try {
			ccInfoList = 
					allowListXML.getAllowedList();

		} catch (SAXException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
	}

	private void loadSensorsList(SensorKeysXMLConfiguration sensorXML) {
		try {
			sensorsList = sensorXML.getSensorsList();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		}
	}

	private void loadAppList(ShortcutsXMLReader xml) {
		try {
			appList = xml.getApplicationList();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		}
	}

	private void clearRemoveTable() {
		for(int i=0;i<ApplicationsList.MAX_APPS;i++) {
			removeTable[i]=Boolean.FALSE;
		}
	}
	private void initGUI() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				ccList.finishAll();
			}
		});
		setIconImage(Toolkit.getDefaultToolkit().getImage("images/ikona.png"));
		//setUndecorated(true);
		setBackground(Color.BLACK);
		setTitle("Hackney");
		setResizable(false);
		setPreferredSize(new Dimension(520, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 280);
		
		
		UIManager.put("TabbedPane.selected", Color.gray);
		UIManager.put("Menu.background",Color.BLACK);
		UIManager.put("Panel.background",Color.BLACK);
		UIManager.put("Button.background", Color.white);
		UIManager.put("Button.foreground", Color.black);
		UIManager.put("ScrollBar.background", Color.gray);
		UIManager.put("OptionPane.background", Color.BLACK);
		UIManager.put("Panel.background", Color.black);
		UIManager.put("OptionPane.messageForeground", Color.white);
		UIManager.put("MenuItem.foreground", Color.white);
		UIManager.put("MenuItem.background", Color.black);
		UIManager.put("Menu.foreground", Color.white);
		UIManager.put("Menu.background", Color.black);
		UIManager.put("Label.foreground", Color.white);

		
		aboutFrame = new AboutFrame();
		model = new ShortcutsTableModel(appList, removeTable,language);
		
		appModel = new ChooseAppComboBoxModel(appList);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Dialog", Font.BOLD, 12));
		tabbedPane.setBorder(null);
		tabbedPane.setForeground(Color.WHITE);
		tabbedPane.setBackground(Color.BLACK);
		new Boolean(Boolean.TRUE);
		
		modifiersCombo = new JComboBox(ShortcutKeys.modifiers_items);
		lettersCombo = new JComboBox(ShortcutKeys.keys_items);
		
		aComboModel = new AComboModel(sensorsList);
		bComboModel = new BComboModel(sensorsList);
		cComboModel = new CComboModel(sensorsList);
		dComboModel = new DComboModel(sensorsList);
		eComboModel = new EComboModel(sensorsList);
		fComboModel = new FComboModel(sensorsList);
		xLeftComboModel = new XLeftComboModel(sensorsList);
		xRightComboModel = new XRightComboModel(sensorsList);
		yLeftComboModel = new YLeftComboModel(sensorsList);
		yRightComboModel = new YRightComboModel(sensorsList);
		zUpComboModel = new ZUpComboModel(sensorsList);
		zDownComboModel = new ZDownComboModel(sensorsList);
		
		chckbxNewCheckBox = new JCheckBox(language.getOnlyAllowed());
		
		shortcutAddButton = new JButton(new ImageIcon("images/list-add.png"));
		
		connTableModel= new ConnectionsTableModel(ccList,language);
		connectionsTable = new JTable(connTableModel);
		connectionsTable.setFillsViewportHeight(true);
		connectionsTable.setForeground(Color.WHITE);
		connectionsTable.setBackground(Color.BLACK);
		connectionsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		startServer(); //Initialize server
		startWifiServer();
		addShortcutState(appList.getCurrentApplication());
		
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		//tabbedPane.set
		tabbedPane.setUI(new BasicTabbedPaneUI() {
            @Override
            protected void installDefaults()
            {
                    super.installDefaults();
                    highlight = TRANSPARENT;
                    lightHighlight = TRANSPARENT;
                    shadow = TRANSPARENT;
                    darkShadow = TRANSPARENT;
                    focus = TRANSPARENT;

                    contentBorderInsets.top = 0;
                    contentBorderInsets.left = 0;
                    contentBorderInsets.right = 0;
                    contentBorderInsets.bottom = 0;
            }
    }); 
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		panel_7 = new JPanel();
		panel_7.setBorder(null);
		panel_7.setBackground(Color.BLACK);
		
		panel_7a = new JPanel();
		panel_7a.setBorder(null);
		panel_7a.setBackground(Color.BLACK);
		
		panel_7b = new JPanel();
		panel_7b.setBorder(null);
		panel_7b.setBackground(Color.BLACK);
		
		panel_7c = new JPanel();
		panel_7c.setBorder(null);
		panel_7c.setBackground(Color.BLACK);
		
		tabbedPane.addTab(language.getMainTab(), null, panel_7, null);
		tglbtnNewToggleButton = new JToggleButton("");
		tglbtnNewToggleButton.setBounds(200, 12, 128, 128);
		tglbtnNewToggleButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
	
				if(tglbtnNewToggleButton.isSelected()==false) {
					server.setOnListening(false);
					ccList.finishAll();
					server.finish();
					
					
				}
				else {
					
					startServer();
					serverThread.start();
				}
			}
		});
		tglbtnWifiToggleButton = new JToggleButton("");
		tglbtnWifiToggleButton.setBounds(200, 12, 128, 128);
		tglbtnWifiToggleButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
	
				if(tglbtnWifiToggleButton.isSelected()==false) {
					serverwifi.setOnListening(false);
					ccList.finishAll();
					serverwifi.finish();
					
					
				}
				else {
					
					startWifiServer();
					wifiServerThread.start();
				}
			}
		});
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.PAGE_AXIS));
		panel_7a.setLayout(new BoxLayout(panel_7a, BoxLayout.LINE_AXIS));
		panel_7a.setAlignmentX(CENTER_ALIGNMENT);
		panel_7b.setLayout(new BoxLayout(panel_7b, BoxLayout.PAGE_AXIS));
		panel_7b.setAlignmentX(CENTER_ALIGNMENT);
		panel_7c.setLayout(new BoxLayout(panel_7c, BoxLayout.PAGE_AXIS));
		panel_7c.setAlignmentX(CENTER_ALIGNMENT);
		
		tglbtnNewToggleButton.setContentAreaFilled(false);
		tglbtnNewToggleButton.setSelectedIcon(new ImageIcon("images/weather-clear-blue.png"));
		tglbtnNewToggleButton.setBackground(SystemColor.controlHighlight);
		tglbtnNewToggleButton.setForeground(SystemColor.control);
		tglbtnNewToggleButton.setBorder(null);
		tglbtnNewToggleButton.setIcon(new ImageIcon("images/weather-clear-night-blue.png"));
		tglbtnNewToggleButton.setAlignmentX(CENTER_ALIGNMENT);
		panel_7b.add(tglbtnNewToggleButton);
		
		tglbtnWifiToggleButton.setContentAreaFilled(false);
		tglbtnWifiToggleButton.setSelectedIcon(new ImageIcon("images/weather-clear.png"));
		tglbtnWifiToggleButton.setBackground(SystemColor.controlHighlight);
		tglbtnWifiToggleButton.setForeground(SystemColor.control);
		tglbtnWifiToggleButton.setBorder(null);
		tglbtnWifiToggleButton.setIcon(new ImageIcon("images/weather-clear-night.png"));
		tglbtnWifiToggleButton.setAlignmentX(CENTER_ALIGNMENT);
		panel_7c.add(tglbtnWifiToggleButton);
		
		
		
		
		lblStartstop = new JLabel(language.getBtCaption());
		lblStartstop.setFont(new Font("Dialog", Font.BOLD, 12));
		lblStartstop.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartstop.setForeground(Color.WHITE);
		lblStartstop.setBackground(Color.BLACK);
		lblStartstop.setBounds(200, 133, 128, 15);
		lblStartstop.setAlignmentX(CENTER_ALIGNMENT);
		
		lblStartstop2 = new JLabel(language.getWifiCaption());
		lblStartstop2.setFont(new Font("Dialog", Font.BOLD, 12));
		lblStartstop2.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartstop2.setForeground(Color.WHITE);
		lblStartstop2.setBackground(Color.BLACK);
		lblStartstop2.setBounds(200, 133, 128, 15);
		lblStartstop2.setAlignmentX(CENTER_ALIGNMENT);
		
		panel_7b.add(lblStartstop);
		panel_7c.add(lblStartstop2);
		
		panel_7a.add(panel_7b);
		panel_7a.add(panel_7c);
		panel_7.add(panel_7a);
		
		
		chckbxNewCheckBox.setBounds(8, 169, 430, 23);
		chckbxNewCheckBox.setForeground(Color.WHITE);
		chckbxNewCheckBox.setBackground(Color.BLACK);
		chckbxNewCheckBox.setAlignmentX(CENTER_ALIGNMENT);
		chckbxNewCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkSelectedCheckBox();
				mainConf.setAllowedStatus(chckbxNewCheckBox.isSelected());
				saveMainConf(mainConfXML);
	
			}
		});
		chckbxNewCheckBox.setSelected(mainConf.isAllowedStatus());
		panel_7.add(chckbxNewCheckBox);
		
		
				
		
		appsShortcutsPanel = new JPanel();
		chooseAppCombo = new JComboBox(appModel);
		chooseAppCombo.setForeground(Color.WHITE);
		chooseAppCombo.setBackground(Color.BLACK);
		chooseAppCombo.setPreferredSize(new Dimension(110, 24));
		chooseAppCombo.setFont(new Font("L M Roman Slant8", chooseAppCombo.getFont().getStyle(), 12));
		appsShortcutsTopPanel = new JPanel();
		appsShortcutsTopPanel.setBorder(null);
		appsShortcutsTopPanel.setBackground(Color.BLACK);
		panel_4 = new JPanel();
		chooseAppPanel = new JPanel();
		chooseAppPanel.setForeground(Color.WHITE);
		chooseAppPanel.setBackground(Color.BLACK);
		panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		pathTextField = new JTextField();
		
		shortcutAddButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		shortcutAddButton.setBorderPainted(false);
		shortcutAddButton.setContentAreaFilled(false);
		shortcutAddButton.setFocusPainted(false);
		shortcutAddButton.setDefaultCapable(false);
		
		//chooseAppCombo.set
		table = new JTable(model);
		//table.setSelectionBackground(UIManager.getColor("Button.disabledToolBarBorderBackground"));
		table.setBorder(null);
		table.setFillsViewportHeight(true);
		table.setForeground(Color.WHITE);
		table.setBackground(Color.BLACK);
		lblNewLabel = new JLabel(language.getPath());
		lblNewLabel.setForeground(Color.WHITE);
		scrollPane_1 = new JScrollPane(table);
		scrollPane_1.setBorder(null);
		
		
		tabbedPane.addTab(language.getShortcutsTab(), null, appsShortcutsPanel, null);
		appsShortcutsPanel.setLayout(new BorderLayout(0, 0));
		
		
		appsShortcutsPanel.add(appsShortcutsTopPanel, BorderLayout.NORTH);
		appsShortcutsTopPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 5));
		
		chooseAppCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearRemoveTable();
				
				appModel.setCurrent(chooseAppCombo.getSelectedIndex());
				addShortcutState(appList.getCurrentApplication());
				pathTextField.setText(appList.getCurrentApplication().getPath());
				model.update();
				
				table.updateUI();
			}
		});
		
		
		appsShortcutsTopPanel.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));
		addApplicationButton = new JButton(new ImageIcon("images/document-new.png"));
		addApplicationButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addApplicationButton.setBorderPainted(false);
		addApplicationButton.setContentAreaFilled(false);
		addApplicationButton.setFocusPainted(false);
		appsShortcutsTopPanel.add(addApplicationButton);
		addApplicationButton.setPreferredSize(new Dimension(32, 32));
		addApplicationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = (String)JOptionPane.showInputDialog(
                new JFrame(),
                language.getGiveApplicationName(),
                language.getNewApplication(),
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                "");
				
				if ((s != null) && (s.length() > 0)) {
			
					appList.addApplication(new ApplicationShortcuts(s,0));

					chooseAppCombo.setSelectedIndex(appList.getSize()-1);
					chooseAppCombo.repaint();
					chooseAppCombo.updateUI();
				}
				
				
			}
		});
		removeApplicationButton = new JButton(new ImageIcon("images/trash.png"));
		removeApplicationButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		removeApplicationButton.setBorderPainted(false);
		removeApplicationButton.setFocusPainted(false);
		removeApplicationButton.setContentAreaFilled(false);
		removeApplicationButton.setPreferredSize(new Dimension(32, 32));
		appsShortcutsTopPanel.add(removeApplicationButton);
		
		removeApplicationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Object[] options = new Object[2];
				options[0] = language.getYes();
				options[1]=language.getNo();
				int n = JOptionPane.showOptionDialog(new JFrame(),
				language.getReallyRemove(),
				language.getQuestion(),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,     //do not use a custom Icon
				options,  //the titles of buttons
				options[0]); //default button title
				
				if(n == JOptionPane.YES_OPTION) {
					appList.removeCurrentApp();
					chooseAppCombo.setSelectedIndex(appList.getCurrent());
					chooseAppCombo.repaint();
				}
			}
		});
		
		
		appsShortcutsTopPanel.add(chooseAppPanel/*, BorderLayout.NORTH*/);
		chooseAppPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		chooseAppPanel.add(chooseAppCombo);
		
		horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(10, 0));
		appsShortcutsTopPanel.add(horizontalStrut);
		
		
		appsShortcutsTopPanel.add(panel_1/*, FlowLayout.*/);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		
		
		panel_1.add(lblNewLabel);
		
		
		pathTextField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(MainFrame.this);

				
		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            pathTextField.setText(file.getPath());
		        }
			}
		});
		

		pathTextField.setText(appList.getCurrentApplication().getPath());
		
		pathTextField.getDocument().addDocumentListener(new DocumentListener() {
			
			public void removeUpdate(DocumentEvent e) {
				// Do nothing
				appList.getCurrentApplication().setPath(pathTextField.getText().toString());
			}
			
			public void insertUpdate(DocumentEvent e) {
				appList.getCurrentApplication().setPath(pathTextField.getText().toString());
			}
			
			public void changedUpdate(DocumentEvent e) {
				// Do nothing
				appList.getCurrentApplication().setPath(pathTextField.getText().toString());
			}
		});
		
		panel_1.add(pathTextField);
		pathTextField.setColumns(13);
		
		
		shortcutAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				appList.getCurrentApplication().addShortcut(new Shortcut(KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, ""));
				addShortcutState(appList.getCurrentApplication());
				table.updateUI();
			}
		});
		shortcutAddButton.setPreferredSize(new Dimension(32, 32));
		appsShortcutsTopPanel.add(shortcutAddButton);
		
		btnNewButton_2 = new JButton("");
		btnNewButton_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton_2.setBorderPainted(false);
		btnNewButton_2.setContentAreaFilled(false);
		btnNewButton_2.setFocusPainted(false);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for(int i = appList.getCurrentApplication().getSize()-1;i>=0;i--){
					if(removeTable[i].equals(Boolean.TRUE)) {
						appList.getCurrentApplication().remove(i);
					}
				}
				clearRemoveTable();
				addShortcutState(appList.getCurrentApplication());
				table.updateUI();
				model.update();
			}
		});
		btnNewButton_2.setIcon(new ImageIcon("images/edit-delete.png"));
		btnNewButton_2.setPreferredSize(new Dimension(32, 32));
		appsShortcutsTopPanel.add(btnNewButton_2);
		
		horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setSize(new Dimension(10, 0));
		appsShortcutsTopPanel.add(horizontalGlue);
				
				
				
				
		table.getColumnModel().getColumn(ShortcutsTableModel.MODIF1_COLUMN).setCellEditor(new DefaultCellEditor(this.modifiersCombo));
		table.getColumnModel().getColumn(ShortcutsTableModel.MODIF2_COLUMN).setCellEditor(new DefaultCellEditor(this.modifiersCombo));
		table.getColumnModel().getColumn(ShortcutsTableModel.MODIF3_COLUMN).setCellEditor(new DefaultCellEditor(this.modifiersCombo));
		table.getColumnModel().getColumn(ShortcutsTableModel.KEY_COLUMN).setCellEditor(new DefaultCellEditor(this.lettersCombo));
					
		appsShortcutsPanel.add(scrollPane_1, BorderLayout.CENTER);

		sensorKeysTab = new JPanel();
		sensorKeysTab.setForeground(Color.WHITE);
		sensorKeysTab.setBackground(Color.BLACK);
		sensorKeysTab.setPreferredSize(new Dimension(0,218));
		tabbedPane.addTab(language.getSensorsTab(), null, sensorKeysTab, null);
		tabbedPane.setEnabledAt(2, true);
		
		playerSelectPanel = new JPanel();
		playerSelectPanel.setForeground(Color.WHITE);
		playerSelectPanel.setBackground(Color.BLACK);
		playerSelectPanel.setBounds(12, 12, 100, 194);
		playerSelectPanel.setToolTipText("");
		playerSelectPanel.setBorder(new TitledBorder(new LineBorder(new Color(238, 238, 238)), language.getPlayer(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		sensorKeysTab.setLayout(null);
		sensorKeysTab.add(playerSelectPanel);
		playerSelectPanel.setLayout(new BoxLayout(playerSelectPanel, BoxLayout.Y_AXIS));
		
		playerOneRadioButton = new JRadioButton(language.getPlayer1());
		playerOneRadioButton.setFocusPainted(false);
		playerOneRadioButton.setForeground(Color.WHITE);
		playerOneRadioButton.setBackground(Color.BLACK);
		playerOneRadioButton.setSelected(true);
		playerOneRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerTwoRadioButton.setSelected(false);
				playerThreeRadioButton.setSelected(false);
				playerFourRadioButton.setSelected(false);
				changePlayerNumber(1);
			}
		});
		
		playerSelectPanel.add(playerOneRadioButton);
		
		playerTwoRadioButton = new JRadioButton(language.getPlayer2());
		playerTwoRadioButton.setFocusPainted(false);
		playerTwoRadioButton.setForeground(Color.WHITE);
		playerTwoRadioButton.setBackground(Color.BLACK);
		playerTwoRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerOneRadioButton.setSelected(false);
				playerThreeRadioButton.setSelected(false);
				playerFourRadioButton.setSelected(false);
				changePlayerNumber(2);
			}
		});
		playerSelectPanel.add(playerTwoRadioButton);
		
		playerThreeRadioButton = new JRadioButton(language.getPlayer3());
		playerThreeRadioButton.setFocusPainted(false);
		playerThreeRadioButton.setForeground(Color.WHITE);
		playerThreeRadioButton.setBackground(Color.BLACK);
		playerThreeRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerTwoRadioButton.setSelected(false);
				playerOneRadioButton.setSelected(false);
				playerFourRadioButton.setSelected(false);
				changePlayerNumber(3);
			}
		});
		playerSelectPanel.add(playerThreeRadioButton);
		
		playerFourRadioButton = new JRadioButton(language.getPlayer4());
		playerFourRadioButton.setFocusPainted(false);
		playerFourRadioButton.setForeground(Color.WHITE);
		playerFourRadioButton.setBackground(Color.BLACK);
		playerFourRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerTwoRadioButton.setSelected(false);
				playerThreeRadioButton.setSelected(false);
				playerOneRadioButton.setSelected(false);
				changePlayerNumber(4);
			}
		});
		playerSelectPanel.add(playerFourRadioButton);
		
		playerKeysSelectPanel = new JPanel();
		playerKeysSelectPanel.setForeground(Color.WHITE);
		playerKeysSelectPanel.setBackground(Color.BLACK);
		playerKeysSelectPanel.setBorder(new TitledBorder(new LineBorder(new Color(255, 255, 255)), language.getKeys(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		playerKeysSelectPanel.setBounds(124, 12, 363, 194);
		sensorKeysTab.add(playerKeysSelectPanel);
		playerKeysSelectPanel.setLayout(null);
		
		fComboBox = new JComboBox(fComboModel);
		fComboBox.setBackground(Color.BLACK);
		fComboBox.setForeground(Color.WHITE);
		fComboBox.setName("");
		fComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getButton() +" F", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		fComboBox.setBounds(275, 135, 76, 47);
		playerKeysSelectPanel.add(fComboBox);
		
		dComboBox = new JComboBox(dComboModel);
		dComboBox.setBackground(Color.BLACK);
		dComboBox.setForeground(Color.WHITE);
		dComboBox.setName("");
		dComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getButton() +" D", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		dComboBox.setBounds(275, 76, 76, 47);
		playerKeysSelectPanel.add(dComboBox);
		
		zUpComboBox = new JComboBox(zUpComboModel);
		zUpComboBox.setBackground(Color.BLACK);
		zUpComboBox.setForeground(Color.WHITE);
		zUpComboBox.setBorder(new TitledBorder(null, language.getForward(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		zUpComboBox.setName("");
		
		zUpComboBox.setBounds(11, 119, 76, 47);
		playerKeysSelectPanel.add(zUpComboBox);
		
		bComboBox = new JComboBox(bComboModel);
		bComboBox.setBackground(Color.BLACK);
		bComboBox.setForeground(Color.WHITE);
		bComboBox.setName("");
		bComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getButton() +" B", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		bComboBox.setBounds(275, 17, 76, 47);
		playerKeysSelectPanel.add(bComboBox);
		
		eComboBox = new JComboBox(eComboModel);
		eComboBox.setForeground(Color.WHITE);
		eComboBox.setBackground(Color.BLACK);
		eComboBox.setName("");
		eComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getButton() +" E", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		eComboBox.setBounds(187, 135, 76, 47);
		playerKeysSelectPanel.add(eComboBox);
		
		cComboBox = new JComboBox(cComboModel);
		cComboBox.setForeground(Color.WHITE);
		cComboBox.setBackground(Color.BLACK);
		cComboBox.setName("");
		cComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)),language.getButton() + " C", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		cComboBox.setBounds(187, 76, 76, 47);
		playerKeysSelectPanel.add(cComboBox);
		
		aComboBox = new JComboBox(aComboModel);
		aComboBox.setForeground(Color.WHITE);
		aComboBox.setBackground(Color.BLACK);
		aComboBox.setName("");
		aComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)),language.getButton() + " A", TitledBorder.LEADING, TitledBorder.TOP, null, Color.GREEN));
		aComboBox.setBounds(187, 17, 76, 47);
		playerKeysSelectPanel.add(aComboBox);
		
		zDownComboBox = new JComboBox(zDownComboModel);
		zDownComboBox.setForeground(Color.WHITE);
		zDownComboBox.setBackground(Color.BLACK);
		zDownComboBox.setName("");
		zDownComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getBack(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		zDownComboBox.setBounds(99, 119, 76, 47);
		playerKeysSelectPanel.add(zDownComboBox);
		
		yRightComboBox = new JComboBox(yRightComboModel);
		yRightComboBox.setForeground(Color.WHITE);
		yRightComboBox.setBackground(Color.BLACK);
		yRightComboBox.setName("");
		yRightComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getRight(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		yRightComboBox.setBounds(99, 40, 76, 47);
		playerKeysSelectPanel.add(yRightComboBox);
		
		xRightComboBox = new JComboBox(xRightComboModel);
		xRightComboBox.setVisible(false);
		xRightComboBox.setName("");
		xRightComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "X Prawo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		xRightComboBox.setBounds(99, 17, 76, 47);
		playerKeysSelectPanel.add(xRightComboBox);
		
		yLeftComboBox = new JComboBox(yLeftComboModel);
		yLeftComboBox.setForeground(Color.WHITE);
		yLeftComboBox.setBackground(Color.BLACK);
		yLeftComboBox.setName("");
		yLeftComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), language.getLeft(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		yLeftComboBox.setBounds(12, 40, 76, 47);
		playerKeysSelectPanel.add(yLeftComboBox);
		
		xLeftComboBox = new JComboBox(xLeftComboModel);
		xLeftComboBox.setVisible(false);
		xLeftComboBox.setName("");
		xLeftComboBox.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "X Lewo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		xLeftComboBox.setBounds(12, 17, 76, 47);
		playerKeysSelectPanel.add(xLeftComboBox);
		
		optionsTab = new JPanel();
		optionsTab.setForeground(Color.WHITE);
		optionsTab.setBackground(Color.BLACK);
		tabbedPane.addTab(language.getOptions(), null, optionsTab, null);
		optionsTab.setLayout(null);
		
		saveAllButton = new JButton("");
		saveAllButton.addActionListener(new SaveAll());
		saveAllButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		saveAllButton.setFocusPainted(false);
		saveAllButton.setContentAreaFilled(false);
		saveAllButton.setBorderPainted(false);
		saveAllButton.setIcon(new ImageIcon("images/media-floppy.png"));
		saveAllButton.setBounds(12, 12, 58, 58);
		optionsTab.add(saveAllButton);
		
		clearChangesSettings = new JButton("");
		clearChangesSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Object[] options = new Object[2];
				options[0] = language.getYes();
				options[1]=language.getNo();
				int n = JOptionPane.showOptionDialog(
					    new JFrame(),
					    language.getClearChangesQuestion(),
					    language.getQuestion(),
					    JOptionPane.YES_NO_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
						null,     //do not use a custom Icon
						options,  //the titles of buttons
						options[0]); //default button title
				if(n==JOptionPane.YES_OPTION){
					loadAppList(xml);
					loadSensorsList(sensorXML);
					loadAllowList(allowListXML);
					
					updateAllModels();
				}
			}
		});
		clearChangesSettings.setFocusPainted(false);
		clearChangesSettings.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		clearChangesSettings.setContentAreaFilled(false);
		clearChangesSettings.setBorderPainted(false);
		clearChangesSettings.setIcon(new ImageIcon("images/clear-changes.png"));
		clearChangesSettings.setBounds(12, 111, 58, 58);
		optionsTab.add(clearChangesSettings);
		
		defaultSettingsButton = new JButton("");
		defaultSettingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] options = new Object[2];
				options[0] = language.getYes();
				options[1]=language.getNo();
				int n = JOptionPane.showOptionDialog(
					    new JFrame(),
					    language.getDefaultSettingsQuestion(),
					    language.getQuestion(),
					    JOptionPane.YES_NO_OPTION,
					    JOptionPane.QUESTION_MESSAGE,
						null,     //do not use a custom Icon
						options,  //the titles of buttons
						options[0]); //default button title
				
				if(n == JOptionPane.YES_OPTION){
					ShortcutsXMLReader shortcutsXML = new ShortcutsXMLReader("conf.xml");
					loadAppList(shortcutsXML);
					SensorKeysXMLConfiguration senXML = new SensorKeysXMLConfiguration("sensor.xml");
					loadSensorsList(senXML);
					AllowListXML allowXML = new AllowListXML("allowlist.xml");
					loadAllowList(allowXML);
					MainConfXML main_XML = new MainConfXML("mainconf.xml");
					loadMainConf(main_XML);
					
					saveAllowList(allowListXML);
					saveMainConf(mainConfXML);
					saveSensorsConf(sensorXML);
					saveShortcutsAppsConf(xml);
					
					
					updateAllModels();
				}
			}
		});
		defaultSettingsButton.setFocusPainted(false);
		defaultSettingsButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		defaultSettingsButton.setContentAreaFilled(false);
		defaultSettingsButton.setBorderPainted(false);
		defaultSettingsButton.setIcon(new ImageIcon("images/defaults_settings.png"));
		defaultSettingsButton.setBounds(217, 12, 58, 58);
		optionsTab.add(defaultSettingsButton);
		
		aboutButton = new JButton("");
		aboutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!aboutFrame.isVisible())
					aboutFrame.setVisible(true);
			}
		});
		aboutButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		aboutButton.setIcon(new ImageIcon("images/about.png"));
		aboutButton.setFocusPainted(false);
		aboutButton.setContentAreaFilled(false);
		aboutButton.setBorderPainted(false);
		aboutButton.setBounds(217, 111, 58, 58);
		optionsTab.add(aboutButton);
		
		saveAllLabel = new JLabel(language.getSaveAll());
		saveAllLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		saveAllLabel.setForeground(Color.WHITE);
		saveAllLabel.setBackground(Color.BLACK);
		saveAllLabel.setBounds(12, 68, 200, 18);
		optionsTab.add(saveAllLabel);
		
		clearChangesLabel = new JLabel(language.getClearChanges());
		clearChangesLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		clearChangesLabel.setForeground(Color.WHITE);
		clearChangesLabel.setBackground(Color.BLACK);
		clearChangesLabel.setBounds(12, 163, 200, 21);
		optionsTab.add(clearChangesLabel);
		
		defaultSettingsLabel = new JLabel(language.getRestoreDefaults());
		defaultSettingsLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		defaultSettingsLabel.setForeground(Color.WHITE);
		defaultSettingsLabel.setBackground(Color.BLACK);
		defaultSettingsLabel.setBounds(217, 67, 200, 21);
		optionsTab.add(defaultSettingsLabel);
		
		aboutLabel = new JLabel(language.getInfoAbout());
		aboutLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		aboutLabel.setBackground(Color.BLACK);
		aboutLabel.setForeground(Color.WHITE);
		aboutLabel.setBounds(217, 161, 200, 24);
		optionsTab.add(aboutLabel);
		
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setForeground(new Color(255, 255, 255));
		panel.setBorder(new TitledBorder(new LineBorder(new Color(255, 255, 255)), language.getChangeLanguage(), TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		panel.setBounds(407, 12, 110, 173);
		optionsTab.add(panel);
		panel.setLayout(null);
		
		polandButton = new JButton("");
		polandButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainConf.setDefaultLanguage("polski");
				saveMainConf(mainConfXML);
				JOptionPane.showMessageDialog(new JFrame(), language.getRestartApplicationRequest(),language.getInformation(),JOptionPane.INFORMATION_MESSAGE);
			}
		});
		polandButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		polandButton.setBorderPainted(false);
		polandButton.setBounds(29, 23, 58, 58);
		panel.add(polandButton);
		polandButton.setContentAreaFilled(false);
		polandButton.setFocusPainted(false);
		polandButton.setIcon(new ImageIcon("images/poland.png"));
		
		btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainConf.setDefaultLanguage("english");
				saveMainConf(mainConfXML);
				JOptionPane.showMessageDialog(new JFrame(), language.getRestartApplicationRequest(), language.getInformation(),JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setFocusPainted(false);
		btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNewButton.setIcon(new ImageIcon("images/uk.png"));
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setBounds(29, 103, 58, 58);
		panel.add(btnNewButton);
	
		modifiersCombo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				table.repaint();
				
			}
		});
		
		lettersCombo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
		
				//model.setValueAt(modifiersCombo.getSelectedItem(), table.getSelectedRow(), table.getSelectedColumn());
				table.repaint();
			}
		});
		connectionsTab = new JPanel();
		connectionsTab.setForeground(Color.WHITE);
		connectionsTab.setBackground(Color.BLACK);
		
		
		
		tabbedPane.addTab(language.getConnectionsTab(), null, connectionsTab, null);
		connectionsTab.setLayout(null);
		
		connectionsScrollPane = new JScrollPane();
		connectionsScrollPane.setBounds(12, 12, 376, 200);
		connectionsTab.add(connectionsScrollPane);
		
		connectionsScrollPane.setViewportView(connectionsTable);
		connectionsTable.updateUI();
		
		addToAllowListButton = new JButton();
		addToAllowListButton.setFocusPainted(false);
		addToAllowListButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		addToAllowListButton.setContentAreaFilled(false);
		addToAllowListButton.setBorderPainted(false);
		addToAllowListButton.setIcon(new ImageIcon("images/allow.png"));
		addToAllowListButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tab[] = connectionsTable.getSelectedRows();
				boolean showDialog = false;
				synchronized (ccList) {
					
					for(int i=0;i<tab.length;i++){
						if(ccInfoList.containMAC(ccList.item(tab[i]).getInfo().getMac())==false){
							ccInfoList.add(ccList.item(tab[i]).getInfo());
							showDialog = true;
						}
					}
				}
				allowListTable.updateUI();
				if(tab.length>0 && showDialog == true)
				JOptionPane.showMessageDialog(new JFrame(), language.getAddedToAllowList(), language.getInformation(),JOptionPane.INFORMATION_MESSAGE);

			}
		});
		addToAllowListButton.setBounds(420, 12, 75, 74);
		connectionsTab.add(addToAllowListButton);
		
		addToAllowedLabel = new JLabel(language.getAddToWhiteList());
		addToAllowedLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		addToAllowedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		addToAllowedLabel.setForeground(Color.WHITE);
		addToAllowedLabel.setBackground(Color.BLACK);
		addToAllowedLabel.setBounds(389, 88, 140, 15);
		connectionsTab.add(addToAllowedLabel);
		
		button = new JButton("");
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tab[] = null;
				try{
					 tab = connectionsTable.getSelectedRows();
				
			//	if(connectionsTable.getSelectedRows().>0){
					synchronized (ccList) {
						
						for(int i=0;i<tab.length;i++){
							//if(ccInfoList.containMAC(ccList.item(i).getInfo().getMac())==false)
								ccList.finish(ccList.item(tab[i]).getInfo().getMac());
						}
					}
				} catch(Exception ex){}
				//}
			}
		});
		button.setFocusPainted(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setIcon(new ImageIcon("images/trash_large.png"));
		button.setBounds(420, 115, 75, 74);
		connectionsTab.add(button);
		
		lblNewLabel_1 = new JLabel(language.getRemove());
		lblNewLabel_1.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBackground(Color.BLACK);
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(420, 190, 75, 15);
		connectionsTab.add(lblNewLabel_1);
		
		allowListTab = new JPanel();
		allowListTab.setForeground(Color.WHITE);
		allowListTab.setBackground(Color.BLACK);
		tabbedPane.addTab(language.getAllowedTab(), null, allowListTab, null);
		tabbedPane.setForegroundAt(5, Color.WHITE);
		tabbedPane.setBackgroundAt(5, Color.BLACK);
		allowListTab.setLayout(null);
		
		allowListScrollPane = new JScrollPane();
		allowListScrollPane.setForeground(Color.WHITE);
		allowListScrollPane.setBorder(null);
		allowListScrollPane.setBackground(Color.BLACK);
		allowListScrollPane.setViewportBorder(null);
		allowListScrollPane.setBounds(12, 12, 376, 200);
		allowListTab.add(allowListScrollPane);
		
		allowListTable = new JTable(allowListTableModel);
		allowListTable.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent e) {
				saveAllowList(allowListXML);
			}
			@Override
			public void componentRemoved(ContainerEvent e) {
				saveAllowList(allowListXML);
			}
		});
		allowListTable.setGridColor(Color.BLACK);
		allowListTable.setBorder(null);
		allowListTable.setForeground(Color.WHITE);
		allowListTable.setFillsViewportHeight(true);
		allowListTable.setBackground(Color.BLACK);
		allowListScrollPane.setViewportView(allowListTable);
		
		allowItemRemoveButton = new JButton();
		allowItemRemoveButton.setFocusPainted(false);
		allowItemRemoveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		allowItemRemoveButton.setBorderPainted(false);
		allowItemRemoveButton.setContentAreaFilled(false);
		allowItemRemoveButton.setIcon(new ImageIcon("images/trash_large.png"));
		allowItemRemoveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ccInfoList.remove(allowListTable.getSelectedRows());
				allowListTable.updateUI();
			}
		});
		allowItemRemoveButton.setBounds(421, 12, 75, 74);
		allowListTab.add(allowItemRemoveButton);
		
		removeFromAllowedLabel = new JLabel(language.getRemove());
		removeFromAllowedLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		removeFromAllowedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		removeFromAllowedLabel.setForeground(Color.WHITE);
		removeFromAllowedLabel.setBackground(Color.BLACK);
		removeFromAllowedLabel.setBounds(388, 88, 141, 15);
		allowListTab.add(removeFromAllowedLabel);
	}
	
	private void startServer(){
		MessageDialogInfo di = new MessageDialogInfo();
		try 
		{
			
			LocalDevice ld=LocalDevice.getLocalDevice();
		//	ld.setDiscoverable(DiscoveryAgent.GIAC);
			this.server=new Server(ld,appList,sensorsList, ccList,connectionsTable,ccInfoList/*,this.onListening*/);
			this.server.setOnListening(true);
			checkSelectedCheckBox();
			serverThread = new Thread(server);
			
		} 
		catch (BluetoothStateException e)
		{
			di.bluetoothDeviceInitializeError();
			System.exit(0);
		}
	}
	
	private void startWifiServer(){
		MessageDialogInfo di = new MessageDialogInfo();
		try 
		{
			
			LocalDevice ld=LocalDevice.getLocalDevice();
		//	ld.setDiscoverable(DiscoveryAgent.GIAC);
			this.serverwifi=new ServerWifi(ld,appList,sensorsList, ccList,connectionsTable,ccInfoList/*,this.onListening*/);
			this.serverwifi.setOnListening(true);
			checkSelectedCheckBox();
			wifiServerThread = new Thread(serverwifi);
			
		} 
		catch (BluetoothStateException e)
		{
			di.bluetoothDeviceInitializeError();
			System.exit(0);
		}
	}
	
	private void addShortcutState(ApplicationShortcuts as){
		if(as.getSize()>=ApplicationsList.MAX_APPS)
			shortcutAddButton.setEnabled(false);
		else
			shortcutAddButton.setEnabled(true);
	}
	
	private void changePlayerNumber(int index){
		
		aComboModel.setPlayerNo(index);
		aComboBox.updateUI();
		bComboModel.setPlayerNo(index);
		bComboBox.updateUI();
		cComboModel.setPlayerNo(index);
		cComboBox.updateUI();
		dComboModel.setPlayerNo(index);
		dComboBox.updateUI();
		eComboModel.setPlayerNo(index);
		eComboBox.updateUI();
		fComboModel.setPlayerNo(index);
		fComboBox.updateUI();
		xLeftComboModel.setPlayerNo(index);
		xLeftComboBox.updateUI();
		xRightComboModel.setPlayerNo(index);
		xRightComboBox.updateUI();
		yLeftComboModel.setPlayerNo(index);
		yLeftComboBox.updateUI();
		yRightComboModel.setPlayerNo(index);
		yRightComboBox.updateUI();
		zUpComboModel.setPlayerNo(index);
		zUpComboBox.updateUI();
		zDownComboModel.setPlayerNo(index);
		zDownComboBox.updateUI();
	
	}

	private void saveMainConf(MainConfXML mainConfXML){
		try {
			mainConfXML.saveConf(mainConf);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
		}
	}
	
	private void saveAllowList(AllowListXML allowListXML) {
		try {
			allowListXML.saveAllowList(ccInfoList);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerFactoryConfigurationError e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		}
	}

	private void saveSensorsConf(SensorKeysXMLConfiguration sensorXML) {
		try {
			sensorXML.saveSensorsList(sensorsList);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerFactoryConfigurationError e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		}
	};

	private void saveShortcutsAppsConf(ShortcutsXMLReader xml) {
		try {
			xml.saveApplicationsList(appList);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
		//	e1.printStackTrace();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerFactoryConfigurationError e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		} catch (TransformerException e1) {
			// TODO Auto-generated catch block
	//		e1.printStackTrace();
		}
		table.repaint();
	}
	
	private void checkSelectedCheckBox() {
		try {
			if(chckbxNewCheckBox.isSelected())
			{
				server.setCheckAllowList(true);
				serverwifi.setCheckAllowList(true);
			}
			else
			{
				server.setCheckAllowList(false);
				serverwifi.setCheckAllowList(false);
			}
		} catch (NullPointerException e1) {
			// TODO Auto-generated catch block
		//	e1.printStackTrace();
		}
	}


	private void updateAllModels() {
		allowListTableModel.setCcListInfo(ccInfoList);
		
		appModel.setAppList(appList);
		
		model.setAppList(appList);
		chooseAppCombo.setSelectedIndex(0);
		chooseAppCombo.updateUI();
		chooseAppCombo.repaint();
		
		aComboModel.setSensorList(sensorsList);
		bComboModel.setSensorList(sensorsList);
		cComboModel.setSensorList(sensorsList);
		dComboModel.setSensorList(sensorsList);
		eComboModel.setSensorList(sensorsList);
		fComboModel.setSensorList(sensorsList);
		
		xLeftComboModel.setSensorList(sensorsList);
		xRightComboModel.setSensorList(sensorsList);
		yRightComboModel.setSensorList(sensorsList);
		yLeftComboModel.setSensorList(sensorsList);
		
		
		zDownComboModel.setSensorList(sensorsList);
		zUpComboModel.setSensorList(sensorsList);
		
		table.updateUI();
		table.repaint();
	}


	private class SaveAll implements ActionListener{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			saveShortcutsAppsConf(xml);
			saveSensorsConf(sensorXML);
			saveAllowList(allowListXML);		
			saveMainConf(mainConfXML);
			JOptionPane.showMessageDialog(new JFrame(), language.getSaved(), language.getInformation(),JOptionPane.INFORMATION_MESSAGE);
		}		
	}
	

	public static final Color TRANSPARENT = new Color(0, 0, 0, 0);
	private JPanel optionsTab;
	private JButton saveAllButton;
	private JButton clearChangesSettings;
	private JButton defaultSettingsButton;
	private JButton aboutButton;
	private JLabel saveAllLabel;
	private JLabel clearChangesLabel;
	private JLabel defaultSettingsLabel;
	private JLabel aboutLabel;
	private JButton polandButton;
	private JPanel panel;
	private JButton btnNewButton;
	private JLabel lblStartstop;
	private JLabel removeFromAllowedLabel;
	private JLabel addToAllowedLabel;
	private JButton button;
	private JLabel lblNewLabel_1;
}// END MainFrame Class

