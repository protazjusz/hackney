package pl.umk.mat.hackney.Frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import pl.umk.mat.hackney.Utility.Informations;

/**
 * Creates frame about program
 * @author HackneyTeam
 *
 */
public class AboutFrame extends JFrame {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7981320841659682147L;
	private JPanel contentPane;
	private JPanel imagePanel;
	private JPanel panel_1;
	private JTextArea textArea;
	private JButton btnNewButton;
	private JLabel lblHackney;
	private JLabel lblNewLabel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AboutFrame frame = new AboutFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AboutFrame() {
		initGUI();
	}
	private void initGUI() {
		try {
			new URI("http://www.hackney.pl");
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setResizable(false);
		setBackground(Color.BLACK);
		setBounds(100, 100, 480, 375);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		imagePanel = new ImagePanel();
		imagePanel.setBackground(Color.BLACK);
		contentPane.add(imagePanel,BorderLayout.WEST);
		
		panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(200, 10));
		panel_1.setBackground(Color.BLACK);
		contentPane.add(panel_1, BorderLayout.EAST);
		panel_1.setLayout(null);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setFont(new Font("Dialog", Font.PLAIN, 10));
		textArea.setBounds(12, 173, 113, 73);
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);
		textArea.setText("Łukasz Rumiński\nMichał Kutzner\nAdrian Dymek\nSzymon Kwiatkowski\nMarcin Langa");
		panel_1.add(textArea);
		
		btnNewButton = new JButton("");
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Informations.openSite("http://www.hackney.tk");
			}
		});
		btnNewButton.setToolTipText("www.hackney.tk");
		btnNewButton.setFocusPainted(false);
		btnNewButton.setSize(new Dimension(50, 50));
		btnNewButton.setMinimumSize(new Dimension(50, 50));
		btnNewButton.setMaximumSize(new Dimension(50, 50));
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setPreferredSize(new Dimension(50, 50));
		btnNewButton.setVerifyInputWhenFocusTarget(false);
		btnNewButton.setIcon(new ImageIcon("images/www.png"));
		btnNewButton.setBounds(136, 270, 64, 52);
		panel_1.add(btnNewButton);
		
		lblHackney = new JLabel("Hackney 2.0");
		lblHackney.setFont(new Font("URW Bookman L", Font.BOLD, 20));
		lblHackney.setForeground(Color.WHITE);
		lblHackney.setBounds(12, 28, 205, 35);
		panel_1.add(lblHackney);
		
		lblNewLabel = new JLabel("Copyright 2011-2013, Hackney Team");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 9));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(12, 59, 188, 15);
		panel_1.add(lblNewLabel);
	}
	
	
	private class ImagePanel extends JPanel{

	    /**
		 * 
		 */
		private static final long serialVersionUID = 6592186571467831139L;
		private BufferedImage image;

	    public ImagePanel() {
	    	setPreferredSize(new Dimension(240, 320));
	       try {                
	          image = ImageIO.read(new File("images/splash.png"));
	       } catch (IOException ex) {
	            // handle exception...
	       }
	    }

	    @Override
	    public void paintComponent(Graphics g) {
	        g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters

	    }

	}
}
