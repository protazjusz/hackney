package pl.umk.mat.hackney.Models;

import javax.swing.table.AbstractTableModel;

import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationShortcuts;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ApplicationsList;
import pl.umk.mat.hackney.RemoteApps.Shortcuts.ShortcutKeys;
import pl.umk.mat.hackney.Utility.Language;


/**
 * Shortcuts Table model class
 * @author HackneyTeam
 *
 */
public class ShortcutsTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8076428912394629241L;
	/**
	 * Constructor
	 * @param list applications list
	 * @param table remove shortcuts table
	 * @param language language version
	 */
	public ShortcutsTableModel(ApplicationsList list, Boolean table[], Language language) {
		
		this.sck = new ShortcutKeys();
		this.appList = list;
		this.removeTable = table;
		this.columnNames = new String[6];
		
		this.columnNames[0] = language.getColumnModif1();
		this.columnNames[1] = language.getColumnModif2();
		this.columnNames[2] = language.getColumnModif3();
		this.columnNames[3] = language.getColumnKey();
		this.columnNames[4] = language.getColumnDescription();
		this.columnNames[5] = language.getColumnRemove();

		this.update();


	}
	
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return app.getSize();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 6;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex==MODIF1_COLUMN)
			return sck.getKey(app.item(rowIndex).getModifier1());
		else
		if(columnIndex==MODIF2_COLUMN)
			return sck.getKey(app.item(rowIndex).getModifier2());
		else
		if(columnIndex==MODIF3_COLUMN)
			return sck.getKey(app.item(rowIndex).getModifier3());
		else
		if(columnIndex==KEY_COLUMN)
			return sck.getKey(app.item(rowIndex).getKey());
		else
		if(columnIndex==DESCRIPTION_COLUMN)
			return app.item(rowIndex).getDescription();
		else
		if(columnIndex == REMOVE_COLUMN)
			return removeTable[rowIndex];
		else
			return "";

	}
	

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if(columnIndex==MODIF1_COLUMN)
			app.item(rowIndex).setModifier1(sck.getKeyCode((String)aValue));
		if(columnIndex==MODIF2_COLUMN)
			app.item(rowIndex).setModifier2(sck.getKeyCode((String)aValue));
		if(columnIndex==MODIF3_COLUMN)
			app.item(rowIndex).setModifier3(sck.getKeyCode((String)aValue));
		if(columnIndex==KEY_COLUMN)
			app.item(rowIndex).setKey(sck.getKeyCode((String)aValue));
		if(columnIndex==DESCRIPTION_COLUMN)
			app.item(rowIndex).setDescription(((String)aValue));
		if(columnIndex==REMOVE_COLUMN)
			removeTable[rowIndex] = ((Boolean)aValue).booleanValue();
		
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class getColumnClass(int columnIndex) {
		if(columnIndex == REMOVE_COLUMN)
	return Boolean.class;
		else
			return Object.class;
	}


	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return columnNames[column];
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int row,int col){
		
		return true;
	}
	
	public void setCurrentApp(int curr){
		
		this.current = curr;
		app = appList.item(this.current);
	}
	
	/**
	 * Sets Applications List to read by model
	 * @param appList  the appList to set
	 */
	public void setAppList(ApplicationsList appList) {
		this.appList = appList;
	}


	public void update(){
		this.app = appList.getCurrentApplication();
	}
	

	private ApplicationShortcuts app;
	private ApplicationsList appList;
	private int current=0;
	private ShortcutKeys sck = null;
	private Boolean removeTable[]; 
	private String columnNames[];
	
	public static final int MODIF1_COLUMN = 0;
	public static final int MODIF2_COLUMN = 1;
	public static final int MODIF3_COLUMN = 2;
	public static final int KEY_COLUMN 	  = 3;
	public static final int DESCRIPTION_COLUMN = 4;
	public static final int REMOVE_COLUMN = 5;

	
}
