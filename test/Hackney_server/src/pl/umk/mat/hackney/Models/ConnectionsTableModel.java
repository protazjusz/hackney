/**
 * 
 */
package pl.umk.mat.hackney.Models;

import javax.swing.table.AbstractTableModel;

import pl.umk.mat.hackney.Connection.ClientConnectionsList;
import pl.umk.mat.hackney.Utility.Language;


/**
 * Connections Table model class
 * @author HackneyTeam
 *
 */
public class ConnectionsTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7460333149856451896L;
	/**
	 * Constructor
	 * @param ccList connections list 
	 * @param language language version
	 */
	public ConnectionsTableModel(ClientConnectionsList ccList, Language language) {
		this.ccList = ccList;
		columnNames = new String[2];
		columnNames[0] = language.getNameColumn();
		columnNames[1] = "MAC/IP";
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return ccList.size();

	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 2;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex==0) 
			return (String) ccList.item(rowIndex).getInfo().getName();
		else
			return (String) ccList.item(rowIndex).getInfo().getMac();

	}

	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}


	private ClientConnectionsList ccList;
	private String columnNames[];
}
