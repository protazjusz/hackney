
package pl.umk.mat.hackney.Models;

import javax.swing.table.AbstractTableModel;

import pl.umk.mat.hackney.Connection.ClientInfoList;
import pl.umk.mat.hackney.Utility.Language;


/**
 * AllowList Table model class
 * @author HackneyTeam
 *
 */
public class AllowListTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4500617138855513985L;
	
	/**
	 * Constructor
	 * @param ccListInfo client devices informations
	 * @param language language version
	 */
	public AllowListTableModel(ClientInfoList ccListInfo, Language language) {
		this.ccListInfo = ccListInfo;
		columnNames = new String[2];
		columnNames[0] = language.getNameColumn();
		columnNames[1] = "MAC/IP";
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return this.ccListInfo.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 2;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		//return null;
		if(columnIndex==0)
			return ccListInfo.item(rowIndex).getName();
		else
			return ccListInfo.item(rowIndex).getMac();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}
	
	/**
	 * Sets new devices informations list
	 * @param ccListInfo  device info
	 */
	public void setCcListInfo(ClientInfoList ccListInfo) {
		this.ccListInfo = ccListInfo;
	}

	private ClientInfoList ccListInfo;
	private String columnNames[];

}
