package pl.umk.mat.hackney.kopyto.ui;

import java.util.ArrayList;
import java.util.List;

import pl.umk.mat.hackney.kopyto.R;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class WifiNetworkList {
	
	private ListView deviceListView;
	
	// interfejs do implementacji reakcji na zdarzenie wybrania el z listy
	public interface DeviceListListener {
		public void onDeviceChoosen(ScanResult device);
	}

	
	private DeviceListListener deviceListListener;
	private List<ScanResult> deviceList;
	private ArrayAdapter<String> adapter;
	
	/**
	 * creates new list
	 * @param listListener
	 */
	public WifiNetworkList(DeviceListListener listListener){
		this.deviceListListener = listListener;
		deviceList = new ArrayList<ScanResult>();
		deviceListView= (ListView)ActivityProperties.get().findViewById(R.id.deviceList);
				
		adapter = new ArrayAdapter<String>(ActivityProperties.get(),R.layout.list_item, R.id.textItem);
		deviceListView.setAdapter(adapter);
		
		deviceListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				deviceListListener.onDeviceChoosen(deviceList.get(position));
			}
		});
		
		
	}
	
	/**
	 * Adds new device to list
	 * @param device
	 */
	public void add(ScanResult device){
			deviceList.add(device);
			adapter.add(device.SSID);
	
	}
	
	public void fillList(List<ScanResult> deviceList){
		this.deviceList.clear();
		adapter.clear();
		this.deviceList=deviceList;
		for(int a=0;a<deviceList.size();a++){
			adapter.add(deviceList.get(a).SSID);
		}
	}
	
	//wyczyszczenie listy i wyszukiwanie na nowo
	/**
	 * Clears and refreshes the list
	 * @param btHandler
	 */
	public void refresh(WifiManager wm){
		deviceList.clear();
		adapter.clear();
		wm.startScan();
	  	try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		btHandler.cancelDiscovery();
//		btHandler.discoverDevices();
	}
	public boolean include(ScanResult device){	
		if (deviceList.indexOf(device)<0)return false;
		return true;
}
	
}
