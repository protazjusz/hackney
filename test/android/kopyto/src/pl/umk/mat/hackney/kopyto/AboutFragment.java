package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Fragment responsible for interacting with user in application About tab
 * 
 * @author HackneyTeam
 *
 */
public class AboutFragment extends Fragment implements OnClickListener {

	Button disconnectButton;

	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.about, container, false);
		
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		disconnectButton =(Button)ActivityProperties.get().findViewById(R.id.disconnectButton);
		disconnectButton.setOnClickListener(this);

	}
	
	@Override
	public void onDestroyView(){
		super.onDestroyView();
		//ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}
	
	/**
	 * Defines on button click reaction
	 */
	@Override
	public void onClick(View arg0) {
		RemoteControlActivity.showDisconnectDialog();
	}	
}
