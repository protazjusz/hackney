package pl.umk.mat.hackney.kopyto;

import java.net.Socket;
import java.util.Locale;

import pl.umk.mat.hackney.networking.WifiHandler;
import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class EnableWifiActivity extends FragmentActivity implements OnClickListener {

	private Button yesButton;
	private Button noButton;
	private TextView tekst;
	private ProgressDialog dialog;
	private AlertDialog alertDialog;
	private WifiManager wm;
	
	public static Socket wifiSocket;
	
	/**
	 * Initialize required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

		setContentView(R.layout.enable_bluetooth);

	//	btHandler = new BluetoothHandler();
		wm=(WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		yesButton = (Button) findViewById(R.id.yesEnableBluetoothButton);
		noButton = (Button) findViewById(R.id.noEnableBluetoothButton);
		tekst = (TextView) findViewById(R.id.textView1);
		tekst.setText(R.string.enableWifiQuestion);

		yesButton.setOnClickListener(this);
		noButton.setOnClickListener(this);
		dialog = new ProgressDialog(this);
	//	Bundle bundle = getIntent().getExtras();

		//next = bundle.getString("nextActivity");          
	}

    /**
     *  Hide dialog on activity end
     */
    @Override
    public void onDestroy(){
    	super.onDestroy(); 
		if(dialog.isShowing()) dialog.dismiss();
    }

    
	
    @Override
    public void onClick(View arg0) {

    	class EnableWifiTask extends AsyncTask<Void, Void, Boolean> {

    		@Override
    		protected Boolean doInBackground(Void... arg0) {
    			  if(wm.isWifiEnabled()==false){
    		        	return WifiHandler.enableWifi(getApplicationContext());
    		       }
    			  return true;
    		}
    		@Override
    		protected void onPostExecute(Boolean result){
    			if((boolean)result) launchDevicesListActivity();
    			else showError();
    		}	
    	}
    	switch(arg0.getId()){

    	case R.id.noEnableBluetoothButton:
    		this.onBackPressed();
    		break;

    	case R.id.yesEnableBluetoothButton:

    		dialog = ProgressDialog.show(this, "", this.getString(R.string.enablingWifiDialog), true);

    		EnableWifiTask ewt= new EnableWifiTask();
    		ewt.execute();
    		break;
    	}
    }
    
    
	public void launchDevicesListActivity() {
		
		while(!wm.isWifiEnabled());
		
		ActivityManager activityManager = new ActivityManager(this);

		activityManager.openActivity(SearchWifiActivity.class);
		
		finish();
	}
    
	
	public void showError() {
		if(dialog.isShowing())dialog.dismiss();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(this.getString(R.string.bluetoothInitializationError))
    	       .setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					ActivityProperties.get().finish();
				}
			});
   
    	alertDialog = builder.create();
    	alertDialog.show(); 
		
	}
}
