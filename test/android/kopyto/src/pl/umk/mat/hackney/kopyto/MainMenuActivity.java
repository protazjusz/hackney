package pl.umk.mat.hackney.kopyto;

import java.util.Locale;

import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 *  Activity responsible for interacting with user in main menu
 * @author HackneyTeam
 */
public class MainMenuActivity extends Activity implements OnClickListener{

	
	private WifiManager wm;
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		
		wm=(WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		
		setContentView(R.layout.main_menu);
		Button searchWifiButton = (Button) findViewById(R.id.main_search_wifi);
		Button searchNewButton = (Button) findViewById(R.id.main_search_new);
		Button searchPairedButton = (Button) findViewById(R.id.main_search_paired);
		Button optionsButton = (Button) findViewById(R.id.main_options);
		Button exitButton = (Button) findViewById(R.id.main_exit);
		
		searchWifiButton.setOnClickListener(this);
		searchNewButton.setOnClickListener(this);
		searchPairedButton.setOnClickListener(this);
		optionsButton.setOnClickListener(this);
		exitButton.setOnClickListener(this);
		
	}

	
	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View arg0) {
		ActivityManager activityMgr = new ActivityManager(this);
		
		
		switch (arg0.getId()){
			case R.id.main_search_wifi:
				if(!wm.isWifiEnabled())activityMgr.openActivityNoHistory(EnableWifiActivity.class);
				else activityMgr.openActivity(SearchWifiActivity.class);
			break;
			
			case R.id.main_search_new:
				if(BluetoothAdapter.getDefaultAdapter()!=null){
					if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) activityMgr.openActivityWithParamAndNoHistory(EnableBluetoothActivity.class,"search");
					else activityMgr.openActivityNoHistory(SearchActivity.class);
				}
				else{
					showBluetoothError();	
				}
				
			break;
			
			case R.id.main_search_paired:
				if(BluetoothAdapter.getDefaultAdapter()!=null){
					if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) activityMgr.openActivityWithParamAndNoHistory(EnableBluetoothActivity.class,"bonded");
					else activityMgr.openActivityNoHistory(BondedDevicesActivity.class);
				}
				else{
					showBluetoothError();
				}
			break;
				
			case R.id.main_options:	
				activityMgr.openActivity(InformationsActivity.class);
				this.finish();
			break;
		
			case R.id.main_exit:
				
				this.finish();
			break;
		
		}
			
	}
	
	
	/**
	 * Disables bluetooth adapter on exit
	 */
	@Override
	public void onDestroy(){
		super.onDestroy();

		if(BluetoothAdapter.getDefaultAdapter()!=null && BluetoothAdapter.getDefaultAdapter().isEnabled())BluetoothAdapter.getDefaultAdapter().disable();
	}
	
	
	/**
	 * Shows bluetooth not supported error dialog
	 */
	public void showBluetoothError() {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(this.getString(R.string.bluetoothNotSupported))
    	       .setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			}).create().show(); 
		
	}
}
