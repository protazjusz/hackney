package pl.umk.mat.hackney.networking;

public class MessageData 
{
	//================================================================================================//
	public static final String GALOP_VERSION = "1.0";
	public static final String UUID_ANDROID = "20120510-0001-0100-0100-000badc0ffee";
	public static final String UUID_JAVA = "20120510000101000100000badc0ffee";
	
	public static final byte LENGTH_STD = 4;
	public static final byte LENGTH_STRING = 32;
	
	public static final byte DEVICE_UNDEF = 0;
	public static final byte DEVICE_J2ME = 1;
	public static final byte DEVICE_ANDROID = 2;
	
	public static final byte CONNECTION_UNDEF = 0;
	public static final byte CONNECTION_HELLO = 1;
	public static final byte CONNECTION_GOODBYE = 2;
	
	public static final byte MOUSE_NUMPAD_START_MOVE = 10;
	public static final byte MOUSE_NUMPAD_STOP = 11;
	public static final byte MOUSE_SCREEN_PRESSED = 12;
	public static final byte MOUSE_SCREEN_MOVE = 13;
	public static final byte MOUSE_SCREEN_RELEASED = 14;
	
	public static final byte KEYBOARD_PRESSED = 20;
	
	public static final byte HOTKEY_APPLIST_REQUEST = 30;
	public static final byte HOTKEY_APPLIST_COUNT = 31;
	public static final byte HOTKEY_APP_CHOSED = 32;
	public static final byte HOTKEY_APPHOTKEY_REQUEST = 33;
	public static final byte HOTKEY_APPHOTKEY_COUNT = 34;
	public static final byte HOTKEY_USED = 35;
	
	public static final byte STRING_EXCHANGE = 39;

	public static final byte HOTKEY_APPLIST_EXCHANGE = STRING_EXCHANGE;
	public static final byte HOTKEY_APPHOTKEY_EXCHANGE = STRING_EXCHANGE;
	
	public static final byte GAMEPAD_ACCEL_MOVE = 40;
	public static final byte GAMEPAD_ACCEL_STOP = 41;
	public static final byte GAMEPAD_KEY_PRESSED = 42;
	public static final byte GAMEPAD_KEY_RELEASED = 43;
	public static final byte GAMEPAD_PLAYER_SET = 44;
	
	//================================================================================================//
	protected byte[] data;

	//================================================================================================//
	public MessageData()
	{
		this(MessageData.CONNECTION_UNDEF);
	}
	
	public MessageData(byte[] bytes)
	{
		this(bytes[0]);
		setSessionID(bytes[1]);
		if (bytes.length > 2)
		{
			byte[] payload = new byte[bytes.length-2];
			System.arraycopy(bytes, 2, payload, 0, bytes.length-2);
			setPayload(payload);
		}
	}
	
	public MessageData(byte messageType)
	{
		if (messageType != MessageData.STRING_EXCHANGE)
			data = new byte[MessageData.LENGTH_STD];
		else
			data = new byte[MessageData.LENGTH_STRING];
		data[0] = messageType;
	}	
	
	public MessageData(byte messageType, byte[] payload)
	{
		if (messageType != MessageData.STRING_EXCHANGE)
		{
			data = new byte[MessageData.LENGTH_STD];
			if (payload != null)
				System.arraycopy(payload, 0, data, 2, (payload.length >= MessageData.LENGTH_STD-2) ? MessageData.LENGTH_STD-2 : payload.length);
		}
		else
		{
			data = new byte[MessageData.LENGTH_STRING];
			if (payload != null)
				System.arraycopy(payload, 0, data, 2, (payload.length >= MessageData.LENGTH_STRING-2) ? MessageData.LENGTH_STRING-2 : payload.length);
		}
		data[0] = messageType;
	}
	
	public MessageData(byte messageType, byte sessionID, byte[] payload)
	{
		this(messageType, payload);
		if (sessionID > 0)
			data[1] = sessionID;
	}
	
	//================================================================================================//
	public byte[] toBytes()
	{
		return data;
	}
	
	public void fromBytes(byte[] bytes)
	{
		if (bytes != null)
		{
			setMessageType(bytes[0]);
			setSessionID(bytes[1]);
			if (bytes.length > 2)
			{
				byte[] payload = new byte[bytes.length-2];
				System.arraycopy(bytes, 2, payload, 0, bytes.length-2);
				setPayload(payload);
			}
		}
	}
	
	public byte getMessageType() 
	{
		return data[0];
	}

	public void setMessageType(byte messageType) 
	{
		if (messageType == MessageData.STRING_EXCHANGE)
			data = new byte[MessageData.LENGTH_STRING];
		data[0] = messageType;
	}
	
	public byte getSessionID() 
	{
		return data[1];
	}

	public void setSessionID(byte sessionID) 
	{
		data[1] = sessionID;
	}	
	
	public byte[] getPayload() 
	{		
		byte[] payload;
		if (data[0] != MessageData.STRING_EXCHANGE)
		{
			payload = new byte[MessageData.LENGTH_STD-2];
			System.arraycopy(data, 2, payload, 0, MessageData.LENGTH_STD-2);				
		}
		else 
		{
			payload = new byte[MessageData.LENGTH_STRING-2];
			System.arraycopy(data, 2, payload, 0, MessageData.LENGTH_STRING-2);	
		}
		return payload;
	}
	
	public void setPayload(byte[] payload) 
	{
		setPayload(data[0], payload);
	}
	
	public void setPayload(byte messageType, byte[] payload) 
	{
		if (messageType != MessageData.STRING_EXCHANGE)
		{
			if (messageType != data[0])
			{
				data = new byte[MessageData.LENGTH_STD];
				data[0] = messageType;
			}
			if (payload != null)
				System.arraycopy(payload, 0, data, 2, (payload.length >= MessageData.LENGTH_STD-2) ? MessageData.LENGTH_STD-2 : payload.length);
		}
		else
		{
			if (messageType != data[0])
			{
				data = new byte[MessageData.LENGTH_STRING];
				data[0] = messageType;
			}
			if (payload != null)
				System.arraycopy(payload, 0, data, 2, (payload.length >= MessageData.LENGTH_STRING-2) ? MessageData.LENGTH_STRING-2 : payload.length);
		}
	}
	
}
