package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityManager;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

/**
 * Activity responsible for displaying splash screen
 * @author HackneyTeam
 *
 */
public class WelcomeActivity extends Activity {

	private Context context = this;

	/**
	 * Initialize activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		showWelcomeScreen(2000);
	}

	/**
	 * Displays splash screen
	 * @param duration amount of time 
	 */
	void showWelcomeScreen(final int duration) {
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						wait(duration);
					}

					ActivityManager activityMgr = new ActivityManager(context);

					activityMgr.openActivity(MainMenuActivity.class);
					finish();

				} catch (InterruptedException e) {
					// nothing that can be done
				}
			}
		};
		t.start();
	}
	
}
