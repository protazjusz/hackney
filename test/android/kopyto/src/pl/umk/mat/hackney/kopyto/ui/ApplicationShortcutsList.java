package pl.umk.mat.hackney.kopyto.ui;

import java.util.ArrayList;
import java.util.List;

import pl.umk.mat.hackney.kopyto.R;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * Class representing list of application shortcuts
 * @author HackneyTeam
 *
 */
public class ApplicationShortcutsList {
	
	private ListView shortcutsListView;

	private List<String> shortcutsList;
	private ArrayAdapter<String> shortcutsAdapter;
	
	public interface ApplicationShortcutsListListener {
		public void onShortcutChoosen(String application);
		
	}
	private ApplicationShortcutsListListener applicationShortcutsListListener;
	
	
	/**
	 * creates new list
	 * @param listListener
	 */
	public ApplicationShortcutsList(ApplicationShortcutsList.ApplicationShortcutsListListener listListener){
		this.applicationShortcutsListListener = listListener;
		shortcutsList = new ArrayList<String>();
		shortcutsListView= (ListView)ActivityProperties.get().findViewById(R.id.shortcutsList);

		shortcutsAdapter = new ArrayAdapter<String>(ActivityProperties.get(),R.layout.list_item, R.id.textItem);
		
		shortcutsListView.setAdapter(shortcutsAdapter);
		
		shortcutsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				applicationShortcutsListListener.onShortcutChoosen(shortcutsList.get(position));
			}
		});
		
		          
	}
	
	/**
	 * Adds new shortcut to list
	 * @param application
	 */
	public void add(String application){
			shortcutsList.add(application);
			shortcutsAdapter.add(application);
	}
	
	/**
	 * Check if list include given shortcut
	 * @param application 
	 * @return 
	 */
	public boolean include(String application){	
			if (shortcutsList.indexOf(application)<0)return false;
			return true;
	}
	
	/** 
	 *  Returns index in the list of given shortcut
	 * @param application
	 * @return
	 */
	public int indexOf(String application){
		
		if(shortcutsList.indexOf(application)<0) return -1;
		else return shortcutsList.indexOf(application);	
	}
	
	/**
	 * clears the list
	 */
	public void clear(){
		shortcutsList.clear();
		shortcutsAdapter.clear();
	}
}
