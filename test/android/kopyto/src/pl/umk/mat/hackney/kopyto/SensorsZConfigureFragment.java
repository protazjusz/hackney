package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Fragment responsible for setting Z axis option in sensors
 * @author HackneyTeam
 *
 */
public class SensorsZConfigureFragment extends Fragment implements OnClickListener, SensorEventListener {
	
	SharedPreferences settingsFile;

	private SensorManager sensorManager;

	private Button saveButton;
	private Button backButton;
	private Button defaultsButton;
	private TextView value;
	private TextView info;
	private TextView title;
	private ProgressBar progressBar;
	private float z;
	private AlertDialog alertDialog;

	private boolean change;
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		sensorManager = (SensorManager) ActivityProperties.get().getSystemService(Context.SENSOR_SERVICE);
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.sensors_configure, container, false);
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(true);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		
		z=3;
		backButton = (Button) ActivityProperties.get().findViewById(R.id.backButton);
		backButton.setOnClickListener(this);
		saveButton = (Button) ActivityProperties.get().findViewById(R.id.saveButton);
		saveButton.setOnClickListener(this);
		defaultsButton = (Button) ActivityProperties.get().findViewById(R.id.defaultsButton);
		defaultsButton.setOnClickListener(this);
		value = (TextView)ActivityProperties.get().findViewById(R.id.currentVal);
		info =(TextView)ActivityProperties.get().findViewById(R.id.configureInfo);
		title = (TextView) ActivityProperties.get().findViewById(R.id.configureTitle);
		progressBar = (ProgressBar) ActivityProperties.get().findViewById(R.id.configureProgressBar);
		progressBar.setProgress((int)z);
		value.setText(Integer.toString((int)z));
		
		title.setText(R.string.zAxisSensitivity);
		info.setText(R.string.zConfigureInfo);
		change = true;
		registerSensors();
	}


	@Override
	public void onPause(){
		super.onPause();
		
		unregisterSensors();
	}
	
	/**
	 * Updates activity layout
	 */
	@Override 
	public void onDestroyView(){
		super.onDestroyView();
		if(change)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(false);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}
	
	/**
	 * Sets this fragment to listen for sensors changes
	 */
	private void registerSensors() 
	{
		sensorManager.registerListener(this, sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0), SensorManager.SENSOR_DELAY_FASTEST);
	}
	
	/**
	 * Sets this fragment to not listen for sensors changes
	 */
	private void unregisterSensors() {
		sensorManager.unregisterListener(this, sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0));
	}

	/**
	 * Sets reaction for buttons click
	 */
	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()){
		
		case R.id.saveButton:
			change = false;
			Editor editor = settingsFile.edit();
			editor.putFloat("defAxisZ", Math.abs(z));
			editor.commit();
			showOnSaveDialog();
			unregisterSensors();
	
			
			break;
		case R.id.backButton:
			change = false;
			FragmentManager fragmentManager=getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			SensorsOptionsFragment sof= new SensorsOptionsFragment();
			fragmentTransaction.replace(R.id.content, sof);
			fragmentTransaction.commitAllowingStateLoss();
			break;
			
		case R.id.defaultsButton:
			change = false;
			editor = settingsFile.edit();
			editor.putFloat("defAxisZ", 3.0f);
    	    editor.commit();
			unregisterSensors();
    	    value.setText(Integer.toString((int)3));
    	    progressBar.setProgress(3);
    	    showOnRestoreDialog();
    	    value.setText(Integer.toString(3));
    	    progressBar.setProgress(3);
    	   
			break;
		}
		
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// nothing
		
	}

	/**
	 * Sets reaction for sensors change
	 */
	@Override
	public void onSensorChanged(SensorEvent arg0) {
		z = arg0.values[2];
		progressBar.setProgress((int)Math.abs(z));
		value.setText(Integer.toString((int)Math.abs(z)));
		
	}
	
	/**
	 * Shows dialog when user click save button, and loads options fragment
	 */
	private void showOnSaveDialog(){
	    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
	    	
	    	builder.setMessage(ActivityProperties.get().getString(R.string.onSaveDialog))
	    	       .setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						FragmentManager fragmentManager=getFragmentManager();
						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						SensorsOptionsFragment sof= new SensorsOptionsFragment();
						fragmentTransaction.replace(R.id.content, sof);
						fragmentTransaction.commitAllowingStateLoss();
						
					}
				});
	   
	    	alertDialog = builder.create();
	    	alertDialog.show(); 	
	    
	    }
	 
	 /**
	 * Shows dialog when user click restore default button, and loads options fragment
	 */
	 private void showOnRestoreDialog(){
	    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
	    	
	    	builder.setMessage(ActivityProperties.get().getString(R.string.onRestoreDefaultsDialog))
	    	       .setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						FragmentManager fragmentManager=getFragmentManager();
						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						SensorsOptionsFragment sof= new SensorsOptionsFragment();
						fragmentTransaction.replace(R.id.content, sof);
						fragmentTransaction.commitAllowingStateLoss();
						
					}
				});
	   
	    	alertDialog = builder.create();
	    	value.setText(Integer.toString(3));
	    	progressBar.setProgress(3);
	    	alertDialog.show(); 	
	    	value.setText(Integer.toString(3));
	    	progressBar.setProgress(3);
	    }
}