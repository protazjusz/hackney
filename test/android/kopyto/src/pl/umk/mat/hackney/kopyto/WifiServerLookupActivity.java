package pl.umk.mat.hackney.kopyto;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Locale;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WifiServerLookupActivity extends Activity implements OnClickListener{
	
	private EditText addressBox;
	private Button searchButton, connectButton,backButton;
	private Socket wifiSocket= EnableWifiActivity.wifiSocket;
	
	/**
	 * Initializes required objects
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
		
		ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		
		
		setContentView(R.layout.server_lookup);
		addressBox = (EditText) this.findViewById(R.id.IPaddress);
		//searchButton = (Button) this.findViewById(R.id.Lookup);
		connectButton = (Button) this.findViewById(R.id.connectButton);
		backButton = (Button) this.findViewById(R.id.backButton);
		
	//	addressBox.setOnClickListener(this);
		//searchButton.setOnClickListener(this);
		connectButton.setOnClickListener(this);
		backButton.setOnClickListener(this);

    }

    private void setupConnection(){
    		RemoteControlActivity.connection = new Connection(wifiSocket);
    	//	Toast.makeText(getBaseContext(), wifiSocket.toString(), Toast.LENGTH_LONG).show();

    		try {
    			RemoteControlActivity.connection.createOutStream();
    			
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			Log.w("stream", e.getMessage());
    			e.printStackTrace();
    			Toast.makeText(getBaseContext(), R.string.connectionError, Toast.LENGTH_LONG).show();
    		}catch (NullPointerException e) {
    			Log.w("stream", e.getCause());
    			e.printStackTrace();
    			Toast.makeText(getBaseContext(), R.string.connectionError, Toast.LENGTH_LONG).show();
    			return ;
    		}
    		
    		try {
				RemoteControlActivity.connection.createInStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.w("stream", e.getMessage());
				Toast.makeText(getBaseContext(), R.string.connectionError, Toast.LENGTH_LONG).show();
				return ;
			}
    		catch (NullPointerException e) {
    			Log.w("stream", e.getCause());
    			e.printStackTrace();
    			Toast.makeText(getBaseContext(), R.string.connectionError, Toast.LENGTH_LONG).show();
    			return ;
    		}
    		
    		class handshakeTask extends AsyncTask<Void, Void, Boolean> {

    			@Override
    			protected Boolean doInBackground(Void... arg0) {
    				
    				return RemoteControlActivity.connection.tryHandShake();
    			}
    			@Override
    			protected void onPostExecute(Boolean result){
    				if(result)launchControl();
    				else ; //TODO: error report
    			}
    			
    		}
    		handshakeTask ht = new handshakeTask();
    		ht.execute();
    }
    
    private void launchControl(){
		if(RemoteControlActivity.connection.isConnected()){
			Intent intent = new Intent(ActivityProperties.get(),RemoteControlActivity.class);
			startActivity(intent);
		}
    }
    
    
    @Override
    public void onClick(View v) {
    	class createSocketTask extends AsyncTask<Void, Void, Boolean> {

			@Override
			protected Boolean doInBackground(Void... arg0) {
				//Looper.prepare();
				try {
					wifiSocket = new Socket(addressBox.getText().toString(),4444);
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
//					Toast.makeText(ActivityProperties.get(), R.string.connectionError, Toast.LENGTH_LONG).show();
//					return false;
					// TODO Auto-generated catch block
					
				//	Toast.makeText(getBaseContext(), e1.getMessage(), Toast.LENGTH_LONG).show();
				} catch (IOException e1) {
					e1.printStackTrace();
//					Toast.makeText(ActivityProperties.get(), R.string.connectionError, Toast.LENGTH_LONG).show();
//					return false;
					// TODO Auto-generated catch block
					
				//	Toast.makeText(getBaseContext(), e1.getMessage(), Toast.LENGTH_LONG).show();
				} catch (Exception e){
//					Toast.makeText(ActivityProperties.get(), R.string.connectionError, Toast.LENGTH_LONG).show();
//					return false;
					//Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
					
				}
				return true;
			}
			@Override
			protected void onPostExecute(Boolean result){
				if(!result) ;// TODO: error report
				else setupConnection();

			}
			
		}	

    	switch(v.getId()){
    	case R.id.connectButton:
    		boolean tmp;
//    		try {
//				wifiSocket = new Socket(addressBox.getText().toString(),4444);
//			} catch (UnknownHostException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//				Toast.makeText(getBaseContext(), e1.getMessage(), Toast.LENGTH_LONG).show();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//				Toast.makeText(getBaseContext(), e1.getMessage(), Toast.LENGTH_LONG).show();
//			} catch (Exception e){
//				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
//				
//			}
    		createSocketTask cst =new createSocketTask();
    		cst.execute();

//    		try {
//				Thread.sleep(10000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
////    		try {
//				wait();
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
    	//	Toast.makeText(getBaseContext(), wifiSocket.toString(), Toast.LENGTH_LONG).show();
//    		RemoteControlActivity.connection = new Connection(wifiSocket);
//    	//	Toast.makeText(getBaseContext(), wifiSocket.toString(), Toast.LENGTH_LONG).show();
//
//    		try {
//    			RemoteControlActivity.connection.createOutStream();
//    			
//    		} catch (IOException e) {
//    			// TODO Auto-generated catch block
//    			Log.w("stream", e.getMessage());
//    			e.printStackTrace();
//    			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
//    		}catch (NullPointerException e) {
//    			Log.w("stream", e.getCause());
//    			e.printStackTrace();
//    			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
//    		}
//    		
//    		try {
//				RemoteControlActivity.connection.createInStream();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				Log.w("stream", e.getMessage());
//			}
    		
    		//===============================================================
//    		class handshakeTask extends AsyncTask<Void, Void, Boolean> {
//
//    			@Override
//    			protected Boolean doInBackground(Void... arg0) {
//    				
//    				return RemoteControlActivity.connection.tryHandShake();
//    			}
//    			@Override
//    			protected void onPostExecute(Boolean result){
//
//    			}
//    			
//    		}
//    		handshakeTask ht = new handshakeTask();
//    		ht.execute();
//    		
//    		try {
//				Thread.sleep(10000);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
    		//===============================================================

    //		RemoteControlActivity.connection.tryHandShake();

//    		if(RemoteControlActivity.connection.isConnected()){
//    			Intent intent = new Intent(ActivityProperties.get(),RemoteControlActivity.class);
//    			startActivity(intent);
//    		}

    	break;
    	case R.id.backButton:
//    		ActivityManager activityManager = new ActivityManager(this);
//
//    		activityManager.openActivityNoHistory(MainMenuActivity.class);
    		this.finish();
    		
    		
    		
    	break;

    	}

    }

}
