package pl.umk.mat.hackney.kopyto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivityManager {

	private Context context;

	public ActivityManager(Context appContext) {
		this.context = appContext;
	}

	// otwarcie activity podanej w parametrze
	void openActivity(Class<?> activity) {
		Intent intent = new Intent(context.getApplicationContext(), activity);
		context.startActivity(intent);
	}
	
	void openActivityWithParamAndNoHistory(Class<?> activity,String param){
		Intent intent = new Intent(context.getApplicationContext(), activity);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		Bundle bundle = new Bundle();
		bundle.putString("nextActivity",param);		
		intent.putExtras(bundle);
		context.startActivity(intent);
	}
	void openActivityNoHistory(Class<?> activity){
		Intent intent = new Intent(context.getApplicationContext(), activity);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		context.startActivity(intent);
	}
}
