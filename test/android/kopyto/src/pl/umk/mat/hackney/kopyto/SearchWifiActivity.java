package pl.umk.mat.hackney.kopyto;

import java.util.Locale;

import pl.umk.mat.hackney.networking.WifiHandler;
import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SearchWifiActivity extends FragmentActivity implements  OnClickListener{

	private BroadcastReceiver mReceiver;
	private IntentFilter f2;
	private Button acceptCurrent;
	private TextView network;
	
	/**
	 * Initializes required objects
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);

		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());      

        setContentView(R.layout.wifi_state);
        
 
        
        mReceiver = new BroadcastReceiver() 
        {       
            @Override
            public void onReceive(Context context, Intent intent) 
            {   
              String action = intent.getAction();        

              if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION))
              {
                 handleWifiStateChange(intent);
              }
            }
        };
        
        f2 = new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        this.registerReceiver(mReceiver, f2);
        
        acceptCurrent=(Button)findViewById(R.id.wifiNextButton);
        acceptCurrent.setOnClickListener(this);
        acceptCurrent.setClickable(false);
        Button searchButton =(Button) findViewById(R.id.wifiSearachButton);
        searchButton.setOnClickListener(this);
        Button backButton =(Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(this);
        
        network= (TextView)findViewById(R.id.currentNetworkValue);
        
    }
    
    

    protected void handleWifiStateChange ( Intent intent )
    {   
        NetworkInfo info = (NetworkInfo)intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);      
        if (info.getState().equals(NetworkInfo.State.CONNECTED))
        {
        	network.setText(WifiHandler.getCurrentNetworkInformation(this).getSSID());
        	acceptCurrent.setClickable(true);
//        	ActivityManager activityManager = new ActivityManager(this);
//
//    		activityManager.openActivityNoHistory(WifiServerLookupActivity.class);
        }

    }
    
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	//unregisterReceiver(mReceiver);
    }
    @Override
    public void onPause(){
    	super.onPause();
    	unregisterReceiver(mReceiver);
    }
    @Override
    public void onResume(){
    	super.onResume();
    	this.registerReceiver(mReceiver, f2);
    }
    @Override
    public void onRestart(){
    	super.onRestart();
    	this.registerReceiver(mReceiver, f2);
    }
    @Override
    public void onStart(){
    	super.onRestart();
    	this.registerReceiver(mReceiver, f2);
    }
    
	
	@Override
	public void onClick(View v) {

		switch (v.getId()){
			case R.id.wifiSearachButton :
				Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(intent);
			break;
			
			case R.id.backButton:
				this.onBackPressed();
			break;
				
			case R.id.wifiNextButton:
	        	ActivityManager activityManager = new ActivityManager(this);
				
	    		activityManager.openActivityNoHistory(WifiServerLookupActivity.class);
			break;
		}
	}
	
}
