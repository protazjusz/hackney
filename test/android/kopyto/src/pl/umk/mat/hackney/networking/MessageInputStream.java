package pl.umk.mat.hackney.networking;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MessageInputStream 
{
	private DataInputStream input;
	
	public MessageInputStream(DataInputStream stream)
	{
		input = stream;
	}
	
	public MessageInputStream(InputStream stream)
	{
		input = new DataInputStream(stream);
	}
	
	public DataInputStream getDataInputStream()
	{
		return input;
	}
	
	public int read(byte[] bytes)
	{
		int number = -1;
		try 
		{
			number = input.read(bytes);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();			
		}
		return number;
	}
	@Deprecated
	public int read(MessageData msgData)
	{
		int number = -1;
		byte[] bytes = msgData.toBytes(); 
		read(bytes); 
		msgData.fromBytes(bytes); 
		return number;
	}
	
	public int available() throws IOException
	{
		return input.available();
	}
	
	public MessageData readMessageData()
	{
		byte[] bytes = new byte[4];
		read(bytes);
		MessageData msgData = new MessageData(bytes);
		return msgData;
	}
	
	public MessageData readStringMessageData()
	{
		byte[] bytes = new byte[32]; 
		read(bytes);
		MessageData msgData = new MessageData(bytes);
		return msgData;
	}
	
	public void close()
	{
		try 
		{
			input.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
}
