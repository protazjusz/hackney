package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Fragment responsible for interacting with user in application sensors module
 * 
 * @author HackneyTeam
 *
 */
public class SensorsFragment extends Fragment implements SensorEventListener {
	
	private Connection connection = RemoteControlActivity.connection;
	private SensorManager sensorManager;
	private Context context;
	private MessageData md;

	public static final String HACKNEY_CONF_FILE = "HackneyConfiguration";
	SharedPreferences settings;
	
	private float defAxisY = 3;
	private float defAxisZ = 3;
	
	private boolean leftSent = false;
	private boolean rightSent = false;
	private boolean upSent = false;
	private boolean downSent = false;
	
	private Button buttonA;
	private Button buttonB;
	private Button buttonC;
	private Button buttonD;
	private Button buttonE;
	private Button buttonF;
	
	/**
	 * Initializes required objects
	 */
	public SensorsFragment() {
		context = ActivityProperties.get(); 
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
	}

	/**
	 * Sets this fragment to listen for sensors changes
	 */
	private void registerSensors()
	{
		sensorManager.registerListener(this, sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0), SensorManager.SENSOR_DELAY_FASTEST);
	}
	
	/**
	 * Sets this fragment to not listen for sensors changes
	 */
	private void unregisterSensors() { 
		sensorManager.unregisterListener(this, sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0));
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	/**
	 * Sets reaction for sensors change
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
	

		if (Math.abs(event.values[1]) > defAxisY)
        {
    		
        	if (event.values[1] < 0)
        	{
        		if (! leftSent)
        		{ 
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)2, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_MOVE, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			leftSent = true;
        		}
        	}
        	else 
        	{ 
        		if (! rightSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)4, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_MOVE, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			rightSent = true;
        		}
        	}
        }
        else 
        {
        	if (event.values[1] < 0)
        	{
        		if (leftSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)2, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_STOP, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			leftSent = false;
        		}
        	}
        	else 
        	{
        		if (rightSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)4, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_STOP, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			rightSent = false;
        		}
        	}        	
        }

        if (Math.abs(event.values[2]) > defAxisZ)
        {

        	if (event.values[2] > 0)
        	{
        		if (! upSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)32, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_MOVE, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes()); 
        			}
        			upSent = true;
        		}
        	}
        	else 
        	{ 
        		if (! downSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)64, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_MOVE, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			downSent = true;
        		}
        	}
        }
        else 
        {
        	if (event.values[2] > 0)
        	{
        		if (upSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)32, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_STOP, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
        			}
        			upSent = false;
        		}
        	}
        	else 
        	{
        		if (downSent)
        		{
        			if (connection.getGamepadPlayerID() != 0) {
        			byte[] bajty = {(byte)64, (byte)connection.getGamepadPlayerID()};
        			md = new MessageData(MessageData.GAMEPAD_ACCEL_STOP, bajty);
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());	
        			}
        			downSent = false;
        		}
        	}        	
        }
        
        
	}

	/**
	 * Loads configuration file
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		settings = context.getSharedPreferences(HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
	    
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
      return inflater.inflate(R.layout.sensors, container, false);
      
  }
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		
		super.onStart();
		ActivityProperties.get().findViewById(R.id.sensors_button).setPressed(true);
		ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		defAxisY = settings.getFloat("defAxisY", 3.0f);
	    defAxisZ = settings.getFloat("defAxisZ", 3.0f);
	    
		registerSensors();  
	    
		buttonA = (Button) getActivity().findViewById(R.id.buttonA); 
	    buttonA.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) { 
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) { 
	        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)2, (byte)connection.getGamepadPlayerID()});
	        			md.setSessionID(connection.getSessionID());
	        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)2, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	    
		buttonB = (Button) getActivity().findViewById(R.id.buttonB); 
	    buttonB.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)4, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)4, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	    
		buttonC = (Button) getActivity().findViewById(R.id.buttonC); 
	    buttonC.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)8, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)8, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	    
		buttonD = (Button) getActivity().findViewById(R.id.buttonD); 
	    buttonD.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)16, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)16, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	    
		buttonE = (Button) getActivity().findViewById(R.id.buttonE); 
	    buttonE.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)32, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)32, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	    
		buttonF = (Button) getActivity().findViewById(R.id.buttonF); 
	    buttonF.setOnTouchListener(new OnTouchListener() {
	        @Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if (event.getAction() == (MotionEvent.ACTION_DOWN)){
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_PRESSED, new byte[]{(byte)64, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
	        		return true;
	        	}
	        	else if (event.getAction() == (MotionEvent.ACTION_UP)) {
	        		if (connection.getGamepadPlayerID() != 0) {
        			md = new MessageData(MessageData.GAMEPAD_KEY_RELEASED, new byte[]{(byte)64, (byte)connection.getGamepadPlayerID()});
        			md.setSessionID(connection.getSessionID());
        			connection.outStream.write(md.toBytes());
	        		}
        			return false;
	        	}
	        	return false;
	        }        
	    });
	}
	
	/**
	 * Sets this fragment to listen for sensors changes
	 */
	@Override
	public void onResume(){
		super.onResume();
		registerSensors();
	}
	
	/**
	 * Sets this fragment to not listen for sensors changes and updates layout
	 */
	@Override
	public void onPause(){
		unregisterSensors();
		ActivityProperties.get().findViewById(R.id.sensors_button).setPressed(false);
		super.onPause();

	}
}
