package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * Fragment responsible for displaying and changing current touchpad options
 * @author hackneyTeam
 *
 */
public class TouchpadOptionsFragment extends Fragment implements OnSeekBarChangeListener, OnClickListener {
	
	SharedPreferences settingsFile;
	
	Button saveButton;
	Button defaultsButton;
	Button backButton;
	SeekBar seekBar;
	float sensitivity;
	AlertDialog alertDialog;
	
	/**
	 * Loads configuration file
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.touchpad_options, container, false);
		
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		ActivityProperties.get().findViewById(R.id.touchpad_button).setSelected(true);
		
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		seekBar = (SeekBar)ActivityProperties.get().findViewById(R.id.seekBar);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar.setProgress(  (int)settingsFile.getFloat("sensitivity", Options.DEFAULT_SENSITIVITY));
		TextView tv = (TextView) ActivityProperties.get().findViewById(R.id.seekValue);
		tv.setText(String.format("%.1f",(((float)seekBar.getProgress()/100)-(float)0.5)*(float)2));
		
		saveButton = (Button) ActivityProperties.get().findViewById(R.id.saveButton);
		saveButton.setOnClickListener(this);
		defaultsButton = (Button) ActivityProperties.get().findViewById(R.id.defaultsButton);
		defaultsButton.setOnClickListener(this);
		backButton = (Button) ActivityProperties.get().findViewById(R.id.backButton);
		backButton.setOnClickListener(this);
	}


	
	/**
	 * Updates activity layout
	 */
	@Override 
	public void onDestroyView(){
		super.onDestroyView();
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
		ActivityProperties.get().findViewById(R.id.touchpad_button).setSelected(false);
	}

	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.saveButton:

			settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
			seekBar = (SeekBar)ActivityProperties.get().findViewById(R.id.seekBar);
			sensitivity= (float)seekBar.getProgress();

			Editor editor = settingsFile.edit();
			editor.putFloat("sensitivity", sensitivity);
			editor.commit();
			showOnSaveDialog();
			break;
			
		case R.id.defaultsButton:

			Editor editor1 = settingsFile.edit();
			editor1.putFloat("sensitivity", (float)50);
			editor1.commit();
			seekBar = (SeekBar)ActivityProperties.get().findViewById(R.id.seekBar);
			seekBar.setProgress(  (int)settingsFile.getFloat("sensitivity", Options.DEFAULT_SENSITIVITY));
			showOnRestoreDialog();
			
			break;

		case R.id.backButton:
			FragmentManager fragmentManager=getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			TouchpadAboutFragment taf= new TouchpadAboutFragment();
			fragmentTransaction.replace(R.id.content, taf);
			fragmentTransaction.commitAllowingStateLoss();


			break;

		}
	}

	/**
	 * Updates value near seek bar when user interacting with it
	 */
	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
		TextView tv = (TextView) ActivityProperties.get().findViewById(R.id.seekValue);
		tv.setText(String.format("%.1f",(((float)arg1/100)-(float)0.5)*(float)2));
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		// do nothing	
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
		// do nothing
	}
	
	/**
	 * Shows dialog when user click save button
	 */
	private void showOnSaveDialog(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
    	
    	builder.setMessage(ActivityProperties.get().getString(R.string.onSaveDialog))
    	       .setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
					
				}
			});
   
    	alertDialog = builder.create();
    	alertDialog.show(); 
   
    }
 
	/**
	 * Shows dialog when user click restore default button
	 */
 private void showOnRestoreDialog(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
    	
    	builder.setMessage(ActivityProperties.get().getString(R.string.onRestoreDefaultsDialog))
    	       .setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
				}
			});
   
    	alertDialog = builder.create();
    	alertDialog.show(); 
    }
}
