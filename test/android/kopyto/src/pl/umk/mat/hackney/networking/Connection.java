package pl.umk.mat.hackney.networking;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;

import pl.umk.mat.hackney.kopyto.RemoteControlActivity;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * Class provides the interface for communication with the server.
 * Creates a connection and opens streams used to send and receive data packed using GALOP .
 * 
 * @author HackneyTeam
 */

public class Connection extends AsyncTask<Void,MessageData,Void>{

	private BluetoothSocket socket;
	private Socket wifiSocket;

	private int mode; // 0 -bt , 1-wifi
	private boolean connected;
	public MessageOutputStream outStream;
	public MessageInputStream inStream;
	private byte sessionID = 0;
	private byte gamepadPlayerID = 0;
	Toast toast;
	
	private Object receiver;
	private String methodName;
	
	private boolean active = true;
	
	
	/**
	 * Sets whenever connection task is active or not. 
	 * @param active boolean
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * 
	 * Gets set id for sensor module 
	 * 
	 * @return set id
	 */
	public byte getGamepadPlayerID() {
		return gamepadPlayerID;
	}

	
	/**
	 * 
	 *  Sets set id for sensor module
	 * 
	 * @param gamepadPlayerID new set id, values from 0 to 4
	 */
	public void setGamepadPlayerID(byte gamepadPlayerID) {
		this.gamepadPlayerID = gamepadPlayerID;
	}

	
	/**
	 * 
	 * Gets session id
	 * 
	 * @return session id
	 */
	public byte getSessionID() {
		return sessionID;
	}

	/**
	 * 
	 * Create new object with given socket
	 * 
	 * @param socket bluetooth socket associated with remote device
	 */
	public Connection(BluetoothSocket socket){

		this.socket = socket;
		this.wifiSocket = null;
		this.mode=0;
	}
//	public Connection(Socket socket){
//		this.wifiSocket = new Socket();
//		this.wifiSocket = socket;
//		this.socket=null;
//		this.mode=1;
//	}
	public Connection(Socket socket){
		this.socket=null;
		this.mode=1;
		this.wifiSocket=socket;
	}

	/**
	 * 
	 * Provides necessary informations to run connection task
	 * 
	 * @param rec object which will receive messages
	 * @param method rec method which will be invoked when received message 
	 */
	public void makeRunnable(Object rec, String method){
		this.receiver = rec;
		this.methodName = method;
	}

	
	/**
	 * 
	 * Open stream for outgoing communication
	 * 
	 * @throws IOException
	 */
	public void createOutStream() throws IOException{
		try{
			if(mode==0){
				outStream = new MessageOutputStream(socket.getOutputStream());
			}
			else if( mode==1){
				if(wifiSocket==null)Log.w("stream", "JEST NULLEM");
				outStream = new MessageOutputStream( this.wifiSocket.getOutputStream());
			}
		
		}
		catch (IOException e){
			connected = false;
			throw e;
		}
	}
	
	/**
	 * 
	 * Open stream for outgoing communication
	 * 
	 * @throws IOException
	 */
	public void createInStream() throws IOException{
		try{
			if(mode==0)
				inStream = new MessageInputStream(socket.getInputStream());
			else if(mode==1)
				inStream = new MessageInputStream(wifiSocket.getInputStream());
		
		}
		catch (IOException e){
			connected = false;
			throw e;
		}
	}
	
	
	/**
	 * 
	 * Sends a greeting message to the server. If the servers response is positive an id session and sensor set id will be granted.   
	 *
	 * @throws IOException
	 * 
	 */
	public boolean tryHandShake(){
		
		MessageData md = new MessageData(MessageData.CONNECTION_HELLO);
		md.setSessionID(MessageData.DEVICE_ANDROID);
		outStream.write(md.toBytes());
	
		
		md =  inStream.readMessageData();
		if (md.getMessageType() == MessageData.CONNECTION_HELLO)
		{
			sessionID = md.getSessionID();
		}
		else{
			connected = false;
			return false;
		}
		md =  inStream.readMessageData();
		if (md.getMessageType() == MessageData.GAMEPAD_PLAYER_SET)
		{
			gamepadPlayerID = md.getPayload()[1];
		}
		else{
			connected = false;
			return false;
		}
		connected = true;
		return true;
	}
	
	/**
	 * 
	 * @return state of connection
	 */
	public boolean isConnected(){
		return connected;
	}
	
	
	/**
	 * 
	 * Disconnects gently with server 
	 * 
	 */
	public void disconnect(){
		outStream.write(new MessageData(new byte[]{ MessageData.CONNECTION_GOODBYE , 0}));
		MessageData data;
	//	data = inStream.readMessageData();
		
		
		connected = false;
	}
	
	
	/**
	 * 
	 * Clears all objects associated with current connection
	 * 
	 * @throws IOException
	 */
	public void end() throws IOException {
        try {
        	connected = false;
        	if(outStream!=null)outStream.close();
        	if(inStream!=null)inStream.close();
            if(socket!=null)socket.close();
            if(wifiSocket!=null)wifiSocket.close();
        } catch (IOException e) { 
        	throw e;
        }
    }

	
	/**
	 * 
	 * Listen for incoming messages and forwards it to onProgressUpdate() method. Make sure makeRunnable() method was called before running it!
	 * 
	 */
	@Override
	protected Void doInBackground(Void... params) {
		while(active){
			byte[] bytes = new byte[2];
			byte[]bytes2;
			byte[]bytes3;
			if(isCancelled())break;
			
			inStream.read(bytes);
			
			if(bytes[0]!= MessageData.STRING_EXCHANGE){
				bytes2 = new byte[2];
				bytes3= new byte[4];
				inStream.read(bytes2);
				for(int i=0;i<2;i++){
					bytes3[i]=bytes[i];
				}
				for(int i=2;i<4;i++){
					bytes3[i]=bytes2[i-2];
				}
			}
			else{
				bytes2 = new byte[30];
				bytes3 = new byte[32];
				inStream.read(bytes2);
				for(int i=0;i<2;i++){
					bytes3[i]=bytes[i];
				}
				for(int i=2;i<32;i++){
					bytes3[i]=bytes2[i-2];
				}
			}
			MessageData md = new MessageData();
			md.fromBytes(bytes3);
			this.publishProgress(md);
		}
		return null;
	}
	
	
	/**
	 * 
	 * Proceed message to given method on given object
	 * 
	 */
	@Override
	protected void onProgressUpdate(MessageData... val){
		try {
			RemoteControlActivity.class.getMethod(methodName, MessageData.class).invoke(receiver, (MessageData)val[0]);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			
		}
	}
}
