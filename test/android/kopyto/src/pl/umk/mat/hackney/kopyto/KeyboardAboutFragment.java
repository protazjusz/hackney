package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Fragment responsible for interacting with user in application keyboard about tab
 * 
 * @author HackneyTeam
 *
 */
public class KeyboardAboutFragment extends Fragment implements OnClickListener {
	
	Button settings;
	Button disconnect;
	TextView info;
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.about_fragment, container, false);
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().findViewById(R.id.keyboard_button).setSelected(true);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		settings=(Button) ActivityProperties.get().findViewById(R.id.settingsButton);
		disconnect=(Button) ActivityProperties.get().findViewById(R.id.disconnectButton);
		info = (TextView) ActivityProperties.get().findViewById(R.id.fragmentInfoText);
		settings.setVisibility(View.GONE);
		disconnect.setOnClickListener(this);
		info.setText(R.string.keyboardInfo);
	}
	
	/**
	 * Updates activity layout
	 */
	@Override
	public void onDestroyView(){
		ActivityProperties.get().findViewById(R.id.keyboard_button).setSelected(false);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
		super.onDestroyView();
	//	ActivityProperties.get().findViewById(R.id.keyboard_button).setSelected(false);
	//	ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}

	/**
	 * Defines on button click reaction
	 */
	@Override
	public void onClick(View v) {
		
		switch(v.getId()){
			case R.id.disconnectButton:
				RemoteControlActivity.showDisconnectDialog();
				break;
		}
	}

}
