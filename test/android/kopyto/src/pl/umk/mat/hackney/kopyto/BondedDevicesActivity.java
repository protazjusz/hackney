package pl.umk.mat.hackney.kopyto;

import java.io.IOException;

import pl.umk.mat.hackney.kopyto.ui.BluetoothDeviceList;
import pl.umk.mat.hackney.networking.BluetoothHandler;
import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Activity responsible for connecting to chosen device
 * 
 * @author HackneyTeam
 *
 */
public class BondedDevicesActivity extends Activity implements BluetoothDeviceList.DeviceListListener{
	
	private BluetoothHandler btHandler = EnableBluetoothActivity.btHandler;
	private BluetoothDevice choosen;
	private ProgressDialog progressDialog;
	private AlertDialog alertDialog;
	
	
	/**
	 * Initialize required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		
		ActivityProperties.set(this);

		if(btHandler==null){
			btHandler = new BluetoothHandler();
		}

		progressDialog = new ProgressDialog(this);
		btHandler.getPairedDevices();

		Button refreshButton =(Button) findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				btHandler.getPairedDevices();
			}


		}
				);
	}

	/**
	 * Set progress on dialog
	 * @param integer progress
	 */
	private void updateProgress(Integer integer) {
		if(progressDialog.isShowing())progressDialog.setProgress(integer);
	}

	/**
	 * Show connection progress dialog 
	 */
	void dialogShow() {
		progressDialog.setMessage(ActivityProperties.get().getString(R.string.connectingDialog));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.show();
	}

	/**
	 * Hide connection progress dialog 
	 */
	void dialogHide() {
		if(progressDialog.isShowing())progressDialog.dismiss();
	}
	
	/**
	 * Show connection error
	 */
	void showConnectionError(){


		if(progressDialog.isShowing())progressDialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(ActivityProperties.get().getString(R.string.connectionErrorDialog))
		.setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		alertDialog = builder.create();
		alertDialog.show(); 	
	}


	/**
	 * Connect to chosen device
	 */
	@Override
	public void onDeviceChoosen(BluetoothDevice device) {
		
		choosen = device;
		
		/**
		 * Task responsible for connect to device in background and publish progress
		 * 
		 * @author HackneyTeam
		 *
		 */
		class ConnectionTask extends AsyncTask<Void, Integer, Boolean>{

			public ConnectionTask(){
				
			}

			@Override
			protected void onPreExecute(){
				dialogShow();
			}

			@Override
			protected Boolean doInBackground(Void... a) {

				btHandler.cancelDiscovery();

				try {
					btHandler.initializeConnection(choosen);
				} catch (IOException e1) {
					return false;
				}
				
				this.publishProgress(10);
				try {
					btHandler.connect(choosen);
				} catch (IOException e) {
					return false;
				}

				this.publishProgress(40);
				
				RemoteControlActivity.connection = new Connection(btHandler.getSocket());
				
				this.publishProgress(50);
				try {
					RemoteControlActivity.connection.createOutStream();
					this.publishProgress(70);
					RemoteControlActivity.connection.createInStream();
				} catch (IOException e) {
					return false;
				}
				this.publishProgress(80);
				if(!RemoteControlActivity.connection.tryHandShake()) return false;
			

				this.publishProgress(90);
				if(RemoteControlActivity.connection.isConnected()){
					Intent intent = new Intent(ActivityProperties.get(),RemoteControlActivity.class);
					startActivity(intent);
				}
				else{
					return false;
				}



				return true;
			}
			@Override
			protected void onPostExecute(Boolean result){
				if(result){
					dialogHide();
				}
				else{
					dialogHide();
					showConnectionError();
				}
			}

			@Override
			protected void onProgressUpdate(Integer... val){
				updateProgress(val[0]);
			}
		}		
		new ConnectionTask().execute();
	}
}
