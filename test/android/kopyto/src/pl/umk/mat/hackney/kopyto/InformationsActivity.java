package pl.umk.mat.hackney.kopyto;

import java.util.Locale;

import pl.umk.mat.hackney.utility.ActivityManager;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Activity responsible for selecting language and displaying informations about application
 * 
 * @author HackneyTeam
 *
 */
public class InformationsActivity extends Activity implements OnItemSelectedListener {
	
	SharedPreferences settings;
	Locale[] locals;
	View contain;
	Toast toast;
	ArrayAdapter<String> aa2;

	/**
	 * Initialize required objects
	 */
		@Override
		public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			ActivityProperties.set(this);
			settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
			Locale tmp =new Locale(settings.getString("lang", Locale.getDefault().getLanguage()));
			if(tmp.getLanguage().compareTo(Locale.getDefault().getLanguage())==0);
			else {
				Locale.setDefault(tmp);
				Editor editor = settings.edit();
				editor.putString("lang", tmp.getLanguage());
				editor.commit();
			}
			Configuration config = new Configuration();
			config.locale = tmp;
			getBaseContext().getResources().updateConfiguration(config, null);
			
			
			setContentView(R.layout.informations);
	        
	        TextView lang = (TextView)this.findViewById(R.id.currentLanguage);
	        
	        Spinner languages = (Spinner)this.findViewById(R.id.spinner1);
	        
	        ArrayAdapter<String> aa = new ArrayAdapter<String>(ActivityProperties.get(),android.R.layout.simple_spinner_item,android.R.id.text1);
	        aa2 = new ArrayAdapter<String>(ActivityProperties.get(),android.R.layout.simple_spinner_item,android.R.id.text1);
	        
	        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        languages.setAdapter(aa);
	        
	        languages.setOnItemSelectedListener(this);
	        
	        locals = new Locale[]{new Locale("pl"), new Locale("en")};
	     
	        lang.setText(R.string.language);
	        for(int i=0;i<locals.length;i++){
	        	aa.add(locals[i].getDisplayName());
	        	aa2.add(locals[i].getLanguage());
	        }
	        languages.setSelection(aa2.getPosition(tmp.getLanguage()));
	      
	        
	        
			
		}
	
	
	/**
	 * Define reaction for selection in spinner
	 */
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		Locale tmp= new Locale(aa2.getItem(arg2));
		if(tmp.getLanguage().compareTo(settings.getString("lang", Locale.getDefault().getLanguage()))!=0 ){
			Editor editor = settings.edit();
			editor.putString("lang", tmp.getLanguage());
			editor.commit();
			
			showChangeLanguageDialog();
		}
	}

	/**
	 * Define reaction for no selection in spinner
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		
	}
	
	/**
	 * Define reaction for hardware back button pressed
	 */
	@Override
	public void onBackPressed(){
		ActivityManager am = new ActivityManager(this);
		am.openActivity(MainMenuActivity.class);
		this.finish();
		
	
	}

	/**
	 * Shows dialog and finishes activity
	 */
	private void showChangeLanguageDialog(){
	    
    	
    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
    	
    	builder.setMessage(ActivityProperties.get().getString(R.string.languageChanged))
    	       .setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					ActivityManager activityMgr = new ActivityManager(ActivityProperties.get());
					activityMgr.openActivity(MainMenuActivity.class);
					
					ActivityProperties.get().finish();
					
				}
			});
   
    	AlertDialog alertDialog = builder.create();
    	alertDialog.show(); 	
    }
}
