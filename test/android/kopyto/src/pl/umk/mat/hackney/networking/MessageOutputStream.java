package pl.umk.mat.hackney.networking;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MessageOutputStream 
{
	private DataOutputStream output;
	
	public MessageOutputStream(DataOutputStream stream)
	{
		output = stream;
	}
	
	public MessageOutputStream(OutputStream stream)
	{
		output = new DataOutputStream(stream);
	}
	
	public DataOutputStream getDataOutputStream()
	{
		return output;
	}
	
	public void write(byte[] bytes)
	{
		try 
		{
			output.write(bytes);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void write(MessageData msgData)
	{
		byte[] data = msgData.toBytes();
		write(data);
	}

	public void flush()
	{
		try 
		{
			output.flush();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		try 
		{
			output.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
}
