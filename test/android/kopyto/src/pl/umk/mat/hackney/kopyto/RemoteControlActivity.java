package pl.umk.mat.hackney.kopyto;

import java.util.Locale;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import pl.umk.mat.hackney.utility.WakeLocker;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * Class which manages modules on user choice
 * 
 * @author HackneyTeam
 *
 */
public class RemoteControlActivity extends FragmentActivity implements OnTouchListener{

	public static Connection connection = null;
	private FragmentManager fragmentManager;
	private int state,previousState;
	private View content;
	private byte shortcutsApplicationsToggle;
	Toast toast;
	
	private boolean run;
	
	/**
	 * Interface used for delivering incoming shortcuts module message to shortcuts fragment 
	 * @author HackneyTeam
	 *
	 */
	public interface MessagesForShortcutsListener {
		public void onApplicationReceived(MessageData application);
		public void onShortcutReceived(MessageData shortcut);
	}
	
	
	public static MessagesForShortcutsListener messagesForShortcutsListener;
	
	/**
	 * Interface used for delivering incoming sensors module message to sensors fragment 
	 * @author HackneyTeam
	 *
	 */
	public interface MessagesForSensorsListener {
		public void onSetReceived(MessageData set);
		
	}
	public static MessagesForSensorsListener messagesForSensorsListener;
	
	
	
	/**
	 * Initializes required objects
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		ActivityProperties.set(this);
		

		connection.makeRunnable(this, "manageIncomingMessages");
		connection.execute();

		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		
		setContentView(R.layout.control);
		fragmentManager = getSupportFragmentManager();
		
		content = findViewById(R.id.content);
		hookButtons();
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		state=R.id.about_button;
		previousState=state;
		manageFragments(state,MotionEvent.ACTION_DOWN);
	}
	
	/**
	 * updates layout and sets screen bright
	 */
	@Override
	public void onResume(){
		super.onResume();
		Button current = (Button)findViewById(state);
		current.setPressed(true);
		
		WakeLocker.acquireWakeLocker(); 
	}
	
    @Override
    public void onPause(){
    	super.onPause();
		WakeLocker.releaseWakeLocker();
    }
	
	
    /**
     * Disconnects from current server
     */
	@Override
    public void onDestroy(){
//		connection.setActive(false);
//		if(connection!=null)connection.disconnect();
		  connection.cancel(true);
		  while(connection.getStatus()==AsyncTask.Status.RUNNING);
    	super.onDestroy();	
    }
	
	/**
	 * Handles screen orientation changes
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	  
	  ActivityProperties.set(this);
		SharedPreferences settings = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		Locale tmp =new Locale(settings.getString("lang", "pl"));
		Locale.setDefault(tmp);
		Configuration config = new Configuration();
		config.locale = tmp;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	  
	  setContentView(R.layout.control);
	  hookButtons();
	 
	  if(previousState!=state && state==R.id.about_button){
		  state=previousState;
		  manageFragments(R.id.about_button,MotionEvent.ACTION_DOWN);
	  }
	  else manageFragments(state,MotionEvent.ACTION_DOWN);
	  
//	  connection.cancel(true);
//	  while(connection.getStatus()==AsyncTask.Status.RUNNING);
	  
	 super.onConfigurationChanged(newConfig);
	  
	}
	
	/**
	 * Initialize layout
	 */
	private void hookButtons(){
		Button mouse = (Button)findViewById(R.id.mouse_button);
		Button keyboard = (Button)findViewById(R.id.keyboard_button);
		Button shortcuts = (Button)findViewById(R.id.shortcuts_button);
		Button sensors = (Button)findViewById(R.id.sensors_button);
		Button touchpad = (Button)findViewById(R.id.touchpad_button);
		Button about = (Button)findViewById(R.id.about_button);
		mouse.setOnTouchListener(this);
		keyboard.setOnTouchListener(this);
		shortcuts.setOnTouchListener(this);
		sensors.setOnTouchListener(this);
		touchpad.setOnTouchListener(this);
		about.setOnTouchListener(this);
		
	
	}
	
	/**
	 * Shows dialog on hardware back button pressed
	 */
	@Override
	public void onBackPressed(){
		showDisconnectDialog();
	}
	
	
	/**
	 * Manages fragments depending on user choice
	 * @param id id of a button which represents specific module
	 * @param action action done on button with given id
	 */
	private void manageFragments(int id,int action){	
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		Button prev,current;
		
		if(action==MotionEvent.ACTION_DOWN){

			switch(id){
			case R.id.keyboard_button:
				previousState =state;
				if(previousState == R.id.sensors_button)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
				prev = (Button) findViewById(previousState);
				prev.setPressed(false);
	
		
				KeyboardFragment kFragment = new KeyboardFragment();
	
				fragmentTransaction.replace(R.id.content, kFragment);
		
				fragmentTransaction.commit();

				state=R.id.keyboard_button;
				current = (Button)findViewById(state);
				current.setPressed(true);

				break;
		
			case R.id.mouse_button:

				previousState =state;
				if(previousState == R.id.sensors_button)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
				prev = (Button) findViewById(previousState);
				prev.setPressed(false);

				MouseFragment mFragment = new MouseFragment();
				fragmentTransaction.replace(R.id.content, mFragment);
				fragmentTransaction.commit();

				state=R.id.mouse_button;
				current = (Button)findViewById(state);
				current.setPressed(true);
				
				break;

			case R.id.shortcuts_button:

				previousState =state;
				if(previousState == R.id.sensors_button)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
				prev = (Button) findViewById(previousState);
				prev.setPressed(false);
				
				ShortcutsFragment shFragment = new ShortcutsFragment();
				
				fragmentTransaction.replace(R.id.content, shFragment);
				fragmentTransaction.commit();

				state=R.id.shortcuts_button;
				current = (Button)findViewById(state);
				current.setPressed(true);
				
				break;

			case R.id.sensors_button:

				previousState =state;
				prev = (Button) findViewById(previousState);
				prev.setPressed(false);
				this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				
				
				
				SensorsFragment sFragment = new SensorsFragment();
				fragmentTransaction.replace(R.id.content, sFragment);
				fragmentTransaction.commit();

				state=R.id.sensors_button;
				current = (Button)findViewById(state);
				current.setPressed(true);
				
				break;
			case R.id.touchpad_button:

				previousState =state;
				if(previousState == R.id.sensors_button)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
				prev = (Button) findViewById(previousState);
				prev.setPressed(false);
				
				TouchpadFragment tFragment = new TouchpadFragment();
				fragmentTransaction.replace(R.id.content, tFragment);
				fragmentTransaction.commit();

				state=R.id.touchpad_button;
				current = (Button)findViewById(state);
				current.setPressed(true);
				
				break;
			case R.id.about_button:

				switch(state){
				case R.id.mouse_button:
					changeButtonsStateOnAbout();
			
					MouseAboutFragment maf = new MouseAboutFragment();
					fragmentTransaction.replace(R.id.content, maf);
					fragmentTransaction.commit();
					
					content.invalidate();
					break;	

				case R.id.touchpad_button:
					changeButtonsStateOnAbout();
					
					TouchpadAboutFragment taf = new TouchpadAboutFragment();
					fragmentTransaction.replace(R.id.content, taf);
					fragmentTransaction.commit();
					
					content.invalidate();
					break;

				case R.id.keyboard_button:
					changeButtonsStateOnAbout();
					
					KeyboardAboutFragment kaf = new KeyboardAboutFragment();
					fragmentTransaction.replace(R.id.content, kaf);
					fragmentTransaction.commit();
					
					content.invalidate();
					break;

				case R.id.shortcuts_button:
					changeButtonsStateOnAbout();
					
					ShortcutsAboutFragment saf = new ShortcutsAboutFragment();
					fragmentTransaction.replace(R.id.content, saf);
					fragmentTransaction.commit();
					
					content.invalidate();
					break;

				case R.id.sensors_button:
					changeButtonsStateOnAbout();
					
					SensorsAboutFragment seaf = new SensorsAboutFragment();
					fragmentTransaction.replace(R.id.content, seaf);
					fragmentTransaction.commit();
					
					content.invalidate();
					break;

				case R.id.about_button:
					previousState=state;
					AboutFragment af = new AboutFragment();
					fragmentTransaction.replace(R.id.content, af);
					fragmentTransaction.commit();
					
					current = (Button)findViewById(state);
					current.setPressed(true);
					previousState=state;
					content.invalidate();
				}
			}
		}
		else if(action==MotionEvent.ACTION_UP){
			switch(id){
		
			case R.id.keyboard_button:

				current = (Button)findViewById(state);
				current.setPressed(true);
				content.invalidate();
				
				break;

			case R.id.mouse_button:

				current = (Button)findViewById(state);
				current.setPressed(true);
				content.invalidate();
				break;

	
			case R.id.shortcuts_button:

				current = (Button)findViewById(state);
				current.setPressed(true);

				content.invalidate();
				break;

			case R.id.sensors_button:

				current = (Button)findViewById(state);
				current.setPressed(true);

				content.invalidate();

				break;
			case R.id.touchpad_button:

				current = (Button)findViewById(state);
				current.setPressed(true);
				content.invalidate();
				break;
			case R.id.about_button:
				current = (Button)findViewById(state);
				current.setPressed(true);
				
				content.invalidate();
				findViewById(R.id.Tabs).invalidate();

				break;
			}
		}
	}

	/**
	 * Sets reaction for touching user interface
	 */
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		manageFragments(arg0.getId(),arg1.getAction());
		
		return true;
	}

	/**
	 * changes state and updates layout on about module selected
	 */
	private void changeButtonsStateOnAbout(){
		Button prev,current;
		previousState =state;
		prev = (Button) findViewById(previousState);
		prev.setPressed(false);
		
		state = R.id.about_button;
		current = (Button)findViewById(state);
		current.setPressed(true);
	}
	
	
	/**
	 * Manages incoming messages depending on its type
	 * @param md message received
	 */
	public void manageIncomingMessages(MessageData md){
		
		switch(md.getMessageType()){
		case MessageData.CONNECTION_GOODBYE:
			showConnectionEnd();
			
			break;
		case MessageData.HOTKEY_APPLIST_COUNT:
			shortcutsApplicationsToggle = 0;
			
			break;
		case MessageData.HOTKEY_APPHOTKEY_COUNT:
			shortcutsApplicationsToggle = 1;
			
			break;
		case MessageData.STRING_EXCHANGE:
			if(shortcutsApplicationsToggle == 0){
				messagesForShortcutsListener.onApplicationReceived(md);
				;
			}
			else{
				messagesForShortcutsListener.onShortcutReceived(md);
			}
			break;
		case MessageData.GAMEPAD_PLAYER_SET:
				messagesForSensorsListener.onSetReceived(md);
			
			break;
			
		}
	}
	
	/**
	 * Shows dialog on disconnect from server 
	 */
	void showConnectionEnd(){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(this.getString(R.string.connectionEnd))
		.setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				connection.setActive(false);
				if(connection!=null)connection.disconnect();
				ActivityProperties.get().finish();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show(); 	
	}
	
	/**
	 * Show dialog with question about disconnecting from server
	 */
	public static void showDisconnectDialog(){

		AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
		builder.setMessage(ActivityProperties.get().getString(R.string.disconnectDialog))
		.setCancelable(false).setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				
				connection.setActive(false);
				if(connection!=null)connection.disconnect();
				
				ActivityProperties.get().finish();
			}
		})
		.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show(); 
		
	}
	
	
}
