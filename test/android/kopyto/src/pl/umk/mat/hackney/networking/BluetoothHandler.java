package pl.umk.mat.hackney.networking;


import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import pl.umk.mat.hackney.kopyto.ui.BluetoothDeviceList;
import pl.umk.mat.hackney.kopyto.ui.BluetoothDeviceList.DeviceListListener;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Looper;


/**
 * 
 * Manages bluetooth adapter and creates socket for connection
 * 
 * @author HackneyTeam
 *
 */

public class BluetoothHandler {
	
	private BluetoothAdapter bluetoothAdapter; 
	private BluetoothSocket socket = null ;
	private final int INTERVAL = 7000;
	private final int MAX_LOOPS = 1000;
	
	
	/**
	 * 
	 * @return bluetooth socket
	 */
	public BluetoothSocket getSocket() {
		return socket;
	}

	/**
	 * 
	 * Constructor gets default device bluetooth adapter
	 */
	public BluetoothHandler(){
		//Pobranie BluetoothAdaptera - swoisty manager bluetooth
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}
	
	
	
	/**
	 * 
	 * Enable bluetooth adapter
	 * @return true if succes, false if fail
	 */
	public boolean enableBluetooth(){	
		
		//Sprawdzenie czy urz�dzenie posiada bluetooth
		if (bluetoothAdapter == null) {
			
			
			return false;
		}
		
		//W��czamy bt je�eli nie jest juz w��czony
		if (!bluetoothAdapter.isEnabled()) {		
			if(bluetoothAdapter.enable()){
				if(waitForState(BluetoothAdapter.STATE_ON, MAX_LOOPS)){
					return true;
				}
				else return false;
			}
			else return false;	
		}
		else {
			return true;		
		}
	}
	
	
	/**
	 * Start discovering bluetooth nearby devices
	 */
	public void discoverDevices(){
		
		//czekanie na uruchomienie bt
		while(bluetoothAdapter.getState()!=BluetoothAdapter.STATE_ON){
		}
		
		if(!bluetoothAdapter.startDiscovery()){
			this.discoverDevices();
		}	
	}
	
	/**
	 * Get paired devices
	 * @return List of devices
	 */
	public BluetoothDeviceList getPairedDevices(){
		//pobranie sparowanych urzadze�
		Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
		BluetoothDeviceList deviceList = new BluetoothDeviceList((DeviceListListener) ActivityProperties.get());
		// i dodanie ich do listy
		if (pairedDevices.size() > 0) {
		    for (BluetoothDevice device : pairedDevices) {
		    	if(device.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.COMPUTER && !deviceList.include(device))deviceList.add(device);
		    }
		}
		return deviceList;
	}
	
	
	/**
	 * Cancel device discovery
	 */
	public void cancelDiscovery(){
		bluetoothAdapter.cancelDiscovery();
		while(bluetoothAdapter.isDiscovering());
	}
	

	/**
	 * 
	 * Initializes connection with bluetoothDevice
	 * 
	 * @param bluetoothDevice device to connect
	 * @throws IOException 
	 */
	public void initializeConnection(BluetoothDevice bluetoothDevice) throws IOException {
		if(socket!=null){
			try {
				socket.close();
				socket=null;
			} catch (IOException e) {
				throw e;
			}
		
		}

		try{
			socket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString(MessageData.UUID_ANDROID));
		} catch (IOException connectException){
			throw connectException;
		}
		
		
	}
	
	/**
	 * Connect socket with bluetoothDevice
	 * @param bluetoothDevice device to connect
	 * @throws IOException 
	 */
	public void connect(BluetoothDevice bluetoothDevice) throws IOException {
		bluetoothAdapter.cancelDiscovery();

		//otwarcie polaczenia
		try {
			if(Looper.myLooper()==null)Looper.prepare();
			socket.connect();	
		} catch (IOException connectException) {
			throw connectException;
		}	
	}
	
	/**
	 * Disables device bluetooth adapter
	 */
	public void disable(){
		if(bluetoothAdapter!=null){
			bluetoothAdapter.disable();
			if(!waitForState(BluetoothAdapter.STATE_OFF,MAX_LOOPS))
				waitForState(BluetoothAdapter.STATE_OFF,MAX_LOOPS);
		}	
	}
	
	/**
	 * 
	 * Wait for bluetooth adapter state
	 * 
	 * @param state target state
	 * @param loops 
	 * @return
	 */
	private boolean waitForState(int state, int loops){
		if(loops>0){
			switch(state){
			case BluetoothAdapter.STATE_ON:
				if(bluetoothAdapter.getState()==BluetoothAdapter.STATE_TURNING_ON){
					try {
						Thread.sleep(INTERVAL);
					} catch (InterruptedException e) {
						return false;
					}
					return waitForState(state,loops-1);
					
				}
				else if(bluetoothAdapter.getState()==BluetoothAdapter.STATE_ON){
					return true;
	
				}
				return false;
			
				
			case BluetoothAdapter.STATE_OFF:
				if(bluetoothAdapter.getState()==BluetoothAdapter.STATE_TURNING_OFF){
					try {
						Thread.sleep(INTERVAL);
					} catch (InterruptedException e) {
						return false;
					}
					return waitForState(BluetoothAdapter.STATE_OFF,loops-1);
					
				}
				else if(bluetoothAdapter.getState()==BluetoothAdapter.STATE_OFF){
					return true;
	
				}
				
				break;
			
			
			}
		}
		
		//za dlugo sie nie udalo
		else {
			return false;
		}
		return false;
		
	}
	
}
