package pl.umk.mat.hackney.networking;

import java.util.List;

import pl.umk.mat.hackney.kopyto.R;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class WifiHandler {
	
	//network list form last scan
	static List<ScanResult> networkList;
	static ProgressDialog dialog;

	public static boolean isWifiOn(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wm.isWifiEnabled();
	}
	
	public static boolean enableWifi(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wm.setWifiEnabled(true);
	}
	
	public static boolean disableWifi(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wm.setWifiEnabled(false);
	}

	public static boolean isWifiConnected(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if(wm.getConnectionInfo()==null)return false;
		else return true;
	}
	public static void logNetworksConfig(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		List<WifiConfiguration> networks = wm.getConfiguredNetworks();
		    for (WifiConfiguration current : networks){
		    	Log.w(current.SSID,"BSSID: "+ current.BSSID);
		    	Log.w(current.SSID,"AllowedAuthAlgorithms: "+ current.allowedAuthAlgorithms);
		    	Log.w(current.SSID,"GroupCiphers: "+ current.allowedGroupCiphers);
		    	Log.w(current.SSID,"keymgmt: "+ current.allowedKeyManagement);
		    	Log.w(current.SSID,"pairCiphers: "+ current.allowedPairwiseCiphers);
		    	Log.w(current.SSID,"Protocols: "+ current.allowedProtocols);
		    	Log.w(current.SSID,"pass: "+ current.preSharedKey);
		    	Log.w(current.SSID,"wep kijs: "+ current.wepKeys);
		    	Log.w(current.SSID,"wep key index: "+ current.wepTxKeyIndex);
		    	Log.w(current.SSID,"hidden SSID: "+ current.hiddenSSID);
		    	Log.w(current.SSID,"==============================");
		    	
		    }
	}
	
	public static WifiInfo getCurrentNetworkInformation(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wm.getConnectionInfo();
	}

	public static List<ScanResult> scan(Context context){
		WifiManager wm=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		dialog = new ProgressDialog(context);

		
		BroadcastReceiver receiver;
		receiver = new BroadcastReceiver() {
        	@Override
			public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
            
                if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) { 	
                	WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                	networkList = wm.getScanResults();
                	dialog.cancel();
                }
        	}
        };
        IntentFilter filter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        context.registerReceiver(receiver, filter);    
        
		wm.startScan();
		dialog = ProgressDialog.show(context, "", context.getString(R.string.enablingBluetoothDialog), true);	
		
		return networkList;
	}
	
	
	
	
	public static boolean connectToNetwork(Context context, ScanResult network, String securityKey) { 
		
		WifiManager wm = (WifiManager)context.getSystemService(android.content.Context.WIFI_SERVICE); 
		WifiConfiguration config; 
		
		String BSSID = network.BSSID;
		String SSID = "\""+network.SSID+"\"";
		int securityType = 3;
		
		//Security type parsing from ScanResult object
		if(network.capabilities.contains("NONE") && network.capabilities.contains("WEP"))securityType=2;
		else if (network.capabilities.contains("NONE"))securityType=3;
		if(network.capabilities.contains("WEP"))securityType=1;
		
		
		
		
		// Checks if that WIFI network we want to connect to is not already known (same binary SSID) 
		List<WifiConfiguration> listConfig = wm.getConfiguredNetworks(); 
		for(int i=0 ; i < listConfig.size() ; i++) { 
			config = listConfig.get(i); 
			if(config.BSSID!=null)
			if(config.BSSID.equalsIgnoreCase(BSSID)) {
				Log.w("SIEC", "Jest");
				return wm.enableNetwork(config.networkId, true); 
			}
		} 
		
		Log.w("SIEC","Nie ma konfiguracji");
		// New network - need to deliver configuration to os
		config = new WifiConfiguration(); 
		config.BSSID = BSSID; 
		config.SSID = SSID; 
		config.priority = 1; 
		
//		WifiConfiguration.AuthAlgorithm.OPEN;   0			WPA2-PSK-CCMP -{}
//		WifiConfiguration.AuthAlgorithm.SHARED; 1
//		WifiConfiguration.AuthAlgorithm.LEAP;   2
		
//		WifiConfiguration.GroupCipher.CCMP;    	3			WPA2-PSK-CCMP - {0,1,2,3}
//		WifiConfiguration.GroupCipher.TKIP;    	2
//		WifiConfiguration.GroupCipher.WEP104 	1
//		WifiConfiguration.GroupCipher.WEP40 	0
//		
//		WifiConfiguration.KeyMgmt.WPA_EAP		2			WPA2-PSK-CCMP -{1}
//		WifiConfiguration.KeyMgmt.WPA_PSK		1
//		WifiConfiguration.KeyMgmt.NONE			0
//		WifiConfiguration.KeyMgmt.IEEE8021X		3
//		
//		WifiConfiguration.Protocol.RSN;			1			WPA2-PSK-CCMP -{0,1}
//		WifiConfiguration.Protocol.WPA;			0
		
//		WifiConfiguration.PairwiseCipher.CCMP;	2			WPA2-PSK-CCMP -{1,2}
//		WifiConfiguration.PairwiseCipher.NONE;	0
//		WifiConfiguration.PairwiseCipher.TKIP	1
		
		
//
	//	if(network.capabilities.contains("CCMP"))
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	//	if(network.capabilities.contains("TKIP"))
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	//	if(network.capabilities.contains("WEP40"))
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
	//	if(network.capabilities.contains("WEP104"))
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
			
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.NONE);
		
	//	if(network.capabilities.contains("WPA")&&network.capabilities.contains("PSK"))
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	//	if(network.capabilities.contains("WPA")&&network.capabilities.contains("EAP"))
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
			
	//	if(network.capabilities.contains("NONE"))
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);//0
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.LEAP);//2
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);//1
			
			
			config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
			config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
			
			
		switch(securityType){ 
		// WPA 
		case 1: 
			config.preSharedKey = securityKey; 
			break; 
		// WEP 
		case 2: 
			config.wepKeys[0] = "\""+securityKey+"\""; 
			config.wepTxKeyIndex = 0; 
			break; 
		// None 
		case 3: 
			break;
		}
		config.status = WifiConfiguration.Status.ENABLED;
		
		int netId = wm.addNetwork(config);
		Log.w("SIEC", String.valueOf(netId));
		return wm.enableNetwork(netId, true);
	}
	
}
