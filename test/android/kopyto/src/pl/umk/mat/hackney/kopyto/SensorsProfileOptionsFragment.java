package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.kopyto.RemoteControlActivity.MessagesForSensorsListener;
import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Fragment responsible for displaying and changing sensors player set
 * @author HackneyTeam
 *
 */
public class SensorsProfileOptionsFragment extends Fragment implements OnClickListener, MessagesForSensorsListener {
	private Connection connection = RemoteControlActivity.connection;
	private MessageData md;
	SharedPreferences settingsFile;
	
	Button backButton;
	private Spinner sets;
	private ArrayAdapter setsAdapter;
	private byte gPlayerID;
	private boolean change;
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
		gPlayerID = connection.getGamepadPlayerID();
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		RemoteControlActivity.messagesForSensorsListener= this;
		return inflater.inflate(R.layout.about_sensors_profile, container, false);
		
	}

	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(true);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		backButton = (Button) ActivityProperties.get().findViewById(R.id.backButton);
		backButton.setOnClickListener(this);
		
		sets = (Spinner)getActivity().findViewById(R.id.spinner1);
	    setsAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.sets, android.R.layout.simple_spinner_item);
	    setsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    sets.setAdapter(setsAdapter);
	    sets.setSelection(connection.getGamepadPlayerID());
        sets.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
				if(pos!=gPlayerID){
					switch(pos)
					{
					case 0: gPlayerID = (byte)0; break;
					case 1: gPlayerID = (byte)1; break;
					case 2: gPlayerID = (byte)2; break;
					case 3: gPlayerID = (byte)3; break;
					case 4: gPlayerID = (byte)4; break;
					default: break;
					}
					if(gPlayerID==0)
						showSetChange(getActivity().getString(R.string.setNotActive));
					else{

						md = new MessageData(MessageData.GAMEPAD_PLAYER_SET);
						md.setSessionID(connection.getSessionID());
						md.setPayload(new byte[]{connection.getGamepadPlayerID(), gPlayerID});
						connection.outStream.write(md.toBytes());
						
					}
					
				}
				ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
			}

			@Override
			public void onNothingSelected(AdapterView<?> w) {
				ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
				
			}});
		
        change = true;
		
		
	}
	
	/**
	 * Updates activity layout
	 */
	@Override 
	public void onDestroyView(){
		super.onDestroyView();
		if(change)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(false);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}

	/**
	 * Defines on button click reaction
	 */
	@Override
	public void onClick(View v) {
		FragmentManager fragmentManager=getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		SensorsAboutFragment sof= new SensorsAboutFragment();
		fragmentTransaction.replace(R.id.content, sof);
		fragmentTransaction.commitAllowingStateLoss();
		change = false;
		
	}

	/**
	 * Sets reaction for message received from server
	 */
	@Override
	public void onSetReceived(MessageData set) {

		byte[] payload = set.getPayload();
		gPlayerID = payload[1];
		connection.setGamepadPlayerID(gPlayerID);
		sets.setSelection(gPlayerID);
			
		if(gPlayerID==0){
			showSetChange(getActivity().getString(R.string.setFail));
		}
		else{		
			showSetChange(getActivity().getString(R.string.setSucces));
		}

	}
	
	/**
	 * Shows confirmation dialog
	 * @param message message to be displayed
	 */
	 void showSetChange(String message){
		    
	    	AlertDialog.Builder builder = new AlertDialog.Builder(ActivityProperties.get());
	    	builder.setMessage(message)
	    	       .setCancelable(true).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
					}
				});
	   
	    	AlertDialog alertDialog = builder.create();
	    	alertDialog.show(); 	
	    }

}