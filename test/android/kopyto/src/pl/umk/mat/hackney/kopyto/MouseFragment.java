package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.networking.Connection;
import pl.umk.mat.hackney.networking.MessageData;
import pl.umk.mat.hackney.utility.ActivityProperties;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Fragment responsible for interacting with user in application mouse module
 * 
 * @author HackneyTeam
 *
 */
public class MouseFragment extends Fragment implements OnTouchListener{
	ImageButton upButton,downButton, leftButton, rightButton;
	private Button left;
	private Button right;
	private ImageButton lock;
	private boolean locked;
	
	
	private Connection connection=RemoteControlActivity.connection;
	Toast toast;
	
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mouse, container, false);
    }
	
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		ActivityProperties.get().findViewById(R.id.mouse_button).setPressed(true);
		
		super.onStart();
        upButton = (ImageButton) getActivity().findViewById(R.id.upButton);        
        upButton.setOnTouchListener(this);
        
        downButton = (ImageButton) getActivity().findViewById(R.id.downButton);        
        downButton.setOnTouchListener(this);
    
        leftButton = (ImageButton) getActivity().findViewById(R.id.leftButton);        
        leftButton.setOnTouchListener(this);
    
        rightButton = (ImageButton)getActivity().findViewById(R.id.rightButton);        
        rightButton.setOnTouchListener(this);
		
		left = (Button) ActivityProperties.get().findViewById(R.id.left_mouse_button);
		right = (Button) ActivityProperties.get().findViewById(R.id.right_mouse_button);
		lock = (ImageButton) ActivityProperties.get().findViewById(R.id.lock_button);
		
		left.setOnTouchListener(this);
		right.setOnTouchListener(this);
		lock.setOnTouchListener(this);
	
	}
	
	
	/**
	 * Updates layout and makes sure that nothing is pressed
	 */
	@Override
	public void onPause(){
		ActivityProperties.get().findViewById(R.id.mouse_button).setPressed(false);
		MessageData data;	
		if(left.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
			connection.outStream.write(data);
		}
		if(right.isPressed()){
			data= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
			connection.outStream.write(data);
		}
		if(lock.isPressed()){
			lock.setPressed(false);
			locked = false;
		}
		if(upButton.isPressed()){
			data= new MessageData(new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 53});
			connection.outStream.write(data);
		}
		if(downButton.isPressed()){
			data= new MessageData(new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 56});
			connection.outStream.write(data);
		}
		if(leftButton.isPressed()){
			data= new MessageData(new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 52});
			connection.outStream.write(data);
		}
		if(rightButton.isPressed()){
			data= new MessageData(new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 54});
			connection.outStream.write(data);
		}
			
		super.onPause();
	}


	/**
	 * Defines reactions for interacting with interface
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		MessageData md;
		byte S[];
		int action = event.getAction();
		if(action == MotionEvent.ACTION_DOWN){
			switch(v.getId()){
				case R.id.leftButton:
					leftButton.setPressed(true);
					S=new byte[]{MessageData.MOUSE_NUMPAD_START_MOVE,0,(byte) 52};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.rightButton:
					rightButton.setPressed(true);
					S=new byte[]{MessageData.MOUSE_NUMPAD_START_MOVE,0,(byte) 54};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.upButton:
					upButton.setPressed(true);
					S=new byte[]{MessageData.MOUSE_NUMPAD_START_MOVE,0,(byte) 53};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.downButton:
					downButton.setPressed(true);
					S=new byte[]{MessageData.MOUSE_NUMPAD_START_MOVE,0,(byte) 56};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				
				
				
				case R.id.left_mouse_button:
					if(!lock.isPressed()){
						//wyslij wcisniecie lewego klawisza
						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)49});
						connection.outStream.write(md);
						
						left.setPressed(true);	
					}
					else {
						if(left.isPressed()){
							left.setPressed(false);
						
							lock.setPressed(false);
						
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
							connection.outStream.write(md);
							locked = false;
						}
						else if(right.isPressed()){
							right.setPressed(false);
						
							lock.setPressed(false);
						
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
							connection.outStream.write(md);
							locked = false;
						}
						else{
							left.setPressed(true);
						
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)49});
							connection.outStream.write(md);
						}
					}
					
				break;
				
				case R.id.right_mouse_button:
					if(!lock.isPressed()){
					
						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)51});
						connection.outStream.write(md);
						right.setPressed(true);
					
					}
					else {
						if(left.isPressed()){
							left.setPressed(false);
						
							lock.setPressed(false);
							
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
							connection.outStream.write(md);
							locked = false;
						}
						else if(right.isPressed()){
							right.setPressed(false);
					
							lock.setPressed(false);
					
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
							connection.outStream.write(md);
							locked = false;
						}
						else{
							right.setPressed(true);
						
							md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_START_MOVE , 0 , (byte)51});
							connection.outStream.write(md);
						}
					}			
					
				break;
				
				case R.id.lock_button:
					lock.setPressed(true);
				
					if(!locked) locked = true;
					else locked = false;
					
					
				break;
				
				
				
				
			}
		}
		else if(action == MotionEvent.ACTION_UP){
			switch(v.getId()){
				case R.id.leftButton:
					leftButton.setPressed(false);
					S=new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 52};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.rightButton:
					rightButton.setPressed(false);
					S=new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 54};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.upButton:
					upButton.setPressed(false);
					S=new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 53};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				case R.id.downButton:
					downButton.setPressed(false);
					S=new byte[]{MessageData.MOUSE_NUMPAD_STOP,0,(byte) 56};
					md = new MessageData(S);
					connection.outStream.write(S);
				break;
				
				
				case R.id.left_mouse_button:

					if(!locked){
						left.setPressed(false);
						

						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
						connection.outStream.write(md);
					}
					
				break;
				
				case R.id.right_mouse_button:

					if(!locked){
						right.setPressed(false);
					

						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
						connection.outStream.write(md);
					}
					
					
				break;
				
				case R.id.lock_button:
					if(!locked){
						lock.setPressed(false);
				
					}
					if(left.isPressed()){
				
						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)49});
						connection.outStream.write(md);
						
						locked = false;
						left.setPressed(false);
				
					}
					else if(right.isPressed()){
		
						md= new MessageData(new byte[]{ MessageData.MOUSE_NUMPAD_STOP , 0 , (byte)51});
						connection.outStream.write(md);
						
						locked = false;
						right.setPressed(false);
				
					}
					
				break;
			
			}
			
		}
		
		
		return true;
	}

}
