package pl.umk.mat.hackney.utility;

import pl.umk.mat.hackney.kopyto.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;

public class PasswordDialog extends DialogFragment {

	public interface NoticeDialogListener {
    	public void onDialogClick(String haslo);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
    	super.onAttach(activity);
    	// Verify that the host activity implements the callback interface
    	try {
    		// Instantiate the NoticeDialogListener so we can send events to the host
    		mListener = (NoticeDialogListener) activity;
    	} catch (ClassCastException e) {
    		// The activity doesn't implement the interface, throw exception
    		throw new ClassCastException(activity.toString()
    				+ " must implement NoticeDialogListener");
    	}
    }
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();
	    		

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    builder.setView(inflater.inflate(R.layout.passw_dialog  , null)).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				EditText et = (EditText) ActivityProperties.get().findViewById(R.id.wifiPassword);
				String tmp=et.getText().toString();
				mListener.onDialogClick(tmp);
				//				
//				String tmp=et.getText().toString();
//				haslo=tmp;
				dialog.cancel();
			}
		});

	    return builder.create();
	}
	
	
	
	
}
