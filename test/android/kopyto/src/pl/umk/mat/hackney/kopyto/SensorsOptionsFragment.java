package pl.umk.mat.hackney.kopyto;

import pl.umk.mat.hackney.utility.ActivityProperties;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Fragment responsible for displaying current sensors options
 * @author hackneyTeam
 *
 */
public class SensorsOptionsFragment extends Fragment implements OnClickListener {
	
	SharedPreferences settingsFile;
	
	Button saveButton;
	Button backButton;
	Button defaultsButton;
	
	
	private TextView yVal;
	private TextView zVal;
	private ProgressBar yBar;
	private ProgressBar zBar;
	private Button yButton;
	private Button zButton;
	private boolean change;
	
	
	/**
	 * Loads configuration file
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);
	}
	
	/**
	 * Loads proper layout from xml
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
			return inflater.inflate(R.layout.sensors_options, container, false);
	}
	
	/**
	 * Initializes required objects
	 */
	@Override
	public void onStart(){
		super.onStart();
		ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(true);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(true);
		
		settingsFile = ActivityProperties.get().getSharedPreferences(Options.HACKNEY_CONF_FILE, Context.MODE_PRIVATE);

		backButton = (Button) ActivityProperties.get().findViewById(R.id.backButton);
		backButton.setOnClickListener(this);

		int valY =(int)settingsFile.getFloat("defAxisY", 3.0f);
		
		yVal = (TextView)ActivityProperties.get().findViewById(R.id.yCurrentTitle);
		yBar = (ProgressBar) ActivityProperties.get().findViewById(R.id.yProgressBar);
		yButton = (Button)ActivityProperties.get().findViewById(R.id.yButton);
		
		yButton.setOnClickListener(this);
		yBar.setProgress((int)valY);
		yVal.setText(getString(R.string.current) + Integer.toString(valY) );
		
		int valZ =(int)settingsFile.getFloat("defAxisZ", 3.0f);
		
		zVal = (TextView)ActivityProperties.get().findViewById(R.id.zCurrentTitle);
		zBar = (ProgressBar) ActivityProperties.get().findViewById(R.id.zProgressBar);
		zButton = (Button)ActivityProperties.get().findViewById(R.id.zButton);
		
		zButton.setOnClickListener(this);
		zBar.setProgress((int)valZ);
		zVal.setText(getString(R.string.current) + Integer.toString(valZ) );

		change = true;

	}

	
	/**
	 * Updates activity layout
	 */
	@Override 
	public void onDestroyView(){
		super.onDestroyView();
		if(change)ActivityProperties.get().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		ActivityProperties.get().findViewById(R.id.sensors_button).setSelected(false);
		ActivityProperties.get().findViewById(R.id.about_button).setPressed(false);
	}
	
	/**
	 * Defines on buttons click reaction
	 */
	@Override
	public void onClick(View v) {
		FragmentManager fragmentManager=getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		switch(v.getId()){
		case R.id.zButton:
			change = false;
			SensorsZConfigureFragment szcf= new SensorsZConfigureFragment();
			fragmentTransaction.replace(R.id.content, szcf);
			fragmentTransaction.commitAllowingStateLoss();
			
			
			break;
		case R.id.yButton:
			change = false;
			SensorsYConfigureFragment sycf= new SensorsYConfigureFragment();
			fragmentTransaction.replace(R.id.content, sycf);
			fragmentTransaction.commitAllowingStateLoss();
			
			break;


		case R.id.backButton:
			change = false;
			SensorsAboutFragment saf= new SensorsAboutFragment();
			fragmentTransaction.replace(R.id.content, saf);
			fragmentTransaction.commitAllowingStateLoss();


			break;


		}
	}
}
