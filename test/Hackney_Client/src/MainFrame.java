import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;



/**
 * Creates main frame of application
 * @author HackneyTeam
 *
 */
public class MainFrame extends JFrame implements MouseListener, ActionListener{

	/**
	 * 
	 */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	static Socket connection;
	static MessageInputStream in;
	static MessageOutputStream out;
	static byte x;
	static JButton up;
	static JButton down;
	static JButton left;
	static JButton right;
	static MainFrame frame;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
			x =-100;
			connection = new Socket("cat.home",4444);
			out = new MessageOutputStream(connection.getOutputStream());
			in = new MessageInputStream(connection.getInputStream());
			System.out.println(in + " "+ out);
			byte pl [] = {0,0};
			
			
			MessageData msg = new MessageData(MessageData.CONNECTION_HELLO,MessageData.DEVICE_J2ME,pl);
			out.write(msg);
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					 frame = new MainFrame();
					 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
					
					frame.setVisible(true);
					frame.setSize(500, 500);
					frame.addMouseListener(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread td=new Thread(new Runnable(){

			@Override
			public void run() {
				byte dane[];
				dane=new byte[32];
			
						
			
					
					while( in.read(dane) != -1) {
					
					MessageData md = new MessageData(dane);
					byte typ = md.getMessageType();
					System.out.println(typ);
					if (typ==MessageData.CONNECTION_GOODBYE)
					{
						MessageData toSend = new MessageData();
						toSend.setMessageType(MessageData.CONNECTION_GOODBYE);
						System.out.println("CONNECTION_GOODBYE");
						out.write(toSend);
						out.close();
						in.close();
						
						
						try {
							connection.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else if (typ==MessageData.CONNECTION_HELLO)
						System.out.println("CONNECTION_HELLO");
					}
					
					
				
				
			}});
		td.start();
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		
	
		
		initGUI();
	}


	private void initGUI() {
		
		up = new JButton("up");
		down = new JButton("down");	
		left = new JButton("left");
		right = new JButton("right");
		JLabel jl = new JLabel("wybierz kierunek, a później tu wciśnij i przytrzymaj");		
		//up.addMouseListener(frame);
		
	//	down.addMouseListener(frame);
		//left.addMouseListener(frame);
//		right.addMouseListener(frame);
		jl.addMouseListener(frame);
		this.setLayout(new BorderLayout());
		
		this.add(BorderLayout.NORTH,up);

		this.add(BorderLayout.SOUTH,down);
		this.add(BorderLayout.EAST,right);
		this.add(BorderLayout.WEST,left);
		this.add(BorderLayout.CENTER,jl);
		up.addActionListener(this);
		down.addActionListener(this);
		left.addActionListener(this);
		right.addActionListener(this);
	
		


	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	
		
		
	  if(arg0.getSource().equals(up))
	
		x =-1;	
	
		
if(arg0.getSource().equals(down))
		
		x =-2;
if(arg0.getSource().equals(left))
	
	x =-3;
if(arg0.getSource().equals(right))
	
	x =-4;
		}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
		byte S[]={(byte) x};
		
		MessageData pakiet= new MessageData(MessageData.MOUSE_NUMPAD_START_MOVE,S);
		out.write(pakiet);
		out.flush();
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
byte S[]={(byte) x};

		MessageData pakiet= new MessageData(MessageData.MOUSE_NUMPAD_STOP,S);
		out.write(pakiet);
		out.flush();
		
	}

	
}

