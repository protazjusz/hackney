

import zesp01.kopytko.grafika.Picture;
//import zesp01.kopytko.grafika.MyszCanva;
import zesp01.kopytko.grafika.Tranzycja;

import java.util.Timer;
import java.util.TimerTask;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;


import zesp01.kopytko.menu.Menu;

import zesp01.kopytko.utility.Dictionary;

/**
 * Glowna klasa Aplikacji MIDlet odpowiada za sterowanie aplikacja.
 * Tworzy Glowne menu i obsluguje czesc jego opcji.
 * Tworzy poboczne menu i obsluguje je.
 * 
 * Ponadto przygowuje konieczne do dalszej pracy obiekty takie jak klasy odpowiedzialne za obsługe Bluetooth czy ekranu. 
 * @author szymonk
 *
 */

public class Main extends MIDlet implements CommandListener
{
	private Command 
		exit, 				// wyjscie z aplikacji
		selectMain; 				// wybór opcji z głównego menu

	
	private Display display;
	private LocalDevice localDevice;
	private DiscoveryAgent discoveryAgent;
	
	private List
		mainMenu;						// Lista głównego menu
	
	private Menu menu;
	
	
	private final static int FIND_DEVICE=0;
	private final static int EXIT=1;
	
	/**
	 *  Konstruktor tworzy instancje obiektu odpowiedzialnego za obsluge menu
	 *  i elementy glownego Menu. 
	 */
	public Main()
	{
		display=Display.getDisplay(this);
		update();
	}
	
	protected void startApp() throws MIDletStateChangeException
	{
		try
		{
			localDevice=LocalDevice.getLocalDevice();
			localDevice.setDiscoverable(DiscoveryAgent.GIAC);
			discoveryAgent=localDevice.getDiscoveryAgent();
			
		} 
		catch (BluetoothStateException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		display.setCurrent(new Picture("/img/splash-mini.png"));
		Timer t=new Timer();
		t.schedule(
				new TimerTask()
				{
					public void run()
					{
						display.setCurrent(mainMenu);
					}
				},2000 );
	}
	
	/**
	 * 	Metoda obsluguje guziki ktore pojawiaja sie w dolnej czesci ekranu
	 */
	public  void commandAction(Command c, Displayable d)
	{
		if(c.equals(exit))
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),"do zobaczenia za tydzień!",null,AlertType.WARNING);
			a.setTimeout(2000);
			a.setType(AlertType.WARNING);
			display.setCurrent(a);
			display.vibrate(1000);
			Timer t=new Timer();
			t.schedule(
					new TimerTask()
					{
						public void run()
						{
							try {
								destroyApp(false);
							} catch (MIDletStateChangeException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							notifyDestroyed();
						}
					},2000 );
		}
		
		if(c.equals(selectMain))
		{
			switch(mainMenu.getSelectedIndex())
			{	
				case FIND_DEVICE:
				{
					menu=new Menu(discoveryAgent,display,mainMenu);
					Tranzycja tr=new Tranzycja("/img/hackney-anim-ogon-mini.gif");
					display.setCurrent(tr);
					menu.wyszukaj();
					break;
				}
				
				case EXIT:
				{
					
						Alert a=new Alert(Dictionary.eng("Uwaga!"),"do zobaczenia za tydzień!",null,AlertType.WARNING);
						a.setTimeout(2000);
						a.setType(AlertType.WARNING);
						display.setCurrent(a);
						display.vibrate(1000);
						Timer t=new Timer();
						t.schedule(
								new TimerTask()
								{
									public void run()
									{
										try {
											destroyApp(false);
										} catch (MIDletStateChangeException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										notifyDestroyed();
									}
								},2000 );
						
					
				
					break;
				}
				
			}
		}

	}

	protected void pauseApp()
	{
		// TODO Auto-generated method stub
	}
	
	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException
	{
		
		
		
	}
	
	/**
	 *  Metoda tworzy potrzebne do dalszej pracy guziki
	 *  oraz Liste glownego Menu
	 */
	private void update()
	{
		selectMain=new Command(Dictionary.eng("Wybierz"),Command.OK,1);
		exit=new Command(Dictionary.eng("Wyjście"),Command.BACK,1);
		
		
		String opcjeMenu[]={
				
				Dictionary.eng("Wyszukaj urzadzenie"),
				
				Dictionary.eng("Wyjście")
				};
		mainMenu=new List(Dictionary.eng("Menu główne"), List.IMPLICIT,opcjeMenu,null);
		mainMenu.setCommandListener(this);
		mainMenu.setSelectCommand(selectMain);
		mainMenu.addCommand(exit);
		mainMenu.addCommand(selectMain);	
		
	}
	
}