package zesp01.kopytko.utility;

import java.util.Hashtable;

/**
 * Klasa zawiera wykorzystywane w aplikacji napisy oraz ich tlumaczenia
 * @author szymon
 *
 */
public class Dictionary 
{
	/* tablice */
	public static  Hashtable english;
	public static  Hashtable bledy;
	
	/* kody błędów */
	public static final int CONNECTION_LOST=0;
	public static final int TIMEOUT=1;
	public static final int NO_PONNIES=2;
	public static final int NO_DEVICES=3;
	public static final int NOTHING=4;
	
	/* Języki */
	public static final int POLISH=0;
	public static final int ENGLISH=1;

	public static int lang;
	
	static
	{
		lang=0;
		english=new Hashtable();
		english.put("Wybierz",new String("Select"));
		english.put("Powrót",new String("Back"));
		english.put("Język",new String("Language"));
		english.put("wyczysc dane",new String("Clear Data"));
		english.put("Wybierz urzadzenie",new String("Choose Device"));
		english.put("Wyszukaj urzadzenie",new String("Find Device"));
		english.put("Opcje",new String("Options"));
		english.put("Wyjście",new String("Exit"));
		english.put("Menu główne",new String("Main menu"));
		english.put("Usunać dane urządzeń?",new String("Clear devices data?"));
		english.put("Tak",new String("Yes"));
		english.put("Nie",new String("No"));
		english.put("Uwaga!",new String("Warning!"));
		english.put("Nie ma czego usuwać!",new String("Nothing to delete"));
		english.put("Brak kucyków",new String("No Ponnies found"));
		english.put("Wybierz kucyka",new String("Choose a ponny"));
		english.put("Zerwano polaczenie z serwerem",new String("The connection to a serwer was lost"));
		english.put("dodać do znanych urzadzeń",new String("Add to known devices"));
		english.put("Brak urzadzen",new String("No devices found"));
		english.put("Wybierz urządzenie",new String("Choose a device"));
		english.put("Specjalne",new String("Special"));
		english.put("Mysz",new String("Mouse"));
		english.put("Klawiatura",new String("Keyboard"));
		english.put("Skróty",new String("Shortcuts"));
		english.put("LPM",new String("LMB"));
		english.put("PPM",new String("RMB"));
		
		
		bledy =new Hashtable();
		bledy.put(new Integer(10*CONNECTION_LOST+POLISH),"Zerwano polaczenie z serwerem");
		bledy.put(new Integer(10*CONNECTION_LOST+ENGLISH),"The connection to a serwer was lost");
	
		bledy.put(new Integer(10*TIMEOUT+POLISH),"Przekroczono czas oczekiwania");
		bledy.put(new Integer(10*TIMEOUT+ENGLISH),"Timeout");
	
		bledy.put(new Integer(10*NO_PONNIES+POLISH),"Brak kucyków");
		bledy.put(new Integer(10*NO_PONNIES+ENGLISH),"No Ponnies found");
		
		bledy.put(new Integer(10*NO_DEVICES+POLISH),"Brak urzadzen");
		bledy.put(new Integer(10*NO_DEVICES+ENGLISH),"No devices found");
		
		bledy.put(new Integer(10*NOTHING+POLISH),"Nie ma czego usuwać!");
		bledy.put(new Integer(10*NOTHING+ENGLISH),"Nothing to delete");
		}
	
		public static String eng(String s)
		{
			if(lang==0) return s;
			return (String) english.get(s);
		}
		
		public static String blad(int x)
		{
			return (String) bledy.get(new Integer(10*x+lang));
		}
}
