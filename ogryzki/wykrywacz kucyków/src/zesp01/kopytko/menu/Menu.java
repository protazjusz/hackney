package zesp01.kopytko.menu;





import zesp01.kopytko.utility.Dictionary;


import zesp01.kopytko.wyszukiwanie.PoszukiwaczKucykow;
import zesp01.kopytko.wyszukiwanie.PoszukiwaczNowychKucykow;


import javax.bluetooth.DiscoveryAgent;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;



/**
 * Rola klasy jest obsługa pozycji z głownego Menu.
 * @author szymon
 *
 */
public class Menu implements CommandListener
{
	private Command 
		wyborurzadzeniaCommand, // wybór urzadzenia
		kucykiPowrot,			// powrot do menu wyboru urządzeń 
		powrotCommand;		// powrót do menu głównego

	private List 
		wyborUrzadzenia,		// Menu urządzeń 
		menu;					// Menu główne
	
	public PoszukiwaczKucykow finder;
	private DiscoveryAgent discoveryAgent;
	private Display display;
	
	
	public Menu(DiscoveryAgent a, Display d, List m)
	{
		menu=m;
		display=d;
		discoveryAgent=a;
		kucykiPowrot=new Command(Dictionary.eng("Powrót"),Command.EXIT,0);
		powrotCommand=new Command(Dictionary.eng("Powrót"),Command.EXIT,0);
	}
	
	/**
	 * Metoda odpowiedzialna za pozycje "Wyszukaj urządzenie" z głownego Menu.
	 * Przekzuje kontrole poszukiwaczowiNowychKucyków.
	 */
	public void wyszukaj()
	{
		finder=new PoszukiwaczNowychKucykow(discoveryAgent);
		finder.setMenu(this);
		Thread t= new Thread (finder);
		t.start();
	}
	
	
	public  void commandAction(Command c, Displayable d)
	{
		if(c.equals(wyborurzadzeniaCommand))
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),"Czy świeci słońce\nCzy leje deszcz\nKucyków tu nie ma\nBo są na łące",null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a, menu);
		}
	
	
		if(c.equals(powrotCommand))
		{
			display.setCurrent(menu);
		}
		
		if(c.equals(kucykiPowrot))
		{
			finder.setPhase(PoszukiwaczKucykow.FIND_PONNIES);
			display.setCurrent(wyborUrzadzenia);
		}
	}
	

	
	/**
	 * Metoda pobiera listę urządzeń i wyświetla ją aby użytkownik mógł wybrać jedno,
	 *  lub informacje że nie ma dostepnego zadnego. 
	 */
	public void wybieranie()
	{
		
		String urzadzenia[];
		urzadzenia=finder.getDevicesNames();
		if((urzadzenia==null || urzadzenia.length==0) )
		{
			Alert a=new Alert(Dictionary.eng("Uwaga!"),Dictionary.blad(Dictionary.NO_DEVICES),null,AlertType.WARNING);
			a.setTimeout(Alert.FOREVER);
			a.setType(AlertType.WARNING);
			display.setCurrent(a, menu);
			display.vibrate(1000);
		}
		else
		{
			wyborUrzadzenia=new List(Dictionary.eng("Wybierz urządzenie")+':',List.IMPLICIT,urzadzenia,null);
			wyborUrzadzenia.setCommandListener(this);
			wyborUrzadzenia.addCommand(powrotCommand);
			wyborurzadzeniaCommand=new Command(Dictionary.eng("Wybierz"),Command.SCREEN,1);
			wyborUrzadzenia.setSelectCommand(wyborurzadzeniaCommand);
			display.setCurrent(wyborUrzadzenia);
			display.vibrate(1000);
		}
	}

}
