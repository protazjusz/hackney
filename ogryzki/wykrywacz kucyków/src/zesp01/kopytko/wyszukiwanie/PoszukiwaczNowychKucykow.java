package zesp01.kopytko.wyszukiwanie;

import java.io.IOException;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.RemoteDevice;


/**
 * Rola klasy jest wyszukiwanie urzadzen, oraz serwisow na tych urzadzeniach
 * 
 * Urzadzenia sa wyszukiwane z uzyciem DiscoveryListenera
 * @author szymon
 *
 */
public class PoszukiwaczNowychKucykow extends  PoszukiwaczKucykow
{
	public PoszukiwaczNowychKucykow(DiscoveryAgent da)
	{
		super(da);
	}

	public void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1)
	{
	 	devices.addElement(arg0);
	}
	
	public   void inquiryCompleted(int arg0)
	{
		
		phase=FIND_PONNIES;
		setDevices();
		menu.wybieranie();
	}

	public void run()
	{
		if(phase==FIND_DEVICES)
		{
			devices.removeAllElements();
			try
			{
				agent.startInquiry(DiscoveryAgent.GIAC, this);
			} 
			catch (BluetoothStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}
	
	public  void setDevices()
	{
		devicesNames=new String[devices.size()];
		devicesAdresses=new String[devices.size()];
		for(int i=0;i<devices.size();i++)
		{
			try
			{
				RemoteDevice temp=(RemoteDevice)devices.elementAt(i);
				devicesAdresses[i]=temp.getBluetoothAddress();
				devicesNames[i]=temp.getFriendlyName(true);	
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
