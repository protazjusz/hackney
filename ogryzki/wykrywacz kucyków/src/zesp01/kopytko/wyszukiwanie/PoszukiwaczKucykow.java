package zesp01.kopytko.wyszukiwanie;


import java.util.TimerTask;
import java.util.Vector;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.microedition.rms.RecordStore;


import zesp01.kopytko.menu.Menu;

/**
 * Rola klasy jest odnajdywanie urzadzen bluetooth (telefonow) oraz serwerow uslugi.
 * Pozwala takze na zapisanie znanych urzadzen.   
 * @author szymon
 *
 */
public abstract class PoszukiwaczKucykow  extends  TimerTask implements DiscoveryListener 
{
	protected DiscoveryAgent agent;
	protected RecordStore database;
	
	protected Vector devices;
	protected Vector ponnies;
	
	protected int currentDevice;
	protected String devicesAdresses[];
	protected String devicesNames[];
	protected String knownDevicesAdresses[];
	protected String knownDevicesNames[];
	
	protected Menu menu;
	
	protected int phase;
	
	public static final int FIND_DEVICES=0;
	public static final int FIND_PONNIES=1;
	
	
	
	public PoszukiwaczKucykow(DiscoveryAgent da)
	{
		devices=new Vector();     
		ponnies=new Vector();
		agent=da;
		phase=FIND_DEVICES;
	}
	/**
	 * Ustawia menu z ktorym zwiazany jest poszukiwacz, czyli klase ktora bedzie powiadamial o zakonczonych poszukiwaniach
	 * @param m menu z ktorym zwiazazy jest poszukiwacz
	 */
	public void setMenu(Menu m)
	{
		menu=m;
	}
	
	public abstract void deviceDiscovered(RemoteDevice arg0, DeviceClass arg1);
	/**
	 * Metoda wywolywana gdy zakonczy sie wyszukiwanie urzadzen
	 */
	public abstract void inquiryCompleted(int arg0);
	
	/**
	 * Metoda ustawia urzadzenia na ktorych moga byc wyszukiwane serwisy
	 */
	public abstract void setDevices();

	/**
	 * Metoda wywolywana po zakonczeniu poszukiwan serwisow
	 */
	public void serviceSearchCompleted(int arg0, int arg1)
	{
		phase=FIND_DEVICES;
	}
	/**
	 * Metoda wywolywana gdy znaleziony zostaje serwer
	 */
	public void servicesDiscovered(int arg0, ServiceRecord[] arg1)
	{
		for(int i=0;i<arg1.length;i++)
		{
			String url=arg1[i].getConnectionURL(0,false);
			ponnies.addElement(url);	
		}
	}
	
	/**
	 * 
	 * @return  nazwy urzadzen
	 */
	public String[] getDevicesNames()
	{
		return devicesNames;
	}
	/**
	 * 
	 * @return adresy urzadzen
	 */
	public String[] getDevicesAdresses()
	{
		return devicesAdresses;
	}

	public abstract void run();
	
	/**
	 * Ustala aktualne urzadzenie.
	 * Aktualne urzadzenie to te na ktoym poszukiwane beda serwisy.
	 * @param i urzadzenie ktore ma zostac oznaczone jako aktualne
	 */
	public void setCurrentDevice(int i)
	{
		currentDevice=i;
	}
	
	/**
	 * Ustala etap wyszukiwania
	 * @param etap jaka chcemy ustawic
	 */
	public void setPhase(int p) {
		phase = p;
	}

}

	


