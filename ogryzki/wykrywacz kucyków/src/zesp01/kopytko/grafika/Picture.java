package zesp01.kopytko.grafika;

import java.io.IOException;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * Rolą klasy jest wyświetlanie obrazka
 * @author szymon
 *
 */
public class Picture extends Canvas 
{
	Image img = null;
	
	/**
	 * Konstruktor tworzy i ustala rozmiar obrazka do wyswietlania
	 * @param url adres obrazka
	 */
	public Picture(String url)
	{
		try
        {
        	img=Image.createImage(url);
        	img=resizeImage(img);    	
        }
        catch (IOException e1)
        {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * Metoda zmienia rozmiar obrazka ,na taki ktory zajmuje caly ekran
	 * @param image obrazek zrodlowy
	 * @return nowy obrazek
	 */
	private Image resizeImage(Image image)
	{
		int srcWidth = image.getWidth();
		int srcHeight = image.getHeight();
		
		int newWidth = this.getWidth();
		int newHeight =this.getHeight();
		
		Image newImage = Image.createImage(newWidth, newHeight);
		Graphics g = newImage.getGraphics();

		for (int y = 0; y < newHeight; y++)
		{
			for (int x = 0; x < newWidth; x++)
			{
				g.setClip(x, y, 1, 1);
				int dx = x * srcWidth / newWidth;
				int dy = y * srcHeight / newHeight;
				g.drawImage(image, x - dx, y - dy,
						Graphics.LEFT | Graphics.TOP);
			}
		}

		Image immutableImage = Image.createImage(newImage);
		return immutableImage;
	}

	/**
	 * Metoda wyswietla obrazek
	 */
	protected void paint(Graphics graph)
	{
		graph.drawImage(img, 0,0, 0);
	}
}
