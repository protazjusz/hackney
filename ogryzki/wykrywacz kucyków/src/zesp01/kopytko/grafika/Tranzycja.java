package zesp01.kopytko.grafika;

import java.io.IOException;

import java.io.InputStream;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Item;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;


/**
 * Rolą klasy jest wyświetlanie ruchomego loga, lub jesli jest to niemozliwe statycznego obrazka
 * @author szymon
 *
 */
public class Tranzycja extends Canvas
{
	Player player;
	VideoControl vidc;
    Item videoItem = null;
    
	public Tranzycja(String url)
	{
		repaint();
		InputStream ins = getClass().getResourceAsStream(url);
        try
        {
        	player = Manager.createPlayer(ins, "image/gif");
			player.start();
	        if ((vidc = (VideoControl) player.getControl("VideoControl")) != null)
	        	vidc.initDisplayMode(VideoControl.USE_DIRECT_VIDEO, this);
	        vidc.setDisplaySize(this.getWidth(),this.getHeight());
	        vidc.setDisplayLocation(0, 0);
	        
	        vidc.setVisible(true);
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MediaException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	protected void paint(Graphics graph) {
		graph.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM));
		graph.setColor(0x000000);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
	}		
}
