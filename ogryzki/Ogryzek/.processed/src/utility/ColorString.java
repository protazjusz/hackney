package utility;


public class ColorString
{
	public static int red=0xFF0000;
	public static int blue=0x0000FF;
	public static int green=0x00FF00;
	public static int white=0xFFFFFF;
	
	private String lancuch;
	int color;
	
	public ColorString(String s, int c)
	{
		lancuch=s;
		color = c;
	}

	public boolean isNewline()
	{
		return (lancuch.charAt(lancuch.length()-1)=='\n');
	}
	public int getColor()
	{
		return color;
	}
	
	public String toString()
	{
		return lancuch;
	}
}


