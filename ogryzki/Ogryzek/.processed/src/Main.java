import grafika.GlownaCanva;


import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import utility.ColorString;


public class Main extends MIDlet implements CommandListener
{
	private Command exitCommand;
	private Display display;
	private GlownaCanva canva;
	
	public Main()
	{
		display=Display.getDisplay(this);
		exitCommand=new Command("Exit",Command.EXIT,0);
	}
	
	protected void startApp() throws MIDletStateChangeException
	{
		canva=new GlownaCanva(display);
		canva.addCommand(exitCommand);
		canva.addStringln("W razie niebezpieczeństwa", ColorString.red);
		canva.addStringln("        wciśnij 5", ColorString.red);
		canva.setCommandListener(this);
		display.setCurrent(canva);
	
		
	}

	public void commandAction(Command c, Displayable d)
	{
		if(c.equals(exitCommand))
		{
			try
			{
				destroyApp(false);
				notifyDestroyed();
			}
			catch (MIDletStateChangeException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	protected void pauseApp()
	{
		// TODO Auto-generated method stub

	}
	
	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException
	{
		// TODO Auto-generated method stub

	}
}
