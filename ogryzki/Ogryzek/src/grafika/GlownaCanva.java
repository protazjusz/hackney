package grafika;


import java.util.Random;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;

import utility.ColorString;



public  class GlownaCanva extends javax.microedition.lcdui.Canvas 
{
	private ColorString S[];
	private String Sw[]={"ancymonie","rogacizno","urwipołciu","łobuzie"};
	private int L=0;
	private int tlo;
	private static int white=0xFFFFFF;
	private Display display;

	public GlownaCanva(Display d)
	{
		display=d;
		tlo=white;
		S=new ColorString [100];
		repaint();	
	}
	
	protected void paint(Graphics graph)
	{
		int wysokoscWiersza=graph.getFont().getHeight()+8;
		int y=0;
		int x=10;
		
		graph.setColor(tlo);
		graph.fillRect(0, 0, graph.getClipWidth(),graph.getClipHeight());
		graph.setColor(255, 0, 0);
		
		for(int i=0;i<L;i++)
		{
			graph.setColor(S[i].getColor());
			graph.drawString(S[i].toString(), x, y*wysokoscWiersza+graph.getClipHeight()/2, 0);
			if(S[i].isNewline()) 
			{
				y++;
				x=10;
			}
			else x+=graph.getFont().stringWidth(S[i].toString());
		}
	}
	
	public void addString(String c,int color)
	{
		S[L++]=new ColorString(c,color);
		repaint();
	}
	
	public void addStringln(String c,int color)
	{
		addString(c+'\n',color);
	}

	void clear()
	{
		S=new ColorString [100];
		L=0;
		repaint();
	}
	
	protected void keyPressed(int keyCode)
	{
		clear();
		if(keyCode=='1') addStringln("Panie!",ColorString.red);
		if(keyCode=='5')
		{
			
			
			Random r=new Random();
			
			addStringln("W razie niebezpieczeństwa",ColorString.red);
			addStringln("        "+Sw[r.nextInt(Sw.length)],ColorString.red);
			display.vibrate(10000);
		}
		
		/*
		 * Tu można opisać wynik dla poszczeŋólnych klawiszy ew jakoś pobudować inaczej 
		 */
		
		else clear();
	}
	
}
