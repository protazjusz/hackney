package pl.hackney.ogryzki.touch;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class TouchTesterActivity extends Activity {
	private int count = 0;
	private int maxClicks;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        ImageView image = (ImageView) this.findViewById(R.id.image);
        
        image.setOnClickListener(imageClickListener);
        
        Random generator = new Random();
		maxClicks = generator.nextInt(4);
		maxClicks += 4;
        
    }
    
	private OnClickListener imageClickListener = new OnClickListener() {
		public void onClick(View v) {
			
			if (count < maxClicks){
			 	AlertDialog alertDialog = new AlertDialog.Builder(TouchTesterActivity.this).create();  
			    alertDialog.setTitle("Ostrze�enie");  
			    alertDialog.setMessage(getRandomMessage());  
			    
			    Message alertYesListener = null;
				alertDialog.setButton("OK", alertYesListener);
			    
			    alertDialog.show();
			    moodyVibra();
			    count++;
		    }
			else
			{
				Toast.makeText(TouchTesterActivity.this, "Starczy ju�. Oszcz�dzaj paluszki.", 10).show();
				finish();
			}
		}
	};
	
	private String getRandomMessage()
	{
		String messages[] = {
				"Nie dotykaj!",
				"Nie dotykaj mnie!",
				"Nie dotykaj mnie. �adnie prosz�.",
				"Uprasza si� o zaprzestanie dotykania ekranu.",
				"Z�y dotyk boli.",
				"Przesta� ju�!",
				"Co tak szybko? Opanuj kopytko.",
				"Prosz�, zabierz palec.",
				"Zakaz dotykania.",
				"Jak to �piewa� MC Hammer?",
				"Can't touch this!",
				"A�a! To bola�o.",
				"Nie tak mocno.",
				"Nie tyle si�y.",
				"Co dotykasz mnie, co?",
				"Ja wiem, �e to ekran dotykowy, ale mi ju� starczy.",
				"Tylko by� dotyka� i dotyka�...",
				"A mo�e ju� starczy?",
				"Bliskie spotkania z palcami nie s� takie fajne.",
				"Wiesz co? Dotyk jest przereklamowany.",
				"Tapnij jeszcze raz, co b�dziesz sobie �a�owa�.",
				"Jeszcze raz!",
				"Fajnie by�o? To jeszcze raz.",
				"To mnie... dotkn�o.",
				"To by�o takie... dotkliwe.",
				"Touch me, touch me. I wanna feel your body..."
		};
		
		int len = messages.length; 
		Random generator = new Random();
		int randomIndex = generator.nextInt(len);
		
		return messages[randomIndex];
	}
	
	private void moodyVibra()
	{
		Random generator = new Random();
		int inMood = generator.nextInt(10);
		
		if (inMood > 3){
			int interval = generator.nextInt(500);
			Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			vibra.vibrate(interval);
		}
	}
}