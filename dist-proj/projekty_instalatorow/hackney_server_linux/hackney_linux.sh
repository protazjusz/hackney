#!/bin/bash
SELF=$(cd $(dirname $0); pwd -P)
btlibrary="libbluetooth.so"
jarfile="hackney.jar"
if ! [ -f $SELF/$btlibrary ]; then    
  find / -type f -name "libbluetooth.so.*" -exec ln -s {} $SELF/$btlibrary 2>/dev/null \;
  echo "Dowiazanie utworzone."
else 
  echo "Dowiazanie istnieje."
fi
echo "Sciezka programu: " $SELF
cd $SELF
export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
java -jar $SELF/$jarfile 
