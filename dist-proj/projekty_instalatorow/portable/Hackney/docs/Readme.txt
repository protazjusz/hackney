﻿Hackney 

Copyright 2011-2012, Zespół Hackney 

Hackney to projekt powstały na Wydziale Matematyki i Informatyki Uniwersytetu Mikołaja Kopernika
w Toruniu, w ramach przedmiotu Programowanie Zespołowe. Pozwala on na zdalne sterowanie komputerem 
za pośrednictwem telefonu komórkowego i technologii Bluetooth.

Zainstalujesz zaraz główną aplikacje, czyli serwer odpowiedzialny za obsługę klientów mobilnych
oraz zarządzający konfiguracją. Jeśli jeszcze nie masz klienta, odwiedź http://hackney.pl/pobierz
ze swojego telefonu komórkowego.


---

Hackney

Copyright 2011-2012, Hackney Team

Hackney is a project developed at the Faculty of Mathematics and Computer Science, 
Nicolaus Copernicus University in Torun, on Team Programming course. It allows you to remotely 
control your computer via mobile phone over Bluetooth.

In a moment, you'll install the main application, which is the server responsible for handling 
mobile clients and configuration management. If you don't have client application visit 
http://hackney.pl/en/download from your mobile phone.