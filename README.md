# README #


### Description ###

Hackney to projekt powstały na Wydziale Matematyki i Informatyki Uniwersytetu Mikołaja Kopernika w Toruniu. Pozwala on na zdalne sterowanie komputerem za pośrednictwem telefonu komórkowego i technologii Bluetooth oraz WiFi.

Hackney is a project developed at the Faculty of Mathematics and Computer Science, Nicolaus Copernicus University in Torun. It allows you to remotely control your computer via mobile phone over Bluetooth or WiFi.


### Additional information ###

Oprogramowanie powstałe w ramach projektu oparte jest na 3-klauzulowej licencji BSD (klient dla JavaME i Androida, serwer dla systemu Windows i OS X) oraz na licencji GPLv2 (serwer dla systemu Linux).

The software is licensed under BSD 3-clause license (JavaME and Android clients, Windows and OS X servers) and GPLv2 license (Linux server).



Strona domowa projektu / Project Homepage: http://bitbucket.org/protazjusz/hackney

Szkic protokołu GaLOP na serwerach IETF / GaLOP protocol draft on IETF servers: http://datatracker.ietf.org/doc/draft-ruminski-homenet-galop-proto